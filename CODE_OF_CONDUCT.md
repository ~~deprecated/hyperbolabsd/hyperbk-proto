<!--
  -- This file is derived from the [Hyperbola Social Contract](https://wiki.hyperbola.info/social_contract).
  -- The adaption was made by André Silva <emulatorman@hyperbola.info> on 2020-02-24.
  -- Changes include the amendment of **Hyperbola and anti-discrimination** with some adaptions.
  -->

Code of Conduct
===============

All of Hyperbola community are to respect the ethics of freedom and free software and are demanded to show the deepest respect among themselves.

Under no circumstances discriminate against people based on age, gender, sex, sexual orientation, disability, religion, ideology, ideas, social class,
nationality, race, intelligence, or any analogous grounds.  Hyperbola encourages freedom of speech.  However, do not curse or use offensive language while
debating within the Hyperbola community.  Do not under any circumstances attack, bully, stalk, or harass any individual (the personal turn) or a certain
group.  Play the ball, not the man.

Any disregard of any of these points will lead to moderation by __[The Support Staff][THE_SUPPORT_STAFF]__, including, but not
limited to, temporary ban of the person(s) in question.

Severe and repeat instances may lead to permanent ban if deemed necessary by __[The Founders][THE_FOUNDERS]__.

[THE_SUPPORT_STAFF]: https://www.hyperbola.info/members/support-staff/
    "The Support Staff"
[THE_FOUNDERS]: https://www.hyperbola.info/members/founders/
    "The Founders"
