<!--
  -- Copyright (c) 1995 FreeBSD Inc.
  -- All rights reserved.
  --
  -- Modifications to support HyperbolaBSD:
  -- Copyright (c) 2020 Hyperbola Project
  --
  -- Redistribution and use in source and binary forms, with or without
  -- modification, are permitted provided that the following conditions
  -- are met:
  -- 1. Redistributions of source code must retain the above copyright
  --    notice, this list of conditions and the following disclaimer.
  -- 2. Redistributions in binary form must reproduce the above copyright
  --    notice, this list of conditions and the following disclaimer in the
  --    documentation and/or other materials provided with the distribution.
  --
  -- THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
  -- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  -- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  -- ARE DISCLAIMED.  IN NO EVENT SHALL [your name] OR CONTRIBUTORS BE LIABLE
  -- FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  -- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  -- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  -- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  -- LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
  -- OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  -- SUCH DAMAGE.
  --
  --	$OpenBSD: style.9,v 1.73 2018/12/05 15:34:52 schwarze Exp $
  -->

<img alt="HYPERBOLABSD_LOGOTYPE_IMAGE" src="https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/logotypes/logotype_symbol_and_text-g_hyperbola-n_hyperbolabsd_symbol_and_text-i0-u_sldstrPblr_sldshpPblr_grdshdPblr_grdlgtPblr-c_hyperbola_and_dark_text-r8910x2048px-a0f1urwgothic_s0-t_svg1d1basic.svg" text="HyperbolaBSD™ logotype image" width="512">
<img alt="HYPER_BOLA_CHARACTER_IMAGE" src="https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/drawings/drawing_character-g_hyperbola-n_hyperbola-p_standing_in_bipedal_and_leaning_his_right_hand-i1-u_sldstr_sldshp_sldshdPblr_sldlgtPblr-c_hypercolour-r2048px2-a0f0s0-t_svg1d1basic.svg" text="Hyper Bola🄯 character image" height="160">

[HyperbolaBSD&trade; Coding Style Guidelines][HBSDCSG]
======================================================

Description
-----------

This article specifies the preferred style for kernel source files in the __HyperbolaBSD&trade;__ source tree.  It is also a guide for<br/>
preferred userland code style.  These guidelines should be followed for all new code.  In general, code can be considered<br/>
___“new code”___ when it makes up about _50%_ or more of the file(s) involved.  This is enough to break precedents in the existing<br/>
code and use the current style guidelines.


        /*
         * Style guide for the HyperbolaBSD KNF (Kernel Normal Form).
         */

        /*
         * VERY important single-line comments look like this.
         */

        /* Most single-line comments look like this. */

        /*
         * Multi-line comments look like this.  Make them real sentences.
         * Fill them so they look like real paragraphs.
         */


Kernel include files _(i.e., __`<sys/*.h>`__)_ come first; normally, you'll need __`<sys/types.h>`__ OR __`<sys/param.h>`__, but not both!<br/>
__`<sys/types.h>`__ includes __`<sys/cdefs.h>`__, and it's okay to depend on that.


        #include <sys/types.h>  /* Non-local includes in brackets. */


If it's a network program, put the network include files next.

        #include <net/if.h>
        #include <net/if_dl.h>
        #include <net/route.h>
        #include <netinet/in.h>

Then there's a blank line, followed by the _/usr/include_ files.  The _/usr/include_ files, for the most part, should be sorted.<br/>

Global pathnames are defined in _/usr/include/paths.h_.  Pathnames local to the program go in pathnames.h in the local<br/>
directory.

        #include <paths.h>

Then there's a blank line, and the user include files.

        #include "pathnames.h"  /* Local includes in double quotes. */

All functions are prototyped somewhere.

Function prototypes for private functions _(i.e., functions not used elsewhere)_ go at the top of the first source module.  In<br/>
userland, functions local to one source module should be declared ___‘static’___.  This should not be done in kernel land since it<br/>
makes it impossible to use the kernel debugger.

Functions used from other parts of the kernel are prototyped in the relevant include file.

Functions that are used locally in more than one module go into a separate header file, e.g., _extern.h_.

Prototypes should not have variable names associated with the types; i.e.,

        void    function(int);

not:

        void    function(int a);

Prototypes may have an extra space after a tab to enable function names to line up:

        static char     *function(int, const char *);
        static void      usage(void);

There should be no space between the function name and the argument list.

Use __`__dead`__ from __`<sys/cdefs.h>`__ for functions that don't return, i.e.,

        __dead void     abort(void);

In header files, put function prototypes within __`__BEGIN_DECLS`__ / __`__END_DECLS`__ matching pairs.  This makes the header<br/>
file usable from __C++__.

Macros are capitalized and parenthesized, and should avoid side-effects.  If they are an inline expansion of a function, the<br/>
function is defined all in lowercase; the macro has the same name all in uppercase.  If the macro needs more than a single<br/>
line, use braces.  Right-justify the backslashes, as the resulting definition is easier to read.  If the macro encapsulates a<br/>
compound statement, enclose it in a ___“do”___ loop, so that it can safely be used in ___“if”___ statements.  Any final statement-<br/>
terminating semicolon should be supplied by the macro invocation rather than the macro, to make parsing easier for pretty-<br/>
printers and editors.

        #define MACRO(x, y) do {                                        \
                variable = (x) + (y);                                   \
                (y) += 2;                                               \
        } while (0)

Enumeration values are all uppercase.

        enum enumtype { ONE, TWO } et;

When defining unsigned integers use ___“unsigned int”___ rather than just ___“unsigned”___; the latter has been a source of confusion in<br/>
the past.

When declaring variables in structures, declare them sorted by use, then by size _(largest to smallest)_, then by alphabetical<br/>
order.  The first category normally doesn't apply, but there are exceptions.  Each one gets its own line.  Put a tab after the first<br/>
word, i.e., use __`‘int^Ix;’`__ and __`‘struct^Ifoo *x;’`__.

Major structures should be declared at the top of the file in which they are used, or in separate header files if they are used<br/>
in multiple source files.  Use of the structures should be by separate declarations and should be ___“extern”___ if they are declared<br/>
in a header file.

        struct foo {
                struct  foo *next;      /* List of active foo */
                struct  mumble amumble; /* Comment for mumble */
                int     bar;
        };
        struct foo *foohead;            /* Head of global foo list */

Use __queue__ macros rather than rolling your own lists, whenever possible.  Thus, the previous example would be better<br/>
written:

        #include <sys/queue.h>
        struct foo {
                LIST_ENTRY(foo) link;   /* Queue macro glue for foo lists */
                struct  mumble amumble; /* Comment for mumble */
                int     bar;
        };
        LIST_HEAD(, foo) foohead;       /* Head of global foo list */

Avoid using typedefs for structure types.  This makes it impossible for applications to use pointers to such a structure<br/>
opaquely, which is both possible and beneficial when using an ordinary struct tag.  When convention requires a typedef,<br/>
make its name match the struct tag.  Avoid typedefs ending in ___`“_t”`___, except as specified in __Standard C__ or by __POSIX__.

        /*
         * All major routines should have a comment briefly describing what
         * they do.  The comment before the "main" routine should describe
         * what the program does.
         */
        int
        main(int argc, char *argv[])
        {
                int aflag, bflag, ch, num;
                const char *errstr;

For consistency, __getopt__ should be used to parse options.  Options should be sorted in the getopt call and the switch<br/>
statement, unless parts of the switch cascade.  Elements in a switch statement that cascade should have a __FALLTHROUGH__<br/>
comment.  Numerical arguments should be checked for accuracy.

        while ((ch = getopt(argc, argv, "abn:")) != -1) {
                switch (ch) {           /* Indent the switch. */
                case 'a':               /* Don't indent the case. */
                        aflag = 1;
                        /* FALLTHROUGH */
                case 'b':
                        bflag = 1;
                        break;
                case 'n':
                        num = strtonum(optarg, 0, INT_MAX, &errstr);
                        if (errstr) {
                                warnx("number is %s: %s", errstr, optarg);
                                usage();
                        }
                        break;
                default:
                        usage();
                }
        }
        argc -= optind;
        argv += optind;

Use a space after keywords *(*___if___*,* ___while___*,* ___for___*,* ___return___*,* ___switch___*)*.  No braces are used for control statements with zero or only a<br/>
single statement unless that statement is more than a single line, in which case they are permitted.

        for (p = buf; *p != '\0'; ++p)
                continue;
        for (;;)
                stmt;
        for (;;) {
                z = a + really + long + statement + that + needs +
                    two + lines + gets + indented + four + spaces +
                    on + the + second + and + subsequent + lines;
        }
        for (;;) {
                if (cond)
                        stmt;
        }

Parts of a __for__ loop may be left empty.

        for (; cnt < 15; cnt++) {
                stmt1;
                stmt2;
        }

Indentation is an 8 character tab.  Second level indents are four spaces.  All code should fit in 80 columns.

        while (cnt < 20)
                z = a + really + long + statement + that + needs +
                    two + lines + gets + indented + four + spaces +
                    on + the + second + and + subsequent + lines;

Do not add whitespace at the end of a line, and only use tabs followed by spaces to form the indentation.  Do not use more<br/>
spaces than a tab will produce and do not use spaces in front of tabs.

Closing and opening braces go on the same line as the else.  Braces that aren't necessary may be left out, unless they<br/>
cause a compiler warning.

        if (test)
                stmt;
        else if (bar) {
                stmt;
                stmt;
        } else
                stmt;

Do not use spaces after function names.  Commas have a space after them.  Do not use spaces after ___`‘(’`___ or ___`‘[’`___ or preceding ___`‘]’`___<br/>
or ___`‘)’`___ characters.

        if ((error = function(a1, a2)))
                exit(error);

Unary operators don't require spaces; binary operators do.  Don't use parentheses unless they're required for precedence,<br/>
the statement is confusing without them, or the compiler generates a warning without them.  Remember that other people<br/>
may be confused more easily than you.

        a = b->c[0] + ~d == (e || f) || g && h ? i : j >> 1;
        k = !(l & FLAGS);

Exits should be __0__ on success, or non-zero for errors.

        /*
         * Avoid obvious comments such as
         * "Exit 0 on success."
         */
        exit(0);

The function type should be on a line by itself preceding the function.

        static char *
        function(int a1, int a2, float fl, int a4)
        {

When declaring variables in functions, declare them sorted by size _(largest to smallest)_, then in alphabetical order; multiple<br/>
ones per line are okay.   Old style function declarations should be avoided.  ANSI style function declarations should go in an<br/>
include file such as ___“extern.h”___.  If a line overflows, reuse the type keyword.

Be careful not to obfuscate the code by initializing variables in the declarations.  Use this feature only thoughtfully.  DO NOT<br/>
use function calls in initializers!

        struct foo one, *two;
        double three;
        int *four, five;
        char *six, seven, eight, nine, ten, eleven, twelve;

        four = myfunction();

Do not declare functions inside other functions.

Casts and __sizeof()__ calls are not followed by a space.  Note that indent does not understand this rule.

Use of the ___“register”___ specifier is discouraged in new code.  Optimizing compilers such as __gcc__ can generally do a better job of<br/>
choosing which variables to place in registers to improve code performance.  The exception to this is in functions containing<br/>
assembly code where the ___“register”___ specifier is required for proper code generation in the absence of compiler optimization.

When using __longjmp()__ or __vfork()__ in a program, the __`-W`__ or __`-Wall`__ flag should be used to verify that the compiler does not<br/>
generate warnings such as

        warning: variable `foo' might be clobbered by `longjmp' or `vfork'.

If any warnings of this type occur, you must apply the ___“volatile”___ type-qualifier to the variable in question.  Failure to do so may<br/>
result in improper code generation when optimization is enabled.  Note that for pointers, the location of ___“volatile”___ specifies if<br/>
the type-qualifier applies to the pointer, or the thing being pointed to.  A volatile pointer is declared with ___“volatile”___ to the right<br/>
of the ___“*”___.  Example:

        char *volatile foo;

says that ___“foo”___ is volatile, but ___“*foo”___ is not.  To make ___“*foo”___ volatile use the syntax

        volatile char *foo;

If both the pointer and the thing pointed to are volatile use

        volatile char *volatile foo;

___“const”___ is also a type-qualifier and the same rules apply.  The description of a read-only hardware register might look<br/>
something like:

        const volatile char *reg;

Global flags set inside signal handlers should be of type ___`“volatile sig_atomic_t”`___ if possible.  This guarantees that the variable<br/>
may be accessed as an atomic entity, even when a signal has been delivered.  Global variables of other types (such as<br/>
structures) are not guaranteed to have consistent values when accessed via a signal handler.

NULL is the preferred null pointer constant.  Use __NULL__ instead of __`(type *)0`__ or __`(type *)NULL`__ in all cases except for arguments<br/>
to variadic functions where the compiler does not know the type.

Don't use ‘!’ for tests unless it's a boolean, i.e., use

        if (*p == '\0')

not

        if (!*p)

Routines returning __void *__ should not have their return values cast to any pointer type.

Use the __err__ and __warn__ family of functions.  Don't roll your own!

        if ((four = malloc(sizeof(struct foo))) == NULL)
                err(1, NULL);
        if ((six = (int *)overflow()) == NULL)
                errx(1, "Number overflowed.");
        return eight;

Old-style function declarations look like this:

        static char *
        function(a1, a2, fl, a4)
                int a1, a2;     /* Declare ints, too, don't default them. */
                float fl;       /* Beware double vs. float prototype differences. */
                int a4;         /* List in order declared. */
        {
                ...
        }

Use __ANSI__ function declarations unless you explicitly need K&R compatibility.  Long parameter lists are wrapped with a<br/>
normal four space indent.

Variable numbers of arguments should look like this:

        #include <stdarg.h>

        void
        vaf(const char *fmt, ...)
        {
                va_list ap;
                va_start(ap, fmt);

                STUFF;

                va_end(ap);

                /* No return needed for void functions. */
        }

        static void
        usage(void)
        {

Usage statements should take the same form as the synopsis in manual pages.  Options without operands come first, in<br/>
alphabetical order inside a single set of braces, followed by options with operands, in alphabetical order, each in braces,<br/>
followed by required arguments in the order they are specified, followed by optional arguments in the order they are<br/>
specified.

A bar (___`‘|’`___) separates either-or options/arguments, and multiple options/arguments which are specified together are placed in<br/>
a single set of braces.

If numbers are used as options, they should be placed first, as shown in the example below.  Uppercase letters take<br/>
precedence over lowercase.

        "usage: f [-12aDde] [-b b_arg] [-m m_arg] req1 req2 [opt1 [opt2]]\n"
        "usage: f [-a | -b] [-c [-de] [-n number]]\n"

The __getprogname__ function may be used instead of hard-coding the program name.

        fprintf(stderr, "usage: %s [-ab]\n", getprogname());

New core kernel code should be reasonably compliant with the style guides.  The guidelines for third-party maintained<br/>
modules and device drivers are more relaxed but at a minimum should be internally consistent with their style.

Whenever possible, code should be run through a code checker (e.g., ___`“gcc -Wall -W -Wpointer-arith -Wbad-function-cast …”`___<br/>
or splint from the ports tree) and produce minimal warnings.  Since lint has been removed, the only lint-style comment that<br/>
should be used is __FALLTHROUGH__, as it's useful to humans.   Other lint-style comments such as __ARGSUSED__, __LINTED__, and<br/>
__NOTREACHED__ may be deleted.

Note that documentation follows its own style guide, as documented in __mdoc__.

History
-------

This article is largely based on the _src/admin/style/style_ file from the __4.4BSD-Lite2__ release, with updates to reflect the<br/>
current practice and desire of the __HyperbolaBSD&trade;__ development.

Licensing
---------

This article is released under the __[FreeBSD&reg; License][FREEBSD_LICENSE]__.

Acknowledgement
---------------

This article is based on __[OpenBSD&reg; Manual Page][OPENBSD_MAN_PAGE]__.

Licenses for _logotype_ and _character_
---------------------------------------

*   [__HyperbolaBSD&trade;__ logotype image _2017/05/12_][LOGOTYPE]<br/>
    by _[Andr&eacute; Silva][EMULATORMAN]_, _[M&aacute;rcio Silva][COADDE]_ and
    _[Jos&eacute; Silva][CRAZYTOON]_ is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.

*   [__Hyper Bola&#127279;__ character image _2017/04_, _2019/01/22_ and
    _2019/07/10_][CHARACTER]<br/>
    by _[Jos&eacute; Silva][CRAZYTOON]_, _[M&aacute;rcio Silva][COADDE]_ and
    _[Andr&eacute; Silva][EMULATORMAN]_ is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_ or<br/>
    _[GNU&reg; Free Documentation License version 1.3 with special exceptions]
[FDL+SE]_.


[HBSDCSG]: https://wiki.hyperbola.info/hyperbolabsd_coding_style_guidelines
    "HyperbolaBSD™ Coding Style Guidelines"

[FREEBSD_LICENSE]: https://www.freebsd.org/copyright/freebsd-license.html
    "FreeBSD® License"

[OPENBSD_MAN_PAGE]: https://man.openbsd.org/
    "OpenBSD® Manual Page"

[LOGOTYPE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/documents/information_character-g_hyperbola-n_hyperbola-t_md.md
    "HyperbolaBSD™ logotype image"
[CHARACTER]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/src/00/drawings/drawing_character-g_hyperbola-n_hyperbola-p_standing_in_bipedal_and_leaning_his_right_hand-i1-u_sldstr_sldshp-c_hypercolour-r2048px2-a0f0s0-t_svg1d2tiny.svg
    "Hyper Bola🄯 character image"

[COADDE]: coadde@hyperbola.info
    "Márcio Silva (Co.Ad.De.) <coadde@hyperbola.info>"
[CRAZYTOON]: crazytoon@hyperbola.info
    "José Silva (Crazytoon) <crazytoon@hyperbola.info>"
[EMULATORMAN]: emulatorman@hyperbola.info
    "André Silva (Emulatorman) <emulatorman@hyperbola.info>"

[FAL]: https://artlibre.org/licence/lal/en/
    "Free Art License version 1.3"
[CCBYSA]: https://creativecommons.org/licenses/by-sa/4.0/
    "Creative Commons® Attribution-ShareAlike 4.0 International License"
[FDL+SE]: https://git.hyperbola.info:50100/culture/hyperbolacc.git/plain/COPYING_FDL_P_SE_V1_3
    "GNU® Free Documentation License version 1.3 with the special exceptions"
