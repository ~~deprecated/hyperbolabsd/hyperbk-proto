__List of trademarks:__

*   __[Hyperbola&trade;][HTM]__
    is a trademark of _[Hyperbola&trade; Project][HYPERBOLA]_.
*   __[HyperbolaBSD&trade;][HBSD]__
    is a trademark of _[Hyperbola&trade; Project][HYPERBOLA]_.

[HTM]: https://www.hyperbola.info/
    "Hyperbola™ trademark"
[HBSD]: https://www.hyperbola.info/
    "HyperbolaBSD™ trademark"
[HYPERBOLA]: https://www.hyperbola.info/
    "Hyperbola™ Project"
