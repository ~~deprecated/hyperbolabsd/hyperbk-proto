/* Public domain. */

#ifndef _ASM_PGTABLE_H
#define _ASM_PGTABLE_H

#include <machine/pmap.h>
#include <machine/pgtable.h>
#include <linux/types.h>

#define pgprot_val(v)	(v)
#define PAGE_KERNEL	0
#define PAGE_KERNEL_IO	0

static inline pgprot_t
pgprot_writecombine(pgprot_t prot)
{
#if PMAP_WC != 0
	return prot | PMAP_WC;
#else
	return prot | PMAP_NOCACHE;
#endif
}

static inline pgprot_t
pgprot_noncached(pgprot_t prot)
{
#if PMAP_DEVICE != 0
	return prot | PMAP_DEVICE;
#else
	return prot | PMAP_NOCACHE;
#endif
}

#if defined(__i386__) || defined(__amd64__)
#define _PAGE_PRESENT	PGTABLE_PG_VALID
#define _PAGE_RW	PGTABLE_PG_READWRITE
#define _PAGE_PAT	PGTABLE_PG_PATBIT
#define _PAGE_PWT	PGTABLE_PG_WTHRU
#define _PAGE_PCD	PGTABLE_PG_NCACHE
#endif

#endif
