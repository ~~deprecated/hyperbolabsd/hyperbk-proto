/* Public domain. */

#ifndef _LINUX_AGP_BACKEND_H
#define _LINUX_AGP_BACKEND_H

#include <machine/sysbus.h>

#if defined(__amd64__) || defined(__i386__)
#define AGP_USER_MEMORY			0
#define AGP_USER_CACHED_MEMORY		SYSBUS_DMA_COHERENT
#endif

#endif
