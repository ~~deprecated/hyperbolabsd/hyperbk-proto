/*
 * Virtual memory swap encrypt header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

enum uvm_swapenc_t {
	/* (uvm) virtual memory, swap encrypt, enable */
	UVM_SWAPENC_ENABLE = 00,

	/* (uvm) virtual memory, swap encrypt, created */
	UVM_SWAPENC_CREATED = 01,

	/* (uvm) virtual memory, swap encrypt, deleted */
	UVM_SWAPENC_DELETED = 02,

	/* (uvm) virtual memory, swap encrypt, maximum i d (identity?) */
	UVM_SWAPENC_MAXIMUMID = 03
};

enum uvm_swapkey_t {
	/* (uvm) virtual memory, swap key, expire */
	UVM_SWAPKEY_EXPIRE = 0170,

	/* (uvm) virtual memory, swap key, size */
	UVM_SWAPKEY_SIZE = 04
};

struct uvm_swap_encrypt_key_t {
	struct {
		unsigned int	key_a[UVM_SWAPKEY_SIZE];
		unsigned short	reference_count;
	} data_s;
};

struct {
	struct {
		int32_t		do_swap_encrypt;
		int32_t		swap_prekey_print;
		u_int32_t	swap_key_expire;
		int32_t		swap_encrypt_initd;
	} data_s;
} uvm_swap_encrypt_s = {
	{
		01,
		00,
		00,
		00
	}
};

void		uvm_swe_control(void *, unsigned long *, void *, unsigned long,
    int32_t *, u_int32_t, int32_t *);
void		uvm_swe_swap_key_prepare(int32_t, void *);
void		uvm_swe_swap_key_create(unsigned int *);
void		uvm_swe_swap_key_cleanup(void *);
void		uvm_swe_swap_key_delete(unsigned int *);
unsigned short	uvm_swe_swap_key_get(struct uvm_swap_encrypt_key_t *);
unsigned short	uvm_swe_swap_key_put(struct uvm_swap_encrypt_key_t *);
void		uvm_swe_encrypt(struct uvm_swap_encrypt_key_t *, char *, char *,
    unsigned long long, unsigned long);
void		uvm_swe_decrypt(struct uvm_swap_encrypt_key_t *, char *, char *,
    unsigned long long, unsigned long);
