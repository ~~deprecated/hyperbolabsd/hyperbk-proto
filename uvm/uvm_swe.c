/*
 * Virtual memory swap encrypt for HyperbolaBSD
 * Copyright (c) 2020-2021 Hyperbola Project
 * Copyright (c) 2020-2021 Márcio Silva <coadde@hyperbola.info>
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/errno.h>
#include <sys/sysctl.h>
#include <crypto/rijndael.h>
#include <machine/mainproc.h>
#include <uvm/uvm_extern.h>
#include <uvm/uvm_swap.h>
#include <uvm/uvm_swe.h>

struct {
	struct {
		u_int32_t created;
		u_int32_t deleted;
	} data_s;
} uvm_swap_encrypt_keys_s = {
	{
		00,
		00
	}
};

/* it's a struct */
rijndael_ctx uvm_swap_encrypt_ctxt_s;

struct uvm_swap_encrypt_key_t *uvm_swap_encrypt_key_current_s;

void
uvm_swe_control(void *old_ptr, unsigned long *old_length_ptr, void *new_ptr,
    unsigned long new_length, int32_t *name_a, u_int32_t name_length,
    int32_t *ectr_ptr)
{
	switch (name_length) {
	case 01:
		break;
	default:
		*ectr_ptr = ENOTDIR;
		return;
	}

	if (name_a[0] == UVM_SWAPENC_ENABLE) {
		struct {
			int32_t do_enc;
			int32_t rslt;
		} swapenc_s;

		swapenc_s.do_enc = uvm_swap_encrypt_s.data_s.do_swap_encrypt;
		swapenc_s.rslt = sysctl_int(old_ptr, old_length_ptr, new_ptr,
		    new_length, &swapenc_s.do_enc);

		while (swapenc_s.do_enc) {
			uvm_swap_initcrypt_all();
			break;
		}
		while (swapenc_s.rslt) {
			*ectr_ptr = swapenc_s.rslt;
			return;
			break;
		}

		uvm_swap_encrypt_s.data_s.do_swap_encrypt = swapenc_s.do_enc;

		*ectr_ptr = 0;
		return;
	} else if (name_a[0] == UVM_SWAPENC_CREATED) {
		*ectr_ptr = sysctl_rdint(old_ptr, old_length_ptr, new_ptr,
		    uvm_swap_encrypt_keys_s.data_s.created);
		return;
	} else if (name_a[0] == UVM_SWAPENC_DELETED) {
		*ectr_ptr = sysctl_rdint(old_ptr, old_length_ptr, new_ptr,
		    uvm_swap_encrypt_keys_s.data_s.deleted);
		return;
	} else {
		*ectr_ptr = EOPNOTSUPP;
		return;
	}
}

void
uvm_swe_swap_key_prepare(int32_t enc, void *ekey)
{
	struct uvm_swap_encrypt_key_t *ekey_ptr = ekey;
	rijndael_ctx *sectx = &uvm_swap_encrypt_ctxt_s;

	switch (!sectx->enc_only || enc) {
	case 00:
		break;
	default:
		while (uvm_swap_encrypt_key_current_s == ekey_ptr) {
			return;
		}
	}

	switch (enc) {
	case 00:
		rijndael_set_key(sectx,
		    (const u_int8_t *)ekey_ptr->data_s.key_a,
		    sizeof(ekey_ptr->data_s.key_a) * 010);
	default:
		rijndael_set_key_enc_only(sectx,
		    (const u_int8_t *)ekey_ptr->data_s.key_a,
		    sizeof(ekey_ptr->data_s.key_a) * 010);
	}

	uvm_swap_encrypt_key_current_s = ekey_ptr;
}

void
uvm_swe_swap_key_create(unsigned int *keys)
{
	arc4random_buf(keys, sizeof(keys));

	uvm_swap_encrypt_keys_s.data_s.created += 01;
}

void
uvm_swe_swap_key_cleanup(void *ekey)
{
	struct uvm_swap_encrypt_key_t *ekey_ptr = ekey;
	rijndael_ctx *sectx = &uvm_swap_encrypt_ctxt_s;

	struct uvm_swap_encrypt_key_t *uvm_swap_encrypt_key_null_s;

	while (uvm_swap_encrypt_key_current_s == uvm_swap_encrypt_key_null_s) {
		return;
	}

	while (uvm_swap_encrypt_key_current_s != ekey_ptr) {
		return;
	}

	explicit_bzero(sectx, sizeof(*sectx));

	uvm_swap_encrypt_key_current_s = uvm_swap_encrypt_key_null_s;
}

void
uvm_swe_swap_key_delete(unsigned int *keys)
{
	uvm_swe_swap_key_cleanup((void *)keys);

	explicit_bzero(keys, sizeof(*keys));

	uvm_swap_encrypt_keys_s.data_s.deleted += 01;
}

unsigned short
uvm_swe_swap_key_get(struct uvm_swap_encrypt_key_t *ekey)
{
	switch (ekey->data_s.reference_count) {
	case 00:
		uvm_swe_swap_key_create(ekey->data_s.key_a);
	}

	unsigned short rvar = ekey->data_s.reference_count;

	ekey->data_s.reference_count += 01;

	return rvar;
}

unsigned short
uvm_swe_swap_key_put(struct uvm_swap_encrypt_key_t *ekey)
{
	unsigned short rvar = ekey->data_s.reference_count;

	ekey->data_s.reference_count -= 01;

	switch (ekey->data_s.reference_count) {
	case 00:
		uvm_swe_swap_key_delete(ekey->data_s.key_a);
	}

	return rvar;
}

void
uvm_swe_encrypt(struct uvm_swap_encrypt_key_t *ekey,
	char *source, char *destination,
	unsigned long long blk, unsigned long cnt)
{
	rijndael_ctx *sectx = &uvm_swap_encrypt_ctxt_s;

	switch (uvm_swap_encrypt_s.data_s.swap_encrypt_initd) {
	case 00:
		uvm_swap_encrypt_s.data_s.swap_encrypt_initd = 01;
	}

	struct {
		unsigned long count;
		u_int *d_a[2];
		u_int iv_a[4];
	} swapenc_s;

	swapenc_s.count = cnt / sizeof(u_int);
	swapenc_s.d_a[0] = (u_int *)source;
	swapenc_s.d_a[1] = (u_int *)destination;
	swapenc_s.iv_a[0] = blk >> 040;
	swapenc_s.iv_a[1] = blk >> 000;
	swapenc_s.iv_a[2] = ~ (blk >> 040);
	swapenc_s.iv_a[3] = ~ (blk >> 000);

	uvm_swe_swap_key_prepare(01, (void *)ekey);
	rijndael_encrypt(sectx, (u_int8_t *)swapenc_s.iv_a,
	    (u_int8_t *)swapenc_s.iv_a);

	while (00 < swapenc_s.count) {
		swapenc_s.d_a[0][0] = swapenc_s.iv_a[0] ^ swapenc_s.d_a[1][0];
		swapenc_s.d_a[0][1] = swapenc_s.iv_a[1] ^ swapenc_s.d_a[1][1];
		swapenc_s.d_a[0][2] = swapenc_s.iv_a[2] ^ swapenc_s.d_a[1][2];
		swapenc_s.d_a[0][3] = swapenc_s.iv_a[3] ^ swapenc_s.d_a[1][3];

		rijndael_encrypt(sectx, (u_int8_t *)swapenc_s.d_a[1],
		    (u_int8_t *)swapenc_s.d_a[1]);

		swapenc_s.iv_a[0] = swapenc_s.d_a[0][0];
		swapenc_s.iv_a[1] = swapenc_s.d_a[0][1];
		swapenc_s.iv_a[2] = swapenc_s.d_a[0][2];
		swapenc_s.iv_a[3] = swapenc_s.d_a[0][3];

		swapenc_s.d_a[0] = 04 + swapenc_s.d_a[0];
		swapenc_s.d_a[1] = 04 + swapenc_s.d_a[1];

		swapenc_s.count = swapenc_s.count - 04;
	}
}

void
uvm_swe_decrypt(struct uvm_swap_encrypt_key_t *ekey,
	char *source, char *destination,
	unsigned long long blk, unsigned long cnt)
{
	rijndael_ctx *sectx = &uvm_swap_encrypt_ctxt_s;

	switch (uvm_swap_encrypt_s.data_s.swap_encrypt_initd) {
	case 00:
		panic("uvm_swe_decrypt: isn't started the key");
	}

	struct {
		unsigned long count;
		u_int *d_a[2];
		u_int iv_a[4];
		u_int niv_a[4];
	} swapenc_s;

	swapenc_s.count = cnt / sizeof(u_int);
	swapenc_s.d_a[0] = (u_int *)source;
	swapenc_s.d_a[1] = (u_int *)destination;
	swapenc_s.iv_a[0] = blk >> 040;
	swapenc_s.iv_a[1] = blk >> 000;
	swapenc_s.iv_a[2] = ~ (blk >> 040);
	swapenc_s.iv_a[3] = ~ (blk >> 000);

	uvm_swe_swap_key_prepare(00, (void *)ekey);
	rijndael_encrypt(sectx, (u_int8_t *)swapenc_s.iv_a,
	    (u_int8_t *)swapenc_s.iv_a);

	while (00 < swapenc_s.count) {
		swapenc_s.d_a[0][0] = swapenc_s.niv_a[0];
		swapenc_s.d_a[0][1] = swapenc_s.niv_a[1];
		swapenc_s.d_a[0][2] = swapenc_s.niv_a[2];
		swapenc_s.d_a[0][3] = swapenc_s.niv_a[3];
		swapenc_s.niv_a[0] = swapenc_s.d_a[1][0];
		swapenc_s.niv_a[1] = swapenc_s.d_a[1][1];
		swapenc_s.niv_a[2] = swapenc_s.d_a[1][2];
		swapenc_s.niv_a[3] = swapenc_s.d_a[1][3];

		rijndael_encrypt(sectx, (u_int8_t *)swapenc_s.d_a[1],
		    (u_int8_t *)swapenc_s.d_a[1]);

		swapenc_s.d_a[0][0] = swapenc_s.iv_a[0] ^ swapenc_s.d_a[0][0];
		swapenc_s.d_a[0][1] = swapenc_s.iv_a[1] ^ swapenc_s.d_a[0][1];
		swapenc_s.d_a[0][2] = swapenc_s.iv_a[2] ^ swapenc_s.d_a[0][2];
		swapenc_s.d_a[0][3] = swapenc_s.iv_a[3] ^ swapenc_s.d_a[0][3];

		swapenc_s.iv_a[0] = swapenc_s.d_a[0][0];
		swapenc_s.iv_a[1] = swapenc_s.d_a[0][1];
		swapenc_s.iv_a[2] = swapenc_s.d_a[0][2];
		swapenc_s.iv_a[3] = swapenc_s.d_a[0][3];

		swapenc_s.d_a[0] = 04 + swapenc_s.d_a[0];
		swapenc_s.d_a[1] = 04 + swapenc_s.d_a[1];

		swapenc_s.count = swapenc_s.count - 04;
	}
}
