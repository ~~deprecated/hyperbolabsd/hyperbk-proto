
SUBDIR=	dev/microcode \
	arch/amd64 arch/i386 arch/arm64 arch/armv7

tags:
	cd ${.CURDIR}/kern; make tags

.include <bsd.subdir.mk>
