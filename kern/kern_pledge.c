
/*
 * Copyright (c) 2015 Nicholas Marriott <nicm@openbsd.org>
 * Copyright (c) 2015 Theo de Raadt <deraadt@openbsd.org>
 *
 * Modifications to support HyperbolaBSD:
 * Copyright (c) 2020-2021 Hyperbola Project
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/param.h>

#include <sys/mount.h>
#include <sys/proc.h>
#include <sys/fcntl.h>
#include <sys/file.h>
#include <sys/filedesc.h>
#include <sys/namei.h>
#include <sys/pool.h>
#include <sys/socketvar.h>
#include <sys/vnode.h>
#include <sys/mbuf.h>
#include <sys/mman.h>
#include <sys/sysctl.h>
#include <sys/syslog.h>
#include <sys/ktrace.h>
#include <sys/acct.h>

#include <sys/ioctl.h>
#include <sys/termios.h>
#include <sys/tty.h>
#include <sys/dev.h>
#include <sys/disklabel.h>
#include <sys/dkio.h>
#include <sys/mtio.h>
#include <sys/sound.h>
#include <sys/videoio.h>
#include <net/bpf.h>
#include <net/route.h>
#include <net/if.h>
#include <net/if_var.h>
#include <netinet/in.h>
#include <netinet6/in6_var.h>
#include <netinet6/nd6.h>
#include <netinet/tcp.h>
#include <net/pfvar.h>

#include <sys/conf.h>
#include <sys/specdev.h>
#include <sys/signal.h>
#include <sys/signalvar.h>
#include <sys/syscl.h>
#include <sys/sysclarg.h>
#include <sys/systm.h>

#include <dev/biovar.h>

#define PLEDGENAMES
#include <sys/pledge.h>

#include "audio.h"
#include "bpfilter.h"
#include "pf.h"
#include "video.h"
#include "pty.h"

#if defined(__amd64__)
#include "vmm.h"
#if NVMM > 0
#include <machine/conf.h>
#endif
#endif

#if defined(__amd64__) || defined(__arm64__) || defined(__i386__)
#include "drm.h"
#endif

uint64_t pledgereq_flags(const char *req);
int	 parsepledges(struct proc *p, const char *kname,
	    const char *promises, u_int64_t *fp);
int	 canonpath(const char *input, char *buf, size_t bufsize);
void	 unveil_destroy(struct process *ps);

/* #define DEBUG_PLEDGE */
#ifdef DEBUG_PLEDGE
int debug_pledge = 1;
#define DPRINTF(x...)    do { if (debug_pledge) printf(x); } while (0)
#define DNPRINTF(n,x...) do { if (debug_pledge >= (n)) printf(x); } while (0)
#else
#define DPRINTF(x...)
#define DNPRINTF(n,x...)
#endif

/*
 * Ordered in blocks starting with least risky and most required.
 */
const uint64_t pledge_syscalls[SYSCL_MAX] = {
	/*
	 * Minimum required
	 */
	[SYSCL_EXIT] = PLEDGE_ALWAYS,
	[SYSCL_KBIND] = PLEDGE_ALWAYS,
	[SYSCL_GET_TCB] = PLEDGE_ALWAYS,
	[SYSCL_SET_TCB] = PLEDGE_ALWAYS,
	[SYSCL_PLEDGE] = PLEDGE_ALWAYS,
	[SYSCL_SENDSYSLOG] = PLEDGE_ALWAYS,	/* stack protector reporting */
	[SYSCL_THRKILL] = PLEDGE_ALWAYS,		/* raise, abort, stack pro */
	[SYSCL_UTRACE] = PLEDGE_ALWAYS,		/* ltrace(1) from ld.so */

	/* "getting" information about self is considered safe */
	[SYSCL_GETUID] = PLEDGE_STDIO,
	[SYSCL_GETEUID] = PLEDGE_STDIO,
	[SYSCL_GETRESUID] = PLEDGE_STDIO,
	[SYSCL_GETGID] = PLEDGE_STDIO,
	[SYSCL_GETEGID] = PLEDGE_STDIO,
	[SYSCL_GETRESGID] = PLEDGE_STDIO,
	[SYSCL_GETGROUPS] = PLEDGE_STDIO,
	[SYSCL_GETLOGIN_R] = PLEDGE_STDIO,
	[SYSCL_GETPGRP] = PLEDGE_STDIO,
	[SYSCL_GETPGID] = PLEDGE_STDIO,
	[SYSCL_GETPPID] = PLEDGE_STDIO,
	[SYSCL_GETSID] = PLEDGE_STDIO,
	[SYSCL_GETTHRID] = PLEDGE_STDIO,
	[SYSCL_GETRLIMIT] = PLEDGE_STDIO,
	[SYSCL_GETRTABLE] = PLEDGE_STDIO,
	[SYSCL_GETTIMEOFDAY] = PLEDGE_STDIO,
	[SYSCL_GETDTABLECOUNT] = PLEDGE_STDIO,
	[SYSCL_GETRUSAGE] = PLEDGE_STDIO,
	[SYSCL_ISSUETUGID] = PLEDGE_STDIO,
	[SYSCL_CLOCK_GETRES] = PLEDGE_STDIO,
	[SYSCL_CLOCK_GETTIME] = PLEDGE_STDIO,
	[SYSCL_GETPID] = PLEDGE_STDIO,

	/*
	 * Almost exclusively read-only, Very narrow subset.
	 * Use of "route", "inet", "dns", "ps", or "vminfo"
	 * expands access.
	 */
	[SYSCL_SYSCTL] = PLEDGE_STDIO,

	/* Support for malloc(3) family of operations */
	[SYSCL_GETENTROPY] = PLEDGE_STDIO,
	[SYSCL_MADVISE] = PLEDGE_STDIO,
	[SYSCL_MINHERIT] = PLEDGE_STDIO,
	[SYSCL_MMAP] = PLEDGE_STDIO,
	[SYSCL_MPROTECT] = PLEDGE_STDIO,
	[SYSCL_MQUERY] = PLEDGE_STDIO,
	[SYSCL_MUNMAP] = PLEDGE_STDIO,
	[SYSCL_MSYNC] = PLEDGE_STDIO,
	[SYSCL_BREAK] = PLEDGE_STDIO,

	[SYSCL_UMASK] = PLEDGE_STDIO,

	/* read/write operations */
	[SYSCL_READ] = PLEDGE_STDIO,
	[SYSCL_READV] = PLEDGE_STDIO,
	[SYSCL_PREAD] = PLEDGE_STDIO,
	[SYSCL_PREADV] = PLEDGE_STDIO,
	[SYSCL_WRITE] = PLEDGE_STDIO,
	[SYSCL_WRITEV] = PLEDGE_STDIO,
	[SYSCL_PWRITE] = PLEDGE_STDIO,
	[SYSCL_PWRITEV] = PLEDGE_STDIO,
	[SYSCL_RECVMSG] = PLEDGE_STDIO,
	[SYSCL_RECVFROM] = PLEDGE_STDIO | PLEDGE_YPACTIVE,
	[SYSCL_FTRUNCATE] = PLEDGE_STDIO,
	[SYSCL_LSEEK] = PLEDGE_STDIO,
	[SYSCL_FPATHCONF] = PLEDGE_STDIO,

	/*
	 * Address selection required a network pledge ("inet",
	 * "unix", "dns".
	 */
	[SYSCL_SENDTO] = PLEDGE_STDIO | PLEDGE_YPACTIVE,

	/*
	 * Address specification required a network pledge ("inet",
	 * "unix", "dns".  SCM_RIGHTS requires "sendfd" or "recvfd".
	 */
	[SYSCL_SENDMSG] = PLEDGE_STDIO,

	/* Common signal operations */
	[SYSCL_NANOSLEEP] = PLEDGE_STDIO,
	[SYSCL_SIGALTSTACK] = PLEDGE_STDIO,
	[SYSCL_SIGPROCMASK] = PLEDGE_STDIO,
	[SYSCL_SIGSUSPEND] = PLEDGE_STDIO,
	[SYSCL_SIGACTION] = PLEDGE_STDIO,
	[SYSCL_SIGRETURN] = PLEDGE_STDIO,
	[SYSCL_SIGPENDING] = PLEDGE_STDIO,
	[SYSCL_GETITIMER] = PLEDGE_STDIO,
	[SYSCL_SETITIMER] = PLEDGE_STDIO,

	/*
	 * To support event driven programming.
	 */
	[SYSCL_POLL] = PLEDGE_STDIO,
	[SYSCL_PPOLL] = PLEDGE_STDIO,
	[SYSCL_KEVENT] = PLEDGE_STDIO,
	[SYSCL_KQUEUE] = PLEDGE_STDIO,
	[SYSCL_SELECT] = PLEDGE_STDIO,
	[SYSCL_PSELECT] = PLEDGE_STDIO,

	[SYSCL_FSTAT] = PLEDGE_STDIO,
	[SYSCL_FSYNC] = PLEDGE_STDIO,

	[SYSCL_SETSOCKOPT] = PLEDGE_STDIO,	/* narrow whitelist */
	[SYSCL_GETSOCKOPT] = PLEDGE_STDIO,	/* narrow whitelist */

	/* F_SETOWN requires PLEDGE_PROC */
	[SYSCL_FCNTL] = PLEDGE_STDIO,

	[SYSCL_CLOSE] = PLEDGE_STDIO,
	[SYSCL_DUP] = PLEDGE_STDIO,
	[SYSCL_DUP2] = PLEDGE_STDIO,
	[SYSCL_DUP3] = PLEDGE_STDIO,
	[SYSCL_CLOSEFROM] = PLEDGE_STDIO,
	[SYSCL_SHUTDOWN] = PLEDGE_STDIO,
	[SYSCL_FCHDIR] = PLEDGE_STDIO,	/* XXX consider tightening */

	[SYSCL_PIPE] = PLEDGE_STDIO,
	[SYSCL_PIPE2] = PLEDGE_STDIO,
	[SYSCL_SOCKETPAIR] = PLEDGE_STDIO,

	[SYSCL_WAIT4] = PLEDGE_STDIO,

	/*
	 * Can kill self with "stdio".  Killing another pid
	 * requires "proc"
	 */
	[SYSCL_KILL] = PLEDGE_STDIO,

	/*
	 * FIONREAD/FIONBIO for "stdio"
	 * Other ioctl are selectively allowed based upon other pledges.
	 */
	[SYSCL_IOCTL] = PLEDGE_STDIO,

	/*
	 * Path access/creation calls encounter many extensive
	 * checks done during pledge_namei()
	 */
	[SYSCL_OPEN] = PLEDGE_STDIO,
	[SYSCL_STAT] = PLEDGE_STDIO,
	[SYSCL_ACCESS] = PLEDGE_STDIO,
	[SYSCL_READLINK] = PLEDGE_STDIO,
	[SYSCL_REALPATH] = PLEDGE_STDIO,

	[SYSCL_ADJTIME] = PLEDGE_STDIO,   /* setting requires "settime" */
	[SYSCL_ADJFREQ] = PLEDGE_SETTIME,
	[SYSCL_SETTIMEOFDAY] = PLEDGE_SETTIME,

	/*
	 * Needed by threaded programs
	 * XXX should we have a new "threads"?
	 */
	[SYSCL_TFORK] = PLEDGE_STDIO,
	[SYSCL_SCHED_YIELD] = PLEDGE_STDIO,
	[SYSCL_FUTEX] = PLEDGE_STDIO,
	[SYSCL_THRSLEEP] = PLEDGE_STDIO,
	[SYSCL_THRWAKEUP] = PLEDGE_STDIO,
	[SYSCL_THREXIT] = PLEDGE_STDIO,
	[SYSCL_THRSIGDIVERT] = PLEDGE_STDIO,

	[SYSCL_FORK] = PLEDGE_PROC,
	[SYSCL_VFORK] = PLEDGE_PROC,
	[SYSCL_SETPGID] = PLEDGE_PROC,
	[SYSCL_SETSID] = PLEDGE_PROC,

	[SYSCL_SETRLIMIT] = PLEDGE_PROC | PLEDGE_ID,
	[SYSCL_GETPRIORITY] = PLEDGE_PROC | PLEDGE_ID,

	[SYSCL_SETPRIORITY] = PLEDGE_PROC | PLEDGE_ID,

	[SYSCL_SETUID] = PLEDGE_ID,
	[SYSCL_SETEUID] = PLEDGE_ID,
	[SYSCL_SETREUID] = PLEDGE_ID,
	[SYSCL_SETRESUID] = PLEDGE_ID,
	[SYSCL_SETGID] = PLEDGE_ID,
	[SYSCL_SETEGID] = PLEDGE_ID,
	[SYSCL_SETREGID] = PLEDGE_ID,
	[SYSCL_SETRESGID] = PLEDGE_ID,
	[SYSCL_SETGROUPS] = PLEDGE_ID,
	[SYSCL_SETLOGIN] = PLEDGE_ID,

	[SYSCL_UNVEIL] = PLEDGE_UNVEIL,

	[SYSCL_EXECVE] = PLEDGE_EXEC,

	[SYSCL_CHDIR] = PLEDGE_RPATH,
	[SYSCL_OPENAT] = PLEDGE_RPATH | PLEDGE_WPATH,
	[SYSCL_FSTATAT] = PLEDGE_RPATH | PLEDGE_WPATH,
	[SYSCL_FACCESSAT] = PLEDGE_RPATH | PLEDGE_WPATH,
	[SYSCL_READLINKAT] = PLEDGE_RPATH | PLEDGE_WPATH,
	[SYSCL_LSTAT] = PLEDGE_RPATH | PLEDGE_WPATH | PLEDGE_TMPPATH,
	[SYSCL_TRUNCATE] = PLEDGE_WPATH,
	[SYSCL_RENAME] = PLEDGE_RPATH | PLEDGE_CPATH,
	[SYSCL_RMDIR] = PLEDGE_CPATH,
	[SYSCL_RENAMEAT] = PLEDGE_CPATH,
	[SYSCL_LINK] = PLEDGE_CPATH,
	[SYSCL_LINKAT] = PLEDGE_CPATH,
	[SYSCL_SYMLINK] = PLEDGE_CPATH,
	[SYSCL_SYMLINKAT] = PLEDGE_CPATH,
	[SYSCL_UNLINK] = PLEDGE_CPATH | PLEDGE_TMPPATH,
	[SYSCL_UNLINKAT] = PLEDGE_CPATH,
	[SYSCL_MKDIR] = PLEDGE_CPATH,
	[SYSCL_MKDIRAT] = PLEDGE_CPATH,

	[SYSCL_MKFIFO] = PLEDGE_DPATH,
	[SYSCL_MKFIFOAT] = PLEDGE_DPATH,
	[SYSCL_MKNOD] = PLEDGE_DPATH,
	[SYSCL_MKNODAT] = PLEDGE_DPATH,

	[SYSCL_REVOKE] = PLEDGE_TTY,	/* also requires PLEDGE_RPATH */

	/*
	 * Classify as RPATH|WPATH, because of path information leakage.
	 * WPATH due to unknown use of mk*temp(3) on non-/tmp paths..
	 */
	[SYSCL_GETCWD] = PLEDGE_RPATH | PLEDGE_WPATH,

	/* Classify as RPATH, because these leak path information */
	[SYSCL_GETDENTS] = PLEDGE_RPATH,
	[SYSCL_GETFSSTAT] = PLEDGE_RPATH,
	[SYSCL_STATFS] = PLEDGE_RPATH,
	[SYSCL_FSTATFS] = PLEDGE_RPATH,
	[SYSCL_PATHCONF] = PLEDGE_RPATH,

	[SYSCL_UTIMES] = PLEDGE_FATTR,
	[SYSCL_FUTIMES] = PLEDGE_FATTR,
	[SYSCL_UTIMENSAT] = PLEDGE_FATTR,
	[SYSCL_FUTIMENS] = PLEDGE_FATTR,
	[SYSCL_CHMOD] = PLEDGE_FATTR,
	[SYSCL_FCHMOD] = PLEDGE_FATTR,
	[SYSCL_FCHMODAT] = PLEDGE_FATTR,
	[SYSCL_CHFLAGS] = PLEDGE_FATTR,
	[SYSCL_CHFLAGSAT] = PLEDGE_FATTR,
	[SYSCL_FCHFLAGS] = PLEDGE_FATTR,

	[SYSCL_CHOWN] = PLEDGE_CHOWN,
	[SYSCL_FCHOWNAT] = PLEDGE_CHOWN,
	[SYSCL_LCHOWN] = PLEDGE_CHOWN,
	[SYSCL_FCHOWN] = PLEDGE_CHOWN,

	[SYSCL_SOCKET] = PLEDGE_INET | PLEDGE_UNIX | PLEDGE_DNS | PLEDGE_YPACTIVE,
	[SYSCL_CONNECT] = PLEDGE_INET | PLEDGE_UNIX | PLEDGE_DNS | PLEDGE_YPACTIVE,
	[SYSCL_BIND] = PLEDGE_INET | PLEDGE_UNIX | PLEDGE_DNS | PLEDGE_YPACTIVE,
	[SYSCL_GETSOCKNAME] = PLEDGE_INET | PLEDGE_UNIX | PLEDGE_DNS | PLEDGE_YPACTIVE,

	[SYSCL_LISTEN] = PLEDGE_INET | PLEDGE_UNIX,
	[SYSCL_ACCEPT4] = PLEDGE_INET | PLEDGE_UNIX,
	[SYSCL_ACCEPT] = PLEDGE_INET | PLEDGE_UNIX,
	[SYSCL_GETPEERNAME] = PLEDGE_INET | PLEDGE_UNIX,

	[SYSCL_FLOCK] = PLEDGE_FLOCK | PLEDGE_YPACTIVE,

	[SYSCL_SWAPCTL] = PLEDGE_VMINFO,	/* XXX should limit to "get" operations */
};

static const struct {
	char *name;
	uint64_t flags;
} pledgereq[] = {
	{ "audio",		PLEDGE_AUDIO },
	{ "bpf",		PLEDGE_BPF },
	{ "chown",		PLEDGE_CHOWN | PLEDGE_CHOWNUID },
	{ "cpath",		PLEDGE_CPATH },
	{ "disklabel",		PLEDGE_DISKLABEL },
	{ "dns",		PLEDGE_DNS },
	{ "dpath",		PLEDGE_DPATH },
	{ "drm",		PLEDGE_DRM },
	{ "error",		PLEDGE_ERROR },
	{ "exec",		PLEDGE_EXEC },
	{ "fattr",		PLEDGE_FATTR | PLEDGE_CHOWN },
	{ "flock",		PLEDGE_FLOCK },
	{ "getpw",		PLEDGE_GETPW },
	{ "id",			PLEDGE_ID },
	{ "inet",		PLEDGE_INET },
	{ "mcast",		PLEDGE_MCAST },
	{ "pf",			PLEDGE_PF },
	{ "proc",		PLEDGE_PROC },
	{ "prot_exec",		PLEDGE_PROTEXEC },
	{ "ps",			PLEDGE_PS },
	{ "recvfd",		PLEDGE_RECVFD },
	{ "route",		PLEDGE_ROUTE },
	{ "rpath",		PLEDGE_RPATH },
	{ "sendfd",		PLEDGE_SENDFD },
	{ "settime",		PLEDGE_SETTIME },
	{ "stdio",		PLEDGE_STDIO },
	{ "tape",		PLEDGE_TAPE },
	{ "tmppath",		PLEDGE_TMPPATH },
	{ "tty",		PLEDGE_TTY },
	{ "unix",		PLEDGE_UNIX },
	{ "unveil",		PLEDGE_UNVEIL },
	{ "video",		PLEDGE_VIDEO },
	{ "vminfo",		PLEDGE_VMINFO },
	{ "vmm",		PLEDGE_VMM },
	{ "wpath",		PLEDGE_WPATH },
	{ "wroute",		PLEDGE_WROUTE },
};

int
parsepledges(struct proc *p, const char *kname, const char *promises, u_int64_t *fp)
{
	size_t rbuflen;
	char *rbuf, *rp, *pn;
	u_int64_t flags = 0, f;
	int error;

	rbuf = malloc(MAXPATHLEN, M_TEMP, M_WAITOK);
	error = copyinstr(promises, rbuf, MAXPATHLEN,
	    &rbuflen);
	if (error) {
		free(rbuf, M_TEMP, MAXPATHLEN);
		return (error);
	}
#ifdef KTRACE
	if (KTRPOINT(p, KTR_STRUCT))
		ktrstruct(p, kname, rbuf, rbuflen-1);
#endif

	for (rp = rbuf; rp && *rp; rp = pn) {
		pn = strchr(rp, ' ');	/* find terminator */
		if (pn) {
			while (*pn == ' ')
				*pn++ = '\0';
		}
		if ((f = pledgereq_flags(rp)) == 0) {
			free(rbuf, M_TEMP, MAXPATHLEN);
			return (EINVAL);
		}
		flags |= f;
	}
	free(rbuf, M_TEMP, MAXPATHLEN);
	*fp = flags;
	return 0;
}

int
sys_pledge(struct proc *p, void *v, register_t *retval)
{
	struct scp_pledge_t /* {
		syscallarg(const char *)promises;
		syscallarg(const char *)execpromises;
	} */	*uap = v;
	struct process *pr = p->p_p;
	uint64_t promises, execpromises;
	int error;

	if (SCARG(uap, promises)) {
		error = parsepledges(p, "pledgereq",
		    SCARG(uap, promises), &promises);
		if (error)
			return (error);

		/* In "error" mode, ignore promise increase requests,
		 * but accept promise decrease requests */
		if (ISSET(pr->ps_flags, PS_PLEDGE) &&
		    (pr->ps_pledge & PLEDGE_ERROR))
			promises &= (pr->ps_pledge & PLEDGE_USERSET);

		/* Only permit reductions */
		if (ISSET(pr->ps_flags, PS_PLEDGE) &&
		    (((promises | pr->ps_pledge) != pr->ps_pledge)))
			return (EPERM);
	}
	if (SCARG(uap, execpromises)) {
		error = parsepledges(p, "pledgeexecreq",
		    SCARG(uap, execpromises), &execpromises);
		if (error)
			return (error);

		/* Only permit reductions */
		if (ISSET(pr->ps_flags, PS_EXECPLEDGE) &&
		    (((execpromises | pr->ps_execpledge) != pr->ps_execpledge)))
			return (EPERM);
	}

	if (SCARG(uap, promises)) {
		pr->ps_pledge = promises;
		pr->ps_flags |= PS_PLEDGE;
		/*
		 * Kill off unveil and drop unveil vnode refs if we no
		 * longer are holding any path-accessing pledge
		 */
		if ((pr->ps_pledge & (PLEDGE_RPATH | PLEDGE_WPATH |
		    PLEDGE_CPATH | PLEDGE_DPATH | PLEDGE_TMPPATH | PLEDGE_EXEC |
		    PLEDGE_UNIX | PLEDGE_UNVEIL)) == 0)
			unveil_destroy(pr);
	}
	if (SCARG(uap, execpromises)) {
		pr->ps_execpledge = execpromises;
		pr->ps_flags |= PS_EXECPLEDGE;
	}
	return (0);
}

int
pledge_syscall(struct proc *p, int code, uint64_t *tval)
{
	p->p_pledge_syscall = code;
	*tval = 0;

	if (code < 0 || code > SYSCL_MAX - 1)
		return (EINVAL);

	if (pledge_syscalls[code] == PLEDGE_ALWAYS)
		return (0);

	if (p->p_p->ps_pledge & pledge_syscalls[code])
		return (0);

	*tval = pledge_syscalls[code];
	return (EPERM);
}

int
pledge_fail(struct proc *p, int error, uint64_t code)
{
	char *codes = "";
	int i;
	struct sigaction sa;

	/* Print first matching pledge */
	for (i = 0; code && pledgenames[i].bits != 0; i++)
		if (pledgenames[i].bits & code) {
			codes = pledgenames[i].name;
			break;
		}
#ifdef KTRACE
	if (KTRPOINT(p, KTR_PLEDGE))
		ktrpledge(p, error, code, p->p_pledge_syscall);
#endif
	if (p->p_p->ps_pledge & PLEDGE_ERROR)
		return (ENOSYS);

	KERNEL_LOCK();
	log(LOG_ERR, "%s[%d]: pledge \"%s\", syscall %d\n",
	    p->p_p->ps_comm, p->p_p->ps_pid, codes, p->p_pledge_syscall);
	p->p_p->ps_acflag |= APLEDGE;

	/* Send uncatchable SIGABRT for coredump */
	memset(&sa, 0, sizeof sa);
	sa.sa_handler = SIG_DFL;
	setsigvec(p, SIGABRT, &sa);
	atomic_clearbits_int(&p->p_sigmask, sigmask(SIGABRT));
	psignal(p, SIGABRT);

	p->p_p->ps_pledge = 0;		/* Disable all PLEDGE_ flags */
	KERNEL_UNLOCK();
	return (error);
}

/*
 * Need to make it more obvious that one cannot get through here
 * without the right flags set
 */
int
pledge_namei(struct proc *p, struct nameidata *ni, char *origpath)
{
	char path[PATH_MAX];
	int error;

	if ((p->p_p->ps_flags & PS_PLEDGE) == 0 ||
	    (p->p_p->ps_flags & PS_COREDUMP))
		return (0);

	if (ni->ni_pledge == 0)
		panic("pledge_namei: ni_pledge");

	/*
	 * We set the BYPASSUNVEIL flag to skip unveil checks
	 * as necessary
	 */

	/* Doing a permitted execve() */
	if ((ni->ni_pledge & PLEDGE_EXEC) &&
	    (p->p_p->ps_pledge & PLEDGE_EXEC))
		return (0);

	error = canonpath(origpath, path, sizeof(path));
	if (error)
		return (error);

	/* Detect what looks like a mkstemp(3) family operation */
	if ((p->p_p->ps_pledge & PLEDGE_TMPPATH) &&
	    (p->p_pledge_syscall == SYSCL_OPEN) &&
	    (ni->ni_pledge & PLEDGE_CPATH) &&
	    strncmp(path, "/tmp/", sizeof("/tmp/") - 1) == 0) {
		ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
		return (0);
	}

	/* Allow unlinking of a mkstemp(3) file...
	 * Good opportunity for strict checks here.
	 */
	if ((p->p_p->ps_pledge & PLEDGE_TMPPATH) &&
	    (p->p_pledge_syscall == SYSCL_UNLINK) &&
	    strncmp(path, "/tmp/", sizeof("/tmp/") - 1) == 0) {
		ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
		return (0);
	}

	/* Whitelisted paths */
	switch (p->p_pledge_syscall) {
	case SYSCL_ACCESS:
		/* tzset() needs this. */
		if (ni->ni_pledge == PLEDGE_RPATH &&
		    strcmp(path, "/etc/localtime") == 0) {
			ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
			return (0);
		}

		/* when avoiding YP mode, getpw* functions touch this */
		if (ni->ni_pledge == PLEDGE_RPATH &&
		    strcmp(path, "/var/run/ypbind.lock") == 0) {
			if ((p->p_p->ps_pledge & PLEDGE_GETPW) ||
			    (ni->ni_unveil == UNVEIL_INSPECT)) {
				ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
				return (0);
			} else
				return (pledge_fail(p, error, PLEDGE_GETPW));
		}
		break;
	case SYSCL_OPEN:
		/* daemon(3) or other such functions */
		if ((ni->ni_pledge & ~(PLEDGE_RPATH | PLEDGE_WPATH)) == 0 &&
		    strcmp(path, "/dev/null") == 0) {
			ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
			return (0);
		}

		/* readpassphrase(3), getpass(3) */
		if ((p->p_p->ps_pledge & PLEDGE_TTY) &&
		    (ni->ni_pledge & ~(PLEDGE_RPATH | PLEDGE_WPATH)) == 0 &&
		    strcmp(path, "/dev/tty") == 0) {
			ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
			return (0);
		}

		/* getpw* and friends need a few files */
		if ((ni->ni_pledge == PLEDGE_RPATH) &&
		    (p->p_p->ps_pledge & PLEDGE_GETPW)) {
			if (strcmp(path, "/etc/spwd.db") == 0)
				return (EPERM); /* don't call pledge_fail */
			if (strcmp(path, "/etc/pwd.db") == 0) {
				ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
				return (0);
			}
			if (strcmp(path, "/etc/group") == 0) {
				ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
				return (0);
			}
			if (strcmp(path, "/etc/netid") == 0) {
				ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
				return (0);
			}
		}

		/* DNS needs /etc/{resolv.conf,hosts,services}. */
		if ((ni->ni_pledge == PLEDGE_RPATH) &&
		    (p->p_p->ps_pledge & PLEDGE_DNS)) {
			if (strcmp(path, "/etc/resolv.conf") == 0) {
				ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
				return (0);
			}
			if (strcmp(path, "/etc/hosts") == 0) {
				ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
				return (0);
			}
			if (strcmp(path, "/etc/services") == 0) {
				ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
				return (0);
			}
		}

		if ((ni->ni_pledge == PLEDGE_RPATH) &&
		    (p->p_p->ps_pledge & PLEDGE_GETPW)) {
			if (strcmp(path, "/var/run/ypbind.lock") == 0) {
				/*
				 * XXX
				 * The current hack for YP support in "getpw"
				 * is to enable some "inet" features until
				 * next pledge call.  This is not considered
				 * worse than pre-pledge, but is a work in
				 * progress, needing a clever design.
				 */
				p->p_p->ps_pledge |= PLEDGE_YPACTIVE;
				ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
				return (0);
			}
			if (strncmp(path, "/var/yp/binding/",
			    sizeof("/var/yp/binding/") - 1) == 0) {
				ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
				return (0);
			}
		}

		/* tzset() needs these. */
		if ((ni->ni_pledge == PLEDGE_RPATH) &&
		    strncmp(path, "/usr/share/zoneinfo/",
		    sizeof("/usr/share/zoneinfo/") - 1) == 0)  {
			ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
			return (0);
		}
		if ((ni->ni_pledge == PLEDGE_RPATH) &&
		    strcmp(path, "/etc/localtime") == 0) {
			ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
			return (0);
		}

		break;
	case SYSCL_READLINK:
		/* Allow /etc/malloc.conf for malloc(3). */
		if ((ni->ni_pledge == PLEDGE_RPATH) &&
		    strcmp(path, "/etc/malloc.conf") == 0) {
			ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
			return (0);
		}
		break;
	case SYSCL_STAT:
		/* DNS needs /etc/resolv.conf. */
		if ((ni->ni_pledge == PLEDGE_RPATH) &&
		    (p->p_p->ps_pledge & PLEDGE_DNS) &&
		    strcmp(path, "/etc/resolv.conf") == 0) {
			ni->ni_cnd.cn_flags |= BYPASSUNVEIL;
			return (0);
		}
		break;
	}

	/*
	 * Ensure each flag of ni_pledge has counterpart allowing it in
	 * ps_pledge.
	 */
	if (ni->ni_pledge & ~p->p_p->ps_pledge)
		return (pledge_fail(p, EPERM, (ni->ni_pledge & ~p->p_p->ps_pledge)));

	/* continue, and check unveil if present */
	return (0);
}

/*
 * Only allow reception of safe file descriptors.
 */
int
pledge_recvfd(struct proc *p, struct file *fp)
{
	struct vnode *vp;

	if ((p->p_p->ps_flags & PS_PLEDGE) == 0)
		return (0);
	if ((p->p_p->ps_pledge & PLEDGE_RECVFD) == 0)
		return pledge_fail(p, EPERM, PLEDGE_RECVFD);

	switch (fp->f_type) {
	case DTYPE_SOCKET:
	case DTYPE_PIPE:
	case DTYPE_DMABUF:
		return (0);
	case DTYPE_VNODE:
		vp = fp->f_data;

		if (vp->v_type != VDIR)
			return (0);
	}
	return pledge_fail(p, EINVAL, PLEDGE_RECVFD);
}

/*
 * Only allow sending of safe file descriptors.
 */
int
pledge_sendfd(struct proc *p, struct file *fp)
{
	struct vnode *vp;

	if ((p->p_p->ps_flags & PS_PLEDGE) == 0)
		return (0);
	if ((p->p_p->ps_pledge & PLEDGE_SENDFD) == 0)
		return pledge_fail(p, EPERM, PLEDGE_SENDFD);

	switch (fp->f_type) {
	case DTYPE_SOCKET:
	case DTYPE_PIPE:
	case DTYPE_DMABUF:
		return (0);
	case DTYPE_VNODE:
		vp = fp->f_data;

		if (vp->v_type != VDIR)
			return (0);
		break;
	}
	return pledge_fail(p, EINVAL, PLEDGE_SENDFD);
}

int
pledge_sysctl(struct proc *p, int miblen, int *mib, void *new)
{
	char	buf[80];
	int	i;

	if ((p->p_p->ps_flags & PS_PLEDGE) == 0)
		return (0);

	if (new)
		return pledge_fail(p, EFAULT, 0);

	/* routing table observation */
	if ((p->p_p->ps_pledge & PLEDGE_ROUTE)) {
		if ((miblen == 6 || miblen == 7) &&
		    mib[0] == CTL_NET && mib[1] == PF_ROUTE &&
		    mib[2] == 0 &&
		    mib[4] == NET_RT_DUMP)
			return (0);

		if (miblen == 6 &&
		    mib[0] == CTL_NET && mib[1] == PF_ROUTE &&
		    mib[2] == 0 &&
		    (mib[3] == 0 || mib[3] == AF_INET6 || mib[3] == AF_INET) &&
		    mib[4] == NET_RT_TABLE)
			return (0);

		if (miblen == 7 &&		/* exposes MACs */
		    mib[0] == CTL_NET && mib[1] == PF_ROUTE &&
		    mib[2] == 0 &&
		    (mib[3] == 0 || mib[3] == AF_INET6 || mib[3] == AF_INET) &&
		    mib[4] == NET_RT_FLAGS && mib[5] == RTF_LLINFO)
			return (0);
	}

	if ((p->p_p->ps_pledge & PLEDGE_WROUTE)) {
		if (miblen == 4 &&
		    mib[0] == CTL_NET && mib[1] == PF_INET6 &&
		    mib[2] == IPPROTO_IPV6 && mib[3] == IPV6CTL_SOIIKEY)
			return (0);
	}

	if (p->p_p->ps_pledge & (PLEDGE_PS | PLEDGE_VMINFO)) {
		if (miblen == 2 &&		/* kern.fscale */
		    mib[0] == CTL_KERN && mib[1] == KERN_FSCALE)
			return (0);
		if (miblen == 2 &&		/* kern.boottime */
		    mib[0] == CTL_KERN && mib[1] == KERN_BOOTTIME)
			return (0);
		if (miblen == 2 &&		/* kern.consdev */
		    mib[0] == CTL_KERN && mib[1] == KERN_CONSDEV)
			return (0);
		if (miblen == 2 &&			/* kern.cptime */
		    mib[0] == CTL_KERN && mib[1] == KERN_CPTIME)
			return (0);
		if (miblen == 3 &&			/* kern.cptime2 */
		    mib[0] == CTL_KERN && mib[1] == KERN_CPTIME2)
			return (0);
		if (miblen == 3 &&			/* kern.cpustats */
		    mib[0] == CTL_KERN && mib[1] == KERN_CPUSTATS)
			return (0);
	}

	if ((p->p_p->ps_pledge & PLEDGE_PS)) {
		if (miblen == 4 &&		/* kern.procargs.* */
		    mib[0] == CTL_KERN && mib[1] == KERN_PROC_ARGS &&
		    (mib[3] == KERN_PROC_ARGV || mib[3] == KERN_PROC_ENV))
			return (0);
		if (miblen == 6 &&		/* kern.proc.* */
		    mib[0] == CTL_KERN && mib[1] == KERN_PROC)
			return (0);
		if (miblen == 3 &&		/* kern.proc_cwd.* */
		    mib[0] == CTL_KERN && mib[1] == KERN_PROC_CWD)
			return (0);
		if (miblen == 2 &&		/* hw.physmem */
		    mib[0] == CTL_HW && mib[1] == HW_PHYSMEM64)
			return (0);
		if (miblen == 2 &&		/* kern.ccpu */
		    mib[0] == CTL_KERN && mib[1] == KERN_CCPU)
			return (0);
		if (miblen == 2 &&		/* vm.maxslp */
		    mib[0] == CTL_VM && mib[1] == VM_MAXSLP)
			return (0);
	}

	if ((p->p_p->ps_pledge & PLEDGE_VMINFO)) {
		if (miblen == 2 &&		/* vm.uvmexp */
		    mib[0] == CTL_VM && mib[1] == VM_UVMEXP)
			return (0);
		if (miblen == 3 &&		/* vfs.generic.bcachestat */
		    mib[0] == CTL_VFS && mib[1] == VFS_GENERIC &&
		    mib[2] == VFS_BCACHESTAT)
			return (0);
	}

	if ((p->p_p->ps_pledge & (PLEDGE_ROUTE | PLEDGE_INET | PLEDGE_DNS))) {
		if (miblen == 6 &&		/* getifaddrs() */
		    mib[0] == CTL_NET && mib[1] == PF_ROUTE &&
		    mib[2] == 0 &&
		    (mib[3] == 0 || mib[3] == AF_INET6 || mib[3] == AF_INET) &&
		    mib[4] == NET_RT_IFLIST)
			return (0);
	}

	if ((p->p_p->ps_pledge & PLEDGE_DISKLABEL)) {
		if (miblen == 2 &&		/* kern.rawpartition */
		    mib[0] == CTL_KERN &&
		    mib[1] == KERN_RAWPARTITION)
			return (0);
		if (miblen == 2 &&		/* kern.maxpartitions */
		    mib[0] == CTL_KERN &&
		    mib[1] == KERN_MAXPARTITIONS)
			return (0);
#ifdef CPU_CHR2BLK
		if (miblen == 3 &&		/* machdep.chr2blk */
		    mib[0] == CTL_MACHDEP &&
		    mib[1] == CPU_CHR2BLK)
			return (0);
#endif /* CPU_CHR2BLK */
	}

	if (miblen >= 3 &&			/* ntpd(8) to read sensors */
	    mib[0] == CTL_HW && mib[1] == HW_SENSORS)
		return (0);

	if (miblen == 6 &&		/* if_nameindex() */
	    mib[0] == CTL_NET && mib[1] == PF_ROUTE &&
	    mib[2] == 0 && mib[3] == 0 && mib[4] == NET_RT_IFNAMES)
		return (0);

	if (miblen == 2) {
		switch (mib[0]) {
		case CTL_KERN:
			switch (mib[1]) {
			case KERN_DOMAINNAME:	/* getdomainname() */
			case KERN_HOSTNAME:	/* gethostname() */
			case KERN_OSTYPE:	/* uname() */
			case KERN_OSRELEASE:	/* uname() */
			case KERN_OSVERSION:	/* uname() */
			case KERN_VERSION:	/* uname() */
			case KERN_CLOCKRATE:	/* kern.clockrate */
			case KERN_ARGMAX:	/* kern.argmax */
			case KERN_NGROUPS:	/* kern.ngroups */
			case KERN_SYSVSHM:	/* kern.sysvshm */
			case KERN_POSIX1:	/* kern.posix1version */
				return (0);
			}
			break;
		case CTL_HW:
			switch (mib[1]) {
			case HW_MACHINE: 	/* uname() */
			case HW_PAGESIZE: 	/* getpagesize() */
			case HW_NCPU:		/* hw.ncpu */
			case HW_NCPUONLINE:	/* hw.ncpuonline */
				return (0);
			}
			break;
		case CTL_VM:
			switch (mib[1]) {
			case VM_PSSTRINGS:	/* setproctitle() */
			case VM_LOADAVG:	/* vm.loadavg / getloadavg(3) */
			case VM_MALLOC_CONF:	/* vm.malloc_conf */
				return (0);
			}
			break;
		default:
			break;
		}
	}

#ifdef CPU_SSE
	if (miblen == 2 &&		/* i386 libm tests for SSE */
	    mib[0] == CTL_MACHDEP && mib[1] == CPU_SSE)
		return (0);
#endif /* CPU_SSE */

	snprintf(buf, sizeof(buf), "%s(%d): pledge sysctl %d:",
	    p->p_p->ps_comm, p->p_p->ps_pid, miblen);
	for (i = 0; i < miblen; i++) {
		char *p = buf + strlen(buf);
		snprintf(p, sizeof(buf) - (p - buf), " %d", mib[i]);
	}
	log(LOG_ERR, "%s\n", buf);

	return pledge_fail(p, EINVAL, 0);
}

int
pledge_chown(struct proc *p, uid_t uid, gid_t gid)
{
	if ((p->p_p->ps_flags & PS_PLEDGE) == 0)
		return (0);

	if (p->p_p->ps_pledge & PLEDGE_CHOWNUID)
		return (0);

	if (uid != -1 && uid != p->p_ucred->cr_uid)
		return (EPERM);
	if (gid != -1 && !groupmember(gid, p->p_ucred))
		return (EPERM);
	return (0);
}

int
pledge_adjtime(struct proc *p, const void *v)
{
	const struct timeval *delta = v;

	if ((p->p_p->ps_flags & PS_PLEDGE) == 0)
		return (0);

	if ((p->p_p->ps_pledge & PLEDGE_SETTIME))
		return (0);
	if (delta)
		return (EPERM);
	return (0);
}

int
pledge_sendit(struct proc *p, const void *to)
{
	if ((p->p_p->ps_flags & PS_PLEDGE) == 0)
		return (0);

	if ((p->p_p->ps_pledge & (PLEDGE_INET | PLEDGE_UNIX | PLEDGE_DNS | PLEDGE_YPACTIVE)))
		return (0);		/* may use address */
	if (to == NULL)
		return (0);		/* behaves just like write */
	return pledge_fail(p, EPERM, PLEDGE_INET);
}

int
pledge_ioctl(struct proc *p, long com, struct file *fp)
{
	struct vnode *vp = NULL;
	int error = EPERM;

	if ((p->p_p->ps_flags & PS_PLEDGE) == 0)
		return (0);

	/*
	 * The ioctl's which are always allowed.
	 */
	switch (com) {
	case FIONREAD:
	case FIONBIO:
	case FIOCLEX:
	case FIONCLEX:
		return (0);
	}

	/* fp != NULL was already checked */
	if (fp->f_type == DTYPE_VNODE) {
		vp = fp->f_data;
		if (vp->v_type == VBAD)
			return (ENOTTY);
	}

	if ((p->p_p->ps_pledge & PLEDGE_INET)) {
		switch (com) {
		case SIOCATMARK:
		case SIOCGIFGROUP:
			if (fp->f_type == DTYPE_SOCKET)
				return (0);
			break;
		}
	}

#if NBPFILTER > 0
	if ((p->p_p->ps_pledge & PLEDGE_BPF)) {
		switch (com) {
		case BIOCGSTATS:	/* bpf: tcpdump privsep on ^C */
			if (fp->f_type == DTYPE_VNODE &&
			    fp->f_ops->fo_ioctl == vn_ioctl &&
			    vp->v_type == VCHR &&
			    cdevsw[major(vp->v_rdev)].d_open == bpfopen)
				return (0);
			break;
		}
	}
#endif /* NBPFILTER > 0 */

	if ((p->p_p->ps_pledge & PLEDGE_TAPE)) {
		switch (com) {
		case MTIOCGET:
		case MTIOCTOP:
			/* for pax(1) and such, checking tapes... */
			if (fp->f_type == DTYPE_VNODE &&
			    vp->v_type == VCHR) {
				if (vp->v_flag & VISTTY)
					return (ENOTTY);
				else
					return (0);
			}
			break;
		}
	}

#if NDRM > 0
	if ((p->p_p->ps_pledge & PLEDGE_DRM)) {
		if ((fp->f_type == DTYPE_VNODE) &&
		    (vp->v_type == VCHR) &&
		    (cdevsw[major(vp->v_rdev)].d_open == drmopen)) {
			error = pledge_ioctl_drm(p, com, vp->v_rdev);
			if (error == 0)
				return 0;
		}
	}
#endif /* NDRM > 0 */

#if NAUDIO > 0
	if ((p->p_p->ps_pledge & PLEDGE_AUDIO)) {
		switch (com) {
		case SOUND_IO_BYTES_CNT_GET:
		case SOUND_IO_PARAMETER_GET:
		case SOUND_IO_PARAMETER_SET:
		case SOUND_IO_START:
		case SOUND_IO_STOP:
			if (fp->f_type == DTYPE_VNODE &&
			    vp->v_type == VCHR &&
			    cdevsw[major(vp->v_rdev)].d_open == audioopen)
				return (0);
		}
	}
#endif /* NAUDIO > 0 */

	if ((p->p_p->ps_pledge & PLEDGE_DISKLABEL)) {
		switch (com) {
		case DIOCGDINFO:
		case DIOCGPDINFO:
		case DIOCRLDINFO:
		case DIOCWDINFO:
		case BIOCDISK:
		case BIOCINQ:
		case BIOCINSTALLBOOT:
		case BIOCVOL:
			if (fp->f_type == DTYPE_VNODE &&
			    ((vp->v_type == VCHR &&
			    cdevsw[major(vp->v_rdev)].d_type == D_DISK) ||
			    (vp->v_type == VBLK &&
			    bdevsw[major(vp->v_rdev)].d_type == D_DISK)))
				return (0);
			break;
		case DIOCMAP:
			if (fp->f_type == DTYPE_VNODE &&
			    vp->v_type == VCHR &&
			    cdevsw[major(vp->v_rdev)].d_ioctl == diskmapioctl)
				return (0);
			break;
		}
	}

#if NVIDEO > 0
	if ((p->p_p->ps_pledge & PLEDGE_VIDEO)) {
		switch (com) {
		case VIDIOC_QUERYCAP:
		case VIDIOC_TRY_FMT:
		case VIDIOC_ENUM_FMT:
		case VIDIOC_S_FMT:
		case VIDIOC_QUERYCTRL:
		case VIDIOC_G_CTRL:
		case VIDIOC_S_CTRL:
		case VIDIOC_G_PARM:
		case VIDIOC_S_PARM:
		case VIDIOC_REQBUFS:
		case VIDIOC_QBUF:
		case VIDIOC_DQBUF:
		case VIDIOC_QUERYBUF:
		case VIDIOC_STREAMON:
		case VIDIOC_STREAMOFF:
		case VIDIOC_ENUM_FRAMESIZES:
		case VIDIOC_ENUM_FRAMEINTERVALS:
		case VIDIOC_DQEVENT:
		case VIDIOC_ENCODER_CMD:
		case VIDIOC_EXPBUF:
		case VIDIOC_G_CROP:
		case VIDIOC_G_EXT_CTRLS:
		case VIDIOC_G_FMT:
		case VIDIOC_G_SELECTION:
		case VIDIOC_QUERYMENU:
		case VIDIOC_SUBSCRIBE_EVENT:
		case VIDIOC_S_EXT_CTRLS:
		case VIDIOC_S_SELECTION:
		case VIDIOC_TRY_DECODER_CMD:
		case VIDIOC_TRY_ENCODER_CMD:
			if (fp->f_type == DTYPE_VNODE &&
			    vp->v_type == VCHR &&
			    cdevsw[major(vp->v_rdev)].d_open == videoopen)
				return (0);
			break;
		}
	}
#endif

#if NPF > 0
	if ((p->p_p->ps_pledge & PLEDGE_PF)) {
		switch (com) {
		case DIOCADDRULE:
		case DIOCGETSTATUS:
		case DIOCNATLOOK:
		case DIOCRADDTABLES:
		case DIOCRCLRADDRS:
		case DIOCRCLRTABLES:
		case DIOCRCLRTSTATS:
		case DIOCRGETTSTATS:
		case DIOCRSETADDRS:
		case DIOCXBEGIN:
		case DIOCXCOMMIT:
		case DIOCKILLSRCNODES:
			if ((fp->f_type == DTYPE_VNODE) &&
			    (vp->v_type == VCHR) &&
			    (cdevsw[major(vp->v_rdev)].d_open == pfopen))
				return (0);
			break;
		}
	}
#endif

	if ((p->p_p->ps_pledge & PLEDGE_TTY)) {
		switch (com) {
#if NPTY > 0
		case PTMGET:
			if ((p->p_p->ps_pledge & PLEDGE_RPATH) == 0)
				break;
			if ((p->p_p->ps_pledge & PLEDGE_WPATH) == 0)
				break;
			if (fp->f_type != DTYPE_VNODE || vp->v_type != VCHR)
				break;
			if (cdevsw[major(vp->v_rdev)].d_open != ptmopen)
				break;
			return (0);
		case TIOCUCNTL:		/* vmd */
			if ((p->p_p->ps_pledge & PLEDGE_RPATH) == 0)
				break;
			if ((p->p_p->ps_pledge & PLEDGE_WPATH) == 0)
				break;
			if (cdevsw[major(vp->v_rdev)].d_open != ptcopen)
				break;
			return (0);
#endif /* NPTY > 0 */
		case TIOCSPGRP:
			if ((p->p_p->ps_pledge & PLEDGE_PROC) == 0)
				break;
			/* FALLTHROUGH */
		case TIOCFLUSH:		/* getty, telnet */
		case TIOCSTART:		/* emacs, etc */
		case TIOCGPGRP:
		case TIOCGETA:
		case TIOCGWINSZ:	/* ENOTTY return for non-tty */
		case TIOCSTAT:		/* csh */
			if (fp->f_type == DTYPE_VNODE && (vp->v_flag & VISTTY))
				return (0);
			return (ENOTTY);
		case TIOCSWINSZ:
		case TIOCEXT:		/* mail, libedit .. */
		case TIOCCBRK:		/* cu */
		case TIOCSBRK:		/* cu */
		case TIOCCDTR:		/* cu */
		case TIOCSDTR:		/* cu */
		case TIOCEXCL:		/* cu */
		case TIOCSETA:		/* cu, ... */
		case TIOCSETAW:		/* cu, ... */
		case TIOCSETAF:		/* tcsetattr TCSAFLUSH, script */
		case TIOCSCTTY:		/* forkpty(3), login_tty(3), ... */
			if (fp->f_type == DTYPE_VNODE && (vp->v_flag & VISTTY))
				return (0);
			break;
		}
	}

	if ((p->p_p->ps_pledge & PLEDGE_ROUTE)) {
		switch (com) {
		case SIOCGIFADDR:
		case SIOCGIFAFLAG_IN6:
		case SIOCGIFALIFETIME_IN6:
		case SIOCGIFDESCR:
		case SIOCGIFFLAGS:
		case SIOCGIFMETRIC:
		case SIOCGIFGMEMB:
		case SIOCGIFRDOMAIN:
		case SIOCGIFDSTADDR_IN6:
		case SIOCGIFNETMASK_IN6:
		case SIOCGIFXFLAGS:
		case SIOCGNBRINFO_IN6:
		case SIOCGIFINFO_IN6:
		case SIOCGIFMEDIA:
			if (fp->f_type == DTYPE_SOCKET)
				return (0);
			break;
		}
	}

	if ((p->p_p->ps_pledge & PLEDGE_WROUTE)) {
		switch (com) {
		case SIOCAIFADDR_IN6:
		case SIOCDIFADDR_IN6:
			if (fp->f_type == DTYPE_SOCKET)
				return (0);
			break;
		case SIOCSIFMTU:
			if (fp->f_type == DTYPE_SOCKET)
				return (0);
			break;
		}
	}

#if NVMM > 0
	if ((p->p_p->ps_pledge & PLEDGE_VMM)) {
		if ((fp->f_type == DTYPE_VNODE) &&
		    (vp->v_type == VCHR) &&
		    (cdevsw[major(vp->v_rdev)].d_open == vmmopen)) {
			error = pledge_ioctl_vmm(p, com);
			if (error == 0)
				return 0;
		}
	}
#endif

	return pledge_fail(p, error, PLEDGE_TTY);
}

int
pledge_sockopt(struct proc *p, int set, int level, int optname)
{
	if ((p->p_p->ps_flags & PS_PLEDGE) == 0)
		return (0);

	/* Always allow these, which are too common to reject */
	switch (level) {
	case SOL_SOCKET:
		switch (optname) {
		case SO_RCVBUF:
		case SO_ERROR:
			return 0;
		}
		break;
	}

	if ((p->p_p->ps_pledge & (PLEDGE_INET|PLEDGE_UNIX|PLEDGE_DNS|PLEDGE_YPACTIVE)) == 0)
		return pledge_fail(p, EPERM, PLEDGE_INET);
	/* In use by some service libraries */
	switch (level) {
	case SOL_SOCKET:
		switch (optname) {
		case SO_TIMESTAMP:
			return 0;
		}
		break;
	}

	/* DNS resolver may do these requests */
	if ((p->p_p->ps_pledge & PLEDGE_DNS)) {
		switch (level) {
		case IPPROTO_IPV6:
			switch (optname) {
			case IPV6_RECVPKTINFO:
			case IPV6_USE_MIN_MTU:
				return (0);
			}
		}
	}

	/* YP may do these requests */
	if (p->p_p->ps_pledge & PLEDGE_YPACTIVE) {
		switch (level) {
		case IPPROTO_IP:
			switch (optname) {
			case IP_PORTRANGE:
				return (0);
			}
			break;

		case IPPROTO_IPV6:
			switch (optname) {
			case IPV6_PORTRANGE:
				return (0);
			}
			break;
		}
	}

	if ((p->p_p->ps_pledge & (PLEDGE_INET|PLEDGE_UNIX)) == 0)
		return pledge_fail(p, EPERM, PLEDGE_INET);
	switch (level) {
	case SOL_SOCKET:
		switch (optname) {
		case SO_RTABLE:
			return pledge_fail(p, EINVAL, PLEDGE_INET);
		}
		return (0);
	}

	if ((p->p_p->ps_pledge & PLEDGE_INET) == 0)
		return pledge_fail(p, EPERM, PLEDGE_INET);
	switch (level) {
	case IPPROTO_TCP:
		switch (optname) {
		case TCP_NODELAY:
		case TCP_MD5SIG:
		case TCP_SACK_ENABLE:
		case TCP_MAXSEG:
		case TCP_NOPUSH:
			return (0);
		}
		break;
	case IPPROTO_IP:
		switch (optname) {
		case IP_OPTIONS:
			if (!set)
				return (0);
			break;
		case IP_TOS:
		case IP_TTL:
		case IP_MINTTL:
		case IP_IPDEFTTL:
		case IP_PORTRANGE:
		case IP_RECVDSTADDR:
		case IP_RECVDSTPORT:
			return (0);
		case IP_MULTICAST_IF:
		case IP_MULTICAST_TTL:
		case IP_MULTICAST_LOOP:
		case IP_ADD_MEMBERSHIP:
		case IP_DROP_MEMBERSHIP:
			if (p->p_p->ps_pledge & PLEDGE_MCAST)
				return (0);
			break;
		}
		break;
	case IPPROTO_ICMP:
		break;
	case IPPROTO_IPV6:
		switch (optname) {
		case IPV6_TCLASS:
		case IPV6_UNICAST_HOPS:
		case IPV6_MINHOPCOUNT:
		case IPV6_RECVHOPLIMIT:
		case IPV6_PORTRANGE:
		case IPV6_RECVPKTINFO:
		case IPV6_RECVDSTPORT:
		case IPV6_V6ONLY:
			return (0);
		case IPV6_MULTICAST_IF:
		case IPV6_MULTICAST_HOPS:
		case IPV6_MULTICAST_LOOP:
		case IPV6_JOIN_GROUP:
		case IPV6_LEAVE_GROUP:
			if (p->p_p->ps_pledge & PLEDGE_MCAST)
				return (0);
			break;
		}
		break;
	case IPPROTO_ICMPV6:
		break;
	}
	return pledge_fail(p, EPERM, PLEDGE_INET);
}

int
pledge_socket(struct proc *p, int domain, unsigned int state)
{
	if (! ISSET(p->p_p->ps_flags, PS_PLEDGE))
		return 0;

	if (ISSET(state, SS_DNS)) {
		if (ISSET(p->p_p->ps_pledge, PLEDGE_DNS))
			return 0;
		return pledge_fail(p, EPERM, PLEDGE_DNS);
	}

	switch (domain) {
	case -1:		/* accept on any domain */
		return (0);
	case AF_INET:
	case AF_INET6:
		if (ISSET(p->p_p->ps_pledge, PLEDGE_INET) ||
		    ISSET(p->p_p->ps_pledge, PLEDGE_YPACTIVE))
			return 0;
		return pledge_fail(p, EPERM, PLEDGE_INET);

	case AF_UNIX:
		if (ISSET(p->p_p->ps_pledge, PLEDGE_UNIX))
			return 0;
		return pledge_fail(p, EPERM, PLEDGE_UNIX);
	}

	return pledge_fail(p, EINVAL, PLEDGE_INET);
}

int
pledge_flock(struct proc *p)
{
	if ((p->p_p->ps_flags & PS_PLEDGE) == 0)
		return (0);

	if ((p->p_p->ps_pledge & PLEDGE_FLOCK))
		return (0);
	return (pledge_fail(p, EPERM, PLEDGE_FLOCK));
}

int
pledge_swapctl(struct proc *p)
{
	if ((p->p_p->ps_flags & PS_PLEDGE) == 0)
		return (0);
	return (EPERM);
}

/* bsearch over pledgereq. return flags value if found, 0 else */
uint64_t
pledgereq_flags(const char *req_name)
{
	int base = 0, cmp, i, lim;

	for (lim = nitems(pledgereq); lim != 0; lim >>= 1) {
		i = base + (lim >> 1);
		cmp = strcmp(req_name, pledgereq[i].name);
		if (cmp == 0)
			return (pledgereq[i].flags);
		if (cmp > 0) { /* not found before, move right */
			base = i + 1;
			lim--;
		} /* else move left */
	}
	return (0);
}

int
pledge_fcntl(struct proc *p, int cmd)
{
	if ((p->p_p->ps_flags & PS_PLEDGE) == 0)
		return (0);
	if ((p->p_p->ps_pledge & PLEDGE_PROC) == 0 && cmd == F_SETOWN)
		return pledge_fail(p, EPERM, PLEDGE_PROC);
	return (0);
}

int
pledge_kill(struct proc *p, pid_t pid)
{
	if ((p->p_p->ps_flags & PS_PLEDGE) == 0)
		return 0;
	if (p->p_p->ps_pledge & PLEDGE_PROC)
		return 0;
	if (pid == 0 || pid == p->p_p->ps_pid)
		return 0;
	return pledge_fail(p, EPERM, PLEDGE_PROC);
}

int
pledge_protexec(struct proc *p, int prot)
{
	if ((p->p_p->ps_flags & PS_PLEDGE) == 0)
		return 0;
	/* Before kbind(2) call, ld.so and crt may create EXEC mappings */
	if (p->p_p->ps_kbind_addr == 0 && p->p_p->ps_kbind_cookie == 0)
		return 0;
	if (!(p->p_p->ps_pledge & PLEDGE_PROTEXEC) && (prot & PROT_EXEC))
		return pledge_fail(p, EPERM, PLEDGE_PROTEXEC);
	return 0;
}

int
canonpath(const char *input, char *buf, size_t bufsize)
{
	const char *p;
	char *q;

	/* can't canon relative paths, don't bother */
	if (input[0] != '/') {
		if (strlcpy(buf, input, bufsize) >= bufsize)
			return ENAMETOOLONG;
		return 0;
	}

	p = input;
	q = buf;
	while (*p && (q - buf < bufsize)) {
		if (p[0] == '/' && (p[1] == '/' || p[1] == '\0')) {
			p += 1;

		} else if (p[0] == '/' && p[1] == '.' &&
		    (p[2] == '/' || p[2] == '\0')) {
			p += 2;

		} else if (p[0] == '/' && p[1] == '.' && p[2] == '.' &&
		    (p[3] == '/' || p[3] == '\0')) {
			p += 3;
			if (q != buf)	/* "/../" at start of buf */
				while (*--q != '/')
					continue;

		} else {
			*q++ = *p++;
		}
	}
	if ((*p == '\0') && (q - buf < bufsize)) {
		*q = 0;
		return 0;
	} else
		return ENAMETOOLONG;
}
