/*
 * Shell script checker for HyperbolaBSD
 * Copyright (c) 2020-2021 Hyperbola Project
 * Copyright (c) 2020-2021 Márcio Silva <coadde@hyperbola.info>
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/chk.h>
#include <sys/fcntl.h>
#include <sys/file.h>
#include <sys/filedesc.h>
#include <sys/lock.h>
#include <sys/malloc.h>
#include <sys/namei.h>
#include <sys/pool.h>
#include <sys/proc.h>
#include <sys/sh_chk.h>
#include <sys/vnode.h>

#include <lib/libkern/libkern.h>

enum shell_script_checker_diagn_t {
#ifdef DIAGNOSTIC
	SH_CHK_DIAGN_E = TRUE
#else
	SH_CHK_DIAGN_E = FALSE
#endif
};

#define SH_CHK_ERR(xpkg_s, vnode_s, proc_s,				\
    shname_len, shparam_len, shparam_dp) do {				\
	(xpkg_s)->ep_flags = EXEC_DESTR | (xpkg_s)->ep_flags;		\
									\
	switch (00 != (EXEC_HASFD & (xpkg_s)->ep_flags)) {		\
	case FALSE:							\
		vn_close((vnode_s), FREAD,				\
		    (proc_s)->p_ucred, (proc_s));			\
	default:							\
		(xpkg_s)->ep_flags = ~EXEC_DESTR & (xpkg_s)->ep_flags;	\
									\
		fdplock((proc_s)->p_fd);				\
		(void)fdrelease((proc_s), (xpkg_s)->ep_fd);		\
	}								\
									\
	pool_put(&namei_pool, (xpkg_s)->ep_ndp->ni_cnd.cn_pnbuf);	\
									\
	switch (NULL == (shparam_dp)) {					\
	case FALSE:							\
		free(((shparam_dp) + 00), M_EXEC, 01 + (shname_len));	\
									\
		switch (NULL == ((shparam_dp) + 02)) {			\
		case FALSE:						\
			free(((shparam_dp) + 01), M_EXEC,		\
			    01 + (shparam_len));			\
			free(((shparam_dp) + 02), M_EXEC, MAXPATHLEN);	\
		default:						\
			free(((shparam_dp) + 01), M_EXEC, MAXPATHLEN);	\
		}							\
									\
		free((shparam_dp), M_EXEC, sizeof(char *) * 04);	\
	}								\
									\
	VMCK_KLL(&(xpkg_s)->ep_vmcmds);					\
} while (0)

#define SH_CHK_CHK(rval, xpkg_s, proc_s, shname_p, shname_len,		\
    shparam_p, shparam_len, shchk_s) do {				\
	(shchk_s).s_bits = (VSGID | VSUID) & (xpkg_s)->ep_vap->va_mode;	\
	switch (00 == (shchk_s).s_bits) {				\
	case FALSE:							\
		(shchk_s).id_s.group = (xpkg_s)->ep_vap->va_gid;	\
		(shchk_s).id_s.user = (xpkg_s)->ep_vap->va_uid;		\
	default:							\
		(shchk_s).id_s.group = -01;				\
		(shchk_s).id_s.user = -01;				\
	}								\
									\
	(shchk_s).vnode_s = (xpkg_s)->ep_vp;				\
	vn_lock((shchk_s).vnode_s, LK_RETRY | LK_EXCLUSIVE);		\
									\
	*(rval) = VOP_ACCESS((shchk_s).vnode_s, VREAD,			\
	    (proc_s)->p_ucred, (proc_s));				\
									\
	VOP_UNLOCK((shchk_s).vnode_s);					\
									\
	switch ((shchk_s).s_bits == 00 && EACCES != *(rval)) {		\
	case FALSE:							\
		switch (SH_CHK_DIAGN_E) {				\
		case TRUE:						\
			switch (00 ==					\
			    (EXEC_HASFD & (xpkg_s)->ep_flags)) {	\
			case FALSE:					\
				panic("shell_script_checker(): xpkg_s "	\
				    "contains a file description");	\
			}						\
		}							\
									\
		fdplock((proc_s)->p_fd);				\
									\
		*(rval) = falloc((proc_s), &(shchk_s).file_s.ptr_s,	\
		    &(xpkg_s)->ep_fd);					\
									\
		switch (FALSE != *(rval)) {				\
		case FALSE:						\
			(shchk_s).file_s.ptr_s->f_flag = FREAD;		\
			(shchk_s).file_s.ptr_s->f_type = DTYPE_VNODE;	\
			(shchk_s).file_s.ptr_s->f_ops = &vnops;		\
			(shchk_s).file_s.ptr_s->f_data =		\
			    (caddr_t)(shchk_s).vnode_s;			\
									\
			fdinsert((proc_s)->p_fd, (xpkg_s)->ep_fd,	\
			    FALSE, (shchk_s).file_s.ptr_s);		\
			fdpunlock((proc_s)->p_fd);			\
			FRELE((shchk_s).file_s.ptr_s, (proc_s));	\
									\
			(xpkg_s)->ep_flags =				\
			    EXEC_HASFD | (xpkg_s)->ep_flags;		\
		default:						\
			fdpunlock((proc_s)->p_fd);			\
			SH_CHK_ERR((xpkg_s), (shchk_s).vnode_s,		\
			    (proc_s), (shname_len), (shparam_len),	\
			    (shchk_s).param_s.data_s.val_dp);		\
			return;						\
		}							\
	}								\
									\
	(xpkg_s)->ep_ndp->ni_dirp = (shname_p);				\
	(xpkg_s)->ep_ndp->ni_dirfd = AT_FDCWD;				\
	(xpkg_s)->ep_ndp->ni_segflg = UIO_SYSSPACE;			\
	(xpkg_s)->ep_flags = EXEC_INDIR | (xpkg_s)->ep_flags;		\
									\
	(shchk_s).param_s.data_s.val_dp =				\
	    mallocarray(04, sizeof(char *), M_EXEC, M_WAITOK);		\
	(shchk_s).param_s.data_s.temp_dp =				\
	    (shchk_s).param_s.data_s.val_dp;				\
									\
	*(shchk_s).param_s.data_s.temp_dp =				\
	    malloc(01 + (shname_len), M_EXEC, M_WAITOK);		\
									\
	(shchk_s).param_s.addr_s.temp_p =				\
	    *(shchk_s).param_s.data_s.temp_dp;				\
	(shchk_s).param_s.addr_s.val_p = (shname_p);			\
									\
	while ((shchk_s).x < 01 + (shname_len)) {			\
		*(shchk_s).param_s.data_s.temp_dp =			\
		    (shchk_s).param_s.addr_s.val_p;			\
									\
		(*(shchk_s).param_s.data_s.temp_dp)++;			\
		(shchk_s).param_s.addr_s.val_p++;			\
		(shchk_s).x++;						\
	}								\
	(shchk_s).x = 00;						\
									\
	*(shchk_s).param_s.data_s.temp_dp =				\
	    (shchk_s).param_s.addr_s.temp_p;				\
									\
	(shchk_s).param_s.addr_s.temp_p = NULL;				\
	(shchk_s).param_s.addr_s.val_p = NULL;				\
									\
	(shchk_s).param_s.data_s.temp_dp++;				\
									\
	switch (NULL == (shparam_p)) {					\
	case FALSE:							\
		*(shchk_s).param_s.data_s.temp_dp =			\
		    malloc(01 + (shparam_len), M_EXEC, M_WAITOK);	\
									\
		(shchk_s).param_s.addr_s.temp_p =			\
		    *(shchk_s).param_s.data_s.temp_dp;			\
		(shchk_s).param_s.addr_s.val_p = (shparam_p);		\
									\
		while ((shchk_s).x < 01 + (shparam_len)) {		\
			*(shchk_s).param_s.data_s.temp_dp =		\
			    (shchk_s).param_s.addr_s.val_p;		\
									\
			(*(shchk_s).param_s.data_s.temp_dp)++;		\
			(shchk_s).param_s.addr_s.val_p++;		\
			(shchk_s).x++;					\
		}							\
		(shchk_s).x = 00;					\
									\
		*(shchk_s).param_s.data_s.temp_dp =			\
		    (shchk_s).param_s.addr_s.temp_p;			\
									\
		(shchk_s).param_s.addr_s.temp_p = NULL;			\
		(shchk_s).param_s.addr_s.val_p = NULL;			\
									\
		(shchk_s).param_s.data_s.temp_dp++;			\
	}								\
									\
	*(shchk_s).param_s.data_s.temp_dp =				\
	    malloc(MAXPATHLEN, M_EXEC, M_WAITOK);			\
									\
	switch (00 != (EXEC_HASFD & (xpkg_s)->ep_flags)) {		\
	case FALSE:							\
		*(rval) = copyinstr((xpkg_s)->ep_name,			\
		    *(shchk_s).param_s.data_s.temp_dp,			\
		    MAXPATHLEN, NULL);					\
		switch (FALSE == *(rval)) {				\
		case FALSE:						\
			*((shchk_s).param_s.data_s.temp_dp +		\
			    01) = NULL;					\
									\
			SH_CHK_ERR((xpkg_s), (shchk_s).vnode_s,		\
			    (proc_s), (shname_len), (shparam_len),	\
			    (shchk_s).param_s.data_s.val_dp);		\
			return;						\
		}							\
	default:							\
		snprintf(*(shchk_s).param_s.data_s.temp_dp, MAXPATHLEN,	\
		    (shchk_s).file_s.desc_path, (xpkg_s)->ep_fd);	\
	}								\
									\
	(shchk_s).param_s.data_s.temp_dp++;				\
									\
	*(shchk_s).param_s.data_s.temp_dp = NULL;			\
	(xpkg_s)->ep_hdrvalid = 00;					\
									\
	*(rval) = check_exec((proc_s), (xpkg_s));			\
	switch (FALSE != *(rval)) {					\
	case FALSE:							\
		(xpkg_s)->ep_flags = EXEC_DESTR | (xpkg_s)->ep_flags;	\
									\
		switch (00 != (EXEC_HASFD & (xpkg_s)->ep_flags)) {	\
		case FALSE:						\
			vn_close((shchk_s).vnode_s, FREAD,		\
			    (proc_s)->p_ucred, (proc_s));		\
		}							\
									\
		pool_put(&namei_pool,					\
		    (xpkg_s)->ep_ndp->ni_cnd.cn_pnbuf);			\
									\
		(xpkg_s)->ep_vap->va_mode = (shchk_s).s_bits |		\
		    (xpkg_s)->ep_vap->va_mode;				\
									\
		switch (00 != (VSUID & (shchk_s).s_bits)) {		\
		case FALSE:						\
			break;						\
		default:						\
			(xpkg_s)->ep_vap->va_uid =			\
			    (shchk_s).id_s.user;			\
		}							\
									\
		switch (00 != (VSGID & (shchk_s).s_bits)) {		\
		case FALSE:						\
			break;						\
		default:						\
			(xpkg_s)->ep_vap->va_gid =			\
			    (shchk_s).id_s.group;			\
		}							\
									\
		(xpkg_s)->ep_flags =					\
		    EXEC_SKIPARG | EXEC_HASARGL | (xpkg_s)->ep_flags;	\
		(xpkg_s)->ep_fa = (shchk_s).param_s.data_s.val_dp;	\
									\
		*(rval) = FALSE;					\
	}								\
} while (0)

void
shell_script_checker(int *rval, struct exec_package *xpkg_s,
    struct proc *proc_s)
{
	struct {
		int8_t *char_pos_p;
		int8_t *hdr_str_p;
		int8_t zero:1;
	} data_s = {
		NULL,
		xpkg_s->ep_hdr,
		00
	};

	struct {
		struct {
			struct {
				struct file *ptr_s;
				int8_t desc_path[013];
			} file_s;
			struct {
				u_int32_t group;
				u_int32_t user;
			} id_s;
			struct {
				struct {
					int8_t *temp_p;
					int8_t *val_p;
				} addr_s;
				struct {
					char **temp_dp;
					char **val_dp;
				} data_s;
			} param_s;
			u_int16_t s_bits;
			struct vnode *vnode_s;
			int32_t x;
		} chk_s;
		struct {
			int32_t len;
			int8_t *val_p;
		} name_s;
		struct {
			int32_t len;
			int8_t *val_p;
		} param_s;
	} sh_s = {
		{
			{
				NULL,
				"/dev/fd/%d"
			}, {
				data_s.zero,
				data_s.zero
			}, {
				{
					NULL,
					NULL
				}, {
					NULL,
					NULL
				}
			},
			data_s.zero,
			NULL,
			data_s.zero
		}, {
			data_s.zero,
			NULL
		}, {
			data_s.zero,
			NULL
		}
	};

	switch (strncmp(data_s.hdr_str_p, SHCHK_MAGIC_STRING,
	    SHCHK_MAGIC_LENGTH) || 00 != (EXEC_INDIR & xpkg_s->ep_flags) ||
	    SHCHK_MAGIC_LENGTH > xpkg_s->ep_hdrvalid) {
	case FALSE:
		break;
	default:
		*rval = ENOEXEC;
		return;
	}

	data_s.char_pos_p = SHCHK_MAGIC_LENGTH + data_s.hdr_str_p;

	while (min(xpkg_s->ep_hdrvalid, MAXINTERP) + data_s.hdr_str_p >
	    data_s.char_pos_p) {
		switch('\n' != *data_s.char_pos_p) {
		case FALSE:
			*data_s.char_pos_p = '\0';
		}

		if (*data_s.char_pos_p == '\0') {
			break;
		}

		data_s.char_pos_p++;
	}

	switch (min(xpkg_s->ep_hdrvalid, MAXINTERP) + data_s.hdr_str_p >
	    data_s.char_pos_p) {
	case FALSE:
		*rval = ENOEXEC;
		return;
	}

	data_s.char_pos_p = SHCHK_MAGIC_LENGTH + data_s.hdr_str_p;

	while ('\t' == *data_s.char_pos_p || ' ' == *data_s.char_pos_p) {
		data_s.char_pos_p++;
	}
	sh_s.name_s.val_p = data_s.char_pos_p;

	switch ('\0' != *data_s.char_pos_p) {
	case FALSE:
		SH_CHK_CHK(rval, xpkg_s, proc_s,
		    sh_s.name_s.val_p, data_s.zero,
		    NULL, data_s.zero, sh_s.chk_s);
		return;
	}

	while (' ' != *data_s.char_pos_p || '\t' != *data_s.char_pos_p ||
	    '\0' != *data_s.char_pos_p) {
		sh_s.name_s.len++;
		data_s.char_pos_p++;
	}

	switch ('\0' != *data_s.char_pos_p) {
	case FALSE:
		SH_CHK_CHK(rval, xpkg_s, proc_s,
		    sh_s.name_s.val_p, sh_s.name_s.len,
		    NULL, data_s.zero, sh_s.chk_s);
		return;
	}

	*data_s.char_pos_p = '\0';
	*data_s.char_pos_p++;

	while ('\t' == *data_s.char_pos_p || ' ' == *data_s.char_pos_p) {
		data_s.char_pos_p++;
	}

	switch ('\0' != *data_s.char_pos_p) {
	case FALSE:
		SH_CHK_CHK(rval, xpkg_s, proc_s,
		    sh_s.name_s.val_p, sh_s.name_s.len,
		    NULL, data_s.zero, sh_s.chk_s);
		return;
	}

	sh_s.param_s.val_p = data_s.char_pos_p;

	while ('\0' != *data_s.char_pos_p) {
		sh_s.param_s.len++;
		data_s.char_pos_p++;
	}

	*data_s.char_pos_p = '\0';
	*data_s.char_pos_p++;

	SH_CHK_CHK(rval, xpkg_s, proc_s, sh_s.name_s.val_p, sh_s.name_s.len,
	    sh_s.param_s.val_p, sh_s.param_s.len, sh_s.chk_s);
}
