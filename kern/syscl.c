/*
 * System call for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

#ifndef PTRACE
#define SYSCL_NAME_PTRACE "unsupported ptrace (#26)"
#else
#define SYSCL_NAME_PTRACE "ptrace"
#endif	/* !PTRACE */

#ifndef KTRACE
#define SYSCL_NAME_KTRACE "unsupported ktrace (#45)"
#else
#define SYSCL_NAME_KTRACE "ktrace"
#endif	/* !KTRACE */

#ifndef ACCOUNTING
#define SYSCL_NAME_ACCT "unsupported acct (#51)"
#else
#define SYSCL_NAME_ACCT "acct"
#endif	/* !ACCOUNTING */

#if !defined(NFSSERVER) && !defined(NFSCLIENT)
#define SYSCL_NAME_NFSSVC "unsupported nfssvc (#155)"
#else
#define SYSCL_NAME_NFSSVC "nfssvc"
#endif	/* !NFSSERVER && !NFSCLIENT */

#ifndef SYSVSEM
#define SYSCL_NAME_SEMCTL "unsupported semctl (#220)"
#define SYSCL_NAME_SEMGET "unsupported semget (#221)"
#define SYSCL_NAME_SEMOP "unsupported semop (#290)"
#define SYSCL_NAME___SEMCTL "unsupported __semctl (#295)"
#else
#define SYSCL_NAME_SEMCTL "unsupported semctl (#220)"
#define SYSCL_NAME_SEMGET "semget"
#define SYSCL_NAME_SEMOP "semop"
#define SYSCL_NAME___SEMCTL "__semctl"
#endif	/* !SYSVSEM */

#ifndef SYSVMSG
#define SYSCL_NAME_MSGCTL "unsupported msgctl (#224)"
#define SYSCL_NAME_MSGGET "unsupported msgget (#225)"
#define SYSCL_NAME_MSGSND "unsupported msgsnd (#226)"
#define SYSCL_NAME_MSGRCV "unsupported msgrcv (#227)"
#define SYSCL_NAME_MSGCTL2 "unsupported msgctl (#297)"
#else
#define SYSCL_NAME_MSGCTL "unsupported msgctl (#224)"
#define SYSCL_NAME_MSGGET "msgget"
#define SYSCL_NAME_MSGSND "msgsnd"
#define SYSCL_NAME_MSGRCV "msgrcv"
#define SYSCL_NAME_MSGCTL2 "msgctl"
#endif	/* !SYSVMSG */

#ifndef SYSVSHM
#define SYSCL_NAME_SHMAT "unsupported shmat (#228)"
#define SYSCL_NAME_SHMCTL "unsupported shmctl (#229)"
#define SYSCL_NAME_SHMDT "unsupported shmdt (#230)"
#define SYSCL_NAME_SHMGET "unsupported shmget (#289)"
#define SYSCL_NAME_SHMCTL2 "unsupported shmctl (#296)"
#else
#define SYSCL_NAME_SHMAT "shmat"
#define SYSCL_NAME_SHMCTL "unsupported shmctl (#229)"
#define SYSCL_NAME_SHMDT "shmdt"
#define SYSCL_NAME_SHMGET "shmget"
#define SYSCL_NAME_SHMCTL2 "shmctl"
#endif	/* !SYSVSHM */

#define SYSCL_NAMES_DEF {						\
	"syscall", "exit", "fork", "read", "write", "open", "close",	\
	"getentropy", "__tfork", "link", "unlink", "wait4", "chdir",	\
	"fchdir", "mknod", "chmod", "chown", "break", "getdtablecount",	\
	"getrusage", "getpid", "mount", "unmount", "setuid", "getuid",	\
	"geteuid", SYSCL_NAME_PTRACE, "recvmsg", "sendmsg", "recvfrom",	\
	"accept", "getpeername", "getsockname", "access", "chflags",	\
	"fchflags", "sync", "obsolete o58_kill (#37)", "stat",		\
	"getppid", "lstat", "dup", "fstatat", "getegid", "profil",	\
	SYSCL_NAME_KTRACE, "sigaction", "getgid", "sigprocmask",	\
	"obsolete ogetlogin (#49)", "setlogin", SYSCL_NAME_ACCT,	\
	"sigpending", "fstat", "ioctl", "reboot", "revoke", "symlink",	\
	"readlink", "execve", "umask", "chroot", "getfsstat", "statfs",	\
	"fstatfs", "fhstatfs", "vfork", "gettimeofday", "settimeofday",	\
	"setitimer", "getitimer", "select", "kevent", "munmap",		\
	"mprotect", "madvise", "utimes", "futimes",			\
	"unsupported mincore (#78)", "getgroups", "setgroups",		\
	"getpgrp", "setpgid", "futex", "utimensat", "futimens",		\
	"kbind", "clock_gettime", "clock_settime", "clock_getres",	\
	"dup2", "nanosleep", "fcntl", "accept4", "__thrsleep", "fsync",	\
	"setpriority", "socket", "connect", "getdents", "getpriority",	\
	"pipe2", "dup3", "sigreturn", "bind", "setsockopt", "listen",	\
	"chflagsat", "pledge", "ppoll", "pselect", "sigsuspend",	\
	"sendsyslog", "unsupported fktrace (#113)", "unveil",		\
	"__realpath", "obsolete t32_gettimeofday (#116)",		\
	"obsolete t32_getrusage (#117)", "getsockopt", "thrkill",	\
	"readv", "writev", "kill", "fchown", "fchmod",			\
	"obsolete orecvfrom (#125)", "setreuid", "setregid", "rename",	\
	"obsolete otruncate (#129)", "obsolete oftruncate (#130)",	\
	"flock", "mkfifo", "sendto", "shutdown", "socketpair", "mkdir",	\
	"rmdir", "obsolete t32_utimes (#138)",				\
	"obsolete 4.2 sigreturn (#139)", "adjtime", "getlogin_r",	\
	"obsolete ogethostid (#142)", "obsolete osethostid (#143)",	\
	"obsolete ogetrlimit (#144)", "obsolete osetrlimit (#145)",	\
	"obsolete okillpg (#146)", "setsid", "quotactl",		\
	"obsolete oquota (#149)", "obsolete ogetsockname (#150)",	\
	"unsupported (#151)", "unsupported (#152)",			\
	"unsupported (#153)", "unsupported (#154)", SYSCL_NAME_NFSSVC,	\
	"obsolete ogetdirentries (#156)", "obsolete statfs25 (#157)",	\
	"obsolete fstatfs25 (#158)", "unsupported (#159)",		\
	"unsupported (#160)", "getfh",					\
	"obsolete ogetdomainname (#162)",				\
	"obsolete osetdomainname (#163)", "unsupported ouname (#164)",	\
	"sysarch", "unsupported (#166)", "unsupported (#167)",		\
	"unsupported (#168)", "obsolete semsys10 (#169)",		\
	"obsolete msgsys10 (#170)", "obsolete shmsys10 (#171)",		\
	"unsupported (#172)", "pread", "pwrite",			\
	"unsupported ntp_gettime (#175)",				\
	"unsupported ntp_adjtime (#176)", "unsupported (#177)",		\
	"unsupported (#178)", "unsupported (#179)",			\
	"unsupported (#180)", "setgid", "setegid", "seteuid",		\
	"obsolete lfs_bmapv (#184)", "obsolete lfs_markv (#185)",	\
	"obsolete lfs_segclean (#186)", "obsolete lfs_segwait (#187)",	\
	"obsolete stat35 (#188)", "obsolete fstat35 (#189)",		\
	"obsolete lstat35 (#190)", "pathconf", "fpathconf", "swapctl",	\
	"getrlimit", "setrlimit", "obsolete ogetdirentries48 (#196)",	\
	"mmap", "__syscall", "lseek", "truncate", "ftruncate",		\
	"sysctl", "mlock", "munlock",					\
	"unsupported sys_undelete (#205)",				\
	"obsolete t32_futimes (#206)", "getpgid",			\
	"obsolete nnpfspioctl (#208)", "utrace", "unsupported (#210)",	\
	"unsupported (#211)", "unsupported (#212)",			\
	"unsupported (#213)", "unsupported (#214)",			\
	"unsupported (#215)", "unsupported (#216)",			\
	"unsupported (#217)", "unsupported (#218)",			\
	"unsupported (#219)", SYSCL_NAME_SEMCTL, SYSCL_NAME_SEMGET,	\
	"obsolete semop35 (#222)", "obsolete semconfig35 (#223)",	\
	SYSCL_NAME_MSGCTL, SYSCL_NAME_MSGGET, SYSCL_NAME_MSGSND,	\
	SYSCL_NAME_MSGRCV, SYSCL_NAME_SHMAT, SYSCL_NAME_SHMCTL,		\
	SYSCL_NAME_SHMDT, "obsolete shmget35 (#231)",			\
	"obsolete t32_clock_gettime (#232)",				\
	"obsolete t32_clock_settime (#233)",				\
	"obsolete t32_clock_getres (#234)",				\
	"unsupported timer_create (#235)",				\
	"unsupported timer_delete (#236)",				\
	"unsupported timer_settime (#237)",				\
	"unsupported timer_gettime (#238)",				\
	"unsupported timer_getoverrun (#239)",				\
	"obsolete t32_nanosleep (#240)", "unsupported (#241)",		\
	"unsupported (#242)", "unsupported (#243)",			\
	"unsupported (#244)", "unsupported (#245)",			\
	"unsupported (#246)", "unsupported (#247)",			\
	"unsupported (#248)", "unsupported (#249)", "minherit",		\
	"obsolete rfork (#251)", "poll", "issetugid", "lchown",		\
	"getsid", "msync", "obsolete semctl35 (#257)",			\
	"obsolete shmctl35 (#258)", "obsolete msgctl35 (#259)",		\
	"unsupported (#260)", "unsupported (#261)",			\
	"unsupported (#262)", "pipe", "fhopen", "unsupported (#265)",	\
	"unsupported (#266)", "preadv", "pwritev", "kqueue",		\
	"obsolete t32_kevent (#270)", "mlockall", "munlockall",		\
	"unsupported sys_getpeereid (#273)",				\
	"unsupported sys_extattrctl (#274)",				\
	"unsupported sys_extattr_set_file (#275)",			\
	"unsupported sys_extattr_get_file (#276)",			\
	"unsupported sys_extattr_delete_file (#277)",			\
	"unsupported sys_extattr_set_fd (#278)",			\
	"unsupported sys_extattr_get_fd (#279)",			\
	"unsupported sys_extattr_delete_fd (#280)", "getresuid",	\
	"setresuid", "getresgid", "setresgid",				\
	"obsolete sys_omquery (#285)", "mquery", "closefrom",		\
	"sigaltstack", SYSCL_NAME_SHMGET, SYSCL_NAME_SEMOP,		\
	"obsolete t32_stat (#291)", "obsolete t32_fstat (#292)",	\
	"obsolete t32_lstat (#293)", "fhstat", SYSCL_NAME___SEMCTL,	\
	SYSCL_NAME_SHMCTL2, SYSCL_NAME_MSGCTL2, "sched_yield",		\
	"getthrid", "obsolete t32___thrsleep (#300)", "__thrwakeup",	\
	"__threxit", "__thrsigdivert", "__getcwd", "adjfreq",		\
	"obsolete getfsstat53 (#306)", "obsolete statfs53 (#307)",	\
	"obsolete fstatfs53 (#308)", "obsolete fhstatfs53 (#309)",	\
	"setrtable", "getrtable", "obsolete t32_getdirentries (#312)",	\
	"faccessat", "fchmodat", "fchownat",				\
	"obsolete t32_fstatat (#316)", "linkat", "mkdirat", "mkfifoat",	\
	"mknodat", "openat", "readlinkat", "renameat", "symlinkat",	\
	"unlinkat", "obsolete t32_utimensat (#326)",			\
	"obsolete t32_futimens (#327)", "obsolete __tfork51 (#328)",	\
	"__set_tcb", "__get_tcb"					\
}

int8_t *__scnsa[] = SYSCL_NAMES_DEF;
int8_t **syscl_names_array = __scnsa;
