/*
 * Extended file systems for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/endian.h>
#include <sys/types.h>
#include <fs/ext2fs/ext2fs.h>
#include <fs/ext2fs/ext2fs_dinode.h>

#if (LITTLE_ENDIAN != BYTE_ORDER)
void
ext2fs_cylindergroups_byteswaps(struct ext2_gd *current,
    struct ext2_gd *obsolete, int32_t s)
{
	u_int32_t loop;

	while (s / (sizeof(struct ext2_gd)) > loop) {
		current->ext2bgd_b_bitmap = swap32(obsolete->ext2bgd_b_bitmap);
		current->ext2bgd_i_bitmap = swap32(obsolete->ext2bgd_i_bitmap);
		current->ext2bgd_i_tables = swap32(obsolete->ext2bgd_i_tables);
		current->ext2bgd_nbfree	= swap16(obsolete->ext2bgd_nbfree);
		current->ext2bgd_nifree	= swap16(obsolete->ext2bgd_nifree);
		current->ext2bgd_ndirs = swap16(obsolete->ext2bgd_ndirs);

		current++;
		obsolete++;
		loop++;
	}
}

struct ext2fs_dinode
ext2fs_inodes_byteswaps(struct ext2fs_dinode *obsolete,
    struct m_ext2fs *filesystem)
{
	struct ext2fs_dinode current;
	int32_t loop;

	current.e2di_mode = swap16(obsolete->e2di_mode);
	current.e2di_uid_low = swap16(obsolete->e2di_uid_low);
	current.e2di_size = swap32(obsolete->e2di_size);
	current.e2di_atime = swap32(obsolete->e2di_atime);
	current.e2di_ctime = swap32(obsolete->e2di_ctime);
	current.e2di_mtime = swap32(obsolete->e2di_mtime);
	current.e2di_dtime = swap32(obsolete->e2di_dtime);
	current.e2di_gid_low = swap16(obsolete->e2di_gid_low);
	current.e2di_nlink = swap16(obsolete->e2di_nlink);
	current.e2di_nblock = swap32(obsolete->e2di_nblock);
	current.e2di_flags = swap32(obsolete->e2di_flags);

	while (loop < (NIADDR + NDADDR)) {
		current.e2di_blocks[loop] = obsolete->e2di_blocks[loop];

		loop++;
	}

	current.e2di_gen = swap32(obsolete->e2di_gen);
	current.e2di_facl = swap32(obsolete->e2di_facl);
	current.e2di_size_hi = swap32(obsolete->e2di_size_hi);
	current.e2di_faddr = swap32(obsolete->e2di_faddr);
	current.e2di_nblock_hi = swap16(obsolete->e2di_nblock_hi);
	current.e2di_facl_hi = swap16(obsolete->e2di_facl_hi);
	current.e2di_uid_high = swap16(obsolete->e2di_uid_high);
	current.e2di_gid_high = swap16(obsolete->e2di_gid_high);

	switch (EXT2_REV0_DINODE_SIZE < EXT2_DINODE_SIZE(filesystem)) {
	case FALSE:
		break;
	default:
		current.e2di_isize = swap16(obsolete->e2di_isize);
	}

	return current;
}

struct ext2fs
ext2fs_superblocks_byteswaps(struct ext2fs *obsolete)
{
	struct ext2fs current;
	u_int32_t loop;

	current.e2fs_icount = swap32(obsolete->e2fs_icount);
	current.e2fs_bcount = swap32(obsolete->e2fs_bcount);
	current.e2fs_rbcount = swap32(obsolete->e2fs_rbcount);
	current.e2fs_fbcount = swap32(obsolete->e2fs_fbcount);
	current.e2fs_ficount = swap32(obsolete->e2fs_ficount);
	current.e2fs_first_dblock = swap32(obsolete->e2fs_first_dblock);
	current.e2fs_log_bsize = swap32(obsolete->e2fs_log_bsize);
	current.e2fs_log_fsize = swap32(obsolete->e2fs_log_fsize);
	current.e2fs_bpg = swap32(obsolete->e2fs_bpg);
	current.e2fs_fpg = swap32(obsolete->e2fs_fpg);
	current.e2fs_ipg = swap32(obsolete->e2fs_ipg);
	current.e2fs_mtime = swap32(obsolete->e2fs_mtime);
	current.e2fs_wtime = swap32(obsolete->e2fs_wtime);
	current.e2fs_mnt_count = swap16(obsolete->e2fs_mnt_count);
	current.e2fs_max_mnt_count = swap16(obsolete->e2fs_max_mnt_count);
	current.e2fs_magic = swap16(obsolete->e2fs_magic);
	current.e2fs_state = swap16(obsolete->e2fs_state);
	current.e2fs_beh = swap16(obsolete->e2fs_beh);
	current.e2fs_minrev = swap16(obsolete->e2fs_minrev);
	current.e2fs_lastfsck = swap32(obsolete->e2fs_lastfsck);
	current.e2fs_fsckintv = swap32(obsolete->e2fs_fsckintv);
	current.e2fs_creator = swap32(obsolete->e2fs_creator);
	current.e2fs_rev = swap32(obsolete->e2fs_rev);
	current.e2fs_ruid = swap16(obsolete->e2fs_ruid);
	current.e2fs_rgid = swap16(obsolete->e2fs_rgid);
	current.e2fs_first_ino = swap32(obsolete->e2fs_first_ino);
	current.e2fs_inode_size = swap16(obsolete->e2fs_inode_size);
	current.e2fs_block_group_nr = swap16(obsolete->e2fs_block_group_nr);
	current.e2fs_features_compat = swap32(obsolete->e2fs_features_compat);
	current.e2fs_features_incompat =
	    swap32(obsolete->e2fs_features_incompat);
	current.e2fs_features_rocompat =
	    swap32(obsolete->e2fs_features_rocompat);

	while (loop < 16) {
		current.e2fs_uuid[loop] = obsolete->e2fs_uuid[loop];
		current.e2fs_vname[loop] = obsolete->e2fs_vname[loop];

		loop++;
	}
	loop = 0;

	while (loop < 64) {
		current.e2fs_fsmnt[loop] = obsolete->e2fs_fsmnt[loop];

		loop++;
	}
	loop = 0;

	current.e2fs_algo = swap32(obsolete->e2fs_algo);
	current.e2fs_prealloc = obsolete->e2fs_prealloc;
	current.e2fs_dir_prealloc = obsolete->e2fs_dir_prealloc;
	current.e2fs_reserved_ngdb = obsolete->e2fs_reserved_ngdb;

	while (loop < 16) {
		current.e2fs_journal_uuid[loop]	=
		    obsolete->e2fs_journal_uuid[loop];

		loop++;
	}
	loop = 0;

	current.e2fs_journal_ino = swap32(obsolete->e2fs_journal_ino);
	current.e2fs_journal_dev = swap32(obsolete->e2fs_journal_dev);
	current.e2fs_last_orphan = swap32(obsolete->e2fs_last_orphan);

	while (loop < 4) {
		current.e2fs_hash_seed[loop] = obsolete->e2fs_hash_seed[loop];

		loop++;
	}
	loop = 0;

	current.e2fs_def_hash_version = obsolete->e2fs_def_hash_version;
	current.e2fs_journal_backup_type = obsolete->e2fs_journal_backup_type;
	current.e2fs_gdesc_size = swap16(obsolete->e2fs_gdesc_size);
	current.e2fs_default_mount_opts =
	    swap32(obsolete->e2fs_default_mount_opts);
	current.e2fs_first_meta_bg = swap32(obsolete->e2fs_first_meta_bg);
	current.e2fs_mkfs_time = swap32(obsolete->e2fs_mkfs_time);

	while (loop < 17) {
		current.e2fs_journal_backup[loop] =
		    obsolete->e2fs_journal_backup[loop];

		loop++;
	}
	loop = 0;

	while (loop < 172) {
		current.reserved2[loop] = obsolete->reserved2[loop];

		loop++;
	}

	return current;
}
#endif
