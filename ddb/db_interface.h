
/*-
 * Copyright (c) 1995 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by Christos Zoulas.
 *
 * Modifications to support HyperbolaBSD:
 * Copyright (c) 2020 Hyperbola Project
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _DDB_DB_INTERFACE_H_
#define _DDB_DB_INTERFACE_H_

#include "db_ffs.h"
#include "db_ifa.h"
#include "db_nfs.h"
#include "db_srp.h"
#include "db_uimb.h"
#include "db_uis.h"
#include "db_vfs.h"

/* arch/<arch>/<arch>/db_disasm.c */
db_addr_t	db_disasm(db_addr_t, boolean_t);

/* arch/<arch>/<arch>/db_trace.c */
void		db_stack_trace_print(db_expr_t, int, db_expr_t, char *,
    int (*)(const char *, ...) __printflike(1, 2));

/* kern/kern_proc.c */
void		db_kill_cmd(db_expr_t, int, db_expr_t, char *);
void		db_show_all_procs(db_expr_t, int, db_expr_t, char *);

/* kern/subr_pool.c */
void		db_show_all_pools(db_expr_t, int, db_expr_t, char *);

/* kern/kern_timeout.c */
void		db_show_callout(db_expr_t, int, db_expr_t, char *);

#endif /* _DDB_DB_INTERFACE_H_ */
