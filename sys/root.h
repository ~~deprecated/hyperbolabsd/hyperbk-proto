/*
 * Root header for HyperbolaBSD
 * Copyright (c) 2020-2021 Hyperbola Project
 * Copyright (c) 2020-2021 Márcio Silva <coadde@hyperbola.info>
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/param.h>

enum root_magic_t {
	/* root magic */
	ROOT_MAGIC = 0x147,

	/* root magic segment */
	ROOT_MAGIC_SEGMENT = 0x148
};

enum _root_t {
	/* root central processing unit */
	ROOT_CPU = 0x001,

	/* root data */
	ROOT_DATA = 0x002,

	/* root stack */
	ROOT_STACK = 0x004
};

void
root_magic_get(u_int32_t *return_ptr, u_int32_t magic_mid_flag)
{
	*return_ptr = 0xffff & htonl(magic_mid_flag) >> 0;
}

void
root_machineid_get(u_int32_t *return_ptr, u_int32_t magic_mid_flag)
{
	*return_ptr = 0x03ff & ntohl(magic_mid_flag) >> 16;
}

void
root_flag_get(u_int32_t *return_ptr, u_int32_t magic_mid_flag)
{
	*return_ptr = 0x003f & ntohl(magic_mid_flag) >> 26;
}

u_int32_t
root_magic_set(u_int32_t cpu, u_int32_t machineid, u_int32_t magic)
{
	return htonl((0001777 & machineid) << 020 | (0177777 & magic) << 000
	    | (0000077 & cpu) << 032);
}

#ifdef _KERNEL
int coredump_write(void *, enum uio_seg, const void *, size_t);
void coredump_unmap(void *, vaddr_t, vaddr_t);
#else
struct root_t {
	struct {
		u_int	magic_mid_flag;	/* magic machine id flag */
		u_short size;
	} data_s;
	struct {
		u_short header_size;
		u_int	number;
	} segment_s;
	struct {
		u_int8_t	name[1 + MAXCOMLEN];
		u_int		signal_number;
		unsigned long 	ucode;
	} data1_s;
	struct {
		unsigned long machine_dependent;
		unsigned long traditional_text;
		unsigned long traditional_data;
		unsigned long traditional_stack;
	} segment_size_s;
};

struct data_s {
	struct {
		u_int		magic_mid_flag;	/* magic machine id flag */
		unsigned long	address;
		unsigned long	size;
	} data_s;
};
#endif
