/*
 * Small Computer System Interface (SCSI) header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/ioccom.h>
#include <sys/types.h>

enum scsi_sense_buff_t {
	/* small computer system interface sense buffer length */
	SCSI_SENSE_BUFF_LENGTH = 060
};

enum scsi_comd_buff_t {
	/* small computer system interface comand buffer length */
	SCSI_COMD_BUFF_LENGTH = 020
};

enum scsi_comd_flg_t {
	/* small computer system interface comand flag read */
	SCSI_COMD_FLG_READ = 001,

	/* small computer system interface comand flag write */
	SCSI_COMD_FLG_WRITE = 002,

	/* small computer system interface comand flag iov (input output v?) */
	SCSI_COMD_FLG_IOV = 004,

	/* small computer system interface comand flag escape */
	SCSI_COMD_FLG_ESCAPE = 020,

	/* small computer system interface comand flag target */
	SCSI_COMD_FLG_TARGET = 040
};

enum scsi_comd_stat_t {
	/* small computer system interface comand status ok */
	SCSI_COMD_STAT_OK = 000,

	/* small computer system interface comand status timeout */
	SCSI_COMD_STAT_TIMEOUT = 001,

	/* small computer system interface comand status busy */
	SCSI_COMD_STAT_BUSY = 002,

	/* small computer system interface comand status sense */
	SCSI_COMD_STAT_SENSE = 003,

	/* small computer system interface comand status unknown */
	SCSI_COMD_STAT_UNKNOWN = 004
};

enum scsi_debug_t {
	/* small computer system interface debug comands and errors */
	SCSI_DEBUG_COMDS_ERRS = 001,

	/* small computer system interface debug flow */
	SCSI_DEBUG_FLOW = 002,

	/* small computer system interface debug flow inside */
	SCSI_DEBUG_FLOW_INSIDE = 004,

	/* small computer system interface debug direct memory access */
	SCSI_DEBUG_DMA = 010
};

enum scsi_addr_type_t {
	/*
	 * small computer system interface address type small computer system
	 * interface
	 */
	SCSI_ADDR_TYPE_SCSI = 000,
	/*
	 * small computer system interface address type advanced technology
	 * attachment packet interface
	 */
	SCSI_ADDR_TYPE_ATAPI = 001
};

struct scsi_request_t {
	struct {
		unsigned long r_flags;
		unsigned long r_time_out;
	} data_s;
	struct {
		u_int8_t data[SCSI_COMD_BUFF_LENGTH];
		u_int8_t length;
	} command_s;
	struct {
		u_int8_t	*address;
		unsigned long	 length;
		unsigned long	 length_used;
	} data_buffer_s;
	struct {
		u_int8_t data[SCSI_SENSE_BUFF_LENGTH];
		u_int8_t length;
		u_int8_t length_used;
	} sense_s;
	struct {
		u_int8_t	data;
		u_int8_t	s_return;
		int32_t		failed;
	} status_s;
};

struct scsi_address_t {
	struct {
		u_int32_t a_type;
		u_int32_t a_bus;
		u_int32_t a_target;
		u_int32_t a_lun;
	} data_s;
};

struct scsi_bioc_device_t {
	struct {
		void *pointer;
	} cookie_s;
	struct {
		int32_t target;
		int32_t lun;
	} data_s;
};

enum scsi_io_t {
	/* small computer system interface input output command */
	SCSI_IO_COMMAND = _IOC(IOC_INOUT, 0121, 0001,
	    sizeof(struct scsi_request_t)),

	/* small computer system interface input output debug */
	SCSI_IO_DEBUG = _IOC(IOC_IN, 0121, 0002, sizeof(int)),

	/* small computer system interface input output reset */
	SCSI_IO_RESET = _IOC(IOC_VOID, 0121, 0007, 0000),

	/* small computer system interface input output identify */
	SCSI_IO_IDENTIFY = _IOC(IOC_OUT, 0121, 0011,
	    sizeof(struct scsi_address_t)),

	/* small computer system interface input output probe */
	SCSI_IO_PROBE = _IOC(IOC_INOUT, 0121, 0177,
	    sizeof(struct scsi_bioc_device_t)),

	/* small computer system interface input output detach */
	SCSI_IO_DETACH = _IOC(IOC_INOUT, 0121, 0200,
	    sizeof(struct scsi_bioc_device_t))
};
