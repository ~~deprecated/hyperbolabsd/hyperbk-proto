/*
 * Shell script checker header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/param.h>

enum check_script_t {
	/* shell -script- checker magic length */
	SHCHK_MAGIC_LENGTH = (1 + 1),

	/* shell -script- checker */
	SHCHK_HEADER_SIZE = (MAXINTERP + 2 + SHCHK_MAGIC_LENGTH)
};

/* shell -script- checker magic string */
static const unsigned char SHCHK_MAGIC_STRING[] = {
	'#', '!', '\0'
};

#ifdef _KERNEL
struct exec_package;

void	shell_script_checker(int *, struct exec_package *, struct proc *);
#endif
