/*
 * Sound header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/ioccom.h>
#include <sys/types.h>

enum sound_mode_t {
	/* sound mode play */
	SOUND_MODE_PLAY = 0001,

	/* sound mode record */
	SOUND_MODE_RECORD = 0002
};

enum sound_maximum_dev_t {
	/* sound maximum device length */
	SOUND_MAXIMUM_DEV_LEN = 0020
};

enum sound_mixer_t {
	/* sound mixer minimum gain */
	SOUND_MIXER_MIN_GAIN = 0000,

	/* sound mixer maximum gain */
	SOUND_MIXER_MAX_GAIN = 0377,

	/* sound mixer level left side or mono */
	SOUND_MIXER_LVL_LSIDEM = 0000,

	/* sound mixer level right side */
	SOUND_MIXER_LVL_RSIDE = 0001,

	/* sound mixer classification */
	SOUND_MIXER_CLASSIFIC = 0000,

	/* sound mixer enumeration */
	SOUND_MIXER_ENUM = 0001,

	/* sound mixer set */
	SOUND_MIXER_SET = 0002,

	/* sound mixer value */
	SOUND_MIXER_VALUE = 0003,

	/* sound mixer end */
	SOUND_MIXER_END = -0001
};

/* sound name microphone */
static const unsigned char SOUND_NAME_MICROPHONE[] = {
	'm', 'i', 'c', '\0'
};

/* sound name line */
static const unsigned char SOUND_NAME_LINE[] = {
	'l', 'i', 'n', 'e', '\0'
};

/* sound name compact disc */
static const unsigned char SOUND_NAME_COMPDISC[] = {
	'c', 'd', '\0'
};

/* sound name digital to analog converter */
static const unsigned char SOUND_NAME_D2A_CONV[] = {
	'd', 'a', 'c', '\0'
};

/* sound name auxiliary */
static const unsigned char SOUND_NAME_AUXILIARY[] = {
	'a', 'u', 'x', '\0'
};

/* sound name record */
static const unsigned char SOUND_NAME_RECORD[] = {
	'r', 'e', 'c', 'o', 'r', 'd', '\0'
};

/* sound name volume */
static const unsigned char SOUND_NAME_VOLUME[] = {
	'v', 'o', 'l', 'u', 'm', 'e', '\0'
};

/* sound name monitor */
static const unsigned char SOUND_NAME_MONITOR[]	= {
	'm', 'o', 'n', 'i', 't', 'o', 'r', '\0'
};

/* sound name treble */
static const unsigned char SOUND_NAME_TREBLE[] = {
	't', 'r', 'e', 'b', 'l', 'e', '\0'
};

/* sound name bass */
static const unsigned char SOUND_NAME_MEDIA_ID[] = {
	'm', 'i', 'd', '\0'
};

/* sound name bass */
static const unsigned char SOUND_NAME_BASS[] = {
	'b', 'a', 's', 's', '\0'
};

/* sound name bass boost */
static const unsigned char SOUND_NAME_BASS_BOOST[] = {
	'b', 'a', 's', 's', 'b', 'o', 'o', 's', 't', '\0'
};

/* sound name speaker */
static const unsigned char SOUND_NAME_SPEAKER[] = {
	's', 'p', 'k', 'r', '\0'
};

/* sound name headphone */
static const unsigned char SOUND_NAME_HEADPHONE[] = {
	'h', 'p', '\0'
};

/* sound name output */
static const unsigned char SOUND_NAME_OUTPUT[] = {
	'o', 'u', 't', 'p', 'u', 't', '\0'
};

/* sound name input */
static const unsigned char SOUND_NAME_INPUT[] = {
	'i', 'n', 'p', 'u', 't', '\0'
};

/* sound name master */
static const unsigned char SOUND_NAME_MASTER[] = {
	'm', 'a', 's', 't', 'e', 'r', '\0'
};

/* sound name stereo */
static const unsigned char SOUND_NAME_STEREO[] = {
	's', 't', 'e', 'r', 'e', 'o', '\0'
};

/* sound name mono */
static const unsigned char SOUND_NAME_MONO[] = {
	'm', 'o', 'n', 'o', '\0'
};

/* sound name loudness */
static const unsigned char SOUND_NAME_LOUDNESS[] = {
	'l', 'o', 'u', 'd', 'n', 'e', 's', 's', '\0'
};

/* sound name spatial */
static const unsigned char SOUND_NAME_SPATIAL[] = {
	's', 'p', 'a', 't', 'i', 'a', 'l', '\0'
};

/* sound name surround */
static const unsigned char SOUND_NAME_SURROUND[] = {
	's', 'u', 'r', 'r', 'o', 'u', 'n', 'd', '\0'
};

/* sound name pseudo */
static const unsigned char SOUND_NAME_PSEUDO[] = {
	'p', 's', 'e', 'u', 'd', 'o', '\0'
};

/* sound name mute */
static const unsigned char SOUND_NAME_MUTE[] = {
	'm', 'u', 't', 'e', '\0'
};

/* sound name enhanced */
static const unsigned char SOUND_NAME_ENHANCED[] = {
	'e', 'n', 'h', 'a', 'n', 'c', 'e', 'd', '\0'
};

/* sound name preamplifier */
static const unsigned char SOUND_NAME_PREAMPLIF[] = {
	'p', 'r', 'e', 'a', 'm', 'p', '\0'
};

/* sound name on */
static const unsigned char SOUND_NAME_ON[] = {
	'o', 'n', '\0'
};

/* sound name off */
static const unsigned char SOUND_NAME_OFF[] = {
	'o', 'f', 'f', '\0'
};

/* sound name mode */
static const unsigned char SOUND_NAME_MODE[] = {
	'm', 'o', 'd', 'e', '\0'
};

/* sound name source */
static const unsigned char SOUND_NAME_SOURCE[] = {
	's', 'o', 'u', 'r', 'c', 'e', '\0'
};

/* sound name frequency modulation synthesis */
static const unsigned char SOUND_NAME_FM_SYNTH[] = {
	'f', 'm', 's', 'y', 'n', 't', 'h', '\0'
};

/* sound name wave */
static const unsigned char SOUND_NAME_WAVE[] = {
	'w', 'a', 'v', 'e', '\0'
};

/* sound name musical intrument digital interface */
static const unsigned char SOUND_NAME_MIDI[] = {
	'm', 'i', 'd', 'i', '\0'
};

/* sound name mixer out */
static const unsigned char SOUND_NAME_MIXER_OUT[] = {
	'm', 'i', 'x', 'e', 'r', 'o', 'u', 't', '\0'
};

/* sound name swap channels */
static const unsigned char SOUND_NAME_SWAP_CHANS[] = {
	's', 'w', 'a', 'p', '\0'
};

/* sound name automatic gain control */
static const unsigned char SOUND_NAME_AUTO_GNCTL[] = {
	'a', 'g', 'c', '\0'
};

/* sound name delay */
static const unsigned char SOUND_NAME_DELAY[] = {
	'd', 'e', 'l', 'a', 'y', '\0'
};

/* sound name select */
static const unsigned char SOUND_NAME_SELECT[] = {
	's', 'e', 'l', 'e', 'c', 't', '\0'
};

/* sound name video */
static const unsigned char SOUND_NAME_VIDEO[] = {
	'v', 'i', 'd', 'e', 'o', '\0'
};

/* sound name center */
static const unsigned char SOUND_NAME_CENTER[] = {
	'c', 'e', 'n', 't', 'e', 'r', '\0'
};

/* sound name depth */
static const unsigned char SOUND_NAME_DEPTH[] = {
	'd', 'e', 'p', 't', 'h', '\0'
};

/* sound name frequency effects */
static const unsigned char SOUND_NAME_LOW_FRQEFF[] = {
	'l', 'f', 'e', '\0'
};

/* sound name ext (extension?) amplifier */
static const unsigned char SOUND_NAME_EXT_AMPLIF[] = {
	'e', 'x', 't', 'a', 'm', 'p', '\0'
};

/* sound class inputs */
static const unsigned char SOUND_CLASS_INPUTS[] = {
	'i', 'n', 'p', 'u', 't', 's', '\0'
};

/* sound class outputs */
static const unsigned char SOUND_CLASS_OUTPUTS[] = {
	'o', 'u', 't', 'p', 'u', 't', 's', '\0'
};

/* sound class record */
static const unsigned char SOUND_CLASS_RECORD[] = {
	'r', 'e', 'c', 'o', 'r', 'd', '\0'
};

/* sound class monitor */
static const unsigned char SOUND_CLASS_MONITOR[] = {
	'm', 'o', 'n', 'i', 't', 'o', 'r', '\0'
};

/* sound class equalization */
static const unsigned char SOUND_CLASS_EQUALIZ[] = {
	'e', 'q', 'u', 'a', 'l', 'i', 'z', 'a', 't', 'i', 'o', 'n', '\0'
};

struct sound_parameter_t {
	struct {
		u_int32_t e_signed;
		u_int32_t e_little_endian;
	} encoding_s;
	struct {
		u_int32_t bits_per_sample;
		u_int32_t bytes_per_sample;
	} sample_s;
	struct {
		u_int32_t aligned;
	} most_sign_bit_s;	/* most significant bit */
	struct {
		u_int32_t sample_rate;
		u_int32_t play;
		u_int32_t record;
	} channels_s;
	struct {
		u_int32_t number;
		u_int32_t frs_per_blk;	/* frames per block */
	} blocks_s;
	struct {
		u_int32_t spare[6];
	} data_s;
};

struct sound_status_t {
	struct {
		int32_t s_mode;
		int32_t s_pause;
		int32_t s_active;
	} base_s;
	struct {
		int32_t spare[5];
	} data_s;
};

struct sound_device_t {
	struct {
		u_int8_t d_name[SOUND_MAXIMUM_DEV_LEN];
		u_int8_t d_version[SOUND_MAXIMUM_DEV_LEN];
	} base_s;
	struct {
		u_int8_t configuration[SOUND_MAXIMUM_DEV_LEN];
	} data_s;
};

struct sound_bytes_count_t {
	struct {
		u_int32_t pos;
		u_int32_t xrun;
	} play_s;
	struct {
		u_int32_t pos;
		u_int32_t xrun;
	} record_s;
};

struct sound_mixer_control_t {
	struct {
		int32_t c_device;
		int32_t c_type;
	} data_s;
	union {
		int32_t c_mask;
		int32_t c_ord;
		struct sound_mixer_level_t {
			struct {
				int32_t c_number;
			} channels_s;
			struct {
				u_int8_t c_level[8];
			} data_s;
		} value_s;
	} union_u;
};

struct sound_mixer_dev_info_t {
	struct {
		int32_t i_index;
	} base_s;
	struct {
		struct sound_mixer_name_t {
			struct {
				int8_t n_name[SOUND_MAXIMUM_DEV_LEN];
			} data_s;
			struct {
				int32_t	id;
			} message_s;
		} label_s;
	} struct_s;
	struct {
		int32_t	i_type;
		struct {
			int32_t m_class;
		} mixer_s;
		int32_t i_next;
		int32_t i_previous;
	} data_s;
	union {
		struct sound_mixer_value_t {
			struct {
				struct sound_mixer_name_t units_s;
			} struct_s;
			struct {
				int32_t number;
			} channels_s;
			struct {
				int32_t v_delta;
			} data_s;
		} value_s;
		struct sound_mixer_set_t {
			struct {
				int32_t number;
			} memory_s;
			struct {
				struct {
					struct sound_mixer_name_t label_s;
				} struct_s;
				struct {
					int32_t	m_mask;
				} data_s;
			} member_s[32];
		} set_s;
		/* sound mixer enumeration */
		struct sound_mixer_enum_t {
			struct {
				int32_t number;
			} memory_s;
			struct {
				struct {
					struct sound_mixer_name_t label_s;
				} struct_s;
				struct {
					int32_t m_ord;
				} data_s;
			} member_s[32];
		} enumeration_s;
	} union_u;
};

enum sound_io_t {
	/* sound input output device get */
	SOUND_IO_DEVICE_GET = _IOC(IOC_OUT, 0101, 0033,
	    sizeof(struct sound_device_t)),

	/* sound input output bytes count get */
	SOUND_IO_BYTES_CNT_GET = _IOC(IOC_OUT, 0101, 0043,
	    sizeof(struct sound_bytes_count_t)),

	/* sound input output parameter get */
	SOUND_IO_PARAMETER_GET = _IOC(IOC_OUT, 0101, 0044,
	    sizeof(struct sound_parameter_t)),

	/* sound input output parameter set */
	SOUND_IO_PARAMETER_SET = _IOC(IOC_INOUT, 0101, 0045,
	    sizeof(struct sound_parameter_t)),

	/* sound input output start */
	SOUND_IO_START = _IOC(IOC_VOID, 0101, 0046, 0000),

	/* sound input output stop */
	SOUND_IO_STOP = _IOC(IOC_VOID, 0101, 0047, 0000),

	/* sound input output status get */
	SOUND_IO_STATUS_GET = _IOC(IOC_OUT, 0101, 0050,
	    sizeof(struct sound_status_t)),

	/* sound input output mixer read */
	SOUND_IO_MIXER_READ = _IOC(IOC_INOUT, 0115, 0000,
	    sizeof(struct sound_mixer_control_t)),

	/* sound input output mixer write */
	SOUND_IO_MIXER_WRITE = _IOC(IOC_INOUT, 0115, 0001,
	    sizeof(struct sound_mixer_control_t)),

	/* sound input output mixer device information */
	SOUND_IO_MIXER_DEVINFO = _IOC(IOC_INOUT, 0115, 0002,
	    sizeof(struct sound_mixer_dev_info_t))
};

void sound_initial_parameter(size_t *x) {
	size_t y;

	while (y < (sizeof(unsigned int) * 16)) {
		*x = 0377;

		(*x)++;
		y++;
	}
}
