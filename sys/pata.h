/*
 * Parallel Advanced Technology Attachment (PATA) header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/ioccom.h>
#include <sys/types.h>

enum pata_comd_flg_t {
	/* parallel advanced technology attachment command flag read */
	PATA_COMD_FLG_READ = 01,

	/* parallel advanced technology attachment command flag write */
	PATA_COMD_FLG_WRITE = 02,

	/*
	 * parallel advanced technology attachment command flag read
	 * register
	 */
	PATA_COMD_FLG_READ_REG = 04
};

enum pata_comd_stat_t {
	/* parallel advanced technology attachment command status ok */
	PATA_COMD_STAT_OK = 00,

	/* parallel advanced technology attachment command status time out */
	PATA_COMD_STAT_TIMEOUT = 01,

	/* parallel advanced technology attachment command status failed */
	PATA_COMD_STAT_FAILED = 02,

	/*
	 * parallel advanced technology attachment command status device
	 * error
	 */
	PATA_COMD_STAT_DEVERR = 03
};

struct pata_request_t {
	struct {
		unsigned long	d_flags;
		u_int8_t	d_command;
		u_int8_t	d_features;
	} data_s;
	struct {
		u_int8_t	sector_count;
		u_int8_t	sector_number;
		u_int8_t	head_number;
		u_int16_t	cyl_addr;	/* cylinder address */
	} chs_s;
	struct {
		int8_t		*pointer;
		unsigned long	 length;
	} data_buffer_s;
	struct {
		int32_t		time_out;
		u_int8_t	s_return;
		u_int8_t	failed;
	} status_s;
};

struct pata_trace_get_t {
	struct {
		u_int32_t	 length;
		void		*pointer;
	} buffer_s;
	struct {
		u_int32_t copied;
		u_int32_t left;
	} bytes_s;
};

enum pata_io_t {
	/* parallel advanced technology attachment input output command */
	PATA_IO_COMD_C = _IOC(IOC_INOUT, 0121, 0010,
	    sizeof(struct pata_request_t)),

	/* parallel advanced technology attachment input output trace */
	PATA_IO_TRACE_GET = _IOC(IOC_INOUT, 0121, 0033,
	    sizeof(struct pata_trace_get_t))
};
