/*
 * System call header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

enum syscl_t {
	/* system call number for syscall() */
	SYSCL = 0000,

	/* system call number for exit() */
	SYSCL_EXIT = 0001,

	/* system call number for fork() */
	SYSCL_FORK = 0002,

	/* system call number for read() */
	SYSCL_READ = 0003,

	/* system call number for write() */
	SYSCL_WRITE = 0004,

	/* system call number for open() */
	SYSCL_OPEN = 0005,

	/* system call number for close() */
	SYSCL_CLOSE = 0006,

	/* system call number for getentropy() */
	SYSCL_GETENTROPY = 0007,

	/* system call number for __tfork() */
	SYSCL_TFORK = 0010,

	/* system call number for link() */
	SYSCL_LINK = 0011,

	/* system call number for unlink() */
	SYSCL_UNLINK = 0012,

	/* system call number for wait4() */
	SYSCL_WAIT4 = 0013,

	/* system call number for chdir() */
	SYSCL_CHDIR = 0014,

	/* system call number for fchdir() */
	SYSCL_FCHDIR = 0015,

	/* system call number for mknod() */
	SYSCL_MKNOD = 0016,

	/* system call number for chmod() */
	SYSCL_CHMOD = 0017,

	/* system call number for chown() */
	SYSCL_CHOWN = 0020,

	/* system call number for break() */
	SYSCL_BREAK = 0021,

	/* system call number for getdtablecount() */
	SYSCL_GETDTABLECOUNT = 0022,

	/* system call number for getrusage() */
	SYSCL_GETRUSAGE = 0023,

	/* system call number for getpid() */
	SYSCL_GETPID = 0024,

	/* system call number for mount() */
	SYSCL_MOUNT = 0025,

	/* system call number for umount() */
	SYSCL_UNMOUNT = 0026,

	/* system call number for setuid() */
	SYSCL_SETUID = 0027,

	/* system call number for getuid() */
	SYSCL_GETUID = 0030,

	/* system call number for geteuid() */
	SYSCL_GETEUID = 0031,

	/* system call number for ptrace() */
	SYSCL_PTRACE = 0032,

	/* system call number for recvmsg() */
	SYSCL_RECVMSG = 0033,

	/* system call number for sendmsg() */
	SYSCL_SENDMSG = 0034,

	/* system call number for recvfrom() */
	SYSCL_RECVFROM = 0035,

	/* system call number for accept() */
	SYSCL_ACCEPT = 0036,

	/* system call number for getpeername() */
	SYSCL_GETPEERNAME = 0037,

	/* system call number for getsockname() */
	SYSCL_GETSOCKNAME = 0040,

	/* system call number for access() */
	SYSCL_ACCESS = 0041,

	/* system call number for chflags() */
	SYSCL_CHFLAGS = 0042,

	/* system call number for fchflags() */
	SYSCL_FCHFLAGS = 0043,

	/* system call number for sync() */
	SYSCL_SYNC = 0044,

	/* system call number for (OpenBSD 58) kill() */
//	SYSCL_OBS_O58_KILL = 0045,

	/* system call number for stat() */
	SYSCL_STAT = 0046,

	/* system call number for getppid() */
	SYSCL_GETPPID = 0047,

	/* system call number for lstat() */
	SYSCL_LSTAT = 0050,

	/* system call number for dup() */
	SYSCL_DUP = 0051,

	/* system call number for fstatat() */
	SYSCL_FSTATAT = 0052,

	/* system call number for getegid() */
	SYSCL_GETEGID = 0053,

	/* system call number for profil() */
	SYSCL_PROFIL = 0054,

	/* system call number for ktrace() */
	SYSCL_KTRACE = 0055,

	/* system call number for sigaction() */
	SYSCL_SIGACTION = 0056,

	/* system call number for getgid() */
	SYSCL_GETGID = 0057,

	/* system call number for sigprocmask() */
	SYSCL_SIGPROCMASK = 0060,

	/* system call number for ogetlogin() */
//	SYSCL_OBS_OGETLOGIN = 0061,

	/* system call number for setlogin() */
	SYSCL_SETLOGIN = 0062,

	/* system call number for acct() */
	SYSCL_ACCT = 0063,

	/* system call number for sigpending() */
	SYSCL_SIGPENDING = 0064,

	/* system call number for fstat() */
	SYSCL_FSTAT = 0065,

	/* system call number for ioctl() */
	SYSCL_IOCTL = 0066,

	/* system call number for reboot() */
	SYSCL_REBOOT = 0067,

	/* system call number for revoke() */
	SYSCL_REVOKE = 0070,

	/* system call number for symlink() */
	SYSCL_SYMLINK = 0071,

	/* system call number for readlink() */
	SYSCL_READLINK = 0072,

	/* system call number for execve() */
	SYSCL_EXECVE = 0073,

	/* system call number for umask() */
	SYSCL_UMASK = 0074,

	/* system call number for chroot() */
	SYSCL_CHROOT = 0075,

	/* system call number for getfsstat() */
	SYSCL_GETFSSTAT = 0076,

	/* system call number for statfs() */
	SYSCL_STATFS = 0077,

	/* system call number for fstatfs() */
	SYSCL_FSTATFS = 0100,

	/* system call number for fhstatfs() */
	SYSCL_FHSTATFS = 0101,

	/* system call number for vfork() */
	SYSCL_VFORK = 0102,

	/* system call number for gettimeofday() */
	SYSCL_GETTIMEOFDAY = 0103,

	/* system call number for settimeofday() */
	SYSCL_SETTIMEOFDAY = 0104,

	/* system call number for setitimer() */
	SYSCL_SETITIMER = 0105,

	/* system call number for getitimer() */
	SYSCL_GETITIMER = 0106,

	/* system call number for select() */
	SYSCL_SELECT = 0107,

	/* system call number for kevent() */
	SYSCL_KEVENT = 0110,

	/* system call number for munmap() */
	SYSCL_MUNMAP = 0111,

	/* system call number for mprotect() */
	SYSCL_MPROTECT = 0112,

	/* system call number for madvise() */
	SYSCL_MADVISE = 0113,

	/* system call number for utimes() */
	SYSCL_UTIMES = 0114,

	/* system call number for futimes() */
	SYSCL_FUTIMES = 0115,

	/* free system call number */
//	SYSCL_ = 0116,

	/* system call number for getgroups() */
	SYSCL_GETGROUPS = 0117,

	/* system call number for setgroups() */
	SYSCL_SETGROUPS = 0120,

	/* system call number for getpgrp() */
	SYSCL_GETPGRP = 0121,

	/* system call number for setpgid() */
	SYSCL_SETPGID = 0122,

	/* system call number for futex() */
	SYSCL_FUTEX = 0123,

	/* system call number for utimensat() */
	SYSCL_UTIMENSAT = 0124,

	/* system call number for futimens() */
	SYSCL_FUTIMENS = 0125,

	/* system call number for kbind() */
	SYSCL_KBIND = 0126,

	/* system call number for clock_gettime() */
	SYSCL_CLOCK_GETTIME = 0127,

	/* system call number for clock_settime() */
	SYSCL_CLOCK_SETTIME = 0130,

	/* system call number for clock_getres() */
	SYSCL_CLOCK_GETRES = 0131,

	/* system call number for dup2() */
	SYSCL_DUP2 = 0132,

	/* system call number for nanosleep() */
	SYSCL_NANOSLEEP = 0133,

	/* system call number for fcntl() */
	SYSCL_FCNTL = 0134,

	/* system call number for accept4() */
	SYSCL_ACCEPT4 = 0135,

	/* system call number for __thrsleep() */
	SYSCL_THRSLEEP = 0136,

	/* system call number for fsync() */
	SYSCL_FSYNC = 0137,

	/* system call number for setpriority() */
	SYSCL_SETPRIORITY = 0140,

	/* system call number for socket() */
	SYSCL_SOCKET = 0141,

	/* system call number for connect() */
	SYSCL_CONNECT = 0142,

	/* system call number for getdents() */
	SYSCL_GETDENTS = 0143,

	/* system call number for getpriority() */
	SYSCL_GETPRIORITY = 0144,

	/* system call number for pipe2() */
	SYSCL_PIPE2 = 0145,

	/* system call number for dup3() */
	SYSCL_DUP3 = 0146,

	/* system call number for sigreturn() */
	SYSCL_SIGRETURN = 0147,

	/* system call number for bind() */
	SYSCL_BIND = 0150,

	/* system call number for setsockopt() */
	SYSCL_SETSOCKOPT = 0151,

	/* system call number for listen() */
	SYSCL_LISTEN = 0152,

	/* system call number for chflagsat() */
	SYSCL_CHFLAGSAT = 0153,

	/* system call number for pledge() */
	SYSCL_PLEDGE = 0154,

	/* system call number for ppoll() */
	SYSCL_PPOLL = 0155,

	/* system call number for pselect() */
	SYSCL_PSELECT = 0156,

	/* system call number for sigsuspend() */
	SYSCL_SIGSUSPEND = 0157,

	/* system call number for sendsyslog() */
	SYSCL_SENDSYSLOG = 0160,

	/* free system call number */
//	SYSCL_ = 0161,

	/* system call number for unveil() */
	SYSCL_UNVEIL = 0162,

	/* system call number for __realpath() */
	SYSCL_REALPATH = 0163,

	/* system call number for t32_gettimeofday() */
//	SYSCL_OBS_T32_GTIMODAY = 0164,

	/* system call number for t32_getrusage() */
//	SYSCL_OBS_T32_GRUSAGE = 0165,

	/* system call number for getsockopt() */
	SYSCL_GETSOCKOPT = 0166,

	/* system call number for thrkill() */
	SYSCL_THRKILL = 0167,

	/* system call number for readv() */
	SYSCL_READV = 0170,

	/* system call number for writev() */
	SYSCL_WRITEV = 0171,

	/* system call number for kill() */
	SYSCL_KILL = 0172,

	/* system call number for fchown() */
	SYSCL_FCHOWN = 0173,

	/* system call number for fchmod() */
	SYSCL_FCHMOD = 0174,

	/* system call number for orecvfrom() */
//	SYSCL_OBS_ORECVFROM = 0175,

	/* system call number for setreuid() */
	SYSCL_SETREUID = 0176,

	/* system call number for setregid() */
	SYSCL_SETREGID = 0177,

	/* system call number for rename() */
	SYSCL_RENAME = 0200,

	/* system call number for otruncate() */
//	SYSCL_OBS_OTRUNCATE = 0201,

	/* system call number for oftruncate() */
//	SYSCL_OBS_OFTRUNCATE = 0202,

	/* system call number for flock() */
	SYSCL_FLOCK = 0203,

	/* system call number for mkfifo() */
	SYSCL_MKFIFO = 0204,

	/* system call number for sendto() */
	SYSCL_SENDTO = 0205,

	/* system call number for shutdown() */
	SYSCL_SHUTDOWN = 0206,

	/* system call number for socketpair() */
	SYSCL_SOCKETPAIR = 0207,

	/* system call number for mkdir() */
	SYSCL_MKDIR = 0210,

	/* system call number for rmdir() */
	SYSCL_RMDIR = 0211,

	/* system call number for t32_utimes() */
//	SYSCL_OBS_T32_UTIMES = 0212,

	/* system call number for sigreturn() */
//	SYSCL_OBS_SIGRETURN = 0213,

	/* system call number for adjtime() */
	SYSCL_ADJTIME = 0214,

	/* system call number for getlogin_r() */
	SYSCL_GETLOGIN_R = 0215,

	/* system call number for ogethostid() */
//	SYSCL_OBS_OGETHOSTID = 0216,


	/* system call number for osethostid() */
//	SYSCL_OBS_OSETHOSTID = 0217,

	/* system call number for ogetrlimit() */
//	SYSCL_OBS_OGETRLIMIT = 0220,

	/* system call number for osetrlimit() */
//	SYSCL_OBS_OSETRLIMIT = 0221,

	/* system call number for okillpg() */
//	SYSCL_OBS_OKILLPG = 0222,

	/* system call number for setsid() */
	SYSCL_SETSID = 0223,

	/* system call number for quotactl() */
	SYSCL_QUOTACTL = 0224,

	/* system call number for oquota() */
//	SYSCL_OBS_OQUOTA = 0225,

	/* system call number for ogetsockname() */
//	SYSCL_OBS_OGETSOCKNAME = 0226,

	/* free system call number */
//	SYSCL_ = 0227,

	/* free system call number */
//	SYSCL_ = 0230,

	/* free system call number */
//	SYSCL_ = 0231,

	/* free system call number */
//	SYSCL_ = 0232,

	/* system call number for nfssvc() */
	SYSCL_NFSSVC = 0233,

	/* system call number for ogetdirentries() */
//	SYSCL_OBS_OGDIRENTRIES = 0234,

	/* system call number for statfs25() */
//	SYSCL_OBS_STATFS25 = 0235,

	/* system call number for fstatfs25() */
//	SYSCL_OBS_FSTATFS25 = 0236,

	/* free system call number */
//	SYSCL_ = 0237,

	/* free system call number */
//	SYSCL_ = 0240,

	/* system call number for getfh() */
	SYSCL_GETFH = 0241,

	/* system call number for ogetdomainname() */
//	SYSCL_OBS_OGDOMAINNAME = 0242,

	/* system call number for osetdomainname() */
//	SYSCL_OBS_OSDOMAINNAME = 0243,

	/* free system call number */
//	SYSCL_ = 0244,

	/* system call number for sysarch() */
	SYSCL_SYSARCH = 0245,

	/* free system call number */
//	SYSCL_ = 0246,

	/* free system call number */
//	SYSCL_ = 0247,

	/* free system call number */
//	SYSCL_ = 0250,

	/* system call number for semsys10() */
//	SYSCL_OBS_SEMSYS10 = 0251,

	/* system call number for msgsys10() */
//	SYSCL_OBS_MSGSYS10 = 0252,

	/* system call number for shmsys10() */
//	SYSCL_OBS_SHMSYS10 = 0253,

	/* free system call number */
//	SYSCL_ = 0254,

	/* system call number for pread() */
	SYSCL_PREAD = 0255,

	/* system call number for pwrite() */
	SYSCL_PWRITE = 0256,

	/* free system call number */
//	SYSCL_ = 0257,

	/* free system call number */
//	SYSCL_ = 0260,

	/* free system call number */
//	SYSCL_ = 0261,

	/* free system call number */
//	SYSCL_ = 0262,

	/* free system call number */
//	SYSCL_ = 0263,

	/* free system call number */
//	SYSCL_ = 0264,

	/* system call number for setgid() */
	SYSCL_SETGID = 0265,

	/* system call number for setegid() */
	SYSCL_SETEGID = 0266,

	/* system call number for seteuid() */
	SYSCL_SETEUID = 0267,

	/* system call number for lfs_bmapv() */
//	SYSCL_OBS_LFS_BMAPV = 0270,

	/* system call number for lfs_markv() */
//	SYSCL_OBS_LFS_MARKV = 0271,

	/* system call number for lfs_segclean() */
//	SYSCL_OBS_LFS_SEGCLEAN = 0272,

	/* system call number for lfs_segwait() */
//	SYSCL_OBS_LFS_SEGWAIT = 0273,

	/* system call number for stat35() */
//	SYSCL_OBS_STAT35 = 0274,

	/* system call number for fstat35() */
//	SYSCL_OBS_FSTAT35 = 0275,

	/* system call number for lstat35() */
//	SYSCL_OBS_LSTAT35 = 0276,

	/* system call number for pathconf() */
	SYSCL_PATHCONF = 0277,

	/* system call number for fpathconf() */
	SYSCL_FPATHCONF = 0300,

	/* system call number for swapctl() */
	SYSCL_SWAPCTL = 0301,

	/* system call number for getrlimit() */
	SYSCL_GETRLIMIT = 0302,

	/* system call number for setrlimit() */
	SYSCL_SETRLIMIT = 0303,

	/* system call number for ogetdirents48() */
//	SYSCL_OBS_OGDIRENTS48 = 0304,

	/* system call number for mmap() */
	SYSCL_MMAP = 0305,

	/* system call number for __syscall() */
	SYSCL_SYSCALL = 0306,

	/* system call number for lseek() */
	SYSCL_LSEEK = 0307,

	/* system call number for truncate() */
	SYSCL_TRUNCATE = 0310,

	/* system call number for ftruncate() */
	SYSCL_FTRUNCATE = 0311,

	/* system call number for sysctl() */
	SYSCL_SYSCTL = 0312,

	/* system call number for mlock() */
	SYSCL_MLOCK = 0313,

	/* system call number for munlock() */
	SYSCL_MUNLOCK = 0314,

	/* free system call number */
//	SYSCL_ = 0315,

	/* system call number for t32_futimes() */
//	SYSCL_OBS_T32_FUTIMES = 0316,

	/* system call number for getpgid() */
	SYSCL_GETPGID = 0317,

	/* system call number for nnpfspioctl() */
//	SYSCL_OBS_NNPFSPIOCTL = 0320,

	/* system call number for utrace() */
	SYSCL_UTRACE = 0321,

	/* free system call number */
//	SYSCL_ = 0322,

	/* free system call number */
//	SYSCL_ = 0323,

	/* free system call number */
//	SYSCL_ = 0324,

	/* free system call number */
//	SYSCL_ = 0325,

	/* free system call number */
//	SYSCL_ = 0326,

	/* free system call number */
//	SYSCL_ = 0327,

	/* free system call number */
//	SYSCL_ = 0330,

	/* free system call number */
//	SYSCL_ = 0331,

	/* free system call number */
//	SYSCL_ = 0332,

	/* free system call number */
//	SYSCL_ = 0333,

	/* free system call number */
//	SYSCL_ = 0334,

	/* system call number for semget() */
	SYSCL_SEMGET = 0335,

	/* system call number for semop35() */
//	SYSCL_OBS_SEMOP35 = 0336,

	/* system call number for semconfig35() */
//	SYSCL_OBS_SEMCONFIG35 = 0337,

	/* free system call number */
//	SYSCL_ = 0340,

	/* system call number for msgget() */
	SYSCL_MSGGET = 0341,

	/* system call number for msgsnd() */
	SYSCL_MSGSND = 0342,

	/* system call number for msgrcv() */
	SYSCL_MSGRCV = 0343,

	/* system call number for shmat() */
	SYSCL_SHMAT = 0344,

	/* free system call number */
//	SYSCL_ = 0345,

	/* system call number for shmdt() */
	SYSCL_SHMDT = 0346,

	/* system call number for shmget35() */
//	SYSCL_OBS_SHMGET35 = 0347,

	/* system call number for t32_clock_gettime() */
//	SYSCL_OBS_T32_CLK_GTIM = 0350,

	/* system call number for t31_clock_settime() */
//	SYSCL_OBS_T32_CLK_STIM = 0351,

	/* system call number for t32_clock_getres() */
//	SYSCL_OBS_T32_CLK_GRES = 0352,

	/* free system call number */
//	SYSCL_ = 0353,

	/* free system call number */
//	SYSCL_ = 0354,

	/* free system call number */
//	SYSCL_ = 0355,

	/* free system call number */
//	SYSCL_ = 0356,

	/* free system call number */
//	SYSCL_ = 0357,

	/* system call number for t32_nanosleep() */
//	SYSCL_OBS_T32_NANOSLP = 0360,

	/* free system call number */
//	SYSCL_ = 0361,

	/* free system call number */
//	SYSCL_ = 0362,

	/* free system call number */
//	SYSCL_ = 0363,

	/* free system call number */
//	SYSCL_ = 0364,

	/* free system call number */
//	SYSCL_ = 0365,

	/* free system call number */
//	SYSCL_ = 0366,

	/* free system call number */
//	SYSCL_ = 0367,

	/* free system call number */
//	SYSCL_ = 0370,

	/* free system call number */
//	SYSCL_ = 0371,

	/* system call number for minherit() */
	SYSCL_MINHERIT = 0372,

	/* system call number for rfork() */
//	SYSCL_OBS_RFORK = 0373,

	/* system call number for poll() */
	SYSCL_POLL = 0374,

	/* system call number for issuetugid() */
	SYSCL_ISSUETUGID = 0375,

	/* system call number for lchown() */
	SYSCL_LCHOWN = 0376,

	/* system call number for getsid() */
	SYSCL_GETSID = 0377,

	/* system call number for msync() */
	SYSCL_MSYNC = 0400,

	/* system call number for semctl35() */
//	SYSCL_OBS_SEMCTL35 = 0401,

	/* system call number for shmctl35() */
//	SYSCL_OBS_SHMCTL35 = 0402,

	/* system call number for msgctl35() */
//	SYSCL_OBS_MSGCTL35 = 0403,

	/* free system call number */
//	SYSCL_ = 0404,

	/* free system call number */
//	SYSCL_ = 0405,

	/* free system call number */
//	SYSCL_ = 0406,

	/* system call number for pipe() */
	SYSCL_PIPE = 0407,

	/* system call number for fhopen() */
	SYSCL_FHOPEN = 0410,

	/* free system call number */
//	SYSCL_ = 0411,

	/* free system call number */
//	SYSCL_ = 0412,

	/* system call number for preadv() */
	SYSCL_PREADV = 0413,

	/* system call number for pwritev() */
	SYSCL_PWRITEV = 0414,

	/* system call number for kqueue() */
	SYSCL_KQUEUE = 0415,

	/* system call number for t32_kevent() */
//	SYSCL_OBS_T32_KEVENT = 0416,

	/* system call number for mlockall() */
	SYSCL_MLOCKALL = 0417,

	/* system call number for munlockall() */
	SYSCL_MUNLOCKALL = 0420,

	/* free system call number */
//	SYSCL_ = 0421,

	/* free system call number */
//	SYSCL_ = 0422,

	/* free system call number */
//	SYSCL_ = 0423,

	/* free system call number */
//	SYSCL_ = 0424,

	/* free system call number */
//	SYSCL_ = 0425,

	/* free system call number */
//	SYSCL_ = 0426,

	/* free system call number */
//	SYSCL_ = 0427,

	/* free system call number */
//	SYSCL_ = 0430,

	/* system call number for getresuid() */
	SYSCL_GETRESUID = 0431,

	/* system call number for setresuid() */
	SYSCL_SETRESUID = 0432,

	/* system call number for getresgid() */
	SYSCL_GETRESGID = 0433,

	/* system call number for setresgid() */
	SYSCL_SETRESGID = 0434,

	/* system call number for omquery() */
//	SYSCL_OBS_OMQUERY = 0435,

	/* system call number for mquery() */
	SYSCL_MQUERY = 0436,

	/* system call number for closefrom() */
	SYSCL_CLOSEFROM = 0437,

	/* system call number for sigaltstack() */
	SYSCL_SIGALTSTACK = 0440,

	/* system call number for shmget() */
	SYSCL_SHMGET = 0441,

	/* system call number for semop() */
	SYSCL_SEMOP = 0442,

	/* system call number for t32_stat() */
//	SYSCL_OBS_T32_STAT = 0443,

	/* system call number for t32_fstat() */
//	SYSCL_OBS_T32_FSTAT = 0444,

	/* system call number for t32_lstat() */
//	SYSCL_OBS_T32_LSTAT = 0445,

	/* system call number for fhstat() */
	SYSCL_FHSTAT = 0446,

	/* system call number for __semctl() */
	SYSCL_SEMCTL = 0447,

	/* system call number for shmctl() */
	SYSCL_SHMCTL = 0450,

	/* system call number for msgctl() */
	SYSCL_MSGCTL = 0451,

	/* system call number for yield() */
	SYSCL_SCHED_YIELD = 0452,

	/* system call number for getthrid() */
	SYSCL_GETTHRID = 0453,

	/* system call number for t32___thrsleep() */
//	SYSCL_OBS_T32_THRSLEEP = 0454,

	/* system call number for __thrwakeup() */
	SYSCL_THRWAKEUP = 0455,

	/* system call number for __threxit() */
	SYSCL_THREXIT = 0456,

	/* system call number for __thrsigdivert() */
	SYSCL_THRSIGDIVERT = 0457,

	/* system call number for __getcwd() */
	SYSCL_GETCWD = 0460,

	/* system call number for adjfreq() */
	SYSCL_ADJFREQ = 0461,

	/* system call number for getfsstat53() */
//	SYSCL_OBS_GETFSSTAT53 = 0462,

	/* system call number for statfs53() */
//	SYSCL_OBS_STATFS53 = 0463,

	/* system call number for fstatfs53() */
//	SYSCL_OBS_FSTATFS53 = 0464,

	/* system call number for fhstatfs53() */
//	SYSCL_OBS_FHSTATFS53 = 0465,

	/* system call number for setrtable() */
	SYSCL_SETRTABLE = 0466,

	/* system call number for getrtable() */
	SYSCL_GETRTABLE = 0467,

	/* system call number for t32_getdirentries() */
//	SYSCL_OBS_T32_GDIRENTS = 0470,

	/* system call number for faccessat() */
	SYSCL_FACCESSAT = 0471,

	/* system call number for fchmodat() */
	SYSCL_FCHMODAT = 0472,

	/* system call number for fchownat() */
	SYSCL_FCHOWNAT = 0473,

	/* system call number for t32_fstatat() */
//	SYSCL_OBS_T32_FSTATAT = 0474,

	/* system call number for linkat() */
	SYSCL_LINKAT = 0475,

	/* system call number for mkdirat() */
	SYSCL_MKDIRAT = 0476,

	/* system call number for mkfifoat() */
	SYSCL_MKFIFOAT = 0477,

	/* system call number for mknodat() */
	SYSCL_MKNODAT = 0500,

	/* system call number for openat() */
	SYSCL_OPENAT = 0501,

	/* system call number for readlinkat() */
	SYSCL_READLINKAT = 0502,

	/* system call number for renameat() */
	SYSCL_RENAMEAT = 0503,

	/* system call number for symlinkat() */
	SYSCL_SYMLINKAT = 0504,

	/* system call number for unlinkat() */
	SYSCL_UNLINKAT = 0505,

	/* system call number for t32_utimensat() */
//	SYSCL_OBS_T32_UTIMNSAT = 0506,

	/* system call number for t32_futimens() */
//	SYSCL_OBS_T32_FUTIMNS = 0507,

	/* system call number for __tfork51() */
//	SYSCL_OBS_TFORK51 = 0510,

	/* system call number for __set_tcb() */
	SYSCL_SET_TCB = 0511,

	/* system call number for __get_tcb() */
	SYSCL_GET_TCB = 0512,

	/* system call maximum */
	SYSCL_MAX = 0513
};
