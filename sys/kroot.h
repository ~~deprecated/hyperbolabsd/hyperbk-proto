/*
 * Kernel root header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

enum kroot_t {
	/* kernel root magic */
	KROOT_MAGIC = 0107712,

	/* kernel root magic segment */
	KROOT_MAGIC_SEGMENT = 0107654
};

struct kroot_physical_ram_t {
	struct {
		u_int64_t start_address;
		u_int64_t size_bytes;
	} segment_s;
};

struct kroot_header_t {
	struct {
		u_int	magic_mid_flag;	/* magic machine id flag */
		u_short	size;
	} data_s;
	struct {
		u_short size;
		u_int	number;
	} segment_s;
};

struct kroot_segment_t {
	struct {
		u_int magic_mid_flag;	/* magic machine id flag */
		u_int size;
	} data_s;
};
