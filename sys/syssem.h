/*
 * Semaphore header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _SYS_IPC_H_
#include <sys/ipc.h>
#endif

#ifdef __BSD_VISIBLE
#include <sys/sysctl.h>

enum _syssem_info_t {
	/* system semaphore information identifiers maximum */
	SYSSEM_INFO_IDMAX = 0x0001,

	/* system semaphore information system maximum */
	SYSSEM_INFO_SYSMAX = 0x0002,

	/* system semaphore information undo maximum */
	SYSSEM_INFO_UNDOMAX = 0x0003,

	/*
	 * system semaphore information semaphores per id (identifier?)
	 * maximum
	 */
	SYSSEM_INFO_SEMPIDMAX = 0x0004,

	/* system semaphore information operations per call maximum */
	SYSSEM_INFO_OPPCALLMAX = 0x0005,

	/* system semaphore information undo entries per process maximum */
	SYSSEM_INFO_UEPPROCMAX = 0x0006,

	/* system semaphore information undo size */
	SYSSEM_INFO_UNDOSIZE = 0x0007,

	/* system semaphore information value maximum */
	SYSSEM_INFO_VALUEMAX = 0x0008,

	/* system semaphore information adjust on exit maximum */
	SYSSEM_INFO_ADJOXMAX = 0x0009,

	/* system semaphore information valid ids (identifiers?) count */
	SYSSEM_INFO_VIDSCNT = 0x000a
};

/* system semaphore information control names */
static const unsigned char SYSSEM_INFO_CTLNAMES[][2][7] = {
	{0, 0},
	{"semmni", CTLTYPE_INT},
	{"semmns", CTLTYPE_INT},
	{"semmnu", CTLTYPE_INT},
	{"semmsl", CTLTYPE_INT},
	{"semopm", CTLTYPE_INT},
	{"semume", CTLTYPE_INT},
	{"semusz", CTLTYPE_INT},
	{"semvmx", CTLTYPE_INT},
	{"semaem", CTLTYPE_INT}
};

enum symsem_undo_t {
	/* system semaphore undo */
	SYSSEM_UNDO = 0x2000
};

enum symsem_ctrl_t  {
	/* system semaphore control count get */
	SYSSEM_CTRL_COUNT_GET = 0x0003,

	/* system semaphore control process id (identifier?) get */
	SYSSEM_CTRL_PROCID_GET = 0x0004,

	/* system semaphore control value get */
	SYSSEM_CTRL_VALUE_GET = 0x0005,

	/* system semaphore control all get */
	SYSSEM_CTRL_ALL_GET = 0x0006,

	/* system semaphore control z count get */
	SYSSEM_CTRL_ZCOUNT_GET = 0x0007,

	/* system semaphore control value set */
	SYSSEM_CTRL_VALUE_SET = 0x0008,

	/* system semaphore control all set */
	SYSSEM_CTRL_ALL_SET = 0x0009
};

enum syssem_perms_t {
	/* system semaphore permissions alter */
	SYSSEM_PERMS_ALTER = 0x0080,

	/* system semaphore permissions read */
	SYSSEM_PERMS_READ = 0x0100
};
#endif

#ifdef _KERNEL
enum _syssem_t {
	/* system semaphore value maximum */
	SYSSEM_VALUE_MAXIMUM = 0x7fff,

	/* system semaphore adjust on exit maximum */
	SYSSEM_ADJOX_MAXIMUM = 0x4000,

	/* system semaphore identifiers maximum */
	SYSSEM_IDS_MAXIMUM = 0x000a,

	/* system semaphore system maximum */
	SYSSEM_SYSTEM_MAXIMUM = 0x003c,

	/* system semaphore undo entries per process maximum */
	SYSSEM_UEPPROC_MAXIMUM = 0x000a,

	/* system semaphore undo maximum */
	SYSSEM_UNDO_MAXIMUM = 0x001e,

	/* system semaphore semaphores per id (identifier?) maximum */
	SYSSEM_SEMPID_MAXIMUM = 0x003c,

	/* system semaphore operations per call maximum */
	SYSSEM_OPPCALL_MAXIMUM = 0x0064,

	/* system semaphore undo size */
	SYSSEM_UNDO_SIZE = SYSSEM_UEPPROC_MAXIMUM * (sizeof(short) * 03 +
	    sizeof(int) * 01) + (sizeof(struct process *) * 02 + sizeof(short *)
	    * 03 + sizeof(short) * 03 + sizeof(int *) * 01 + sizeof(int) * 01)
};
#endif

union syssem_union_t {
	struct {
		u_int16_t *pointer;
	} array_s;
	struct {
		int32_t value;
	} data_s;
	/* system semaphore id data structure */
	struct syssem_id_datastruct_t {
		struct {
			struct ipc_perm permission_s;
		} ipc_s;
		struct {
			struct syssem_t {
				struct {
					u_int16_t value;
				} data_s;
				struct {
					int32_t id;
				} process_s;
				struct {
					u_int16_t number;
					u_int16_t zero;
				} awaiting_count_s;
			} *pointer_s;
		} base_s;
		struct {
			u_int16_t number;
		} symsems_s;
		struct {
			int64_t time;
		} last_operation_s;
		struct {
			long svabi;
		} pad_first_s;
		struct {
			int64_t time;
		} last_change_s;
		struct {
			long svabi;
		} pad_s;
		struct {
			long svabi;
		} pad_last_s[4];
	} buffer_s;
};

struct syssem_buffer_t {
	struct {
		u_int16_t number;
	} data_s;
	struct {
		int16_t value;
		int16_t flags;
	} operation_s;
};

#ifdef _KERNEL
struct syssem_undo_t {
	struct {
		struct {
			struct syssem_undo_t *pointer_s;
		} singly_lnlste_s;	/* singly linked list entry */
	} next_s;
	struct {
		struct process *pointer_s;
	} process_s;
	struct {
		int16_t count;
	} data_s;
	/* system semaphore undo entries */
	struct syssem_undoe_t {
		struct {
			int16_t adjust_on_exit;
			int16_t number;
			int32_t id;
		} data_s[1];
	} entries_s;
};

/* system semaphore system control */
struct syssem_sysctrl_t {
	struct {
		/* system semaphore information */
		struct syssem_info_t {
			struct {
				int32_t identifiers;
				int32_t system;
				int32_t undo;
				/* semaphores per id */
				int32_t sems_per_id;
				/* operations per call */
				int32_t ops_per_call;
				/* undo entries per process */
				int32_t ues_per_proc;
			} maximum_n_s;	/* maximum number */
			struct {
				int32_t undo;
			} size_s;
			struct {
				int32_t value;
				int32_t adjust_on_exit;
			} maximum_s;
		} information_s;
	} data_s;
	struct {
		struct syssem_id_datastruct_t id_data_s[1];
	} structure_s;
};
extern struct {
	struct syssem_info_t data_s;
} syssem_info_s;	/* system semaphore information */
extern struct {
	struct syssem_id_datastruct_t **dpointer_s;
} syssem_idlist_s;
#else
__BEGIN_DECLS
__END_DECLS
#endif
