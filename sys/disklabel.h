
/*
 * Copyright (c) 1987, 1988, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * Modifications to support HyperbolaBSD:
 * Copyright (c) 2020 Hyperbola Project
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)disklabel.h	8.2 (Berkeley) 7/10/94
 */

/*
 * Disk description table, see disktab(5)
 */
#define	_PATH_DISKTAB	"/etc/disktab"
#define	DISKTAB		"/etc/disktab"		/* deprecated */

/*
 * Each disk has a label which includes information about the hardware
 * disk geometry, filesystem partitions, and drive specific information.
 * The location of the label, as well as the number of partitions the
 * label can describe and the number of the "whole disk" (raw)
 * partition are machine dependent.
 */
#include <machine/disklabel.h>

#include <sys/uuid.h>

/*
 * The absolute maximum number of disk partitions allowed.
 * This is the maximum value of MAXPARTITIONS for which 'struct disklabel'
 * is <= DEV_BSIZE bytes long.  If MAXPARTITIONS is greater than this, beware.
 */
#define	MAXMAXPARTITIONS	22
#if MAXPARTITIONS > MAXMAXPARTITIONS
#warn beware: MAXPARTITIONS bigger than MAXMAXPARTITIONS
#endif

/*
 * Translate between device numbers and major/disk unit/disk partition.
 */
#define	DISKUNIT(dev)	(minor(dev) / MAXPARTITIONS)
#define	DISKPART(dev)	(minor(dev) % MAXPARTITIONS)
#define	RAW_PART	2	/* 'c' partition */
#define	DISKMINOR(unit, part) \
    (((unit) * MAXPARTITIONS) + (part))
#define	MAKEDISKDEV(maj, unit, part) \
    (makedev((maj), DISKMINOR((unit), (part))))
#define	DISKLABELDEV(dev) \
    (MAKEDISKDEV(major(dev), DISKUNIT(dev), RAW_PART))

#define DISKMAGIC	((u_int32_t)0x82564557)	/* The disk magic number */

#define MAXDISKSIZE	0x7fffffffffffLL	/* 47 bits of reach */

#ifndef _LOCORE
struct disklabel {
	u_int32_t d_magic;		/* the magic number */
	u_int16_t d_type;		/* drive type */
	u_int16_t d_subtype;		/* controller/d_type specific */
	char	  d_typename[16];	/* type name, e.g. "eagle" */
	char	  d_packname[16];	/* pack identifier */

			/* disk geometry: */
	u_int32_t d_secsize;		/* # of bytes per sector */
	u_int32_t d_nsectors;		/* # of data sectors per track */
	u_int32_t d_ntracks;		/* # of tracks per cylinder */
	u_int32_t d_ncylinders;		/* # of data cylinders per unit */
	u_int32_t d_secpercyl;		/* # of data sectors per cylinder */
	u_int32_t d_secperunit;		/* # of data sectors (low part) */

	u_char	d_uid[8];		/* Unique label identifier. */

	/*
	 * Alternate cylinders include maintenance, replacement, configuration
	 * description areas, etc.
	 */
	u_int32_t d_acylinders;		/* # of alt. cylinders per unit */

			/* hardware characteristics: */
	u_int16_t d_bstarth;		/* start of useable reg. (high part) */
	u_int16_t d_bendh;		/* size of useable reg. (high part) */
	u_int32_t d_bstart;		/* start of useable region */
	u_int32_t d_bend;		/* end of useable region */
	u_int32_t d_flags;		/* generic flags */
#define NDDATA 5
	u_int32_t d_drivedata[NDDATA];	/* drive-type specific information */
	u_int16_t d_secperunith;	/* # of data sectors (high part) */
	u_int16_t d_version;		/* version # (1=48 bit addressing) */
#define NSPARE 4
	u_int32_t d_spare[NSPARE];	/* reserved for future use */
	u_int32_t d_magic2;		/* the magic number (again) */
	u_int16_t d_checksum;		/* xor of data incl. partitions */

			/* filesystem and partition information: */
	u_int16_t d_npartitions;	/* number of partitions in following */
	u_int32_t d_bbsize;		/* size of boot area at sn0, bytes */
	u_int32_t d_sbsize;		/* max size of fs superblock, bytes */
	struct	partition {		/* the partition table */
		u_int32_t p_size;	/* number of sectors (low part) */
		u_int32_t p_offset;	/* starting sector (low part) */
		u_int16_t p_offseth;	/* starting sector (high part) */
		u_int16_t p_sizeh;	/* number of sectors (high part) */
		u_int8_t p_fstype;	/* filesystem type, see below */
		u_int8_t p_fragblock;	/* encoded filesystem frag/block */
		u_int16_t p_cpg;	/* UFS: FS cylinders per group */
	} d_partitions[MAXPARTITIONS];	/* actually may be more */
};


struct	__partitionv0 {		/* old (v0) partition table entry */
	u_int32_t p_size;	/* number of sectors in partition */
	u_int32_t p_offset;	/* starting sector */
	u_int32_t p_fsize;	/* filesystem basic fragment size */
	u_int8_t p_fstype;	/* filesystem type, see below */
	u_int8_t p_frag;	/* filesystem fragments per block */
	union {
		u_int16_t cpg;	/* UFS: FS cylinders per group */
		u_int16_t sgs;	/* LFS: FS segment shift */
	} __partitionv0_u1;
};
#endif /* _LOCORE */


#define DISKLABELV1_FFS_FRAGBLOCK(fsize, frag) 			\
	((fsize) * (frag) == 0 ? 0 :				\
	(((ffs((fsize) * (frag)) - 13) << 3) | (ffs(frag))))

#define DISKLABELV1_FFS_BSIZE(i) ((i) == 0 ? 0 : (1 << (((i) >> 3) + 12)))
#define DISKLABELV1_FFS_FRAG(i) ((i) == 0 ? 0 : (1 << (((i) & 0x07) - 1)))
#define DISKLABELV1_FFS_FSIZE(i) (DISKLABELV1_FFS_FRAG(i) == 0 ? 0 : \
	(DISKLABELV1_FFS_BSIZE(i) / DISKLABELV1_FFS_FRAG(i)))

#define DL_GETPSIZE(p)		(((u_int64_t)(p)->p_sizeh << 32) + (p)->p_size)
#define DL_SETPSIZE(p, n)	do { \
					u_int64_t x = (n); \
					(p)->p_sizeh = x >> 32; \
					(p)->p_size = x; \
				} while (0)
#define DL_GETPOFFSET(p)	(((u_int64_t)(p)->p_offseth << 32) + \
				    (p)->p_offset)
#define DL_SETPOFFSET(p, n)	do { \
					u_int64_t x = (n); \
					(p)->p_offseth = x >> 32; \
					(p)->p_offset = x; \
				} while (0)

#define DL_GETDSIZE(d)		(((u_int64_t)(d)->d_secperunith << 32) + \
				    (d)->d_secperunit)
#define DL_SETDSIZE(d, n)	do { \
					u_int64_t x = (n); \
					(d)->d_secperunith = x >> 32; \
					(d)->d_secperunit = x; \
				} while (0)
#define DL_GETBSTART(d)		(((u_int64_t)(d)->d_bstarth << 32) + \
				    (d)->d_bstart)
#define DL_SETBSTART(d, n)	do { \
					u_int64_t x = (n); \
					(d)->d_bstarth = x >> 32; \
					(d)->d_bstart = x; \
				} while (0)
#define DL_GETBEND(d)		(((u_int64_t)(d)->d_bendh << 32) + \
				    (d)->d_bend)
#define DL_SETBEND(d, n)	do { \
					u_int64_t x = (n); \
					(d)->d_bendh = x >> 32; \
					(d)->d_bend = x; \
				} while (0)

#define DL_BLKSPERSEC(d)	((d)->d_secsize / DEV_BSIZE)
#define DL_SECTOBLK(d, n)	((n) * DL_BLKSPERSEC(d))
#define DL_BLKTOSEC(d, n)	((n) / DL_BLKSPERSEC(d))
#define DL_BLKOFFSET(d, n)	(((n) % DL_BLKSPERSEC(d)) * DEV_BSIZE)

/* d_type values: */
#define	DTYPE_SMD		1		/* SMD, XSMD; VAX hp/up */
#define	DTYPE_MSCP		2		/* MSCP */
#define	DTYPE_DEC		3		/* other DEC (rk, rl) */
#define	DTYPE_SCSI		4		/* SCSI */
#define	DTYPE_ESDI		5		/* ESDI interface */
#define	DTYPE_ST506		6		/* ST506 etc. */
#define	DTYPE_HPIB		7		/* CS/80 on HP-IB */
#define	DTYPE_HPFL		8		/* HP Fiber-link */
#define	DTYPE_FLOPPY		10		/* floppy */
#define	DTYPE_CCD		11		/* was: concatenated diskdev */
#define	DTYPE_VND		12		/* vnode pseudo-disk */
#define	DTYPE_ATAPI		13		/* ATAPI */
#define DTYPE_RAID		14		/* was: RAIDframe */
#define DTYPE_RDROOT		15		/* ram disk root */

#ifdef DKTYPENAMES
static char *dktypenames[] = {
	"unknown",
	"SMD",
	"MSCP",
	"old DEC",
	"SCSI",
	"ESDI",
	"ST506",
	"HP-IB",
	"HP-FL",
	"type 9",
	"floppy",
	"ccd",			/* deprecated */
	"vnd",
	"ATAPI",
	"RAID",
	"rdroot",
	NULL
};
#define DKMAXTYPES	(sizeof(dktypenames) / sizeof(dktypenames[0]) - 1)
#endif

/*
 * Filesystem type and version.
 * Used to interpret other filesystem-specific
 * per-partition information.
 */
#define FS_UNUSED	0		/* unused */
#define FS_SWAP		1		/* swap */
#define FS_V6		2		/* Sixth Edition */
#define FS_V7		3		/* Seventh Edition */
#define FS_SYSV		4		/* System V */
#define FS_V71K		5		/* V7 with 1K blocks (4.1, 2.9) */
#define FS_V8		6		/* Eighth Edition, 4K blocks */
#define FS_BSDFFS	7		/* 4.2BSD fast file system */
#define FS_MSDOS	8		/* MSDOS file system */
#define FS_BSDLFS	9		/* 4.4BSD log-structured file system */
#define FS_OTHER	10		/* in use, but unknown/unsupported */
#define FS_HPFS		11		/* OS/2 high-performance file system */
#define FS_ISO9660	12		/* ISO 9660, normally CD-ROM */
#define FS_BOOT		13		/* partition contains bootstrap */
#define FS_ADOS		14		/* AmigaDOS fast file system */
#define FS_HFS		15		/* Macintosh HFS */
#define FS_ADFS		16		/* Acorn Disk Filing System */
#define FS_EXT2FS	17		/* Linux kernel ext2 to ext4 fs */
#define FS_CCD		18		/* ccd component */
#define FS_RAID		19		/* RAIDframe or softraid */
#define FS_NTFS		20		/* NT file system */
#define FS_UDF		21		/* UDF (DVD) file system */

#ifdef DKTYPENAMES
static char *fstypenames[] = {
	"unused",
	"swap",
	"Version6",
	"Version7",
	"SystemV",
	"4.1BSD",
	"Eighth-Edition",
	"4.2BSD",
	"MSDOS",
	"4.4LFS",
	"unknown",
	"HPFS",
	"ISO9660",
	"boot",
	"ADOS",
	"HFS",
	"ADFS",
	"ext2fs",
	"ccd",
	"RAID",
	"NTFS",
	"UDF",
	NULL
};

/* Similar to the above, but used for things like the mount command. */
static char *fstypesnames[] = {
	"",		/* 0 */
	"",		/* 1 */
	"",		/* 2 */
	"",		/* 3 */
	"",		/* 4 */
	"",		/* 5 */
	"",		/* 6 */
	"ffs",		/* 7 */
	"msdos",	/* 8 */
	"lfs",		/* 9 */
	"",		/* 10 */
	"",		/* 11 */
	"cd9660",	/* 12 */
	"",		/* 13 */
	"ados",		/* 14 */
	"",		/* 15 */
	"",		/* 16 */
	"ext2fs",	/* 17 */
	"",		/* 18 */
	"",		/* 19 */
	"ntfs",		/* 20 */
	"udf",		/* 21 */
	NULL
};

#define FSMAXTYPES	(sizeof(fstypenames) / sizeof(fstypenames[0]) - 1)
#endif

/*
 * flags shared by various drives:
 */
#define		D_BADSECT	0x04		/* supports bad sector forw. */
#define		D_VENDOR	0x08		/* vendor disklabel */

/*
 * Drive data for SMD.
 */
#define	d_smdflags	d_drivedata[0]
#define		D_SSE		0x1		/* supports skip sectoring */
#define	d_mindist	d_drivedata[1]
#define	d_maxdist	d_drivedata[2]
#define	d_sdist		d_drivedata[3]

/*
 * Drive data for ST506.
 */
#define d_precompcyl	d_drivedata[0]
#define d_gap3		d_drivedata[1]		/* used only when formatting */

/*
 * Drive data for SCSI.
 */
#define	d_blind		d_drivedata[0]

#ifndef _LOCORE
/*
 * Structure used internally to retrieve information about a partition
 * on a disk.
 */
struct partinfo {
	struct disklabel *disklab;
	struct partition *part;
};

/* GUID partition table -- located at sector 1 of some disks. */
#define	GPTSECTOR		1	/* DOS boot block relative sector # */
#define	GPTSIGNATURE		0x5452415020494645LL
				/* ASCII string "EFI PART" encoded as 64-bit */
#define	GPTREVISION		0x10000		/* GPT header version 1.0 */
#define	NGPTPARTITIONS		128
#define	GPTDOSACTIVE		0x2
#define	GPTMINHDRSIZE		92
#define	GPTMINPARTSIZE		128
#define	GPTPARTNAMESIZE		36

/* all values in the GPT need to be little endian as per UEFI specification */
struct gpt_header {
	u_int64_t gh_sig;	/* "EFI PART" */
	u_int32_t gh_rev;	/* GPT Version 1.0: 0x00000100 */
	u_int32_t gh_size;	/* Little-Endian */
	u_int32_t gh_csum;	/* CRC32: with this field as 0 */
	u_int32_t gh_rsvd;	/* always zero */
	u_int64_t gh_lba_self;	/* LBA of this header */
	u_int64_t gh_lba_alt;	/* LBA of alternate header */
	u_int64_t gh_lba_start;	/* first usable LBA */
	u_int64_t gh_lba_end;	/* last usable LBA */
	struct uuid gh_guid;	/* disk GUID used to identify the disk */
	u_int64_t gh_part_lba;	/* starting LBA of GPT partition entries */
	u_int32_t gh_part_num;	/* # of partition entries */
	u_int32_t gh_part_size;	/* size per entry, shall be 128*(2**n)
				   with n >= 0 */
	u_int32_t gh_part_csum;	/* CRC32 checksum of all partition entries:
				 * starts at gh_part_lba and is computed over
				 * a byte length of gh_part_num*gh_part_size */
	/* the rest of the block is reserved by UEFI and must be zero */
};

struct gpt_partition {
	struct uuid gp_type;	/* partition type GUID */
	struct uuid gp_guid;	/* unique partition GUID */
	u_int64_t gp_lba_start;	/* starting LBA of this partition */
	u_int64_t gp_lba_end;	/* ending LBA of this partition, inclusive,
				   usually odd */
	u_int64_t gp_attrs;	/* attribute flags */
	u_int16_t gp_name[GPTPARTNAMESIZE]; /* partition name, utf-16le */
	/* the rest of the GPT partition entry, if any, is reserved by UEFI
	   and must be zero */
};

/*
 * Known GPT partition types.
 */

/* unused partition type */
#define GPT_GUID_UNUSED \
    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, \
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

/* EFI System partition type */
#define GPT_GUID_EFI_SYSTEM \
    { 0xc1, 0x2a, 0x73, 0x28, 0xf8, 0x1f, 0x11, 0xd2, \
      0xba, 0x4b, 0x00, 0xa0, 0xc9, 0x3e, 0xc9, 0x3b }

/* BIOS boot partition type */
#define GPT_GUID_BIOS_BOOT \
    { 0x21, 0x68, 0x61, 0x48, 0x64, 0x49, 0x6e, 0x6f, \
      0x74, 0x4e, 0x65, 0x65, 0x64, 0x45, 0x46, 0x49 }

/* MBR partition scheme */
#define GPT_GUID_MBR \
    { 0x02, 0x4d, 0xee, 0x41, 0x33, 0x37, 0x11, 0xd3, \
      0x9d, 0x69, 0x00, 0x08, 0xc7, 0x81, 0xf3, 0x9f }

/* HyperbolaBSD partition type */
#define GPT_GUID_HBSD \
    { 0x45, 0xed, 0xeb, 0x33, 0x49, 0xa3, 0x41, 0x14, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperbolaBSD x86_32/i686 partition type */
#define GPT_GUID_HBSD_X32 \
    { 0x45, 0xed, 0xe8, 0x50, 0x49, 0xa3, 0x41, 0x14, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperbolaBSD x86_64/amd64 partition type */
#define GPT_GUID_HBSD_X64 \
    { 0x45, 0xed, 0xe8, 0x51, 0x49, 0xa3, 0x41, 0x14, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperbolaBSD arm32/armv7 partition type */
#define GPT_GUID_HBSD_A32 \
    { 0x45, 0xed, 0xe8, 0x10, 0x49, 0xa3, 0x41, 0x14, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperbolaBSD arm64/aarch64 partition type */
#define GPT_GUID_HBSD_A64 \
    { 0x45, 0xed, 0xe8, 0x11, 0x49, 0xa3, 0x41, 0x14, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperbolaBSD /home partition type */
#define GPT_GUID_HBSD_HOME \
    { 0x45, 0xed, 0xcd, 0x5a, 0x49, 0xa3, 0x41, 0x14, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperbolaBSD /srv partition type */
#define GPT_GUID_HBSD_SRV \
    { 0x45, 0xed, 0x8a, 0x6e, 0x49, 0xa3, 0x41, 0x14, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperbolaBSD swap partition type */
#define GPT_GUID_HBSD_SWAP \
    { 0x45, 0xed, 0xb4, 0x37, 0x49, 0xa3, 0x41, 0x14, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperbolaBSD RAID partition type */
#define GPT_GUID_HBSD_RAID \
    { 0x45, 0xed, 0xa5, 0xfb, 0x49, 0xa3, 0x41, 0x14, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperbolaBSD slice; contains subpartitions */
#define GPT_GUID_HBSD_SL \
    { 0x45, 0xed, 0x64, 0x0a, 0x49, 0xa3, 0x41, 0x14, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperBK (for other systems with HyperBK) partition type */
#define GPT_GUID_HYPERBK \
    { 0x45, 0xed, 0xeb, 0x33, 0x88, 0xe6, 0x69, 0xc0, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperBK x86_32/i686 (for other systems with HyperBK) partition type */
#define GPT_GUID_HYPERBK_X32 \
    { 0x45, 0xed, 0xe8, 0x50, 0x88, 0xe6, 0x69, 0xc0, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperBK x86_64/amd64 (for other systems with HyperBK) partition type */
#define GPT_GUID_HYPERBK_X64 \
    { 0x45, 0xed, 0xe8, 0x51, 0x88, 0xe6, 0x69, 0xc0, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperBK arm32/armv7 (for other systems with HyperBK) partition type */
#define GPT_GUID_HYPERBK_A32 \
    { 0x45, 0xed, 0xe8, 0x10, 0x88, 0xe6, 0x69, 0xc0, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperBK arm64/aarch64 (for other systems with HyperBK) partition type */
#define GPT_GUID_HYPERBK_A64 \
    { 0x45, 0xed, 0xe8, 0x11, 0x88, 0xe6, 0x69, 0xc0, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperBK /home (for other systems with HyperBK) partition type */
#define GPT_GUID_HYPERBK_HOME \
    { 0x45, 0xed, 0xcd, 0x5a, 0x88, 0xe6, 0x69, 0xc0, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperBK /srv (for other systems with HyperBK) partition type */
#define GPT_GUID_HYPERBK_SRV \
    { 0x45, 0xed, 0x8a, 0x6e, 0x88, 0xe6, 0x69, 0xc0, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperBK swap (for other systems with HyperBK) partition type */
#define GPT_GUID_HYPERBK_SWAP \
    { 0x45, 0xed, 0xb4, 0x37, 0x88, 0xe6, 0x69, 0xc0, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperBK RAID (for other systems with HyperBK) partition type */
#define GPT_GUID_HYPERBK_RAID \
    { 0x45, 0xed, 0xa5, 0xfb, 0x88, 0xe6, 0x69, 0xc0, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* HyperBK slice (for other systems with HyperBK); contains subpartitions */
#define GPT_GUID_HYPERBK_SL \
    { 0x45, 0xed, 0x64, 0x0a, 0x88, 0xe6, 0x69, 0xc0, \
      0xb6, 0x5a, 0x51, 0xba, 0xfc, 0x41, 0x9b, 0xec }

/* System partition types */
#if defined(__HYPERBOLABSD__) && !defined(__HYPERBK__)
#define GPT_GUID_SYS GPT_GUID_HBSD
#define GPT_GUID_SYS_X32 GPT_GUID_HBSD_X32
#define GPT_GUID_SYS_X64 GPT_GUID_HBSD_X64
#define GPT_GUID_SYS_A32 GPT_GUID_HBSD_A32
#define GPT_GUID_SYS_A64 GPT_GUID_HBSD_A64
#define GPT_GUID_SYS_HOME GPT_GUID_HBSD_HOME
#define GPT_GUID_SYS_SRV GPT_GUID_HBSD_SRV
#define GPT_GUID_SYS_SWAP GPT_GUID_HBSD_SWAP
#define GPT_GUID_SYS_RAID GPT_GUID_HBSD_RAID
#define GPT_GUID_SYS_SL GPT_GUID_HBSD_SL
#elif defined(__HYPERBK__) && !defined(__HYPERBOLABSD__)
#define GPT_GUID_SYS GPT_GUID_HYPERBK
#define GPT_GUID_SYS_X32 GPT_GUID_HYPERBK_X32
#define GPT_GUID_SYS_X64 GPT_GUID_HYPERBK_X64
#define GPT_GUID_SYS_A32 GPT_GUID_HYPERBK_A32
#define GPT_GUID_SYS_A64 GPT_GUID_HYPERBK_A64
#define GPT_GUID_SYS_HOME GPT_GUID_HYPERBK_HOME
#define GPT_GUID_SYS_SRV GPT_GUID_HYPERBK_SRV
#define GPT_GUID_SYS_SWAP GPT_GUID_HYPERBK_SWAP
#define GPT_GUID_SYS_RAID GPT_GUID_HYPERBK_RAID
#define GPT_GUID_SYS_SL GPT_GUID_HYPERBK_SL
#endif

/* OpenBSD partition type */
#define GPT_GUID_OPENBSD \
    { 0x82, 0x4c, 0xc7, 0xa0, 0x36, 0xa8, 0x11, 0xe3, \
      0x89, 0x0a, 0x95, 0x25, 0x19, 0xad, 0x3f, 0x61 }

/* Linux kernel partition type */
#define GPT_GUID_LINUX \
    { 0x0f, 0xc6, 0x3d, 0xaf, 0x84, 0x83, 0x47, 0x72, \
      0x8e, 0x79, 0x3d, 0x69, 0xd8, 0x47, 0x7d, 0xe4 }

/* Linux kernel x86_32/i*86 partition type */
#define GPT_GUID_LINUX_X32 \
    { 0x44, 0x47, 0x95, 0x40, 0xf2, 0x97, 0x41, 0xb2, \
      0x9a, 0xf7, 0xd1, 0x31, 0xd5, 0xf0, 0x45, 0x8a }

/* Linux kernel x86_64/AMD64 partition type */
#define GPT_GUID_LINUX_X64 \
    { 0x4f, 0x68, 0xbc, 0xe3, 0xe8, 0xcd, 0x4d, 0xb1, \
      0x96, 0xe7, 0xfb, 0xca, 0xf9, 0x84, 0xb7, 0x09 }

/* Linux kernel ARM32 partition type */
#define GPT_GUID_LINUX_A32 \
    { 0x69, 0xda, 0xd7, 0x10, 0x2c, 0xe4, 0x4e, 0x3c, \
      0xb1, 0x6c, 0x21, 0xa1, 0xd4, 0x9a, 0xbe, 0xd3 }

/* Linux kernel ARM64/aarch64 partition type */
#define GPT_GUID_LINUX_A64 \
    { 0xb9, 0x21, 0xb0, 0x45, 0x1d, 0xf0, 0x41, 0xc3, \
      0xaf, 0x44, 0x4c, 0x6f, 0x28, 0x0d, 0x3f, 0xae }

/* Linux kernel /boot partition type */
#define GPT_GUID_LINUX_BOOT \
    { 0xbc, 0x13, 0xc2, 0xff, 0x59, 0xe6, 0x42, 0x62, \
      0xa3, 0x52, 0xb2, 0x75, 0xfd, 0x6f, 0x71, 0x72 }

/* Linux kernel /home partition type */
#define GPT_GUID_LINUX_HOME \
    { 0x93, 0x3a, 0xc7, 0xe1, 0x2e, 0xb4, 0x4f, 0x13, \
      0xb8, 0x44, 0x0e, 0x14, 0xe2, 0xae, 0xf9, 0x15 }

/* Linux kernel /srv partition type */
#define GPT_GUID_LINUX_SRV \
    { 0x3b, 0x8f, 0x84, 0x25, 0x20, 0xe0, 0x4f, 0x3b, \
      0x90, 0x7f, 0x1a, 0x25, 0xa7, 0x6f, 0x98, 0xe8 }

/* Linux kernel swap partition type */
#define GPT_GUID_LINUX_SWAP \
    { 0x06, 0x57, 0xfd, 0x6d, 0xa4, 0xab, 0x43, 0xc4, \
      0x84, 0xe5, 0x09, 0x33, 0xc8, 0x4b, 0x4f, 0x4f }

/* Linux kernel LVM partition type */
#define GPT_GUID_LINUX_LVM \
    { 0xe6, 0xd6, 0xd3, 0x79, 0xf5, 0x07, 0x44, 0xc2, \
      0xa2, 0x3c, 0x23, 0x8f, 0x2a, 0x3d, 0xf9, 0x28 }

/* Linux kernel RAID partition type */
#define GPT_GUID_LINUX_RAID \
    { 0xa1, 0x9d, 0x88, 0x0f, 0x05, 0xfc, 0x4d, 0x3b, \
      0xa0, 0x06, 0x74, 0x3f, 0x0f, 0x84, 0x91, 0x1e }

/* Linux kernel plain dm-crypt partition type */
#define GPT_GUID_LINUX_PDMC \
    { 0x7f, 0xfe, 0xc5, 0xc9, 0x2d, 0x00, 0x49, 0xb7, \
      0x89, 0x41, 0x3e, 0xa1, 0x0a, 0x55, 0x86, 0xb7 }

/* Linux kernel LUKS partition type */
#define GPT_GUID_LINUX_LUKS \
    { 0xca, 0x7d, 0x7c, 0xcb, 0x63, 0xed, 0x4c, 0x53, \
      0x86, 0x1c, 0x17, 0x42, 0x53, 0x60, 0x59, 0xcc }

/* Linux kernel reserved partition type */
#define GPT_GUID_LINUX_RSVD \
    { 0x8d, 0xa6, 0x33, 0x39, 0x00, 0x07, 0x60, 0xc0, \
      0xc4, 0x36, 0x08, 0x3a, 0xc8, 0x23, 0x09, 0x08 }

/* Chrome OS / partition type */
#define GPT_GUID_CHROME_ROOT \
    { 0x3c, 0xb8, 0xe2, 0x02, 0x3b, 0x7e, 0x47, 0xdd, \
      0x8a, 0x3c, 0x7f, 0xf2, 0xa1, 0x3c, 0xfc, 0xec }

/* Chrome OS future use partition type */
#define GPT_GUID_CHROME_FTUSE \
    { 0x2e, 0x0a, 0x75, 0x3d, 0x9e, 0x48, 0x43, 0xb0, \
      0x83, 0x37, 0xb1, 0x51, 0x92, 0xcb, 0x1b, 0x5e }

/* CoreOS resizable rootfs partition type */
#define GPT_GUID_COREOS_ROOT \
    { 0x38, 0x84, 0xdd, 0x41, 0x85, 0x82, 0x44, 0x04, \
      0xb9, 0xa8, 0xe9, 0xb8, 0x4f, 0x2d, 0xf5, 0x0e }

/* CoreOS /usr partition type */
#define GPT_GUID_COREOS_USR \
    { 0x5d, 0xfb, 0xf5, 0xf4, 0x28, 0x48, 0x4b, 0xac, \
      0xaa, 0x5e, 0x0d, 0x9a, 0x20, 0xb7, 0x45, 0xa6 }

/* CoreOS rootfs on RAID partition type */
#define GPT_GUID_COREOS_RAID \
    { 0xbe, 0x90, 0x67, 0xb9, 0xea, 0x49, 0x4f, 0x15, \
      0xb4, 0xf6, 0xf3, 0x6f, 0x8c, 0x9e, 0x18, 0x18 }

/* CoreOS OEM customizations partition type */
#define GPT_GUID_COREOS_OEM \
    { 0xc9, 0x5d, 0xc2, 0x1a, 0xdf, 0x0e, 0x43, 0x40, \
      0x8d, 0x7b, 0x26, 0xcb, 0xfa, 0x9a, 0x03, 0xe0 }

/* Linux kernel ONIE Boot partition type */
#define GPT_GUID_ONIE_BOOT \
    { 0x74, 0x12, 0xf7, 0xd5, 0xa1, 0x56, 0x4b, 0x13, \
      0x81, 0xdc, 0x86, 0x71, 0x74, 0x92, 0x93, 0x25 }

/* Linux kernel ONIE Config partition type */
#define GPT_GUID_ONIE_CONFIG \
    { 0xd4, 0xe6, 0xe2, 0xcd, 0x44, 0x69, 0x46, 0xf3, \
      0xb5, 0xcb, 0x1b, 0xff, 0x57, 0xaf, 0xc1, 0x49 }

/* FreeBSD bootstrap partition type */
#define GPT_GUID_FREEBSD_BOOT \
    { 0x83, 0xbd, 0x6b, 0x9d, 0x7f, 0x41, 0x11, 0xdc, \
      0xbe, 0x0b, 0x00, 0x15, 0x60, 0xb8, 0x4f, 0x0f }

/* FreeBSD slice; contains subpartitions */
#define GPT_GUID_FREEBSD_SL \
    { 0x51, 0x6e, 0x7c, 0xb4, 0x6e, 0xcf, 0x11, 0xd6, \
      0x8f, 0xf8, 0x00, 0x02, 0x2d, 0x09, 0x71, 0x2b }

/* FreeBSD UFS partition type */
#define GPT_GUID_FREEBSD_UFS \
    { 0x51, 0x6e, 0x7c, 0xb6, 0x6e, 0xcf, 0x11, 0xd6, \
      0x8f, 0xf8, 0x00, 0x02, 0x2d, 0x09, 0x71, 0x2b }

/* FreeBSD ZFS volume manager partition type */
#define GPT_GUID_FREEBSD_ZFS \
    { 0x51, 0x6e, 0x7c, 0xba, 0x6e, 0xcf, 0x11, 0xd6, \
      0x8f, 0xf8, 0x00, 0x02, 0x2d, 0x09, 0x71, 0x2b }

/* FreeBSD swap partition type */
#define GPT_GUID_FREEBSD_SWAP \
    { 0x51, 0x6e, 0x7c, 0xb5, 0x6e, 0xcf, 0x11, 0xd6, \
      0x8f, 0xf8, 0x00, 0x02, 0x2d, 0x09, 0x71, 0x2b }

/* FreeBSD Vinum volume manager partition type */
#define GPT_GUID_FREEBSD_VVM \
    { 0x51, 0x6e, 0x7c, 0xb8, 0x6e, 0xcf, 0x11, 0xd6, \
      0x8f, 0xf8, 0x00, 0x02, 0x2d, 0x09, 0x71, 0x2b }

/* NetBSD FFS partition type */
#define GPT_GUID_NETBSD_FFS \
    { 0x49, 0xf4, 0x8d, 0x5a, 0xb1, 0x0e, 0x11, 0xdc, \
      0xb9, 0x9b, 0x00, 0x19, 0xd1, 0x87, 0x96, 0x48 }

/* NetBSD LFS partition type */
#define GPT_GUID_NETBSD_LFS \
    { 0x49, 0xf4, 0x8d, 0x82, 0xb1, 0x0e, 0x11, 0xdc, \
      0xb9, 0x9b, 0x00, 0x19, 0xd1, 0x87, 0x96, 0x48 }

/* NetBSD swap partition type */
#define GPT_GUID_NETBSD_SWAP \
    { 0x49, 0xf4, 0x8d, 0x32, 0xb1, 0x0e, 0x11, 0xdc, \
      0xb9, 0x9b, 0x00, 0x19, 0xd1, 0x87, 0x96, 0x48 }

/* NetBSD RAID partition type */
#define GPT_GUID_NETBSD_RAID \
    { 0x49, 0xf4, 0x8d, 0xaa, 0xb1, 0x0e, 0x11, 0xdc, \
      0xb9, 0x9b, 0x00, 0x19, 0xd1, 0x87, 0x96, 0x48 }

/* NetBSD concatenated partition type */
#define GPT_GUID_NETBSD_CONCD \
    { 0x2d, 0xb5, 0x19, 0xc4, 0xb1, 0x0f, 0x11, 0xdc, \
      0xb9, 0x9b, 0x00, 0x19, 0xd1, 0x87, 0x96, 0x48 }

/* NetBSD encrypted partition type */
#define GPT_GUID_NETBSD_ENCRD \
    { 0x2d, 0xb5, 0x19, 0xec, 0xb1, 0x0f, 0x11, 0xdc, \
      0xb9, 0x9b, 0x00, 0x19, 0xd1, 0x87, 0x96, 0x48 }

/* illumos/Solaris /boot partition type */
#define GPT_GUID_ILLUMOS_BOOT \
    { 0x6a, 0x82, 0xcb, 0x45, 0x1d, 0xd2, 0x11, 0xb2, \
      0x99, 0xa6, 0x08, 0x00, 0x20, 0x73, 0x66, 0x31 }

/* illumos/Solaris / partition type */
#define GPT_GUID_ILLUMOS \
    { 0x6a, 0x85, 0xcf, 0x4d, 0x1d, 0xd2, 0x11, 0xb2, \
      0x99, 0xa6, 0x08, 0x00, 0x20, 0x73, 0x66, 0x31 }

/* illumos/Solaris swap partition type */
#define GPT_GUID_ILLUMOS_SWAP \
    { 0x6a, 0x87, 0xc4, 0x6f, 0x1d, 0xd2, 0x11, 0xb2, \
      0x99, 0xa6, 0x08, 0x00, 0x20, 0x73, 0x66, 0x31 }

/* illumos/Solaris backup partition type */
#define GPT_GUID_ILLUMOS_BCKP \
    { 0x6a, 0x8b, 0x64, 0x2b, 0x1d, 0xd2, 0x11, 0xb2, \
      0x99, 0xa6, 0x08, 0x00, 0x20, 0x73, 0x66, 0x31 }

/* illumos/Solaris /usr (and Darwin ZFS) partition type */
#define GPT_GUID_IUSR_P_DZFS \
    { 0x6a, 0x89, 0x8c, 0xc3, 0x1d, 0xd2, 0x11, 0xb2, \
      0x99, 0xa6, 0x08, 0x00, 0x20, 0x73, 0x66, 0x31 }

/* illumos/Solaris /var partition type */
#define GPT_GUID_ILLUMOS_VAR \
    { 0x6a, 0x8E, 0xf2, 0xe9, 0x1d, 0xd2, 0x11, 0xb2, \
      0x99, 0xa6, 0x08, 0x00, 0x20, 0x73, 0x66, 0x31 }

/* illumos/Solaris /home partition type */
#define GPT_GUID_ILLUMOS_HOME \
    { 0x6a, 0x90, 0xba, 0x39, 0x1d, 0xd2, 0x11, 0xb2, \
      0x99, 0xa6, 0x08, 0x00, 0x20, 0x73, 0x66, 0x31 }

/* illumos/Solaris alternate partition type */
#define GPT_GUID_ILLUMOS_ALT \
    { 0x6a, 0x92, 0x83, 0xa5, 0x1d, 0xd2, 0x11, 0xb2, \
      0x99, 0xa6, 0x08, 0x00, 0x20, 0x73, 0x66, 0x31 }

/* illumos/Solaris reserved partition type */
#define GPT_GUID_ILLUMOS_RSV0 \
    { 0x6a, 0x94, 0x5a, 0x3b, 0x1d, 0xd2, 0x11, 0xb2, \
      0x99, 0xa6, 0x08, 0x00, 0x20, 0x73, 0x66, 0x31 }

/* illumos/Solaris reserved partition type */
#define GPT_GUID_ILLUMOS_RSV1 \
    { 0x6a, 0x96, 0x30, 0xd1, 0x1d, 0xd2, 0x11, 0xb2, \
      0x99, 0xa6, 0x08, 0x00, 0x20, 0x73, 0x66, 0x31 }

/* illumos/Solaris reserved partition type */
#define GPT_GUID_ILLUMOS_RSV2 \
    { 0x6a, 0x98, 0x07, 0x67, 0x1d, 0xd2, 0x11, 0xb2, \
      0x99, 0xa6, 0x08, 0x00, 0x20, 0x73, 0x66, 0x31 }

/* illumos/Solaris reserved partition type */
#define GPT_GUID_ILLUMOS_RSV3 \
    { 0x6a, 0x96, 0x23, 0x7f, 0x1d, 0xd2, 0x11, 0xb2, \
      0x99, 0xa6, 0x08, 0x00, 0x20, 0x73, 0x66, 0x31 }

/* illumos/Solaris reserved partition type */
#define GPT_GUID_ILLUMOS_RSV4 \
    { 0x6a, 0x9d, 0x2A, 0xc7, 0x1d, 0xd2, 0x11, 0xb2, \
      0x99, 0xa6, 0x08, 0x00, 0x20, 0x73, 0x66, 0x31 }

/* Haiku BFS partition type */
#define GPT_GUID_HAIKU_BFS \
    { 0x42, 0x46, 0x53, 0x31, 0x3b, 0xa3, 0x10, 0xf1, \
      0x80, 0x2a, 0x48, 0x61, 0x69, 0x6b, 0x75, 0x21 }

/* Plan9 partition type */
#define GPT_GUID_PLAN9 \
    { 0xc9, 0x18, 0x18, 0xf9, 0x80, 0x25, 0x47, 0xaf, \
      0x89, 0xd2, 0xf0, 0x30, 0xd7, 0x00, 0x0c, 0x2c }

/* Freedesktop shared boot loader configuration partition type */
#define GPT_GUID_SHARED_BCFG \
    { 0xbc, 0x13, 0xc2, 0xff, 0x59, 0xe6, 0x42, 0x62, \
      0xa3, 0x52, 0xb2, 0x75, 0xfd, 0x6f, 0x71, 0x72 }

/* NT Basic Data (FAT/NTFS/exFAT) partition type */
#define GPT_GUID_NT_BDATA \
    { 0xeb, 0xd0, 0xa0, 0xa2, 0xb9, 0xe5, 0x44, 0x33, \
      0x87, 0xc0, 0x68, 0xb6, 0xb7, 0x26, 0x99, 0xc7 }

/* NT LDM metadata partition type */
#define GPT_GUID_NT_LDM_MD \
    { 0x58, 0x08, 0xc8, 0xaa, 0x7e, 0x8f, 0x42, 0xe0, \
      0x85, 0xd2, 0xe1, 0xe9, 0x04, 0x34, 0xcf, 0xb3 }

/* NT LDM data partition type */
#define GPT_GUID_NT_LDM_DATA \
    { 0xaf, 0x9b, 0x60, 0xa0, 0x14, 0x31, 0x4f, 0x62, \
      0xbc, 0x68, 0x33, 0x11, 0x71, 0x4a, 0x69, 0xad }

/* NT Storage Spaces partition type */
#define GPT_GUID_NT_STO_SPCS \
    { 0xe7, 0x5c, 0xaf, 0x8f, 0xf6, 0x80, 0x4c, 0xee, \
      0xaf, 0xa3, 0xb0, 0x01, 0xe5, 0x6e, 0xfc, 0x2d }

/* NT GPFS partition type */
#define GPT_GUID_NT_GPFS \
    { 0x37, 0xaf, 0xfc, 0x90, 0xef, 0x7d, 0x4e, 0x96, \
      0x91, 0xc3, 0x2d, 0x7a ,0xe0, 0x55, 0xb1, 0x74 }

/* Darwin APFS / FileVault APFS partition type */
#define GPT_GUID_DARWIN_APFS \
    { 0x7c, 0x34, 0x57, 0xef, 0x00, 0x00, 0x11, 0xaa, \
      0xaa, 0x11, 0x00, 0x30, 0x65, 0x43, 0xec, 0xac }

/* Darwin HFS+/HFSX partition type */
#define GPT_GUID_DARWIN_HFS \
    { 0x48, 0x46, 0x53, 0x00, 0x00, 0x00, 0x11, 0xaa, \
      0xaa, 0x11, 0x00, 0x30, 0x65, 0x43, 0xec, 0xac }

/* Darwin Boot HFS+/HFSX partition type */
#define GPT_GUID_DARWIN_BOOT \
    { 0x42, 0x6f, 0x6F, 0x74, 0x00, 0x00, 0x11, 0xaa, \
      0xaa, 0x11, 0x00, 0x30, 0x65, 0x43, 0xec, 0xac }

/* Darwin Core Storage Container / FileVault HFS+/HFSX partition type */
#define GPT_GUID_DARWIN_CSC \
    { 0x53, 0x74, 0x6f, 0x72, 0x61, 0x67, 0x11, 0xaa, \
      0xaa, 0x11, 0x00, 0x30, 0x65, 0x43, 0xec, 0xac }

/* Darwin RAID partition type */
#define GPT_GUID_DARWIN_RAID \
    { 0x52, 0x41, 0x49, 0x44, 0x5f, 0x4f, 0x11, 0xaa, \
      0xaa, 0x11, 0x00, 0x30, 0x65, 0x43, 0xec, 0xac }

/* Darwin RAID offline partition type */
#define GPT_GUID_DARWIN_RAIDO \
    { 0x52, 0x41, 0x49, 0x44, 0x00, 0x00, 0x11, 0xaa, \
      0xaa, 0x11, 0x00, 0x30, 0x65, 0x43, 0xec, 0xac }

/* VeraCrypt encrypted data partition type */
#define GPT_GUID_VERACRYPT \
    { 0x8c, 0x8f, 0x8e, 0xff, 0xac, 0x95, 0x47, 0x70, \
      0x81, 0x4a, 0x21, 0x99, 0x4f, 0x2d, 0xbc, 0x8f }

/* DOS partition table -- located at start of some disks. */
#define	DOS_LABELSECTOR 1
#define	DOSBBSECTOR	0		/* DOS boot block relative sector # */
#define	DOSPARTOFF	446
#define	DOSDISKOFF	444
#define	NDOSPART	4
#define	DOSACTIVE	0x80		/* active partition */

#define	DOSMBR_SIGNATURE	(0xaa55)
#define	DOSMBR_SIGNATURE_OFF	(0x1fe)

/* Maximum number of Extended Boot Records (EBRs) to traverse. */
#define	DOS_MAXEBR	256

struct dos_partition {
	u_int8_t	dp_flag;	/* bootstrap flags */
	u_int8_t	dp_shd;		/* starting head */
	u_int8_t	dp_ssect;	/* starting sector */
	u_int8_t	dp_scyl;	/* starting cylinder */
	u_int8_t	dp_typ;		/* partition type (see below) */
	u_int8_t	dp_ehd;		/* end head */
	u_int8_t	dp_esect;	/* end sector */
	u_int8_t	dp_ecyl;	/* end cylinder */
	u_int32_t	dp_start;	/* absolute starting sector number */
	u_int32_t	dp_size;	/* partition size in sectors */
};

/* Isolate the relevant bits to get sector and cylinder. */
#define	DPSECT(s)	((s) & 0x3f)
#define	DPCYL(c, s)	((c) + (((s) & 0xc0) << 2))

/*
 * Known MBR partition types.
 */

/* unused partition */
#define MBR_PID_UNUSED				0x00

/* 12-bit FAT */
#define MBR_PID_FAT12				0x01

/* 16-bit FAT, less than 32M */
#define MBR_PID_FAT16				0x04

/* Extended; contains subpartitions */
#define MBR_PID_EXT_CHS				0x05

/* 16-bit FAT, more than 32M */
#define MBR_PID_FAT16B				0x06

/* NT IFS/HPFS/NTFS/exFAT */
#define MBR_PID_NT				0x07

/* 32-bit FAT */
#define MBR_PID_FAT32				0x0b

/* 32-bit FAT, LBA-mapped */
#define MBR_PID_FAT32_LBA			0x0c

/* 16-bit FAT, LBA-mapped */
#define MBR_PID_FAT16B_LBA			0x0e

/* Extended, LBA-mapped; contains subpartitions */
#define MBR_PID_EXT_LBA				0x0f

/* hidden 12-bit FAT */
#define MBR_PID_FAT12_H				0x11

/* hidden 16-bit FAT, less than 32M */
#define MBR_PID_FAT16_H				0x14

/* hidden Extended; contains subpartitions */
#define MBR_PID_EXT_H				0x15

/* hidden 16-bit FAT, more than 32M */
#define MBR_PID_FAT16B_H			0x16

/* NT hidden HPFS/NTFS/exFAT */
#define MBR_PID_NT_H				0x17

/* hidden 32-bit FAT */
#define MBR_PID_FAT32_H				0x1b

/* hidden 32-bit FAT, LBA-mapped */
#define MBR_PID_FAT32_H_LBA			0x1c

/* hidden 16-bit FAT, LBA-mapped */
#define MBR_PID_FAT16B_H_LBA			0x1e

/* hidden Extended, LBA-mapped; contains subpartitions */
#define MBR_PID_EXT_H_LBA			0x1f

/* AtheOS AthFS/AFS partition type */
#define MBR_PID_ATHFS				0x2a

/* SyllableOS SylStor partition type */
#define MBR_PID_SYLSTOR				0x2b

/* JFS partition type */
#define MBR_PID_JFS				0x35

/* Plan9 partition type */
#define MBR_PID_PLAN9				0x39

/* UFS partition type */
#define MBR_PID_UFS_CHS				0x63

/* HyperbolaBSD slice; contains subpartitions */
#define MBR_PID_HBSD_SL				0x6a

/* HyperBK slice (for other systems with HyperBK); contains subpartitions */
#define MBR_PID_HYPERBK_SL			0x6b

/* DragonFly slice; contains subpartitions */
#define MBR_PID_DRAGONFLY_SL			0x6c

/* Minix partition type */
#define MBR_PID_MINIX				0x81

/* Linux and Hurd kernel swap partition type */
#define MBR_PID_LINUX_HURD_SW			0x82

/* Linux and Hurd kernel partition type */
#define MBR_PID_LINUX_HURD			0x83

/* Linux kernel Extended; contains sub-partitions */
#define MBR_PID_LINUX_EXT			0x85

/* Linux kernel plaintext partition table */
#define MBR_PID_LINUX_PPT			0x88

/* FreeDOS hidden 12-bit FAT */
#define MBR_PID_FDOS_F12_H			0x8d

/* Linux kernel LVM partition type, contains subpartitions */
#define MBR_PID_LINUX_LVM			0x8e

/* FreeDOS hidden 16-bit FAT, less than 32M */
#define MBR_PID_FDOS_F16_H			0x90

/* FreeDOS hidden Extended; contains subpartitions */
#define MBR_PID_FDOS_EXT_H			0x91

/* FreeDOS hidden 16-bit FAT, more than 32M */
#define MBR_PID_FDOS_F16B_H			0x92

/* Linux kernel hidden partition type */
#define MBR_PID_LINUX_H				0x93

/* FreeDOS hidden 32-bit FAT */
#define MBR_PID_FDOS_F32_H			0x97

/* FreeDOS hidden 32-bit FAT, LBA-mapped */
#define MBR_PID_FDOS_F32_H_L			0x98

/* FreeDOS hidden 16-bit FAT, LBA-mapped */
#define MBR_PID_FDOS_F16B_H_L			0x9a

/* FreeDOS hidden Extended, LBA-mapped; contains subpartitions */
#define MBR_PID_FDOS_EXT_H_L			0x9b

/* HyperBK RAID (for other systems with HyperBK) partition type */
#define MBR_PID_HYPERBK_RAID			0x9c

/* HyperbolaBSD RAID partition type */
#define MBR_PID_HBSD_RAID			0x9d

/* FreeBSD/386BSD slice; contains subpartitions */
#define MBR_PID_FREEBSD_SL			0xa5

/* OpenBSD slice; contains subpartitions */
#define MBR_PID_OPENBSD_SL			0xa6

/* Darwin UFS partition type */
#define MBR_PID_DARWIN_UFS			0xa8

/* NetBSD slice; contains subpartitions */
#define MBR_PID_NETBSD_SL			0xa9

/* Darwin HFS+ boot partition type */
#define MBR_PID_DARWIN_BOOT			0xab

/* Darwin RAID partition type */
#define MBR_PID_DARWIN_RAID			0xac

/* Darwin HFS/HFS+/HFSX partition type */
#define MBR_PID_DARWIN_HFS			0xaf

/* illumos/Solaris x86 partition type */
#define MBR_PID_ILLUMOS_X86			0xbf

/* HyperbolaBSD swap partition type */
#define MBR_PID_HBSD_SW				0xd2

/* HyperbolaBSD partition type */
#define MBR_PID_HBSD				0xd3

/* HyperBK swap (for other systems with HyperBK) partition type */
#define MBR_PID_HYPERBK_SW			0xd7

/* HyperBK (for other systems with HyperBK) partition */
#define MBR_PID_HYPERBK				0xd9

/* Linux kernel LUKS partition type */
#define MBR_PID_LINUX_LUKS			0xe8

/* HyperbolaBSD Extended; contains sub-partitions */
#define MBR_PID_HBSD_EXT			0xe9

/* HyperBK Extended (for other systems with HyperBK); contains subpartitions */
#define MBR_PID_HYPERBK_EXT			0xea

/* Haiku/BeOS BFS partition type */
#define MBR_PID_HAIKU_BFS			0xeb

/* EFI Hybrid MBR partition type */
#define MBR_PID_EFI_HYBRIDMBR			0xed

/* EFI Protective partition type */
#define MBR_PID_EFI_PROTECT			0xee

/* EFI System partition type */
#define MBR_PID_EFI_SYSTEM			0xef

/* Linux kernel RAID partition type */
#define MBR_PID_LINUX_RAID			0xfd

/* System partition types */
#if defined(__HYPERBOLABSD__) && !defined(__HYPERBK__)
#define MBR_PID_SYS_SL MBR_PID_HBSD_SL
#define MBR_PID_SYS MBR_PID_HBSD_
#define MBR_PID_SYS_SW MBR_PID_HBSD_SW
#define MBR_PID_SYS_EXT MBR_PID_HBSD_EXT
#define MBR_PID_SYS_RAID MBR_PID_HBSD_RAID
#elif defined(__HYPERBK__) && !defined(__HYPERBOLABSD__)
#define MBR_PID_SYS_SL MBR_PID_HYPERBK_SL
#define MBR_PID_SYS MBR_PID_HYPERBK
#define MBR_PID_SYS_SW MBR_PID_HYPERBK_SW
#define MBR_PID_SYS_EXT MBR_PID_HYPERBK_EXT
#define MBR_PID_SYS_RAID MBR_PID_HYPERBK_RAID
#endif

struct dos_mbr {
	u_int8_t		dmbr_boot[DOSPARTOFF];
	struct dos_partition	dmbr_parts[NDOSPART];
	u_int16_t		dmbr_sign;
} __packed;

#ifdef _KERNEL
void	 diskerr(struct buf *, char *, char *, int, int, struct disklabel *);
u_int	 dkcksum(struct disklabel *);
int	 initdisklabel(struct disklabel *);
int	 checkdisklabel(void *, struct disklabel *, u_int64_t, u_int64_t);
int	 setdisklabel(struct disklabel *, struct disklabel *, u_int);
int	 readdisklabel(dev_t, void (*)(struct buf *), struct disklabel *, int);
int	 writedisklabel(dev_t, void (*)(struct buf *), struct disklabel *);
int	 bounds_check_with_label(struct buf *, struct disklabel *);
int	 readdisksector(struct buf *, void (*)(struct buf *),
	    struct disklabel *, u_int64_t);
int	 readdoslabel(struct buf *, void (*)(struct buf *),
	    struct disklabel *, daddr_t *, int);
#ifdef CD9660
int iso_disklabelspoof(dev_t dev, void (*strat)(struct buf *),
	struct disklabel *lp);
#endif
#ifdef UDF
int udf_disklabelspoof(dev_t dev, void (*strat)(struct buf *),
	struct disklabel *lp);
#endif
#endif
#endif /* _LOCORE */

#if !defined(_KERNEL) && !defined(_LOCORE)

#include <sys/cdefs.h>

__BEGIN_DECLS
struct disklabel *getdiskbyname(const char *);
__END_DECLS

#endif
