/*
 * System call properties header for HyperbolaBSD
 * Copyright (c) 2020-2021 Hyperbola Project
 * Copyright (c) 2020-2021 Márcio Silva <coadde@hyperbola.info>
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifdef _SYSCL_PRS_APAD
#undef _SYSCL_PRS_APAD
#endif

#ifdef _SYSCL_PRS_DTYPE
#undef _SYSCL_PRS_DTYPE
#endif

#define _SYSCL_PRS_DTYPE int32_t

#define _SYSCL_PRS_APAD(x)	\
    sizeof(x) <= sizeof(long) ? sizeof(long) - sizeof(x) : 0

#define _SYSCL_PRS_UNION						\
	union {								\
		struct {						\
			struct {					\
				char value[				\
				    _SYSCL_PRS_APAD(_SYSCL_PRS_DTYPE)]; \
			} pad_s;					\
			struct {					\
				_SYSCL_PRS_DTYPE value;			\
			} datum_s;					\
		} big_endian_s;						\
		struct {						\
			struct {					\
				_SYSCL_PRS_DTYPE value;			\
			} datum_s;					\
		} little_endian_s;					\
		struct {						\
			long value;					\
		} pad_s;						\
	}

/* system call properties for exit() */
struct scp_exit_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION rvalue_u;
};

/* system call properties for read() */
struct scp_read_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION buffer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION numberbyte_u;
};

/* system call properties for write() */
struct scp_write_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const void *
	_SYSCL_PRS_UNION buffer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION numberbyte_u;
};

/* system call properties for open() */
struct scp_open_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION mode_u;
};

/* system call properties for close() */
struct scp_close_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
};

/* system call properties for getentropy() */
struct scp_getentropy_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION buffer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION numberbyte_u;
};

/* system call properties for tfork() */
struct scp_tfork_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct tfork *
	_SYSCL_PRS_UNION parameter_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION parametersize_u;
};

/* system call properties for link() */
struct scp_link_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION link_u;
};

/* system call properties for unlink() */
struct scp_unlink_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
};

/* system call properties for wait4() */
struct scp_wait4_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION processid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t *
	_SYSCL_PRS_UNION status_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION options_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct rusage *
	_SYSCL_PRS_UNION rusage_u;
};

/* system call properties for chdir() */
struct scp_chdir_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
};

/* system call properties for fchdir() */
struct scp_fchdir_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
};

/* system call properties for mknod() */
struct scp_mknod_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION mode_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION device_u;
};

/* system call properties for chmod() */
struct scp_chmod_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION mode_u;
};

/* system call properties for chown() */
struct scp_chown_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION userid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION groupid_u;
};

/* system call properties for break() */
struct scp_break_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int8_t *
	_SYSCL_PRS_UNION numbersize_u;
};

/* system call properties for getrusage() */
struct scp_getrusage_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION who_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct rusage *
	_SYSCL_PRS_UNION rusage_u;
};

/* system call properties for mount() */
struct scp_mount_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION type_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION data_u;
};

/* system call properties for umount() */
struct scp_unmount_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for setuid() */
struct scp_setuid_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION userid_u;
};

/* system call properties for ptrace() */
struct scp_ptrace_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION request_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION processid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int8_t *
	_SYSCL_PRS_UNION address_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION data_u;
};

/* system call properties for recvmsg() */
struct scp_recvmsg_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION s_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct msghdr *
	_SYSCL_PRS_UNION message_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for sendmsg() */
struct scp_sendmsg_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION s_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct msghdr *
	_SYSCL_PRS_UNION message_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for recvfrom() */
struct scp_recvfrom_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION s_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION buffer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION length_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct sockaddr *
	_SYSCL_PRS_UNION from_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t *
	_SYSCL_PRS_UNION fromlengthaddress_u;
};

/* system call properties for accept() */
struct scp_accept_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION s_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct sockaddr *
	_SYSCL_PRS_UNION name_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t *
	_SYSCL_PRS_UNION anamelength_u;
};

/* system call properties for getpeername() */
struct scp_getpeername_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION fdes_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct sockaddr *
	_SYSCL_PRS_UNION asa_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t *
	_SYSCL_PRS_UNION alength_u;
};

/* system call properties for getsockname() */
struct scp_getsockname_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION fdes_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct sockaddr *
	_SYSCL_PRS_UNION asa_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t *
	_SYSCL_PRS_UNION alength_u;
};

/* system call properties for access() */
struct scp_access_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION amode_u;
};

/* system call properties for chflags() */
struct scp_chflags_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for fchflags() */
struct scp_fchflags_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for stat() */
struct scp_stat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct stat *
	_SYSCL_PRS_UNION ub_u;
};

/* system call properties for lstat() */
struct scp_lstat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct stat *
	_SYSCL_PRS_UNION ub_u;
};

/* system call properties for dup() */
struct scp_dup_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
};

/* system call properties for fstatat() */
struct scp_fstatat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct stat *
	_SYSCL_PRS_UNION buffer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flag_u;
};

/* system call properties for profil() */
struct scp_profil_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION samples_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION size_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE unsigned long
	_SYSCL_PRS_UNION offset_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION scale_u;
};

/* system call properties for ktrace() */
struct scp_ktrace_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION fname_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION ops_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION facs_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION processid_u;
};

/* system call properties for sigaction() */
struct scp_sigaction_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION signalnumber_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct sigaction *
	_SYSCL_PRS_UNION nsa_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct sigaction *
	_SYSCL_PRS_UNION osa_u;
};

/* system call properties for sigprocmask() */
struct scp_sigprocmask_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION how_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION mask_u;
};

/* system call properties for setlogin() */
struct scp_setlogin_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION namebuffer_u;
};

/* system call properties for acct() */
struct scp_acct_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
};

/* system call properties for fstat() */
struct scp_fstat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct stat *
	_SYSCL_PRS_UNION statbuffer_u;
};

/* system call properties for ioctl() */
struct scp_ioctl_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE unsigned long
	_SYSCL_PRS_UNION com_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION data_u;
};

/* system call properties for reboot() */
struct scp_reboot_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION option_u;
};

/* system call properties for revoke() */
struct scp_revoke_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
};

/* system call properties for symlink() */
struct scp_symlink_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION link_u;
};

/* system call properties for readlink() */
struct scp_readlink_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int8_t *
	_SYSCL_PRS_UNION buffer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION count_u;
};

/* system call properties for execve() */
struct scp_execve_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int8_t *const *
	_SYSCL_PRS_UNION argumentpointer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int8_t *const *
	_SYSCL_PRS_UNION environmentpointer_u;
};

/* system call properties for umask() */
struct scp_umask_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION newmask_u;
};

/* system call properties for chroot() */
struct scp_chroot_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
};

/* system call properties for getfsstat() */
struct scp_getfsstat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct statfs *
	_SYSCL_PRS_UNION buffer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION buffersize_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for statfs() */
struct scp_statfs_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct statfs *
	_SYSCL_PRS_UNION buffer_u;
};

/* system call properties for fstatfs() */
struct scp_fstatfs_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct statfs *
	_SYSCL_PRS_UNION buffer_u;
};

/* system call properties for fhstatfs() */
struct scp_fhstatfs_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct fhandle *
	_SYSCL_PRS_UNION fhandlepointer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct statfs *
	_SYSCL_PRS_UNION buffer_u;
};

/* system call properties for gettimeofday() */
struct scp_gettimeofday_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct timeval *
	_SYSCL_PRS_UNION timepointer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct timezone *
	_SYSCL_PRS_UNION timezonepointer_u;
};

/* system call properties for settimeofday() */
struct scp_settimeofday_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct timeval *
	_SYSCL_PRS_UNION timepointer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct timezone *
	_SYSCL_PRS_UNION timezonepointer_u;
};

/* system call properties for setitimer() */
struct scp_setitimer_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION which_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct itimerval *
	_SYSCL_PRS_UNION itimevalue_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct itimerval *
	_SYSCL_PRS_UNION oitimevalue_u;
};

/* system call properties for getitimer() */
struct scp_getitimer_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION which_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct itimerval *
	_SYSCL_PRS_UNION itimevalue_u;
};

/* system call properties for select() */
struct scp_select_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION nd_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct fd_set *
	_SYSCL_PRS_UNION in_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct fd_set *
	_SYSCL_PRS_UNION out_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct fd_set *
	_SYSCL_PRS_UNION ex_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct timeval *
	_SYSCL_PRS_UNION timevalue_u;
};

/* system call properties for kevent() */
struct scp_kevent_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct kevent *
	_SYSCL_PRS_UNION changelist_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION numberchanges_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct kevent *
	_SYSCL_PRS_UNION eventlist_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION numberevents_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct timespec *
	_SYSCL_PRS_UNION timeout_u;
};

/* system call properties for munmap() */
struct scp_munmap_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION address_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION length_u;
};

/* system call properties for mprotect() */
struct scp_mprotect_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION address_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION length_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION protect_u;
};

/* system call properties for madvise() */
struct scp_madvise_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION address_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION length_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION behavior_u;
};

/* system call properties for utimes() */
struct scp_utimes_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct timeval *
	_SYSCL_PRS_UNION timepointer_u;
};

/* system call properties for futimes() */
struct scp_futimes_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct timeval *
	_SYSCL_PRS_UNION timepointer_u;
};

/* system call properties for getgroups() */
struct scp_getgroups_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION groupidsetsize_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t *
	_SYSCL_PRS_UNION groupidset_u;
};

/* system call properties for setgroups() */
struct scp_setgroups_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION groupidsetsize_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const u_int32_t *
	_SYSCL_PRS_UNION groupidset_u;
};

/* system call properties for setpgid() */
struct scp_setpgid_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION processid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION processgroupid_u;
};

/* system call properties for futex() */
struct scp_futex_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int *
	_SYSCL_PRS_UNION futex_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION operation_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION value_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct timespec *
	_SYSCL_PRS_UNION timeout_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int *
	_SYSCL_PRS_UNION g_u;
};

/* system call properties for utimensat() */
struct scp_utimensat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct timespec *
	_SYSCL_PRS_UNION times_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flag_u;
};

/* system call properties for futimens() */
struct scp_futimens_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct timespec *
	_SYSCL_PRS_UNION times_u;
};

/* system call properties for kbind() */
struct scp_kbind_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct kbind *
	_SYSCL_PRS_UNION parameter_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION parametersize_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE long long
	_SYSCL_PRS_UNION processcookie_u;
};

/* system call properties for clock_gettime() */
struct scp_clock_gettime_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION clockid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct timespec *
	_SYSCL_PRS_UNION timepointer_u;
};

/* system call properties for clock_settime() */
struct scp_clock_settime_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION clockid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct timespec *
	_SYSCL_PRS_UNION timepointer_u;
};

/* system call properties for clock_getres() */
struct scp_clock_getres_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION clockid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct timespec *
	_SYSCL_PRS_UNION timepointer_u;
};

/* system call properties for dup2() */
struct scp_dup2_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION from_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION to_u;
};

/* system call properties for nanosleep() */
struct scp_nanosleep_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct timespec *
	_SYSCL_PRS_UNION rqtp_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct timespec *
	_SYSCL_PRS_UNION rmtp_u;
};

/* system call properties for fcntl() */
struct scp_fcntl_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION command_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION argument_u;
};

/* system call properties for accept4() */
struct scp_accept4_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION s_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct sockaddr *
	_SYSCL_PRS_UNION name_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t *
	_SYSCL_PRS_UNION anamelength_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for thrsleep() */
struct scp_thrsleep_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const volatile void *
	_SYSCL_PRS_UNION ident_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION clockid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct timespec *
	_SYSCL_PRS_UNION timepointer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION lock_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int32_t *
	_SYSCL_PRS_UNION abort_u;
};

/* system call properties for fsync() */
struct scp_fsync_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
};

/* system call properties for setpriority() */
struct scp_setpriority_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION which_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION who_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION priority_u;
};

/* system call properties for socket() */
struct scp_socket_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION domain_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION type_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION protocol_u;
};

/* system call properties for connect() */
struct scp_connect_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION s_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct sockaddr *
	_SYSCL_PRS_UNION name_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION namelength_u;
};

/* system call properties for getdents() */
struct scp_getdents_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION buffer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION bufferlength_u;
};

/* system call properties for getpriority() */
struct scp_getpriority_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION which_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION who_u;
};

/* system call properties for pipe2() */
struct scp_pipe2_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t *
	_SYSCL_PRS_UNION filedatapointer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for dup3() */
struct scp_dup3_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION from_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION to_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for sigreturn() */
struct scp_sigreturn_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct sigcontext *
	_SYSCL_PRS_UNION signalcontextpointer_u;
};

/* system call properties for bind() */
struct scp_bind_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION s_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct sockaddr *
	_SYSCL_PRS_UNION name_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION namelength_u;
};

/* system call properties for setsockopt() */
struct scp_setsockopt_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION s_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION level_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION name_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const void *
	_SYSCL_PRS_UNION value_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION valuesize_u;
};

/* system call properties for listen() */
struct scp_listen_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION s_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION backlog_u;
};

/* system call properties for chflagsat() */
struct scp_chflagsat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION flags_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION atflags_u;
};

/* system call properties for pledge() */
struct scp_pledge_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION promises_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION executablepromises_u;
};

/* system call properties for ppoll() */
struct scp_ppoll_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct pollfd *
	_SYSCL_PRS_UNION filedatastructure_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION numberfiledatastructure_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct timespec *
	_SYSCL_PRS_UNION timespecification_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const u_int32_t *
	_SYSCL_PRS_UNION mask_u;
};

/* system call properties for pselect() */
struct scp_pselect_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION nd_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct fd_set *
	_SYSCL_PRS_UNION in_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct fd_set *
	_SYSCL_PRS_UNION out_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct fd_set *
	_SYSCL_PRS_UNION ex_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct timespec *
	_SYSCL_PRS_UNION timespecification_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const u_int32_t *
	_SYSCL_PRS_UNION mask_u;
};

/* system call properties for sigsuspend() */
struct scp_sigsuspend_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION mask_u;
};

/* system call properties for sendsyslog() */
struct scp_sendsyslog_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION buffer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION numberbyte_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for unveil() */
struct scp_unveil_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION permissions_u;
};

/* system call properties for realpath() */
struct scp_realpath_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION pathname_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int8_t *
	_SYSCL_PRS_UNION resolved_u;
};

/* system call properties for getsockopt() */
struct scp_getsockopt_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION s_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION level_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION name_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION value_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t *
	_SYSCL_PRS_UNION avaluesize_u;
};

/* system call properties for thrkill() */
struct scp_thrkill_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION processid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t *
	_SYSCL_PRS_UNION status_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION value_u;
};

/* system call properties for readv() */
struct scp_readv_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct iovec *
	_SYSCL_PRS_UNION iovpointer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION iovcnt_u;
};

/* system call properties for writev() */
struct scp_writev_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct iovec *
	_SYSCL_PRS_UNION iovpointer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION iovcnt_u;
};

/* system call properties for kill() */
struct scp_kill_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION processid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION signalnumber_u;
};

/* system call properties for fchown() */
struct scp_fchown_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION userid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION groupid_u;
};

/* system call properties for fchmod() */
struct scp_fchmod_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION mode_u;
};

/* system call properties for setreuid() */
struct scp_setreuid_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION ruserid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION euserid_u;
};

/* system call properties for setregid() */
struct scp_setregid_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION rgroupid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION egroupid_u;
};

/* system call properties for rename() */
struct scp_rename_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION from_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION to_u;
};

/* system call properties for flock() */
struct scp_flock_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION how_u;
};

/* system call properties for mkfifo() */
struct scp_mkfifo_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION mode_u;
};

/* system call properties for sendto() */
struct scp_sendto_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION s_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const void *
	_SYSCL_PRS_UNION buffer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION length_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct sockaddr *
	_SYSCL_PRS_UNION to_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t *
	_SYSCL_PRS_UNION tolength_u;
};

/* system call properties for shutdown() */
struct scp_shutdown_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION s_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION how_u;
};

/* system call properties for socketpair() */
struct scp_socketpair_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION domain_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION type_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION protocol_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t *
	_SYSCL_PRS_UNION rsv_u;
};

/* system call properties for mkdir() */
struct scp_mkdir_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION mode_u;
};

/* system call properties for rmdir() */
struct scp_rmdir_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
};

/* system call properties for adjtime() */
struct scp_adjtime_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct timeval *
	_SYSCL_PRS_UNION delta_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct timeval *
	_SYSCL_PRS_UNION olddelta_u;
};

/* system call properties for getlogin_r() */
struct scp_getlogin_r_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int8_t *
	_SYSCL_PRS_UNION namebuffer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION namelength_u;
};

/* system call properties for quotactl() */
struct scp_quotactl_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION command_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION userid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int8_t *
	_SYSCL_PRS_UNION argument_u;
};

/* system call properties for nfssvc() */
struct scp_nfssvc_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flag_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION argumentpointer_u;
};

/* system call properties for getfh() */
struct scp_getfh_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION fname_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct fhandle *
	_SYSCL_PRS_UNION fhandlepointer_u;
};

/* system call properties for sysarch() */
struct scp_sysarch_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION op_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION parameters_u;
};

/* system call properties for pread() */
struct scp_pread_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION buffer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION numberbyte_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION pad_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int64_t
	_SYSCL_PRS_UNION offset_u;
};

/* system call properties for pwrite() */
struct scp_pwrite_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const void *
	_SYSCL_PRS_UNION buffer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION numberbyte_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION pad_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int64_t
	_SYSCL_PRS_UNION offset_u;
};

/* system call properties for setgid() */
struct scp_setgid_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION groupid_u;
};

/* system call properties for setegid() */
struct scp_setegid_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION egroupid_u;
};

/* system call properties for seteuid() */
struct scp_seteuid_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION euserid_u;
};

/* system call properties for pathconf() */
struct scp_pathconf_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION name_u;
};

/* system call properties for fpathconf() */
struct scp_fpathconf_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION name_u;
};

/* system call properties for swapctl() */
struct scp_swapctl_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION command_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const void *
	_SYSCL_PRS_UNION argument_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION misc_u;
};

/* system call properties for getrlimit() */
struct scp_getrlimit_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION which_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct rlimit *
	_SYSCL_PRS_UNION rlimitpointer_u;
};

/* system call properties for setrlimit() */
struct scp_setrlimit_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION which_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct rlimit *
	_SYSCL_PRS_UNION rlimitpointer_u;
};

/* system call properties for mmap() */
struct scp_mmap_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION address_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION length_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION protect_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE long
	_SYSCL_PRS_UNION pad_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int64_t
	_SYSCL_PRS_UNION pos_u;
};

/* system call properties for lseek() */
struct scp_lseek_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION pad_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int64_t
	_SYSCL_PRS_UNION offset_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION whence_u;
};

/* system call properties for truncate() */
struct scp_truncate_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION pad_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int64_t
	_SYSCL_PRS_UNION length_u;
};

/* system call properties for ftruncate() */
struct scp_ftruncate_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION pad_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int64_t
	_SYSCL_PRS_UNION length_u;
};

/* system call properties for sysctl() */
struct scp_sysctl_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int32_t *
	_SYSCL_PRS_UNION name_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION namelength_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION old_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long *
	_SYSCL_PRS_UNION oldlengthpointer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION new_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION newlength_u;
};

/* system call properties for mlock() */
struct scp_mlock_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const void *
	_SYSCL_PRS_UNION address_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION length_u;
};

/* system call properties for munlock() */
struct scp_munlock_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const void *
	_SYSCL_PRS_UNION address_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION length_u;
};

/* system call properties for getpgid() */
struct scp_getpgid_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION processid_u;
};

/* system call properties for utrace() */
struct scp_utrace_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION label_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const void *
	_SYSCL_PRS_UNION address_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION length_u;
};

/* system call properties for semget() */
struct scp_semget_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE long
	_SYSCL_PRS_UNION key_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION numbersemaphores_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION semaphoreflags_u;
};

/* system call properties for msgget() */
struct scp_msgget_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE long
	_SYSCL_PRS_UNION key_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION messageflags_u;
};

/* system call properties for msgsnd() */
struct scp_msgsnd_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION messageqid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const void *
	_SYSCL_PRS_UNION messagepointer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION messagesize_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION messageflags_u;
};

/* system call properties for msgrcv() */
struct scp_msgrcv_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION messageqid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION messagepointer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION messagesize_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE long
	_SYSCL_PRS_UNION messagetype_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION messageflags_u;
};

/* system call properties for shmat() */
struct scp_shmat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION sharedmemoryid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const void *
	_SYSCL_PRS_UNION sharedmemoryaddress_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION sharedmemoryflags_u;
};

/* system call properties for shmdt() */
struct scp_shmdt_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const void *
	_SYSCL_PRS_UNION sharedmemoryaddress_u;
};

/* system call properties for minherit() */
struct scp_minherit_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION address_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION length_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION inherit_u;
};

/* system call properties for poll() */
struct scp_poll_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct pollfd *
	_SYSCL_PRS_UNION filedatastructure_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION numberfiledatastructure_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION timeout_u;
};

/* system call properties for lchown() */
struct scp_lchown_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION userid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION groupid_u;
};

/* system call properties for getsid() */
struct scp_getsid_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION processid_u;
};

/* system call properties for msync() */
struct scp_msync_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION address_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION length_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for pipe() */
struct scp_pipe_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t *
	_SYSCL_PRS_UNION filedatapointer_u;
};

/* system call properties for fhopen() */
struct scp_fhopen_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct fhandle *
	_SYSCL_PRS_UNION fhandlepointer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for preadv() */
struct scp_preadv_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct iovec *
	_SYSCL_PRS_UNION iovpointer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION iovcnt_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION pad_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int64_t
	_SYSCL_PRS_UNION offset_u;
};

/* system call properties for pwritev() */
struct scp_pwritev_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct iovec *
	_SYSCL_PRS_UNION iovpointer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION iovcnt_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION pad_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int64_t
	_SYSCL_PRS_UNION offset_u;
};

/* system call properties for mlockall() */
struct scp_mlockall_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for getresuid() */
struct scp_getresuid_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t *
	_SYSCL_PRS_UNION ruserid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t *
	_SYSCL_PRS_UNION euserid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t *
	_SYSCL_PRS_UNION suserid_u;
};

/* system call properties for setresuid() */
struct scp_setresuid_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION ruserid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION euserid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION suserid_u;
};

/* system call properties for getresgid() */
struct scp_getresgid_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t *
	_SYSCL_PRS_UNION rgroupid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t *
	_SYSCL_PRS_UNION egroupid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t *
	_SYSCL_PRS_UNION sgroupid_u;
};

/* system call properties for setresuid() */
struct scp_setresgid_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION rgroupid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION egroupid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION sgroupid_u;
};

/* system call properties for mquery() */
struct scp_mquery_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION address_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION length_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION protect_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE long
	_SYSCL_PRS_UNION pad_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int64_t
	_SYSCL_PRS_UNION pos_u;
};

/* system call properties for closefrom() */
struct scp_closefrom_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
};

/* system call properties for sigaltstack() */
struct scp_sigaltstack_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct sigaltstack *
	_SYSCL_PRS_UNION numbersigaltstack_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct sigaltstack *
	_SYSCL_PRS_UNION offsetsigaltstack_u;
};

/* system call properties for shmget() */
struct scp_shmget_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE long
	_SYSCL_PRS_UNION key_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION size_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION sharedmemoryflag_u;
};

/* system call properties for semop() */
struct scp_semop_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION semaphoreid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct sembuf *
	_SYSCL_PRS_UNION semaphoreops_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION numbersemaphoreops_u;
};

/* system call properties for fhstat() */
struct scp_fhstat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct fhandle *
	_SYSCL_PRS_UNION fhandlepointer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct stat *
	_SYSCL_PRS_UNION statbuffer_u;
};

/* system call properties for semctl() */
struct scp_semctl_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION semaphoreid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION semaphorenumber_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION command_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE union semun *
	_SYSCL_PRS_UNION argument_u;
};

/* system call properties for shmctl() */
struct scp_shmctl_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION sharedmemoryid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION command_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct shmid_ds *
	_SYSCL_PRS_UNION buffer_u;
};

/* system call properties for msgctl() */
struct scp_msgctl_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION messageqid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION command_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE struct msqid_ds *
	_SYSCL_PRS_UNION buffer_u;
};

/* system call properties for thrwakeup() */
struct scp_thrwakeup_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const volatile void *
	_SYSCL_PRS_UNION ident_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION number_u;
};

/* system call properties for threxit() */
struct scp_threxit_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t *
	_SYSCL_PRS_UNION notdead_u;
};

/* system call properties for thrsigdivert() */
struct scp_thrsigdivert_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION signalmask_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE siginfo_t *
	_SYSCL_PRS_UNION info_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const struct timespec *
	_SYSCL_PRS_UNION timeout_u;
};

/* system call properties for getcwd() */
struct scp_getcwd_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int8_t *
	_SYSCL_PRS_UNION buffer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION length_u;
};

/* system call properties for adjfreq() */
struct scp_adjfreq_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int64_t *
	_SYSCL_PRS_UNION frequence_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int64_t *
	_SYSCL_PRS_UNION oldfrequence_u;
};

/* system call properties for setrtable() */
struct scp_setrtable_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION rtableid_u;
};

/* system call properties for faccessat() */
struct scp_faccessat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION amode_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for fchmodat() */
struct scp_fchmodat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION mode_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for fchownat() */
struct scp_fchownat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION userid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION groupid_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for linkat() */
struct scp_linkat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata1_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path1_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata2_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path2_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
};

/* system call properties for mkdirat() */
struct scp_mkdirat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION mode_u;
};

/* system call properties for mkfifoat() */
struct scp_mkfifoat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION mode_u;
};

/* system call properties for mknodat() */
struct scp_mknodat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION mode_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION device_u;
};

/* system call properties for openat() */
struct scp_openat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flags_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_int32_t
	_SYSCL_PRS_UNION mode_u;
};

/* system call properties for readlinkat() */
struct scp_rlinkat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int8_t *
	_SYSCL_PRS_UNION buffer_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE u_long
	_SYSCL_PRS_UNION count_u;
};

/* system call properties for renameat() */
struct scp_renameat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION fromfiledata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION from_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION tofiledata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION to_u;
};

/* system call properties for symlinkat() */
struct scp_symlinkat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION link_u;
};

/* system call properties for unlinkat() */
struct scp_unlinkat_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION filedata_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE const int8_t *
	_SYSCL_PRS_UNION path_u;
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE int32_t
	_SYSCL_PRS_UNION flag_u;
};

/* system call properties for set_tcb() */
struct scp_set_tcb_t {
#undef _SYSCL_PRS_DTYPE
#define _SYSCL_PRS_DTYPE void *
	_SYSCL_PRS_UNION tcb_u;
};

#undef _SYSCL_PRS_APAD
#undef _SYSCL_PRS_DTYPE
#undef _SYSCL_PRS_UNION



/* dev/rnd.c */
int sys_getentropy(struct proc *, void *, register_t *);

#if defined(ACCOUNTING)
/* kern/kern_acct.c */
int sys_acct(struct proc *, void *, register_t *);
#endif

/* kern/kern_descrip.c */
int sys_close(struct proc *, void *, register_t *);
int sys_getdtablecount(struct proc *, void *, register_t *);
int sys_dup(struct proc *, void *, register_t *);
int sys_fstat(struct proc *, void *, register_t *);
int sys_dup2(struct proc *, void *, register_t *);
int sys_fcntl(struct proc *, void *, register_t *);
int sys_dup3(struct proc *, void *, register_t *);
int sys_flock(struct proc *, void *, register_t *);
int sys_fpathconf(struct proc *, void *, register_t *);
int sys_closefrom(struct proc *, void *, register_t *);


/* kern/kern_exit.c */
int sys_exit(struct proc *, void *, register_t *);
int sys_wait4(struct proc *, void *, register_t *);
int sys_threxit(struct proc *, void *, register_t *);

/* kern/kern_event.c */
int sys_kevent(struct proc *, void *, register_t *);

/* kern/kern_fork.c */
int sys_fork(struct proc *, void *, register_t *);
int sys_tfork(struct proc *, void *, register_t *);
int sys_vfork(struct proc *, void *, register_t *);

#if defined(KTRACE)
/* kern/kern_ktrace.c */
int sys_ktrace(struct proc *, void *, register_t *);
#endif

/* kern/kern_resource.c */
int sys_getrusage(struct proc *, void *, register_t *);
int sys_setpriority(struct proc *, void *, register_t *);
int sys_getpriority(struct proc *, void *, register_t *);
int sys_getrlimit(struct proc *, void *, register_t *);
int sys_setrlimit(struct proc *, void *, register_t *);

/* kern/kern_pledge.c */
int sys_pledge(struct proc *, void *, register_t *);

/* kern/kern_prot.c */
int sys_getpid(struct proc *, void *, register_t *);
int sys_setuid(struct proc *, void *, register_t *);
int sys_getuid(struct proc *, void *, register_t *);
int sys_geteuid(struct proc *, void *, register_t *);
int sys_getppid(struct proc *, void *, register_t *);
int sys_getegid(struct proc *, void *, register_t *);
int sys_getgid(struct proc *, void *, register_t *);
int sys_setlogin(struct proc *, void *, register_t *);
int sys_getgroups(struct proc *, void *, register_t *);
int sys_setgroups(struct proc *, void *, register_t *);
int sys_setpgid(struct proc *, void *, register_t *);
int sys_getlogin_r(struct proc *, void *, register_t *);
int sys_setsid(struct proc *, void *, register_t *);
int sys_setgid(struct proc *, void *, register_t *);
int sys_setegid(struct proc *, void *, register_t *);
int sys_seteuid(struct proc *, void *, register_t *);
int sys_getresuid(struct proc *, void *, register_t *);
int sys_setresuid(struct proc *, void *, register_t *);
int sys_getresgid(struct proc *, void *, register_t *);
int sys_setresgid(struct proc *, void *, register_t *);
int sys_getthrid(struct proc *, void *, register_t *);
int sys_set_tcb(struct proc *, void *, register_t *);
int sys_get_tcb(struct proc *, void *, register_t *);

/* kern/kern_sig.c */
int sys_sigaction(struct proc *, void *, register_t *);
int sys_sigprocmask(struct proc *, void *, register_t *);
int sys_sigpending(struct proc *, void *, register_t *);
int sys_sigsuspend(struct proc *, void *, register_t *);
int sys_thrkill(struct proc *, void *, register_t *);
int sys_kill(struct proc *, void *, register_t *);
int sys_sigaltstack(struct proc *, void *, register_t *);
int sys_thrsigdivert(struct proc *, void *, register_t *);

/* kern/kern_synch.c */
int sys_thrsleep(struct proc *, void *, register_t *);
int sys_sched_yield(struct proc *, void *, register_t *);
int sys_thrwakeup(struct proc *, void *, register_t *);

/* kern/kern_sysctl.c */
int sys_sysctl(struct proc *, void *, register_t *);

/* kern/kern_time.c */
int sys_gettimeofday(struct proc *, void *, register_t *);
int sys_settimeofday(struct proc *, void *, register_t *);
int sys_setitimer(struct proc *, void *, register_t *);
int sys_getitimer(struct proc *, void *, register_t *);
int sys_clock_gettime(struct proc *, void *, register_t *);
int sys_clock_settime(struct proc *, void *, register_t *);
int sys_clock_getres(struct proc *, void *, register_t *);
int sys_nanosleep(struct proc *, void *, register_t *);
int sys_adjtime(struct proc *, void *, register_t *);
int sys_adjfreq(struct proc *, void *, register_t *);

/* kern/kern_xxx.c */
int sys_reboot(struct proc *, void *, register_t *);

/* kern/subr_log.c */
int sys_sendsyslog(struct proc *, void *, register_t *);

/* kern/subr_prof.c */
int sys_profil(struct proc *, void *, register_t *);

/* kern/sys_futex.c */
int sys_futex(struct proc *, void *, register_t *);

/* kern/sys_generic.c */
int sys_read(struct proc *, void *, register_t *);
int sys_write(struct proc *, void *, register_t *);
int sys_ioctl(struct proc *, void *, register_t *);
int sys_select(struct proc *, void *, register_t *);
int sys_ppoll(struct proc *, void *, register_t *);
int sys_pselect(struct proc *, void *, register_t *);
int sys_readv(struct proc *, void *, register_t *);
int sys_writev(struct proc *, void *, register_t *);

/* kern/sys_pipe.c */
int sys_pipe2(struct proc *, void *, register_t *);

#if defined(PTRACE)
/* kern/sys_process.c */
int sys_ptrace(struct proc *, void *, register_t *);
#endif

#if defined(SYSVMSG)
/* kern/sysv_msg.c */
int sys_msgget(struct proc *, void *, register_t *);
int sys_msgsnd(struct proc *, void *, register_t *);
int sys_msgrcv(struct proc *, void *, register_t *);
int sys_msgctl(struct proc *, void *, register_t *);
#endif

#if defined(SYSVSEM)
/* kern/sysv_sem.c */
int sys_semget(struct proc *, void *, register_t *);
int sys_semop(struct proc *, void *, register_t *);
int sys_semctl(struct proc *, void *, register_t *);
#endif

#if defined(SYSVSHM)
#endif

/* kern/uipc_syscalls.c */
int sys_recvmsg(struct proc *, void *, register_t *);
int sys_sendmsg(struct proc *, void *, register_t *);
int sys_recvfrom(struct proc *, void *, register_t *);
int sys_accept(struct proc *, void *, register_t *);
int sys_getpeername(struct proc *, void *, register_t *);
int sys_getsockname(struct proc *, void *, register_t *);
int sys_accept4(struct proc *, void *, register_t *);
int sys_socket(struct proc *, void *, register_t *);
int sys_connect(struct proc *, void *, register_t *);
int sys_bind(struct proc *, void *, register_t *);
int sys_setsockopt(struct proc *, void *, register_t *);
int sys_listen(struct proc *, void *, register_t *);
int sys_getsockopt(struct proc *, void *, register_t *);
int sys_sndto(struct proc *, void *, register_t *);
int sys_shutdown(struct proc *, void *, register_t *);
int sys_socketpair(struct proc *, void *, register_t *);
int sys_setrtable(struct proc *, void *, register_t *);
int sys_getrtable(struct proc *, void *, register_t *);

/* kern/vfs_syscalls.c */
int sys_open(struct proc *, void *, register_t *);
int sys_link(struct proc *, void *, register_t *);
int sys_unlink(struct proc *, void *, register_t *);
int sys_chdir(struct proc *, void *, register_t *);
int sys_fchdir(struct proc *, void *, register_t *);
int sys_mknod(struct proc *, void *, register_t *);
int sys_chmod(struct proc *, void *, register_t *);
int sys_chown(struct proc *, void *, register_t *);
int sys_mount(struct proc *, void *, register_t *);
int sys_unmount(struct proc *, void *, register_t *);
int sys_access(struct proc *, void *, register_t *);
int sys_chflags(struct proc *, void *, register_t *);
int sys_fchflags(struct proc *, void *, register_t *);
int sys_sync(struct proc *, void *, register_t *);
int sys_stat(struct proc *, void *, register_t *);
int sys_lstat(struct proc *, void *, register_t *);
int sys_fstatat(struct proc *, void *, register_t *);
int sys_revoke(struct proc *, void *, register_t *);
int sys_symlink(struct proc *, void *, register_t *);
int sys_readlink(struct proc *, void *, register_t *);
int sys_umask(struct proc *, void *, register_t *);
int sys_chroot(struct proc *, void *, register_t *);
int sys_getfsstat(struct proc *, void *, register_t *);
int sys_statfs(struct proc *, void *, register_t *);
int sys_fstatfs(struct proc *, void *, register_t *);
int sys_fhstatfs(struct proc *, void *, register_t *);
int sys_utimes(struct proc *, void *, register_t *);
int sys_futimes(struct proc *, void *, register_t *);
int sys_utimensat(struct proc *, void *, register_t *);
int sys_futimens(struct proc *, void *, register_t *);
int sys_fsync(struct proc *, void *, register_t *);
int sys_getdents(struct proc *, void *, register_t *);
int sys_chflagsat(struct proc *, void *, register_t *);
int sys_unveil(struct proc *, void *, register_t *);
int sys_realpath(struct proc *, void *, register_t *);
int sys_fchown(struct proc *, void *, register_t *);
int sys_fchmod(struct proc *, void *, register_t *);
int sys_setreuid(struct proc *, void *, register_t *);
int sys_setregid(struct proc *, void *, register_t *);
int sys_rename(struct proc *, void *, register_t *);
int sys_mkfifo(struct proc *, void *, register_t *);
int sys_mkdir(struct proc *, void *, register_t *);
int sys_rmdir(struct proc *, void *, register_t *);
int sys_quotactl(struct proc *, void *, register_t *);
int sys_getfh(struct proc *, void *, register_t *);
int sys_pread(struct proc *, void *, register_t *);
int sys_pwrite(struct proc *, void *, register_t *);
int sys_pathconf(struct proc *, void *, register_t *);
int sys_lseek(struct proc *, void *, register_t *);
int sys_truncate(struct proc *, void *, register_t *);
int sys_ftruncate(struct proc *, void *, register_t *);
int sys_fhstat(struct proc *, void *, register_t *);
int sys_faccessat(struct proc *, void *, register_t *);
int sys_fchmodat(struct proc *, void *, register_t *);
int sys_fchownat(struct proc *, void *, register_t *);
int sys_linkat(struct proc *, void *, register_t *);
int sys_mkdirat(struct proc *, void *, register_t *);
int sys_mkfifoat(struct proc *, void *, register_t *);
int sys_mknodat(struct proc *, void *, register_t *);
int sys_openat(struct proc *, void *, register_t *);
int sys_readlinkat(struct proc *, void *, register_t *);
int sys_renameat(struct proc *, void *, register_t *);
int sys_symlinkat(struct proc *, void *, register_t *);
int sys_unlinkat(struct proc *, void *, register_t *);

/* kern/vfs_getcwd.c */
int sys_getcwd(struct proc *, void *, register_t *);

#if defined(NFSSERVER) || defined(NFSCLIENT)
/* nfs/nfs_syscalls.c */
int sys_nfssvc(struct proc *, void *, register_t *);
#endif


/* uvm/uvm_swap.c */
int sys_swapctl(struct proc *, void *, register_t *);

/* uvm/uvm_unix.c */
int sys_break(struct proc *, void *, register_t *);
