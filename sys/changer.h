/*
 * Changer header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/ioccom.h>
#include <sys/types.h>

enum changer_etypes_t {
	/* changer element types picker */
	CHANGER_ETYPES_PICKER = 000,

	/* changer element types slot */
	CHANGER_ETYPES_SLOT = 001,

	/* changer element types portal */
	CHANGER_ETYPES_PORTAL = 002,

	/* changer element types drive */
	CHANGER_ETYPES_DRIVE = 003
};

enum _changer_voltag_t {
	/* changer volume tag maximum length */
	CHANGER_VOLTAG_MAXLEN = 040
};

enum changer_mvmflg_t {
	/* changer move media flag invert */
	CHANGER_MVMFLG_INVERT = 001
};

enum changer_xcmflg_t {
	/* changer exchange media flag invert 1 */
	CHANGER_XCMFLG_INVERT1 = 001,

	/* changer exchange media flag invert 2 */
	CHANGER_XCMFLG_INVERT2 = 002
};

enum changer_pomflg_t {
	/* changer position media flag invert */
	CHANGER_POMFLG_INVERT = 001
};

enum _changer_estreq_t {
	/* changer element status request volume tags */
	CHANGER_ESTREQ_VOLTAGS = 001
};

enum _changer_estatus_t {
	/* changer element status full */
	CHANGER_ESTATUS_FULL = 001,

	/* changer element status imp exp */
	CHANGER_ESTATUS_IMPEXP = 002,

	/* changer element status except */
	CHANGER_ESTATUS_EXCEPT = 004,

	/* changer element status access */
	CHANGER_ESTATUS_ACCESS = 010,

	/* changer element status exe nab */
	CHANGER_ESTATUS_EXENAB = 020,

	/* changer element status ine nab */
	CHANGER_ESTATUS_INENAB = 040,

	/* changer element status picker mask */
	CHANGER_ESTATUS_PCKMSK = 005,

	/* changer element status slot mask */
	CHANGER_ESTATUS_SLTMSK = 010,

	/* changer element status portal mask */
	CHANGER_ESTATUS_PRTMSK = 077,

	/* changer element status drive mask */
	CHANGER_ESTATUS_DRVMSK = 010
};

/* changer element status bits */
static const unsigned char CHANGER_ESTATUS_BITS[] =
    "\20\6INEAB\5EXENAB\4ACCESS\3EXCEPT\2IMPEXP\1FULL";

struct changer_exchange_t {
	struct {
		int32_t type;
		int32_t unit;
	} source_s;
	struct {
		int32_t type;
		int32_t unit;
	} fst_dest_s;	/* first destination */
	struct {
		int32_t type;
		int32_t unit;
	} snd_dest_s;	/* second destination */
	struct {
		int32_t flags;
	} data_s;
};

struct changer_move_t {
	struct {
		int32_t type;
		int32_t unit;
	} from_s;
	struct {
		int32_t type;
		int32_t unit;
	} to_s;
	struct {
		int32_t flags;
	} data_s;
};

struct changer_parameters_t {
	struct {
		int32_t picker;
	} current_s;
	struct {
		int32_t pickers;
		int32_t slots;
		int32_t portals;
		int32_t drivers;
	} number_s;
};

struct changer_position_t {
	struct {
		int32_t type;
		int32_t unit;
	} base_s;
	struct {
		int32_t flags;
	} data_s;
};

/* changer element status request */
struct changer_estreq_t {
	struct {
		int32_t type;
	} base_s;
	struct {
		int32_t flags;
	} data_s;
	struct {
		/* changer element status */
		struct changer_estatus_t {
			struct {
				int32_t type;
			} base_s;
			struct {
				u_char flags;
			} data_s;
			struct {
				/* changer volume tag */
				struct changer_voltag_t {
					struct {
						u_int8_t volume[1 +
						    CHANGER_VOLTAG_MAXLEN];
					} id_s;
					struct {
						u_short serial;
					} data_s;
				/*
				 * primary volume tag and alternate volume tag
				 */
				} primary_voltag_s, alternate_voltag_s;
			} struct_s;
		} *data_s;
	} struct_s;
};

enum changer_io_t {
	/* changer input output move */
	CHANGER_IO_MOVE = _IOC(IOC_IN, 0143, 0101,
	    sizeof(struct changer_move_t)),

	/* changer input output exchange */
	CHANGER_IO_EXCHANGE = _IOC(IOC_IN, 0143, 0102,
	    sizeof(struct changer_exchange_t)),

	/* changer input output position */
	CHANGER_IO_POSITION = _IOC(IOC_IN, 0143, 0103,
	    sizeof(struct changer_position_t)),

	/* changer input output picker get */
	CHANGER_IO_PICKER_GET = _IOC(IOC_OUT, 0143, 0104, sizeof(u_int32_t)),

	/* changer input output picker set */
	CHANGER_IO_PICKER_SET = _IOC(IOC_IN, 0143, 0105, sizeof(u_int32_t)),

	/* changer input output parameters get */
	CHANGER_IO_PRM_GET = _IOC(IOC_OUT, 0143, 0106,
	    sizeof(struct changer_parameters_t)),

	/* changer input output status get */
	CHANGER_IO_STATUS_GET = _IOC(IOC_IN, 0143, 0110,
	    sizeof(struct changer_estreq_t))
};
