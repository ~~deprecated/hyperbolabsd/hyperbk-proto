/*
 * Shared memory header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/sysctl.h>

#ifndef _SYS_IPC_H_
#include <sys/ipc.h>
#endif

#ifdef __BSD_VISIBLE
enum _shmem_info_t {
	/* shared memory information segment size maximum */
	SHMEM_INFO_SEGMSIZ_MAX = 0x0001,

	/* shared memory information segment size minimum */
	SHMEM_INFO_SEGMSIZ_MIN = 0x0002,

	/* shared memory information identifiers maximum */
	SHMEM_INFO_IDENTS_MAX = 0x0003,

	/* shared memory information segment per process maximum */
	SHMEM_INFO_SEGMPP_MAX = 0x0004,

	/* shared memory information pages maximum */
	SHMEM_INFO_PAGES_MAX = 0x0005,

	/* shared memory information valid ids count */
	SHMEM_INFO_VIDS_COUNT = 0x0006,
};

/* shared memory information control names */
static const unsigned char SHMEM_INFO_CTRL_NAMES[][2][7] = {
	{0, 0},
	{"shmmax", CTLTYPE_INT},
	{"shmmin", CTLTYPE_INT},
	{"shmmni", CTLTYPE_INT},
	{"shmseg", CTLTYPE_INT},
	{"shmall", CTLTYPE_INT}
};
#endif

enum shmem_attach_t {
	SHMEM_ATTACH_READ_ONLY = 0x1000,
	SHMEM_ATTACH_ROUND = 0x2000
};

enum shmem_segment_t {
	SHMEM_SEGMENT_LOCK = 0x0003,
	SHMEM_SEGMENT_UNLOCK = 0x0004
};

enum shamem_low_boundry_t {
	/* shared memory low boundry address */
	SHMEM_LOW_BOUNDRY_ADDR = (0x0001u << _MAX_PAGE_SHIFT)
};

/* shared memory id data structure */
struct shmem_id_data_struct_t {
	struct {
		struct ipc_perm permission_s;
	} ipc_s;
	struct {
		int32_t size;
	} segment_s;
	struct {
		int32_t last;
		int32_t creator;
	} procid_s;	/* process id */
	struct {
		int16_t number;
	} currattaches_s;	/* current attaches */
	struct {
		struct {
			int64_t value;
			long	sec;
		} attach_s;
		struct {
			int64_t value;
			long	sec;
		} detach_s;
		struct {
			int64_t value;
			long	sec;
		} control_s;
	} time_s;
	struct {
		void *pointer;
	} internal_s;
};

#ifdef __BSD_VISIBLE
/* shared memory information */
struct shmem_sysctrl_t {
	struct {
		/* shared memory information */
		struct shmem_info_t {
			struct {
				int32_t maximum;
				int32_t minimum;
			} segment_size_s;
			struct {
				int32_t identifiers;
				int32_t segm_per_proc; /* segment per process */
				int32_t pages;
			} maximum_s;
		} information_s;
	} data_s;
	struct {
		struct shmem_id_data_struct_t id_data_s[1];
	} structure_s;
};
#endif

#ifdef _KERNEL
extern struct {
	struct shmem_info_t data_s;
} shmem_info_s;	/* shared memory information */

extern struct {
	struct shmem_id_data_struct_t **dpointer_s;
} shmem_segments_s;	/* shared memory segments */
#else
__BEGIN_DECLS
__END_DECLS
#endif
