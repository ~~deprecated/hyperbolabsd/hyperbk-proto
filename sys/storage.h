/*
 * Storage header for HyperbolaBSD
 * Copyright (c) 2020-2021 Hyperbola Project
 * Copyright (c) 2020-2021 Márcio Silva <coadde@hyperbola.info>
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _SYS_STORAGE_H_
#define _SYS_STORAGE_H_

#include <sys/dev.h>
#include <sys/mutex.h>
#include <sys/rwlock.h>
#include <sys/time.h>
#include <sys/types.h>

enum storage_flg_t {
	/* storage flag constructed */
	STORAGE_FLG_CONSTRUCT = 001,

	/* storage flag opened */
	STORAGE_FLG_OPENED = 002,

	/* storage flag label unread */
	STORAGE_FLG_LBL_UNREAD = 004,

	/* storage flag label valid */
	STORAGE_FLG_LBL_VALID = 010
};

enum storage_stat_t {
	/* storage status closed */
	STORAGE_STAT_CLOSED = 000,

	/* storage status want open */
	STORAGE_STAT_WNT_O = 001,

	/* storage status want raw open */
	STORAGE_STAT_WNT_RAW_O = 002,

	/* storage status label read */
	STORAGE_STAT_LBL_READ = 003,

	/* storage status open */
	STORAGE_STAT_O = 004,

	/* storage status raw open */
	STORAGE_STAT_RAW_O = 005,

	/* storage status name length */
	STORAGE_STAT_NAME_LEN = 020
};

enum storage_mapflg_t {
	/* storage map flag partition open */
	STORAGE_MAPFLG_PART_O = 001,

	/* storage map flag block open */
	STORAGE_MAPFLG_BLOCK_O = 002
};

struct storage_status_t {
	struct {
		int8_t name[STORAGE_STAT_NAME_LEN];
	} data_s;
	/* storage status structure */
	struct storage_stat_struct_t {
		struct {
			int32_t counter;
		} busy_s;
		struct {
			u_quad_t read;
			u_quad_t write;
		} transfers_s;
		struct {
			u_quad_t operations;
		} seek_s;
		struct {
			u_quad_t read;
			u_quad_t write;
		} bytes_s;
		struct {
			struct timeval attach_s;
			struct timeval stamp_s;
			struct timeval spent_busy_s;
		} time_value_s;
	} structure_s;
};

struct storage_t {
	struct {
		struct {
			struct storage_t *next_s;
			struct storage_t **previous_s;
		} tailq_entry_s;
	} link_s;
	struct {
		struct rwlock lock_s;
		struct mutex mutex_s;
	} struct_s;
	struct {
		int8_t *pointer;
	} name_s;
	struct {
		struct	device_t *pointer_s;
		int32_t number;
	} device_s;
	struct {
		int32_t flags;
	} data_s;
	struct {
		struct storage_stat_struct_t structure_s;
	} status_s;
	struct {
		int32_t block;
		int32_t character;
		int32_t composite;
	} open_mask_s;
	struct {
		int32_t state;
	} label_s;
	struct {
		int32_t blocks;
		int32_t bytes;
	} shift_s;
	struct {
		struct disklabel *pointer_s;
	} label_info_s;	/* label information */
};

struct storage_list_t {
	struct {
		struct storage_t *first_s;
		struct storage_t **last_s;
	} tailq_head_s;
};

#ifdef _KERNEL
extern struct {
	struct {
		struct storage_list_t struct_s;
	} data_s;
} storage_list_s;

extern struct {
	struct {
		int32_t number;
	} data_s;
} storage_count_s;

extern struct {
	struct {
		int32_t status;
	} data_s;
} storage_change_s;
#endif

#endif /* _SYS_STORAGE_H_ */
