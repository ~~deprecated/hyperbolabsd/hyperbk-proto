/*
 * Device header for HyperbolaBSD
 * Copyright (c) 2020-2021 Hyperbola Project
 * Copyright (c) 2020-2021 Márcio Silva <coadde@hyperbola.info>
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _SYS_DEV_H_
#define _SYS_DEV_H_

#include <sys/types.h>

#define DEV_CLASS_DULL 00	/* device class dull */
#define DEV_CLASS_CPU 01	/* device class central processing unit */
#define DEV_CLASS_STORAGE 02	/* device class storage */
#define DEV_CLASS_NETWORK 03	/* device class network */
#define DEV_CLASS_TAPE 04	/* device class tape */
#define DEV_CLASS_SERIALLINE 05	/* device class serial line */

enum dev_action_t {
	/* device action deactivate */
	DEV_ACTION_DEACTIVATE = 01,

	/* device action quiesce */
	DEV_ACTION_QUIESCE = 02,

	/* device action suspend */
	DEV_ACTION_SUSPEND = 03,

	/* device action resume */
	DEV_ACTION_RESUME = 04,

	/* device action wakeup */
	DEV_ACTION_WAKEUP = 05,

	/* device action power down */
	DEV_ACTION_POWERDOWN = 06
};

enum dev_flags_t {
	/* device flags active */
	DEV_FLAGS_ACTIVE = 01,
};

enum dev_findst_t {
	/* device finding state not found */
	DEV_FINDST_NFOUND = 00,

	/* device finding state found */
	DEV_FINDST_FOUND = 01,

	/* device finding state star */
	DEV_FINDST_STAR = 02,

	/* device finding state not found disabled */
	DEV_FINDST_NFOUND_DIS = 03,

	/* device finding state star disabled */
	DEV_FINDST_STAR_DIS = 04
};

enum dev_detach_t {
	/* device detach force */
	DEV_DETACH_FORCE = 01,

	/* device detach quiet */
	DEV_DETACH_QUIET = 02
};

enum dev_st_t {
	/* device status quiet */
	DEV_ST_QUIET = 00,

	/* device status unconfigured */
	DEV_ST_UNCONFIGURED = 01,

	/* device status unsupported */
	DEV_ST_UNSUPPORTED = 02
};

#ifdef _KERNEL
enum dev_fw_t {
	/* device firmware maximum size */
	DEV_FW_MAXIMUM_SIZE = 040*040*040*040*005
};
#endif

struct device_t {
	struct {
		int8_t e_class;
	} enumerator_s;
	struct {
		struct {
			struct device_t *next_s;
			struct device_t **previous_s;
		} tailq_entry_s;
	} list_s;
	struct {
		/* configuration data */
		struct config_data_t {
			struct {
				struct cfattach *attach_s;
				struct cfdriver *driver_s;
			} config_s;	/* configuration */
			struct {
				int16_t number;
			} unit_s;
			struct {
				int16_t finding;
			} state_s;
			struct {
				long *pointer;
			} locators_s;
			struct {
				int32_t flags;
			} data_s;
			struct {
				int8_t *pointer;
			} p_parents_s;	/* potential parents */
			struct {
				int32_t names;
			} locator_s;
			struct {
				int8_t first_unit;
			} star_s;
		} *pointer_s;
		struct {
			int32_t number;
		} unit_s;
		struct {
			int8_t name[16];
		} external_s;
		struct {
			struct device_t *pointer_s;
		} parent_s;
		struct {
			int32_t flags;
		} data_s;
		struct {
			int32_t count;
		} reference_s;
	} config_data_s;	/* configuration data */
};

extern struct {
	struct config_data_t *data_s;
} device_config_s[1];	/* device configuration */

struct device_list_t {
	struct {
		struct device_t *first_s;
		struct device_t **last_s;
	} tailq_head_s;
};

/* device configuration attach */
struct device_config_attach_t {
	struct {
		u_long size;
	} device_s;
};

/* device configuration driver */
struct device_config_driver_t {
	struct {
		void **devices;
	} found_s;
	struct {
		int8_t		*name;
		u_int8_t	 d_class;
	} device_s;
	struct {
		int32_t config_subdevices;	/* configure subdevices */
	} indirect_s;
	struct {
		int32_t found_devices_array;
	} size_s;
};

struct pseudo_device_t {
	struct {
		int32_t count;
	} initial_s;
};

#ifdef _KERNEL
#ifdef DIAGNOSTIC
extern struct {
	struct {
		int32_t done;
	} initial_s;
} pseudo_device_s;
#endif

extern struct {
	struct {
		struct device_list_t all;
	} data_s;
} device_list_s;

extern struct {
	struct {
		int32_t verbose;
	} data_s;
} device_autoconfig_s;	/* device autoconfiguration */

extern volatile struct {
	struct {
		int32_t pending;
	} data_s;
} device_vol_config_s;	/* device volatile configuration */

struct device_name_to_block_t {
	struct {
		int8_t *pointer;
	} name_s;
	struct {
		int32_t major;
	} data_s;
};
#endif

#endif /* _SYS_DEV_H_ */
