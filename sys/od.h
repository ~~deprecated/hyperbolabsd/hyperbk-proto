/*
 * Optical Disc (OD) header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/ioccom.h>
#include <sys/types.h>

enum od_schlh_sst_t {
	/* optical disc subchannel header sound status audio invalid */
	OD_SCHLH_SST_AUDIOINV = 0000,

	/* optical disc subchannel header sound status play in progress */
	OD_SCHLH_SST_PLYINPROG = 0021,

	/* optical disc subchannel header sound status play paused */
	OD_SCHLH_SST_PLYPAUSED = 0022,

	/* optical disc subchannel header sound status play completed */
	OD_SCHLH_SST_PLYCOMPL = 0023,

	/* optical disc subchannel header sound status play error */
	OD_SCHLH_SST_PLYERROR = 0024,

	/* optical disc subchannel header sound status no status */
	OD_SCHLH_SST_NOSTATUS = 0025
};

enum od_schlh_addrf_t  {
	/* optical disc subchannel read address format logical block address */
	OD_SCHLR_ADDRF_LBA = 0001,

	/* optical disc subchannel read address format minute second frame */
	OD_SCHLR_ADDRF_MSF = 0002,

	/* optical disc subchannel read address format subq data */
	OD_SCHLR_ADDRF_SUBQD = 0000,

	/* optical disc subchannel read address format current position */
	OD_SCHLR_ADDRF_CURPOS = 0001,

	/* optical disc subchannel read address format media catalog */
	OD_SCHLR_ADDRF_MCTLG = 0002,

	/* optical disc subchannel read address format track information */
	OD_SCHLR_ADDRF_TRKINFO = 0003
};

enum od_loadst_opt_t {
	/* optical disc load status option abort */
	OD_LOADST_OPT_ABORT = 0001,

	/* optical disc load status option unload */
	OD_LOADST_OPT_UNLOAD = 0002,

	/* optical disc load status option load */
	OD_LOADST_OPT_LOAD = 0003
};

enum _cd_toc_t {
	/* compact disc table of contents -entry- read track leadout */
	CD_TOC_READ_TRKLEADOUT = 0252
};

enum dvd_gpkt_cmd_t {
	/* digital versatile disc generic packet command structure read/send */
	DVD_GPKT_CMD_STR_RSEND = 0255,

	/* digital versatile disc generic packet command key report */
	DVD_GPKT_CMD_KEY_RPT = 0244,

	/* digital versatile disc generic packet command key send */
	DVD_GPKT_CMD_KEY_SEND = 0243
};

enum dvd_str_t {
	/* digital versatile disc structure physical */
	DVD_STR_PHYSICAL = 0000,

	/* digital versatile disc structure copyright */
	DVD_STR_COPYRIGHT = 0001,

	/* digital versatile disc structure disc key */
	DVD_STR_DISC_KEY = 0002,

	/* digital versatile disc structure burst cutting area */
	DVD_STR_BCA = 0003,

	/* digital versatile disc structure manufacturing */
	DVD_STR_MANUFACTURING = 0004
};

enum dvd_staauth_t {
	/*
	 * digital versatile disc state authentication load status send a g i d
	 */
	DVD_STAAUTH_LST_SAGID = 0000,

	/* digital versatile disc state authentication host send challenge */
	DVD_STAAUTH_HOST_SCHLG = 0001,

	/* digital versatile disc state authentication load status send key 1 */
	DVD_STAAUTH_LST_SK1 = 0002,

	/*
	 * digital versatile disc state authentication load status send
	 * challenge
	 */
	DVD_STAAUTH_LST_SCHLG = 0003,

	/* digital versatile disc state authentication host send key 2 */
	DVD_STAAUTH_HOST_SK2 = 0004
};

enum dvd_staterm_t {
	/* digital versatile disc state termination established */
	DVD_STATERM_ESTAB = 0005,

	/* digital versatile disc state termination failure */
	DVD_STATERM_FAILURE = 0006
};

enum dvd_staother_t {
	/* digital versatile disc state other load status send title key */
	DVD_STAOTHER_LST_STK = 0007,

	/* digital versatile disc state other load status send a s f */
	DVD_STAOTHER_LST_SASF = 0010,

	/* digital versatile disc state other invalidate a g i d */
	DVD_STAOTHER_INVDAGID = 0011,

	/*
	 * digital versatile disc state other load status send regional playback
	 * control
	 */
	DVD_STAOTHER_LST_SRPC = 0012,

	/*
	 * digital versatile disc state other host send regional playback
	 * control
	 */
	DVD_STAOTHER_HOST_SRPC = 0013
};

enum dvd_stadata_t {
	/* digital versatile disc state data key size */
	DVD_STADATA_KEY_SIZE = 0005,

	/* digital versatile disc state data challenge size */
	DVD_STADATA_CHLG_SIZE = 0012
};

enum dvd_cpprot_mgt_cr_t {
	/* digital versatile disc copy protection management copyright no */
	DVD_CPPROT_MGT_CR_NO = 0000,

	/* digital versatile disc copy protection management copyright yes */
	DVD_CPPROT_MGT_CR_YES = 0001
};

enum dvd_cpprot_sec_t {
	/* digital versatile disc copy protection security none */
	DVD_CPPROT_SEC_NONE = 0000,

	/* digital versatile disc copy protection security exist */
	DVD_CPPROT_SEC_EXIST = 0001
};

enum dvd_copy_generation_management_system_t {
	/*
	 * digital versatile disc copy generation managenent system unrestricted
	 */
	DVD_CPGEN_MGTSYS_UR = 0000,

	/* digital versatile disc copy generation managenent system single */
	DVD_CPGEN_MGTSYS_S = 0001,

	/*
	 * digital versatile disc copy generation managenent system restricted
	 */
	DVD_CPGEN_MGTSYS_R = 0002
};

/* compact disk table of contents entry */
struct cd_toc_t {
	struct {
		u_int32_t :8;
	} nothing_first_s;
	/* optical disc address and control */
	struct od_addr_a_ctrl_t {
#if _BIG_ENDIAN == _BYTE_ORDER
		u_int32_t b_address_type:4;
		u_int32_t b_control:4;
#else
		u_int32_t b_control:4;
		u_int32_t b_address_type:4;
#endif
	} byte_order_s;
	struct {
		u_int32_t value:8;
	} track_s;
	struct {
		u_int32_t :8;
	} nothing_last_s;
	struct {
		union od_address_format_t {
			struct {
				u_int8_t address[4];
			} data_s;
			struct {
				u_int32_t :8;
				u_int32_t f_minute:8;
				u_int32_t f_second:8;
				u_int32_t f_frame:8;
			} min_sec_fr_s;	/* minute second frame */
			struct {
				u_int value;
			} logic_blk_addr_s;	/* logical block address */
		} union_u;
	} format_address_s;
};

/* optical disk subchannel information */
struct od_schl_info_t {
	struct {
		/* optical disc subchannel information */
		struct od_schl_header_t {
			struct {
				u_int16_t :8;
			} nothing_s;
			struct {
				u_int16_t sound:8;
			} status_s;
			struct {
				u_int8_t length[2];
			} data_s;
		} header_s;
	} base_s;
	union {
		/* optical disc subchannel media catalog */
		struct od_schl_mctlg_t {
			struct {
				struct {
					u_int32_t format:8;
				} data_s;
				struct {
					u_int32_t :24;
				} nothing_s;
			} base_s;
			/* optical disc valid data */
			struct od_valdata_t {
				struct {
#if _BIG_ENDIAN == _BYTE_ORDER
					u_int8_t valid:7;
					u_int8_t :1;
#else
					u_int8_t :1;
					u_int8_t valid:7;
#endif
				} byte_order_s;
				struct {
					u_int8_t number[15];
				} data_s;
			} mc_s;
		} media_catalog_s;
		/* optical disc subchannel position data */
		struct od_schl_pos_data_t {
			/* optical disc byte index and track */
			struct od_byte_idx_a_trk_t {
				struct {
					u_int32_t format:8;
				} data_s;
				struct od_addr_a_ctrl_t byte_order_s;
				struct {
					u_int32_t track:8;
					u_int32_t index:8;
				} number_s;
			} base_s;
			struct {
				union od_address_format_t absolute_u;
				union od_address_format_t relative_u;
			} address_s;
		} position_data_s;
		/* optical disc subchannel q data */
		struct od_schl_q_data_t {
			struct od_byte_idx_a_trk_t base_s;
			struct {
				u_int8_t absolute[4];
				u_int8_t relative[4];
			} address_s;
			struct od_valdata_t mc_s;
			struct od_valdata_t ti_s;
		} q_data_s;
		/* optical disc subchannel track information */
		struct od_schl_track_info_t {
			struct {
				struct {
					u_int32_t format:8;
				} data_s;
				struct {
					u_int32_t :8;
				} nothing_first_s;
				struct {
					u_int32_t number:8;
				} track_s;
				struct {
					u_int32_t :8;
				} nothing_last_s;
				struct od_valdata_t ti_s;
			} base_s;
		} track_information_s;
	} union_u;
};

/* optical disc input output track */
struct od_io_play_trk_t {
	/* optical disc input output track and index */
	struct od_io_trk_a_idx_t {
		u_int16_t track:8;
		u_int16_t index:8;
	} start_s;
	struct od_io_trk_a_idx_t end_s;
};

/* optical disc input output play blocks */
struct od_io_play_blks_t {
	struct {
		u_int64_t block:32;
		u_int64_t length:32;
	} base_s;
};

/* optical disk input output read subchannel */
struct od_io_read_schl_t {
	struct {
		u_int16_t address:8;
		u_int16_t data:8;
	} format_s;
	struct {
		u_int8_t value;
	} track_s;
	struct {
		struct {
			int32_t value;
		} length_s;
		struct od_schl_info_t *pointer_s;
	} data_s;
};

/* compact disc input output table of contents header */
struct cd_io_toc_header_t {
	struct {
		u_int32_t length:16;
	} data_s;
	struct {
		u_int32_t starting:8;
		u_int32_t ending:8;
	} track_s;
};

/* compact disc input output table of contents entry */
struct cd_io_read_toc_t {
	struct {
		u_int16_t format:8;
	} address_s;
	struct {
		u_int16_t starting:8;
	} track_s;
	struct {
		struct {
			u_int16_t value;
		} length_s;
		struct cd_toc_t *pointer_s;
	} data_s;
};

struct od_io_patch_t {
	struct {
		u_int8_t value;
	} data_s[4];
};

struct od_io_volume_t {
	struct {
		u_int8_t value;
	} data_s[4];
};

/* optical disc input output play minute second frame */
struct od_io_play_min_sec_fr_t {
	/* optical disc input output minute second frame */
	struct od_io_min_sec_fr_t {
		u_int8_t minute;
		u_int8_t second;
		u_int8_t frame;
	} start_s;
	struct od_io_min_sec_fr_t end_s;
};

struct od_io_load_status_t {
	struct {
		u_int16_t l_options:8;
		u_int16_t l_slot:8;
	} base_s;
};

union dvd_structure_t {
	struct dvd_burst_cutting_area_t {
		struct {
			u_char d_type;
		} base_s;
		struct {
			int32_t d_length;
			u_char	d_value[188];
		} data_s;
	} burst_cutting_area_s;
	struct dvd_copyright_t {
		struct {
			u_char d_type;
		} base_s;
		struct {
			u_char number;
		} layer_s;
		struct {
			int32_t d_cpst;
			u_char	d_rmi;
		} data_s;
	} copyright_s;
	struct dvd_disc_key_t {
		struct {
			u_char d_type;
		} base_s;
		struct {
			u_char d_agid;
			u_char d_value[2048];
		} data_s;
	} disc_key_s;
	struct dvd_manufacture_t {
		struct {
			u_char d_type;
		} base_s;
		struct {
			u_char number;
		} layer_s;
		struct {
			int32_t d_length;
			u_char	d_value[2048];
		} data_s;
	} manufacture_s;
	struct dvd_physical_t {
		struct {
			u_char d_type;
		} base_s;
		struct {
			struct {
				u_char value;
			} number_s;
			struct dvd_layer_t {
				struct {
					u_short version:8;
					u_short type:8;
				} book_s;
				struct {
					u_char minimum;
				} rate_s;
				struct {
					u_char size;
				} disc_s;
				struct {
					u_char type;
				} layer_s;
				struct {
					u_char path;
				} track_s;
				struct {
					u_char number;
				} layers_s;
				struct {
					u_short track:8;
					u_short linear:8;
				} density_s;
				struct {
					u_char value;
				} burst_cutting_area_s;
				struct {
					struct {
						u_int value;
					} start_s;
					struct {
						u_int value;
						u_int l0;
					} end_s;
				} sector_s;
			} data_s[4];
		} layer_s;
	} physical_s;
	struct {
		u_char d_type;
	} base_s;
};

struct dvd_key_t {
	u_char value[5];
};

struct dvd_challenge_t {
	u_char value[10];
};

/* digital versatile disc auth information */
union dvd_auth_info_t {
	/* disc versatile disc send challenge */
	struct dvd_send_chlg_t {
		struct dvd_send_t {
			u_char d_type;
			u_char d_agid;
		} base_s;
		struct {
			u_char value;
		} challenge_s[DVD_STADATA_CHLG_SIZE];
	} host_send_chlg_s;	/* host send challenge */
	struct dvd_send_key_t {
		struct dvd_send_t base_s;
		struct {
			u_char value;
		} key_s[DVD_STADATA_KEY_SIZE];
	} host_send_key_s;
	/* digital versatile disc host send rpc state */
	struct dvd_host_send_rpcst_t {
		struct {
			u_char d_type;
		} base_s;
		struct {
			u_char value;
		} pdrc_s;
	} host_send_rpcstate_s;
	/* digital versatile disc load status send a g i d */
	struct dvd_lst_send_agid_t {
		struct dvd_send_t base_s;
	} loadstatus_send_agid_s;
	/* digital versatile disc load status send a s f */
	struct dvd_lst_send_asf_t {
		struct dvd_send_t base_s;
		struct {
			u_char value;
		} asf_s;
	} loadstatus_send_asf_s;
	/* load status send challenge */
	struct dvd_send_chlg_t loadstatus_send_chlg_s;
	/* load status send key */
	struct dvd_send_key_t loadstatus_send_key_s;
	/* digital versatile disc load status send rpc state */
	struct dvd_lst_s_rpcst_t {
		struct {
			u_char d_type;
		} base_s;
		struct {
			u_char d_vra;
			u_char d_ucca;
		} data_s;
		struct {
			u_char mask;
		} region_s;
		struct {
			u_char scheme;
		} rpc_s;
	} loadstatus_s_rpcst_s;	/* load status send rpc state */
	/* digital versatile disc load status send title key */
	struct dvd_lst_stk_t {
		struct dvd_send_t base_s;
		struct {
			u_char value;
		} titlekey_s[DVD_STADATA_KEY_SIZE];
		struct {
			/* logical block address */
			int32_t d_logic_blk_addr;
			/* copy protection management */
			u_char	d_cpprot_mgt;
			/* copy protection security */
			u_char	d_cpprot_sec;
			/* copy generation management system */
			u_char	d_cpgen_mgtsys;
		} data_s;
	} loadstatus_stk_s;	/* load status send title key */
	struct {
		u_char d_type;
	} base_s;
};

enum od_io_t {
	/* optical disc input output play tracks */
	OD_IO_PLAY_TRACKS = _IOC(IOC_IN, 00143, 00001,
	    sizeof(struct od_io_play_trk_t)),

	/* optical disc input output play blocks */
	OD_IO_PLAY_BLOCKS = _IOC(IOC_IN, 00143, 00002,
	    sizeof(struct od_io_play_blks_t)),

	/* optical disc input output read subchannel */
	OD_IO_READ_SUBCHANNEL = _IOC(IOC_INOUT, 00143, 00003,
	    sizeof(struct od_io_read_schl_t)),

	/* optical disc input output read m s address */
	OD_IO_READ_MS_ADDRESS = _IOC(IOC_INOUT, 00143, 00006, sizeof(int)),

	/* optical disc input output set patch */
	OD_IO_SET_PATCH =_IOC(IOC_IN, 00143, 00011,
	    sizeof(struct od_io_patch_t)),

	/* optical disc input output play minute second frame */
	OD_IO_PLAY_MSF = _IOC(IOC_IN, 00143, 00031,
	    sizeof(struct od_io_play_min_sec_fr_t)),

	/* optical disc input output load status */
	OD_IO_LOAD_STATUS = _IOC(IOC_IN, 00143, 00032,
	    sizeof(struct od_io_load_status_t)),

	/* optical disc input output get volume */
	OD_IO_GET_VOLUME = _IOC(IOC_OUT, 00143, 00012,
	    sizeof(struct od_io_volume_t)),

	/* optical disc input output set volume */
	OD_IO_SET_VOLUME = _IOC(IOC_IN, 00143, 00013,
	    sizeof(struct od_io_volume_t)),

	/* optical disc input output set mono */
	OD_IO_SET_MONO = _IOC(IOC_VOID, 00143, 00014, 00000),

	/* optical disc input output set stereo */
	OD_IO_SET_STEREO = _IOC(IOC_VOID, 00143, 00015, 00000),

	/* optical disc input output set mute */
	OD_IO_SET_MUTE = _IOC(IOC_VOID, 00143, 00016, 00000),

	/* optical disc input output set left */
	OD_IO_SET_LEFT = _IOC(IOC_VOID, 00143, 00017, 00000),

	/* optical disc input output set right */
	OD_IO_SET_RIGHT = _IOC(IOC_VOID, 00143, 00020, 00000),

	/* optical disc input output set debug */
	OD_IO_SET_DEBUG = _IOC(IOC_VOID, 00143, 00021, 00000),

	/* optical disc input output clear debug */
	OD_IO_CLEAR_DEBUG = _IOC(IOC_VOID, 00143, 00022, 00000),

	/* optical disc input output pause */
	OD_IO_PAUSE = _IOC(IOC_VOID, 00143, 00023, 00000),

	/* optical disc input output resume */
	OD_IO_RESUME = _IOC(IOC_VOID, 00143, 00024, 00000),

	/* optical disc input output reset */
	OD_IO_RESET = _IOC(IOC_VOID, 00143, 00025, 00000),

	/* optical disc input output start */
	OD_IO_START = _IOC(IOC_VOID, 00143, 00026, 00000),

	/* optical disc input output stop */
	OD_IO_STOP = _IOC(IOC_VOID, 00143, 00027, 00000),

	/* optical disc input output eject */
	OD_IO_EJECT = _IOC(IOC_VOID, 00143, 00030, 00000),

	/* optical disc input output allow */
	OD_IO_ALLOW = _IOC(IOC_VOID, 00143, 00031, 00000),

	/* optical disc input output prevent */
	OD_IO_PREVENT = _IOC(IOC_VOID, 00143, 00032, 00000),

	/* optical disc input output close */
	OD_IO_CLOSE = _IOC(IOC_VOID, 00143, 00033, 00000)
};

enum cd_io_t {
	/* compact disc input output read table of contents header */
	CD_IO_READ_TOC_HEADER = _IOC(IOC_OUT, 00143, 00004,
	    sizeof(struct cd_io_toc_header_t)),

	/* compact disc input output read table of contents entries/entrys */
	CD_IO_READ_TOC_ENTRS = _IOC(IOC_INOUT, 00143, 00005,
	    sizeof(struct cd_io_read_toc_t))
};

enum dvd_io_t {
	/* digital versatile disc input output read structure */
	DVD_IO_READ_STRUCTURE = _IOC(IOC_INOUT, 00144, 00000,
	    sizeof(union dvd_structure_t)),

	/* digital versatile disc input output write structure */
	DVD_IO_WRITE_STRUCTURE = _IOC(IOC_INOUT, 00144, 00001,
	    sizeof(union dvd_structure_t)),

	/* digital versatile disc input output auth information */
	DVD_IO_AUTH_INFO = _IOC(IOC_INOUT, 00144, 00002,
	    sizeof(union dvd_auth_info_t))
};
