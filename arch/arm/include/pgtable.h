/*
 * Page Table Entry (PTE) header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

#ifndef _LOCORE
	typedef u_int pgdir_t;
	typedef u_int pgtable_t;
#endif

enum pgtable_lvl2_lgpg_t {
	/* page table entry level 2 large page size */
	PGTABLE_LVL2_LGPG_SIZE = 00200000,

	/* page table entry level 2 large page offset */
	PGTABLE_LVL2_LGPG_OFF = (- 1) + PGTABLE_LVL2_LGPG_SIZE,

	/* page table entry level 2 large page frame */
	PGTABLE_LVL2_LGPG_FRM = ~ PGTABLE_LVL2_LGPG_OFF,

	/* page table entry level 2 large page shift */
	PGTABLE_LVL2_LGPG_SHFT = 00000020
};

enum pgtable_lvl2_smpg_t {
	/* page table entry level 2 small page size */
	PGTABLE_LVL2_SMPG_SIZE = 00010000,

	/* page table entry level 2 small page offset */
	PGTABLE_LVL2_SMPG_OFF = (- 1) + PGTABLE_LVL2_SMPG_SIZE,

	/* page table entry level 2 small page frame */
	PGTABLE_LVL2_SMPG_FRM = ~ PGTABLE_LVL2_SMPG_OFF,

	/* page table entry level 2 small page shift */
	PGTABLE_LVL2_SMPG_SHFT = 00000014
};

enum pgtable_lvl2_tnpg_t {
	/* page table entry level 2 tiny page size */
	PGTABLE_LVL2_TNPG_SIZE = 00002000,

	/* page table entry level 2 tiny page offset */
	PGTABLE_LVL2_TNPG_OFF = (- 1) + PGTABLE_LVL2_TNPG_SIZE,

	/* page table entry level 2 tiny page frame */
	PGTABLE_LVL2_TNPG_FRM = ~ PGTABLE_LVL2_TNPG_OFF,

	/* page table entry level 2 tiny page shift */
	PGTABLE_LVL2_TNPG_SHFT = 00000012
};

enum pgtable_lvl1_addr_t {
	/* page table entry level 1 address bits */
	PGTABLE_LVL1_ADDR_BITS = 037774000000
};

enum pgtable_lvl2_addr_t {
	/* page table entry level 2 address bits */
	PGTABLE_LVL2_ADDR_BITS = 000003770000
};

enum pgtable_lvl1_tbl_t {
	/* page table entry level 1 table size */
	PGTABLE_LVL1_TBL_SIZ = 040000,
};

enum pgtable_lvl2_tbl_t {
	/* page table entry level 2 table size */
	PGTABLE_LVL2_TBL_SIZ = 010000,

	/* page table entry level 2 table size real */
	PGTABLE_LVL2_TBL_SIZ_R = 002000
};

enum pgtable_lvl1_tp_t {
	/* page table entry level 1 type invalid */
	PGTABLE_LVL1_TP_INV = 00,

	/* page table entry level 1 type coarse */
	PGTABLE_LVL1_TP_CRS = 01,

	/* page table entry level 1 type section */
	PGTABLE_LVL1_TP_SECT = 02,

	/* page table entry level 1 type fine */
	PGTABLE_LVL1_TP_FINE = 03,

	/* page table entry level 1 type mask */
	PGTABLE_LVL1_TP_MASK = 03
};

enum pgtable_lvl1_sect_t {
	/* page table entry level 1 section size */
	PGTABLE_LVL1_SECT_SIZE = 04000000,

	/* page table entry level 1 section offset */
	PGTABLE_LVL1_SECT_OFF = (- 1) + PGTABLE_LVL1_SECT_SIZE,

	/* page table entry level 1 section frame */
	PGTABLE_LVL1_SECT_FRM = ~ PGTABLE_LVL1_SECT_OFF,

	/* page table entry level 1 section shift */
	PGTABLE_LVL1_SECT_SHFT = 00000024,

	/* page table entry level 1 section buffeable */
	PGTABLE_LVL1_SECT_BUFF = 000000000004,

	/* page table entry level 1 section cacheable */
	PGTABLE_LVL1_SECT_CACH = 000000000010,

	/* page table entry level 1 section implementation defined */
	PGTABLE_LVL1_SECT_IDEF = 000000000012,

	/* page table entry level 1 section domain mask */
	PGTABLE_LVL1_SECT_DMSK = 000000000017 << 000000000005,

	/* page table entry level 1 section physical address mask */
	PGTABLE_LVL1_SECT_PAM = 037774000000
};

enum pgtable_lvl1_sec7_t {
	/* page table entry level 1 section armv7 type extension mask */
	PGTABLE_LVL1_SEC7_TXM = 00000007 << 00000014,

	/* page table entry level 1 section armv7 non secure */
	PGTABLE_LVL1_SEC7_NONS = 02000000,

	/* page table entry level 1 section armv7 super section */
	PGTABLE_LVL1_SEC7_SSEC = 01000000,

	/* page table entry level 1 section armv7 not global */
	PGTABLE_LVL1_SEC7_NOTG = 00400000,

	/* page table entry level 1 section armv7 shareable */
	PGTABLE_LVL1_SEC7_SHA = 00200000,

	/* page table entry level 1 section armv7 access flag */
	PGTABLE_LVL1_SEC7_AFLG = 00002000,

	/* page table entry level 1 section armv7 implementation defined */
	PGTABLE_LVL1_SEC7_IDEF = 00001000,

	/* page table entry level 1 section armv7 execute never */
	PGTABLE_LVL1_SEC7_XN = 00000020,

	/* page table entry level 1 section armv7 privileged execute never */
	PGTABLE_LVL1_SEC7_PXN = 00000001
};

enum pgtable_lvl1_crs_t {
	/* page table entry level 1 coarse implementation 0 defined */
	PGTABLE_LVL1_CRS_I0D = 000000000004,

	/* page table entry level 1 coarse implementation 1 defined */
	PGTABLE_LVL1_CRS_I1D = 000000000010,

	/* page table entry level 1 coarse implementation 2 defined */
	PGTABLE_LVL1_CRS_I2D = 000000000012,

	/* page table entry level 1 coarse domain mask */
	PGTABLE_LVL1_CRS_DMSK = 000000000017 << 000000000005,

	/* page table entry level 1 coarse physical address mask */
	PGTABLE_LVL1_CRS_PAM = 037777776000
};

enum pgtable_lvl1_crs7_t {
	/* page table entry level 1 coarse armv7 implementation defined */
	PGTABLE_LVL1_CRS7_IDEF = 00001000,

	/* page table entry level 1 coarse armv7 non secure */
	PGTABLE_LVL1_CRS7_NONS = 00000010,

	/* page table entry level 1 coarse armv7 privileged execute never */
	PGTABLE_LVL1_CRS7_PXN = 00000001
};

enum pgtable_lvl1_fine_t {
	/* page table entry level 1 fine implementation 0 defined */
	PGTABLE_LVL1_FINE_I0D = 000000000004,

	/* page table entry level 1 fine implementation 1 defined */
	PGTABLE_LVL1_FINE_I1D = 000000000010,

	/* page table entry level 1 fine implementation 2 defined */
	PGTABLE_LVL1_FINE_I2D = 000000000012,

	/* page table entry level 1 fine domain mask */
	PGTABLE_LVL1_FINE_DMSK = 000000000017 << 000000000005,

	/* page table entry level 1 fine physical address mask */
	PGTABLE_LVL1_FINE_PAM = 037777770000
};

enum pgtable_lvl2_tp_t {
	/* page table entry level 2 type invalid */
	PGTABLE_LVL2_TP_INV = 00,

	/* page table entry level 2 type large page */
	PGTABLE_LVL2_TP_LGPG = 01,

	/* page table entry level 2 type small page */
	PGTABLE_LVL2_TP_SMPG = 02,

	/* page table entry level 2 type tiny page */
	PGTABLE_LVL2_TP_TNPG = 03,

	/* page table entry level 2 type mask */
	PGTABLE_LVL2_TP_MASK = 03
};

enum pgtable_lvl2_pg_t {
	/* page table entry level 2 page buffeable */
	PGTABLE_LVL2_PG_BUFF = 000000000004,

	/* page table entry level 2 page cacheable */
	PGTABLE_LVL2_PG_CACH = 000000000010
};

enum pgtable_lvl2_lpg7_t {
	/* page table entry level 2 large page armv7 type extension mask */
	PGTABLE_LVL2_LGP7_TXM = 00000007 << 00000014,

	/* page table entry level 2 large page armv7 execute never */
	PGTABLE_LVL2_LGP7_XN = 00100000,

	/* page table entry level 2 small page armv7 type extension mask */
	PGTABLE_LVL2_SMP7_TXM = 00000007 << 00000006,

	/* page table entry level 2 small page armv7 execute never */
	PGTABLE_LVL2_SMP7_XN = 00000001,

	/* page table entry level 2 page armv7 access flag */
	PGTABLE_LVL2_P7_AFLG = 00020,

	/* page table entry level 2 page armv7 shareable */
	PGTABLE_LVL2_P7_SHA = 02000,

	/* page table entry level 2 page armv7 not global */
	PGTABLE_LVL2_P7_NOTG = 04000
};

enum pgtable_aperm_t {
	/* page table entry access permissions kernel space read only */
	PGTABLE_APERM_KRO = 00,

	/* page table entry access permissions kernel space read + write */
	PGTABLE_APERM_KRW = 01,

	/*
	 * page table entry access permissions kernel space read + write and
	 * user space read only
	 */
	PGTABLE_APERM_KRW_URO = 02,

	/*
	 * page table entry access permissions kernel space read + write and
	 * user space read + write
	 */
	PGTABLE_APERM_KRW_URW = 03
};

enum pgtable_aper7_t {
	/* page table entry access permissions armv7 kernel space read only */
	PGTABLE_APER7_KRO = 05,

	/*
	 * page table entry access permissions armv7 kernel space read only and
	 * user space read only
	 */
	PGTABLE_APER7_KRO_URO = 07,
};

enum pgtable_dactl_t {
	/* page table entry domain access control fault */
	PGTABLE_DACTL_FAULT = 00,

	/* page table entry domain access control client */
	PGTABLE_DACTL_CLIENT = 01,

	/* page table entry domain access control reserved */
	PGTABLE_DACTL_RESERVED = 02,

	/* page table entry domain access control manager */
	PGTABLE_DACTL_MANAGER = 03
};

int32_t
pagetable_level1_any_domain(int32_t value)
{
	return value << 005;
}

int32_t
pagetable_level1_section_accesspermitions(int32_t value)
{
	return value << 012;
}

int32_t
pagetable_level1or2_any_armv7_typeextension(int32_t value)
{
	return (007 & value) << 014;
}

int32_t
pagetable_level2_page_accesspermitions0(int32_t value)
{
	return value << 004;
}

int32_t
pagetable_level2_page_accesspermitions1(int32_t value)
{
	return value << 006;
}

int32_t
pagetable_level2_page_accesspermitions2(int32_t value)
{
	return value << 010;
}

int32_t
pagetable_level2_page_accesspermitions3(int32_t value)
{
	return value << 012;
}

int32_t
pagetable_level2_page_accesspermitions(int32_t value)
{
	return (value << 012) | (value << 010) | (value << 006) |
	    (value << 004);
}

int32_t
pagetable_level2_page_armv7_small_typeextension(int32_t value)
{
	return (007 & value) << 006;
}

int32_t
pagetable_level2_page_armv7_accesspermitions(int32_t value)
{
	return ((002 & value) << 004) | ((004 & value) << 007);
}
