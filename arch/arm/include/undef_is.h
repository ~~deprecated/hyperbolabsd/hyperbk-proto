/*
 * Undefined instructions header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifdef _KERNEL
enum undefis_fltp_t {
	/* undefined instructions; floating points co-processor */
	UNDEFIS_FLTP_COPROC = 001,

	/* undefined instructions; floating points co-processor 2 */
	UNDEFIS_FLTP_COPROC2 = 002
};

enum undefis_max_coprocs_t {
	/* undefined instructions; maximum co-processors */
	UNDEFIS_MAX_COPROCS = 010
};

/* undefined instructions handler */
struct undefis_handler_t {
	struct {
		struct undefis_handler_t *next_s;
		struct undefis_handler_t *previous_s;
	} link_s;
//	struct {
//	} function_s;
};
#endif
