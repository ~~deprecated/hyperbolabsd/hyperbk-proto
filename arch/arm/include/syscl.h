/*
 * System call header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

enum _syscl_sync_i_cache_t {
	/* system call sync i cache */
	SYSCL_SYNC_I_CACHE = 00
};

enum syscl_drain_write_buf_t {
	/* system call drain write buffer */
	SYSCL_DRAIN_WRITE_BUF = 01
};

struct syscl_sync_i_cache_t {
	struct {
		unsigned long address;
		unsigned long length;
	} arguments_s;
};

//#ifndef _KERNEL
//__BEGIN_DECLS
//__END_DECLS
//#endif
