/*
 * Signal header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _LOCORE
#if __XPG_VISIBLE >= 420 || __BSD_VISIBLE
#include <sys/types.h>

struct sig_context_t {
	struct {
		int32_t value;
	} cookie_s;
	struct {
		int32_t value;
	} mask_s;
	struct {
		u_int32_t spsr;	/* saved process state register */
	} prog_status_reg_s;	/* program status register */
	struct {
		u_int32_t r0;		/* general purpose r0 (user mode) */
		u_int32_t r1;		/* general purpose r1 (user mode) */
		u_int32_t r2;		/* general purpose r2 (user mode) */
		u_int32_t r3;		/* general purpose r3 (user mode) */
		u_int32_t r4;		/* general purpose r4 (user mode) */
		u_int32_t r5;		/* general purpose r5 (user mode) */
		u_int32_t r6;		/* general purpose r6 (user mode) */
		u_int32_t r7_syscl;	/* general purpose r7 (system call
					    number; user mode) */
		u_int32_t r8;		/* general purpose r8 (user mode) */
		u_int32_t r9;		/* general purpose r9 (user mode) */
		u_int32_t r10;		/* general purpose r10 (user mode) */
		u_int32_t r11_fp;	/* general purpose r11 (frame pointer;
					    user mode) */
		u_int32_t r12_ip;	/* general purpose r12 (intra procedural
					    call; user mode) */
		u_int32_t r13_sp_usr;	/* general purpose r13_usr (stack
					    pointer; user mode) */
		u_int32_t r14_lr_usr;	/* general purpose r14_usr (link
					    register; user mode) */
		u_int32_t r14_lr_svc;	/* general purpose r14_svc (link
					    register; supervisor mode) */
		u_int32_t r15_pc;	/* general purpose r15 (program
					    counter) */
	} general_reg_s;
	struct {
		u_int32_t used;
		u_int32_t scr;
		u_int64_t reg[32];
	} floating_point_s;
};
#endif	/* __XPG_VISIBLE >= 420 || __BSD_VISIBLE */
#endif	/* !_LOCORE */
