/*
 * Fast Interrupt Request (FIQ) header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

enum fintreq_handler_canpush_t {
	/* fast interrupt request handler, can push */
	FINTREQ_HNDLER_CANPUSH = 01
};

/* fast interrupt request handler */
struct fintreq_handler_t {
	struct {
		struct fintreq_handler_t *next_s;
		struct fintreq_handler_t **previous_s;
	} list_s;
	struct {
		void *pointer;
	} function_s;
	struct {
		unsigned long value;
	} size_s;
	struct {
		int32_t value;
	} flags_s;
	struct {
		/* fast interrupt request */
		struct fintreq_t {
			u_int32_t r8;	/* r8 (general purpose) */
			u_int32_t r9;	/* r9 (general purpose) */
			u_int32_t r10;	/* r10 (general purpose) */
			u_int32_t r11;	/* r11 and fp (frame pointer) */
			u_int32_t r12;	/* r12 and ip (intra procedural call) */
			u_int32_t r13;	/* r13 and sp (stack pointer) */
		} register_s;
	} registers_s;
};
