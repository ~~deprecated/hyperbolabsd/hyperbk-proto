/*
 * Standard for Floating-Point Arithmetic (IEEE 754) header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

enum ieee754_sgl_t {
	/* ieee 754 single exponent bits */
	IEEE754_SGL_EXP_BITS = 000000010,

	/* ieee 754 single fraction bits */
	IEEE754_SGL_FRC_BITS = 000000027
};

enum ieee754_dbl_t {
	/* ieee 754 double exponent bits */
	IEEE754_DBL_EXP_BITS = 000000013,

	/* ieee 754 double fraction high bits */
	IEEE754_DBL_FRC_HBITS = 000000024,

	/* ieee 754 double fraction low bits */
	IEEE754_DBL_FRC_LBITS = 000000040,

	/* ieee 754 double fraction bits */
	IEEE754_DBL_FRC_BITS = 000000064
};

#ifndef __VFP_FP__
enum ieee754_e80_t {
	/* ieee 754 extend 80 exponent bits */
	IEEE754_E80_EXP_BITS = 000000017,

	/* ieee 754 extend 80 fraction high bits */
	IEEE754_E80_FRC_HBITS = 000000037,

	/* ieee 754 extend 80 fraction low bits */
	IEEE754_E80_FRC_LBITS = 000000040,

	/* ieee 754 extend 80 fraction bits */
	IEEE754_E80_FRC_BITS = 000000100
};

enum ieee754_ext_t {
	/* ieee 754 extend exponent bits */
	IEEE754_EXT_EXP_BITS = 000000017,

	/* ieee 754 extend fraction high bits */
	IEEE754_EXT_FRC_HBITS = 000000020,

	/* ieee 754 extend fraction high mbits */
	IEEE754_EXT_FRC_HMBITS = 000000040,

	/* ieee 754 extend fraction low mbits */
	IEEE754_EXT_FRC_LMBITS = 000000040,

	/* ieee 754 extend fraction low bits */
	IEEE754_EXT_FRC_LBITS = 000000040,

	/* ieee 754 extend fraction bits */
	IEEE754_EXT_FRC_BITS = 000000160,
};
#endif

enum ieee754_exp_infnan_t {
	/* ieee 754 single exponent infinite or not a number (nan) */
	IEEE754_SGL_EXP_INFNAN = 000000377,

	/* ieee 754 double exponent infinite or not a number (nan) */
	IEEE754_DBL_EXP_INFNAN = 000003777,
#ifndef __VFP_FP__
	/* ieee 754 extend 80 exponent infinite or not a number (nan) */
	IEEE754_E80_EXP_INFNAN = 000077777,

	/* ieee 754 extend exponent infinite or not a number (nan) */
	IEEE754_EXT_EXP_INFNAN = 000077777
#endif
};

enum ieee754_exp_bias_t {
	/* ieee 754 single exponent bias */
	IEEE754_SGL_EXP_BIAS = 000000177,

	/* ieee 754 double exponent bias */
	IEEE754_DBL_EXP_BIAS = 000001777,
#ifndef __VFP_FP__
	/* ieee 754 extend 80 exponent bias */
	IEEE754_E80_EXP_BIAS = 000037777,

	/* ieee 754 extend exponent bias */
	IEEE754_EXT_EXP_BIAS = 000037777
#endif
};

#if FALSE
enum ieee754_quiet_nan_t {
	/* ieee 754 single quiet not a number (nan) */
	IEEE754_SGL_QUIET_NAN = 020000000,

	/* ieee 754 double quiet not a number (nan) */
	IEEE754_DBL_QUIET_NAN = 002000000,
#ifndef __VFP_FP__
	/* ieee 754 extend 80 quiet not a number (nan) */
	IEEE754_E80_QUIET_NAN = 000100000,

	/* ieee 754 extend quiet not a number (nan) */
	IEEE754_EXT_QUIET_NAN = 000100000
#endif
};
#endif

struct ieee754_single_t {
	struct {
		u_int32_t fraction:23;
		u_int32_t exponent:8;
		u_int32_t signal:1;
	} data_s;
};

#ifdef __VFP_FP__
struct ieee754_double_t {
	struct {
		struct {
			u_int32_t low;
			u_int32_t high:20;
		} fraction_s;
		u_int32_t exponent:11;
		u_int32_t signal:1;
	} data_s;
};
#else
struct ieee754_double_t {
	struct {
		u_int32_t fraction_high:20;
		u_int32_t exponent:11;
		u_int32_t signal:1;
		u_int32_t low;
	} data_s;
};

union ieee754_double_union_t {
	struct ieee754_double_t double_s;
	double float_double;
};

struct ieee754_extend80_t {
	struct {
		u_int32_t exponent:15;
		u_int32_t zero:16;
		u_int32_t signal:1;
		u_int32_t fraction_high:31;
		u_int32_t j:1;
		u_int32_t fraction_low;
	} data_s;
};

struct ieee754_extend_t {
	struct {
		u_int32_t fraction_high:16;
		u_int32_t exponent:15;
		u_int32_t signal:1;
		struct {
			u_int32_t high_m;
			u_int32_t low_m;
			u_int32_t low;
		} fraction_s;
	} data_s;
};
#endif
