/*
 * Stack frame header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _LOCORE
#include <sys/siginfo.h>

#include <machine/sig.h>

/* stack frame trap */
struct stkfrm_trap_t {
	struct {
		long spsr;	/* saved process state register */
	} prog_status_reg_s;	/* program status register */
	struct {
		long r0;		/* general purpose r0 */
		long r1;		/* general purpose r1 */
		long r2;		/* general purpose r2 */
		long r3;		/* general purpose r3 */
		long r4;		/* general purpose r4 */
		long r5;		/* general purpose r5 */
		long r6;		/* general purpose r6 */
		long r7_syscl;		/* general purpose r7 */
		long r8;		/* general purpose r8 */
		long r9;		/* general purpose r9 */
		long r10;		/* general purpose r10 */
		long r11_fp;		/* general purpose r11 */
		long r12_ip;		/* general purpose r12 */
		long r13_sp_usr;	/* general purpose r13_usr */
		long r14_lr_usr;	/* general purpose r14_usr */
		long r13_sp_svc;	/* general purpose r13_svc */
		long r14_lr_svc;	/* general purpose r14_svc */
		long r15_pc;		/* general purpose r15 */
	} general_reg_s;
	struct {
		long pad;
	} pad_reg_s;
};

/* stack frame interrupt request */
struct stkfrm_irq_t {
	struct {
		u_long spsr;	/* saved process state register */
	} prog_status_reg_s;	/* program status register */
	struct {
		u_long r0;		/* general purpose r0 */
		u_long r1;		/* general purpose r1 */
		u_long r2;		/* general purpose r2 */
		u_long r3;		/* general purpose r3 */
		u_long r4;		/* general purpose r4 */
		u_long r5;		/* general purpose r5 */
		u_long r6;		/* general purpose r6 */
		u_long r7_syscl;	/* general purpose r7 */
		u_long r8;		/* general purpose r8 */
		u_long r9;		/* general purpose r9 */
		u_long r10;		/* general purpose r10 */
		u_long r11_fp;		/* general purpose r11 */
		u_long r12_ip;		/* general purpose r12 */
		u_long r13_sp_usr;	/* general purpose r13_usr */
		u_long r14_lr_usr;	/* general purpose r14_usr */
		u_long r13_sp_svc;	/* general purpose r13_svc */
		u_long r14_lr_svc;	/* general purpose r14_svc */
		u_long r15_pc;		/* general purpose r15 */
	} general_reg_s;
	struct {
		u_long pad;
	} pad_reg_s;
};

/* stack frame switch */
struct stkfrm_switch_t {
	struct {
		u_long pad;
	} pad_reg_s;
	struct {
		u_long r4;		/* general purpose r4 */
		u_long r5;		/* general purpose r5 */
		u_long r6;		/* general purpose r6 */
		u_long r7_syscl;	/* general purpose r7 */
		u_long r15_pc;		/* general purpose r15 */
	} general_reg_s;
};

/* stack frame signal */
struct stkfrm_signal_t {
	struct {
		int32_t value;
	} number_s;
	struct {
		siginfo_t *pointer_s;
	} information_ptr_s;
	struct {
		struct sig_context_t *pointer_s;
	} context_ptr_s;
//	struct {
//	} handler_ptr_s;	/* handler pointer */
	struct {
		struct sig_context_t structure_s;
	} context_s;
	struct {
		siginfo_t structure_s;
	} information_s;
};

#if FALSE
/* stack frame signal information */
struct stkfrm_signal_info_t {
	struct {
		siginfo_t structure_s;
	} information_s;
	struct {
		struct sig_context_t structure_s;
	} ucontext_s;
};
#endif	/* FALSE */

/* stack frame */
struct stkfrm_t {
	struct {
		u_long r11_fp;	/* general purpose r11 */
		u_long r13_sp;	/* general purpose r13 */
		u_long r14_lr;	/* general purpose r14 */
		u_long r15_pc;	/* general purpose r15 */
	} general_reg_s;
};

u_int32_t
stkfrm_trap_usermode(struct stkfrm_trap_t *trap)
{
	return 0x10 == (0x1f & trap->prog_status_reg_s.spsr);
}
#endif	/* !_LOCORE */
