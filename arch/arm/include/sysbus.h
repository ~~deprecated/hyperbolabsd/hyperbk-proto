/*
 * System bus header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

enum sysbus_sp_barrier_t {
	/* system bus space barrier read */
	SYSBUS_SP_BARRIER_R = 000001,

	/* system bus space barrier write */
	SYSBUS_SP_BARRIER_W = 000002
};

enum sysbus_sp_map_t {
	/* system bus space map cacheable */
	SYSBUS_SP_MAP_CACHE = 000001,

	/* system bus space map linear */
	SYSBUS_SP_MAP_LIN = 000002,

	/* system bus space map prefetchable */
	SYSBUS_SP_MAP_PR = 000010
};

enum sysbus_dma_t {
	/* system bus direct memory access wait ok */
	SYSBUS_DMA_WAIT_OK = 000000,

	/* system bus direct memory access no wait */
	SYSBUS_DMA_NO_WAIT = 000001,

	/* system bus direct memory access allocation now */
	SYSBUS_DMA_ALLOC_NOW = 000002,

	/* system bus direct memory access coherent */
	SYSBUS_DMA_COHERENT = 000004,

	/* system bus direct memory access streaming */
	SYSBUS_DMA_STREAMING = 000010,

	/* system bus direct memory access bus 1 */
	SYSBUS_DMA_BUS1 = 000020,

	/* system bus direct memory access bus 2 */
	SYSBUS_DMA_BUS2 = 000040,

	/* system bus direct memory access bus 3 */
	SYSBUS_DMA_BUS3 = 000100,

	/* system bus direct memory access bus 4 */
	SYSBUS_DMA_BUS4 = 000200,

	/* system bus direct memory access read */
	SYSBUS_DMA_READ = 000400,

	/* system bus direct memory access write */
	SYSBUS_DMA_WRITE = 001000,

	/* system bus direct memory access no cache */
	SYSBUS_DMA_NO_CACHE = 002000,

	/* system bus direct memory access zero */
	SYSBUS_DMA_ZERO = 004000,

	/* system bus direct memory access 64bits (dva) */
	SYSBUS_DMA_DVA_64BITS = 010000
};

enum sysbus_dmamap_coherent_t {
	/* system bus direct memory access map coherent */
	SYSBUS_DMAMAP_COHERENT = 0200000
};

enum sysbus_dma_sync_t {
	/* system bus direct memory access sync pre read */
	SYSBUS_DMA_SYNC_PRE_R = 000001,

	/* system bus direct memory access sync post read */
	SYSBUS_DMA_SYNC_POST_R = 000002,

	/* system bus direct memory access sync pre write */
	SYSBUS_DMA_SYNC_PRE_W = 000004,

	/* system bus direct memory access sync post write */
	SYSBUS_DMA_SYNC_POST_W = 000010
};

#ifdef _SYSBUS_DMA_PRIV	/* system bus direct memory access private */
enum sysbus_buff_typ_t {
	/* system bus buffer type invalid */
	SYSBUS_BUFF_TYP_INV = 000000,

	/* system bus buffer type linear */
	SYSBUS_BUFF_TYP_LIN = 000001,

	/* system bus buffer type mbuffer */
	SYSBUS_BUFF_TYP_MBUFF = 000002,

	/* system bus buffer type uio */
	SYSBUS_BUFF_TYP_UIO = 000003,

	/* system bus buffer type raw */
	SYSBUS_BUFF_TYP_RAW = 000004
};
#endif

struct sysbus_address_t {
	struct {
		unsigned long value;
	} data_s;
};

struct sysbus_size_t {
	struct {
		unsigned long value;
	} data_s;
};

/* system bus space tag pointer */
struct sysbus_space_tag_ptr_t {
	struct {
		struct sysbus_space_t *pointer_s;
	} data_s;
};

struct sysbus_space_handle_t {
	struct {
		unsigned long value;
	} data_s;
};

struct sysbus_space_t {
	struct {
		void *pointer;
	} cookie_s;
//	struct {
//		struct {
//			struct {
//			} data_s;
//		} mapping_s;
//		struct {
//			struct {
//			} data_s;
//		} allocation_s;
//		struct {
//			struct {
//			} address_s;
//		} virtual_s;
//		struct {
//			struct {
//			} data_s;
//		} mmap_s;
//		struct {
//			struct {
//			} data_s;
//		} barrier_s;
//		struct {
//			struct {
//			} single_s;
//			struct {
//			} multiple_s;
//			struct {
//			} region_s;
//		} read_s;
//		struct {
//			struct {
//			} single_s;
//			struct {
//			} multiple_s;
//			struct {
//			} region_s;
//		} write_s;
//		struct {
//			struct {
//			} multiple_s;
//			struct {
//			} region_s;
//		} set_s;
//		struct {
//			struct {
//			} data_s;
//		} copy_s;
//	} operations_s;
};

/* system bus direct memory access tag pointer */
struct sysbus_dma_tag_ptr_t {
	struct {
		struct sysbus_dma_tag_t *pointer_s;
	} data_s;
};

/* system bus direct memory access map pointer */
struct sysbus_dma_map_ptr_t {
	struct {
		struct sysbus_dma_map_t *pointer_s;
	} data_s;
};

/* system bus direct memory access segment */
struct sysbus_dma_segment_t {
	struct {
		struct sysbus_address_t data_s;
	} address_s;
	struct {
		struct sysbus_size_t length_s;
	} size_s;
	struct {
		struct sysbus_address_t address_s;
	} _virtual_s;
	struct {
		int32_t value;
	} _coherent_s;
};

/* system bus direct memory access range */
struct sysbus_dma_range_t {
	struct {
		struct sysbus_address_t base_s;
	} system_s;
	struct {
		struct sysbus_address_t base_s;
	} bus_s;
	struct {
		struct sysbus_size_t length_s;
	} data_s;
};

/* system bus direct memory access tag */
struct sysbus_dma_tag_t {
	struct {
		struct	sysbus_dma_range_t *pointer_s;
		int32_t count;
	} _ranges_s;
	struct {
		void *pointer;
	} _cookie_s;
//	struct {
//		struct {
//		} mapping_s;
//		struct {
//		} memory_s;
//	} functions_s;
};

struct sysbus_dma_map_t {
	struct {
		struct sysbus_size_t size_s;
	} _data_s;
	struct {
		int32_t count;
	} _segment_s;
	struct {
		struct sysbus_size_t max_segment_s;
		struct sysbus_size_t boundary_s;
	} _size_s;
	struct {
		int32_t flags;
	} _misc_s;
	struct {
		void	*original;
		int32_t  type;
	} _buffer_s;
	struct {
		struct proc *pointer_s;
	} _process_s;
	struct {
		void *count;
	} _cookie_s;
	struct {
		struct sysbus_size_t size_s;
	} map_s;
	struct {
		int32_t number;
		struct	sysbus_dma_segment_t length_s[1];
	} segments_s;
};
