/*
 * Copyright (c) 2014 Patrick Wildt <patrick@blueri.se>
 *
 * Modifications to support HyperbolaBSD:
 * Copyright (c) 2021 Hyperbola Project
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef _MACHINE_REG_H_
#define _MACHINE_REG_H_

struct reg {
	uint32_t	r_reg[13];
	uint32_t	r_sp;
	uint32_t	r_lr;
	uint32_t	r_pc;
	uint32_t	r_cpsr;
};

struct fpreg {
	uint64_t	fp_reg[32];
	uint32_t	fp_scr;
};

#endif /* !_MACHINE_REG_H_ */
