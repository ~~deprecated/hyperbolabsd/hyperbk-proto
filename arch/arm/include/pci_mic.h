/*
 * PCI machine-independent code header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

struct pci_mic_chipset_t {
	struct {
		struct arm_pci_mic_chipset_t {
			struct {
				void *pointer;
			} configuration_s;
			struct {
				void *pointer;
			} interrupt_s;
		} *pointer_s;
	} tag_s;
};

struct pci_mic_intr_t {
	struct {
		struct {
			u_int value;
		} data_s;
	} handle_s;
};
