/*
 * Programable Read Only Memory (PROM) header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/ioccom.h>
#include <sys/types.h>

/* prom ioc description */
struct prom_ioc_desc_t {
	struct {
		int32_t id;	/* identification */
	} node_s;
	struct {
		int32_t	 length;
		int8_t	*pointer;
	} name_s;
	struct {
		int32_t  length;
		int8_t	*pointer;
	} buffer_s;
};

enum prom_io_t {
	/* programable read only memory input output get */
	PROM_IO_GET = _IOC(IOC_INOUT, 0117, 01, sizeof(struct prom_ioc_desc_t)),

	/* programable read only memory input output set */
	PROM_IO_SET = _IOC(IOC_IN, 0117, 02, sizeof(struct prom_ioc_desc_t)),

	/* programable read only memory input output next property */
	PROM_IO_NEXT_PROPERTY = _IOC(IOC_INOUT, 0117, 03,
	    sizeof(struct prom_ioc_desc_t)),

	/* programable read only memory input output get option node */
	PROM_IO_GET_OPT_NODE = _IOC(IOC_OUT, 0117, 04, sizeof(int32_t)),

	/* programable read only memory input output get next node */
	PROM_IO_GET_NEXT_NODE = _IOC(IOC_INOUT, 0117, 05, sizeof(int32_t)),

	/* programable read only memory input output get child */
	PROM_IO_GET_CHILD = _IOC(IOC_INOUT, 0117, 06, sizeof(int32_t))
};
