/*
 * Page Table Entry (PTE) header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

#ifndef _LOCORE
	typedef u_int pgdir_t;
	typedef u_int pgtable_t;
#endif

enum pgtable_lvl4_shift_t {
	/* page table entry level 4 shift */
	PGTABLE_LVL4_SHIFT = 047
};

enum pgtable_lvl3_shift_t {
	/* page table entry level 3 shift */
	PGTABLE_LVL3_SHIFT = 036
};

enum pgtable_lvl2_shift_t {
	/* page table entry level 2 shift */
	PGTABLE_LVL2_SHIFT = 025
};

enum pgtable_lvl1_shift_t {
	/* page table entry level 1 shift */
	PGTABLE_LVL1_SHIFT = 014
};

enum pgtable_nbpd_lvl4_t {
	/* page table entry nbpd level 4 */
	PGTABLE_NBPD_LVL4 = 001ULL << PGTABLE_LVL4_SHIFT
};

enum pgtable_nbpd_lvl3_t {
	/* page table entry nbpd level 3 */
	PGTABLE_NBPD_LVL3 = 001ULL << PGTABLE_LVL3_SHIFT
};

enum pgtable_nbpd_lvl2_t {
	/* page table entry nbpd level 2 */
	PGTABLE_NBPD_LVL2 = 001ULL << PGTABLE_LVL2_SHIFT
};

enum pgtable_nbpd_lvl1_t {
	/* page table entry nbpd level 1 */
	PGTABLE_NBPD_LVL1 = 001ULL << PGTABLE_LVL1_SHIFT
};

enum pgtable_lvl4_mask_t {
	/* page table entry level 4 mask */
	PGTABLE_LVL4_MASK = 07770000000000000UL
};

enum pgtable_lvl3_mask_t {
	/* page table entry level 3 mask */
	PGTABLE_LVL3_MASK = 00007770000000000UL
};

enum pgtable_lvl2_mask_t {
	/* page table entry level 2 mask */
	PGTABLE_LVL2_MASK = 00000007770000000UL
};

enum pgtable_lvl1_mask_t {
	/* page table entry level 1 mask */
	PGTABLE_LVL1_MASK = 00000000007770000UL
};

enum pgtable_lvl4_frame_t {
	/* page table entry level 4 frame */
	PGTABLE_LVL4_FRAME = PGTABLE_LVL4_MASK
};

enum pgtable_lvl3_frame_t {
	/* page table entry level 3 frame */
	PGTABLE_LVL3_FRAME = PGTABLE_LVL3_MASK | PGTABLE_LVL4_MASK
};

enum pgtable_lvl2_frame_t {
	/* page table entry level 2 frame */
	PGTABLE_LVL2_FRAME = PGTABLE_LVL2_MASK | PGTABLE_LVL3_MASK
};

enum pgtable_lvl1_frame_t {
	/* page table entry level 1 frame */
	PGTABLE_LVL1_FRAME = PGTABLE_LVL1_MASK | PGTABLE_LVL2_MASK
};

enum pgtable_pgmask_lvl2_t {
	/* page table entry page mask level 2 frame */
	PGTABLE_PGMASK_LVL2 = (- 1) + PGTABLE_NBPD_LVL2
};

enum pgtable_pg_t {
	/* page table entry page read only */
	PGTABLE_PG_READONLY = 00000000000000000000000UL,

	/* page table entry page valid */
	PGTABLE_PG_VALID = 00000000000000000000001UL,

	/* page table entry page read -and- write */
	PGTABLE_PG_READWRITE = 00000000000000000000002UL,

	/* page table entry page user accessible */
	PGTABLE_PG_USRACCESS = 00000000000000000000004UL,

	/* page table entry page protection */
	PGTABLE_PG_PROTECTION = 00000000000000000000006UL,

	/* page table entry page write through */
	PGTABLE_PG_WTHRU = 00000000000000000000010UL,

	/* page table entry page non cacheable */
	PGTABLE_PG_NCACHE = 00000000000000000000020UL,

	/* page table entry page used */
	PGTABLE_PG_USED = 00000000000000000000040UL,

	/* page table entry page modified */
	PGTABLE_PG_MODIFIED = 00000000000000000000100UL,

	/* page table entry page pat bit */
	PGTABLE_PG_PATBIT = 00000000000000000000200UL,

	/* page table entry page -page- size 2mb */
	PGTABLE_PG_SIZE2MB = 00000000000000000000200UL,

	/* page table entry page global */
	PGTABLE_PG_GLOBAL = 00000000000000000000400UL,

	/* page table entry page available 1 */
	PGTABLE_PG_AVL1 = 00000000000000000001000UL,

	/* page table entry page available 2 */
	PGTABLE_PG_AVL2 = 00000000000000000002000UL,

	/* page table entry page available 3 */
	PGTABLE_PG_AVL3 = 00000000000000000004000UL,

	/* page table entry page pat on large pages */
	PGTABLE_PG_PATONLGPGS = 00000000000000000010000UL,

	/* page table entry page large page frame mask */
	PGTABLE_PG_LGPGFRMMASK = 00000177777777770000000UL,

	/* page table entry page frame */
	PGTABLE_PG_FRAME = 00000177777777777770000UL,

	/* page table entry page not executable */
	PGTABLE_PG_NOTEXEC = 01000000000000000000000UL,

	/* page table entry page w b (write bit?) */
	PGTABLE_PG_WB = 00,

	/* page table entry page w c (write cache or write cycle?) */
	PGTABLE_PG_WC = PGTABLE_PG_WTHRU,

	/* page table entry page u c (user cache?) minus */
	PGTABLE_PG_UCMINUS = PGTABLE_PG_NCACHE,

	/* page table entry page u c (user cache?) */
	PGTABLE_PG_UC = PGTABLE_PG_NCACHE | PGTABLE_PG_WTHRU,

	/* page table entry page kernel read only */
	PGTABLE_PG_KREADONLY = 00000000000000000000000UL,

	/* page table entry page kernel read -and- write */
	PGTABLE_PG_KREADWRITE = 00000000000000000000002UL
};

/*
 * SLAT (Second Level Address Translation) is the term used to refer to
 * both EPT and RVI. Extended Page Table (EPT) is used on Intel while
 * Rapid Virtualization Indexing (RVI) is used for AMD.
 */
enum pgtable_slat_t {
	/* page table entry second level address translation read */
	PGTABLE_SLAT_READ = 0001ULL,

	/* page table entry second level address translation write */
	PGTABLE_SLAT_WRITE = 0002ULL,

	/* page table entry second level address translation executable */
	PGTABLE_SLAT_EXEC = 0004ULL,

	/* page table entry second level address translation w b (write bit?) */
	PGTABLE_SLAT_WB = 0060ULL,

	/* page table entry second level address translation page size */
	PGTABLE_SLAT_PGSIZE = 0200ULL
};

enum pgtable_pgexc_t {
	/* page table entry page exception protection violation */
	PGTABLE_PGEXC_PROTVIO = 001,

	/* page table entry page exception write cycle */
	PGTABLE_PGEXC_WCYCLE = 002,

	/* page table entry page exception user mode */
	PGTABLE_PGEXC_USRMODE = 004,

	/* page table entry page exception instruction fetch blocked */
	PGTABLE_PGEXC_IFBLKD = 020
};

#ifdef _KERNEL
extern struct {
	pgtable_t notexecutable;
	pgtable_t global_kernel;
} pagetable_page_s;
#endif

int64_t
pagetable_round_pdr(u_int64_t value)
{
	int64_t x = (- 01) + PGTABLE_NBPD_LVL2;
	int64_t y = x + (unsigned long)value;
	return ~ x & y;
}
