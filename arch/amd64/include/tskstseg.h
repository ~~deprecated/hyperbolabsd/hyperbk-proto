/*
 * Task State Segment (TSS) header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

struct task_state_segment_t {
	struct {
		u_int :32;
	} reserved_a_s;
	struct {
		struct {
			struct {
				/* 64bits stack pointer */
				unsigned long long rsp;
			} integer_s;
		} privilege_level0_s;
		struct {
			struct {
				/* 64bits stack pointer */
				unsigned long long rsp;
			} integer_s;
		} privilege_level1_s;
		struct {
			struct {
				/* 64bits stack pointer */
				unsigned long long rsp;
			} integer_s;
		} privilege_level2_s;
	} cpu_register_s;
	struct {
		u_int :32;
		u_int :32;
	} reserved_b_s;
	struct {
		unsigned long long interrupt[7];
	} stack_table_s;
	struct {
		u_int	:32;
		u_int	:32;
		u_short :16;
	} reserved_c_s;
	struct {
		u_short base;
	} input_output_s;
} __task_state_segment_packed_s;
