/*
 * PCI machine-independent code header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

#include <machine/sysbus.h>

enum pci_mic_t {
	/*
	 * peripheral component interconnect -x86_64- machine independent code
	 * interrupt line no connection
	 */
	PCI_MIC_ILINE_NCNX = 00000377,

	/*
	 * peripheral component interconnect machine independent code
	 * input/output start
	 */
	PCI_MIC_IO_START = 00002000,

	/*
	 * peripheral component interconnect machine independent code
	 * input/output end
	 */
	PCI_MIC_IO_END = 00177777,

	/*
	 * peripheral component interconnect machine independent code memory
	 * start
	 */
	PCI_MIC_MEMORY_START = 04000000,

	/*
	 * peripheral component interconnect machine independent code have
	 * message signalled interrupts extended (msi-x)
	 */
	PCI_MIC_HAVE_MSIX = 00000001
};

struct {
	struct {
		struct sysbus_dma_tag_ptr_t tag_s;
	} data_s;
} pci_mic_bus_dma_s;

struct pci_mic_chipset_t {
	struct {
		void *pointer;
	} tag_s;
};

struct pci_mic_intr_t {
	struct {
		struct {
			u_int value;
		} tag_s;
		struct {
			int32_t h_line;
			int32_t h_pin;
		} data_s;
	} handle_s;
};

extern struct {
	struct {
		struct extent *extent_s;
	} data_s;
} pci_mic_io_s;

extern struct {
	struct {
		struct extent *extent_s;
	} data_s;
} pci_mic_memory_s;

extern struct {
	struct {
		struct extent *extent_s;
	} data_s;
} pci_mic_bus_s;
