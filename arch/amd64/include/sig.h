/*
 * Signal header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#if __XPG_VISIBLE >= 420 || __BSD_VISIBLE
#include <sys/types.h>

struct sig_context_t {
	struct {
		int64_t rdi;	/* destination index */
		int64_t rsi;	/* source index */
	} index_reg_s;
	struct {
		int64_t rdx;	/* data */
		int64_t rcx;	/* counter */
		int64_t r8;	/* general purpose r8 */
		int64_t r9;	/* general purpose r9 */
		int64_t r10;	/* general purpose r10 */
		int64_t r11;	/* general purpose r11 */
		int64_t r12;	/* general purpose r12 */
		int64_t r13;	/* general purpose r13 */
		int64_t r14;	/* general purpose r14 */
		int64_t r15;	/* general purpose r15 */
	} general_reg_0_s;
	struct {
		int64_t rbp;	/* base pointer */
	} pointer_reg_0_s;
	struct {
		int64_t rbx;	/* base */
		int64_t rax;	/* accumulator */
	} general_reg_1_s;
	struct {
		int64_t gs;	/* general purpose g segment */
		int64_t fs;	/* general purpose f segment */
		int64_t es;	/* extra segment */
		int64_t ds;	/* data segment */
	} segment_reg_s;
	struct {
		int64_t trapno;
	} trapno_reg_s;
	struct {
		int64_t err;
	} err_reg_s;
	struct {
		int64_t rip;	/* instruction pointer */
	} pointer_reg_1_s;
	struct {
		int64_t cs;	/* extra segment */
	} extra_seg_reg_s;
	struct {
		int64_t rflags;
	} rflags_reg_s;
	struct {
		int64_t rsp;	/* stack pointer */
		int64_t ss;	/* stack segment */
	} stack_reg_s;
	struct {
		struct fltpunit_fxsave_t *state_s;
	} floating_point_s;
	struct {
		int32_t unused;
	} unused_s;
	struct {
		int32_t value;
	} mask_s;
	struct {
		int64_t value;
	} cookie_s;
};
#endif	/* __XPG_VISIBLE >= 420 || __BSD_VISIBLE */
