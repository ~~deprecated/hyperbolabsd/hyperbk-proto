/*
 * Interrupt definitions header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

enum intrpt_plvl_t {
	/* interrupt priority level number */
	INTRPT_PLVL_NUMBER = 020,

	/* interrupt priority level none  */
	INTRPT_PLVL_NONE = 000,

	/* interrupt priority level software clock */
	INTRPT_PLVL_SOFT_CLOCK = 004,

	/* interrupt priority level software network */
	INTRPT_PLVL_SOFT_NET = 005,

	/* interrupt priority level block input/output */
	INTRPT_PLVL_BLOCK_IO = 006,

	/* interrupt priority level network */
	INTRPT_PLVL_NETWORK = 007,

	/* interrupt priority level software teletype */
	INTRPT_PLVL_SOFT_TTY = 010,

	/* interrupt priority level teletype */
	INTRPT_PLVL_TTY = 011,

	/* interrupt priority level virtual memory */
	INTRPT_PLVL_VIRT_MEM = 012,

	/* interrupt priority level audio */
	INTRPT_PLVL_AUDIO = 013,

	/* interrupt priority level clock, -status- -clock- -and- schreduler */
	INTRPT_PLVL_CLK_SCH = 014,

	/* interrupt priority level high */
	INTRPT_PLVL_HIGH = 015,

	/* interrupt priority level, inter-processor interrupt */
	INTRPT_PLVL_IPROCINT = 016,

	/* interrupt priority level m p floor */
	INTRPT_PLVL_MP_FLOOR = 0011,

	/* interrupt priority level m p safe */
	INTRPT_PLVL_MP_SAFE = 0400
};

enum intrpt_styp_t {
	/* interrupt sharing type none */
	INTRPT_STYP_NONE = 00,

	/* interrupt sharing type pulse */
	INTRPT_STYP_PULSE = 01,

	/* interrupt sharing type edge */
	INTRPT_STYP_EDGE = 02,

	/* interrupt sharing type level */
	INTRPT_STYP_LEVEL = 03
};

enum intrpt_lireq_t {
	/* interrupt; legacy interrupt requests, inter-processor interrupt */
	INTRPT_LIREQ_IPI = 077,

	/* interrupt; legacy interrupt requests, timer */
	INTRPT_LIREQ_TIMER = 076,

	/* interrupt; legacy interrupt requests, xen */
	INTRPT_LIREQ_XEN = 072,

	/* interrupt; legacy interrupt requests, hyper-v */
	INTRPT_LIREQ_HYPERV = 071
};

enum intrpt_sireq_t {
	/* interrupt; soft interrupt requests, clock */
	INTRPT_SIREQ_CLOCK = 075,

	/* interrupt; soft interrupt requests, network */
	INTRPT_SIREQ_NETWORK = 074,

	/* interrupt; soft interrupt requests, teletype */
	INTRPT_SIREQ_TTY = 073
};

enum intrpt_sources_maximum_t {
	/* interrupt sources, maximum */
	INTRPT_SOURCES_MAXIMUM = 0100
};

enum intrpt_legacy_irqs_num_t {
	/* interrupt; legacy interrupt requests, number */
	INTRPT_LEGACY_IRQS_NUM = 0020
};

enum intrpt_dtbl_t {
	/* interrupt descriptor table low */
	INTRPT_DTBL_LOW = INTRPT_LEGACY_IRQS_NUM + 040,

	/* interrupt descriptor table high */
	INTRPT_DTBL_HIGH = 0357
};

enum intrpt_iprocint_t {
	/* interrupt; inter-processor interrupt, halt */
	INTRPT_IPROCINT_HALT = 00001,

	/* interrupt; inter-processor interrupt, n op (no operation ?) */
	INTRPT_IPROCINT_NOP = 00002,

	/* interrupt; inter-processor interrupt, p c t r */
	INTRPT_IPROCINT_PCTR = 00020,

	/* interrupt; inter-processor interrupt, memory type range registers */
	INTRPT_IPROCINT_MTRR = 00040,

	/* interrupt; inter-processor interrupt, set perf */
	INTRPT_IPROCINT_S_PERF = 00100,

	/* interrupt; inter-processor interrupt, d d b */
	INTRPT_IPROCINT_DDB = 00200,

	/* interrupt; inter-processor interrupt, start v m m */
	INTRPT_IPROCINT_ST_VMM = 00400,

	/* interrupt; inter-processor interrupt, stop v m m */
	INTRPT_IPROCINT_SP_VMM = 01000,

	/* interrupt; inter-processor interrupt, number */
	INTRPT_IPROCINT_NUMBER = 012
};

enum intrpt_ireent_magiic_t {
	/* interrupt; i r e e n t magic */
	INTRPT_IREENT_MAGIC = 03001014551
};
