/*
 * Floating Point Unit (FPU) header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __FLTPUNIT__
#define __FLTPUNIT__

#include <sys/types.h>

enum fltpun_init_t {
	/* floating point unit initial npxcw */
	FLTPUN_INIT_NPXCW = 0001577,

	/* floating point unit initial mxcsr */
	FLTPUN_INIT_MXCSR = 0017600,

	/* floating point unit initial mxcsr_mask */
	FLTPUN_INIT_MXCSR_MASK = 0177677
};

struct fltpunit_save_t {
	struct {
		struct fltpunit_fxsave_t {
			struct {
				unsigned short		fcw;
				unsigned short		fsw;
				unsigned char		ftw;
				unsigned char		unused_1;
				unsigned short		fop;
				unsigned long long	rip;
				unsigned long long	rdp;
				unsigned int		mxcsr;
				unsigned int		mxcsr_mask;
				unsigned long long	st[8][2];
				unsigned long long	xmm[16][2];
				unsigned char		unused_3[96];
			} register_s;
		} fxsave_s;
		struct fltpunit_fxstate_t {
			struct {
				unsigned long long value;
				unsigned long long xcomp;
			} bv_s;
			struct {
				unsigned char array0[0];
				unsigned char array[40];
			} rsrv_s;
		} xstate_s;
	} structure_s;
	struct {
		unsigned long long array[16][2];
	} ymm_s;
	struct {
		unsigned short status;
		unsigned short tag;
	} exception_s;
};

#ifdef _KERNEL
extern struct {
	unsigned long length;
} fltpunit_save_s;

extern struct {
	unsigned int mask;
} fltpunit_mxcsr_s;

extern struct {
	unsigned long long mask;
} fltpunit_xsave_s;

//static inline int32_t
//fltpunit_reset(void)
//{
//	return xrstor_user(&proc0.p_addr->u_pcb.pcb_savefpu,
//	    fltpunit_xsave_s.mask);
//}

static inline void
fltpunit_fninit(void)
{
	__asm__("fninit");
}

static inline void
fltpunit_fwait(void)
{
	__asm__("fwait");
}

static inline void
fltpunit_fxsave(struct fltpunit_save_t *address)
{
	__asm__("fxsave %0" : "=m" (*address));
}

static inline void
fltpunit_ldmxcsr(u_int32_t *address)
{
	__asm__("ldmxcsr %0" : : "m" (*address));
}

static inline void
fltpunit_fldcw(u_int16_t *address)
{
	__asm__("fldcw %0" : : "m" (*address));
}

#define FLTPUNIT_XSAVE(address, msk) do {				\
	u_int high = (unsigned long long)(msk) >> 040;			\
	u_int low = (unsigned long long)(msk);				\
	__asm__ __volatile__("xsave %0" :				\
	    "=m" ((struct fltpunit_save_t *)(*address))) :		\
	    "=d" (high), "=a" (low) : "memory");			\
} while (0)
#endif	/* _KERNEL */

#endif /* __FLTPUNIT__ */
