/*
 * Process trace header for HyperbolaBSD
 * Copyright (c) 2020-2021 Hyperbola Project
 * Copyright (c) 2020-2021 Márcio Silva <coadde@hyperbola.info>
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _MACHINE_PROCTR_H_
#define _MACHINE_PROCTR_H_

#include <sys/ptrace.h>

#define PROCTR_STEP_DEF

enum proctr_t {
	/* process trace step */
	PROCTR_STEP = 00 + PT_FIRSTMACH,

	/* process trace get integer registers */
	PROCTR_GET_INT_REGS = 01 + PT_FIRSTMACH,

	/* process trace set integer registers */
	PROCTR_SET_INT_REGS = 02 + PT_FIRSTMACH,

	/* process trace get floating point registers */
	PROCTR_GET_FLOAT_REGS = 03 + PT_FIRSTMACH,

	/* process trace set floating point registers */
	PROCTR_SET_FLOAT_REGS = 04 + PT_FIRSTMACH
};
#endif /* !_MACHINE_PROCTR_H_ */
