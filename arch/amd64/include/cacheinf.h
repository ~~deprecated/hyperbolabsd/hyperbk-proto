/*
 * Cache information header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

enum cacheinf_instr_t {
	/* cache information instruction translation lookaside buffer */
	CACHEINF_INSTR_TLB = 000,

	/* cache information instruction translation lookaside buffer 2 */
	CACHEINF_INSTR_TLB2 = 001,

	/* cache information instruction */
	CACHEINF_INSTR = 004
};

enum cacheinf_data_t {
	/* cache information data translation lookaside buffer */
	CACHEINF_DATA_TLB = 002,

	/* cache information data translation lookaside buffer 2 */
	CACHEINF_DATA_TLB2 = 003,

	/* cache information data */
	CACHEINF_DATA = 005
};

enum cacheinf_level2_t {
	/* cache information level 2 */
	CACHEINF_LEVEL2 = 006
};

enum cacheinf_level3_t {
	/* cache information level 3 */
	CACHEINF_LEVEL3 = 007
};

enum cacheinf_count_t {
	/* cache information count */
	CACHEINF_COUNT = 010
};

struct cacheinf_t {
	struct {
		unsigned char index;
		unsigned char description;
		unsigned char associativity;
	} data_s;
	struct {
		unsigned int total;
		unsigned int line;
	} size_s;
	struct {
		const int8_t *pointer;
	};
};

u_char
cacheinf_lvl1_eax_ebx_data_tlb_assn(int64_t data)
{
	return 0000377 & (data >> 030);
}

u_char
cacheinf_lvl1_eax_ebx_data_tlb_entries(int64_t data)
{
	return 0000377 & (data >> 020);
}

u_char
cacheinf_lvl1_eax_ebx_instr_tlb_assn(int64_t data)
{
	return 0000377 & (data >> 010);
}

u_char
cacheinf_lvl1_eax_ebx_instr_tlb_entries(int64_t data)
{
	return 0000377 & (data >> 000);
}

u_int32_t
cacheinf_lvl1_ecx_data_edx_instr_size(int64_t data)
{
	return 0002000 * cacheinf_lvl1_eax_ebx_data_tlb_assn(data);
}

u_char
cacheinf_lvl1_ecx_data_edx_instr_assn(int64_t data)
{
	return 0000001 * cacheinf_lvl1_eax_ebx_data_tlb_entries(data);
}

u_char
cacheinf_lvl1_ecx_data_edx_instr_lpt(int64_t data)
{
	return 0000001 * cacheinf_lvl1_eax_ebx_instr_tlb_assn(data);
}

u_char
cacheinf_lvl1_ecx_data_edx_instr_ls(int64_t data)
{
	return 0000001 * cacheinf_lvl1_eax_ebx_instr_tlb_entries(data);
}

u_char
cacheinf_lvl2_eax_ebx_data_tlb_assn(int64_t data)
{
	return 0000017 & (data >> 034);
}

u_int16_t
cacheinf_lvl2_eax_ebx_data_tlb_entries(int64_t data)
{
	return 0007777 & (data >> 020);
}

u_char
cacheinf_lvl2_eax_ebx_instr_utlb_assn(int64_t data)
{
	return 0000017 & (data >> 014);
}

u_int16_t
cacheinf_lvl2_eax_ebx_instr_utlb_entries(int64_t data)
{
	return 0007777 & (data >> 000);
}

u_int32_t
cacheinf_lvl2_ecx_cache_size(int64_t data)
{
	return 00001 * 02000 * (0177777 & (data >> 020));
}

u_char
cacheinf_lvl2_ecx_lvl3_edx_cache_assn(int64_t data)
{
	return 00001 * 00001 * (0000017 & (data >> 014));
}

u_char
cacheinf_lvl2_ecx_lvl3_edx_cache_lpt(int64_t data)
{
	return 00001 * 00001 * (0000017 & (data >> 010));
}

u_char
cacheinf_lvl2_ecx_lvl3_edx_cache_ls(int64_t data)
{
	return 00001 * 00001 * (0000377 & (data >> 000));
}

u_int64_t
cacheinf_lvl3_edx_cache_size(int64_t data)
{
	return 01000 * 02000 * (0037777 & (data >> 022));
}
