/*
 * Stack frame header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* stack frame trap */
struct stkfrm_trap_t {
	struct {
		long rdi;	/* destination index */
		long rsi;	/* source index */
	} index_reg_s;
	struct {
		long rdx;	/* data */
		long rcx;	/* counter */
		long r8;	/* general purpose r8 */
		long r9;	/* general purpose r9 */
		long r10;	/* general purpose r10 */
		long r11;	/* general purpose r11 */
		long r12;	/* general purpose r12 */
		long r13;	/* general purpose r13 */
		long r14;	/* general purpose r14 */
		long r15;	/* general purpose r15 */
	} general_reg_0_s;
	struct {
		long err;
	} err_reg_s;
	struct {
		long rbx;	/* base */
		long rax;	/* accumulator */
	} general_reg_1_s;
	struct {
		long trapno;
	} trapno_reg_s;
	struct {
		long rbp;	/* base pointer */
		long rip;	/* instruction pointer */
	} pointer_reg_s;
	struct {
		long cs;	/* extra segment */
	} extra_seg_reg_s;
	struct {
		long rflags;
	} rflags_reg_s;
	struct {
		long rsp;	/* stack pointer */
		long ss;	/* stack segment */
	} stack_reg_s;
};

/* stack frame interrupt */
struct stkfrm_intrpt_t {
	struct {
		long rdi;	/* destination index */
		long rsi;	/* source index */
	} index_reg_s;
	struct {
		long rdx;	/* data */
		long rcx;	/* counter */
		long r8;	/* general purpose r8 */
		long r9;	/* general purpose r9 */
		long r10;	/* general purpose r10 */
		long r11;	/* general purpose r11 */
		long r12;	/* general purpose r12 */
		long r13;	/* general purpose r13 */
		long r14;	/* general purpose r14 */
		long r15;	/* general purpose r15 */
	} general_reg_0_s;
	struct {
		long err;
	} err_reg_s;
	struct {
		long rbx;	/* base */
		long rax;	/* accumulator */
	} general_reg_1_s;
	struct {
		long ppl;
	} ppl_reg_s;
	struct {
		long rbp;	/* base pointer */
		long rip;	/* instruction pointer */
	} pointer_reg_s;
	struct {
		long cs;	/* extra segment */
	} extra_seg_reg_s;
	struct {
		long rflags;
	} rflags_reg_s;
	struct {
		long rsp;	/* stack pointer */
		long ss;	/* stack segment */
	} stack_reg_s;
};

/* stack frame switch */
struct stkfrm_switch_t {
	struct {
		long r15;	/* general purpose r15 */
		long r14;	/* general purpose r14 */
		long r13;	/* general purpose r13 */
		long r12;	/* general purpose r12 */
	} general_reg_0_s;
	struct {
		long rbp;	/* base pointer */
	} pointer_reg_0_s;
	struct {
		long rbx;	/* base */
	} general_reg_1_s;
	struct {
		long rip;	/* instruction pointer */
	} pointer_reg_1_s;
};

/* stack frame call */
struct stkfrm_call_t {
	struct {
		struct stkfrm_call_t *pointer_s;
	} frame_s;
	struct {
		long long address;
	} ret_s;
	struct {
		long long value0;
	} argument_s;
};

/* stack frame interrupt return q */
struct stkfrm_intrpt_ret_q_t {
	struct {
		long rip;	/* instruction pointer */
	} pointer_reg_s;
	struct {
		long cs;	/* extra segment */
	} extra_seg_reg_s;
	struct {
		long rflags;
	} rflags_reg_s;
	struct {
		long rsp;	/* stack pointer */
		long ss;	/* stack segment */
	} stack_reg_s;
};
