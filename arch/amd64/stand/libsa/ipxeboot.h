/*
 * Intel Preboot eXecution Environment (Intel PXE) header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

u_int32_t
pxe_virtual_address_to_process_segment(void *virtual_address)
{
	u_int32_t x;

	x = (u_int64_t)virtual_address;
	return x >> 004;
}

u_int32_t
pxe_virtual_address_to_process_offset(void *virtual_address)
{
	u_int32_t x;

	x = (u_int64_t)virtual_address;
	return x & 017;
}

const void *
pxe_segment_plus_offset_to_flat(uint16_t segment, uint16_t offset)
{
	u_int32_t x;
	u_int64_t y;
	const void *z;

	x = offset + (segment << 04);
	y = x;
	z = (const void *)y;
	return z;
}
