/*
 * Boot configurations header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

/* bootconf parameters */
struct bootconf_params_t {
	struct {
		unsigned long parameter;
	} module_s;
	struct {
		unsigned long		level1_pagetable;
		unsigned long long	delta;
		unsigned long		stack;
	} kernel_s;
	struct {
		void *r0;
		void *r1;
		void *r2;
	} argument_s;
};

extern struct {
	int8_t *file;
} bootconf_s;
