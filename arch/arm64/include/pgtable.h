/*
 * Page Table Entry (PTE) header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/param.h>
#include <sys/types.h>

#ifndef _LOCORE
	typedef u_int pgdir_t;
	typedef u_int pgtable_t;
#endif

enum pgtable_lvl2_lgpg_t {
	/* page table entry level 2 large page size */
	PGTABLE_LVL2_LGPG_SIZE = 00200000,

	/* page table entry level 2 large page offset */
	PGTABLE_LVL2_LGPG_OFF = (- 1) + PGTABLE_LVL2_LGPG_SIZE,

	/* page table entry level 2 large page frame */
	PGTABLE_LVL2_LGPG_FRM = ~ PGTABLE_LVL2_LGPG_OFF,

	/* page table entry level 2 large page shift */
	PGTABLE_LVL2_LGPG_SHFT = 00000020
};

enum pgtable_lvl2_smpg_t {
	/* page table entry level 2 small page size */
	PGTABLE_LVL2_SMPG_SIZE = 00010000,

	/* page table entry level 2 small page offset */
	PGTABLE_LVL2_SMPG_OFF = (- 1) + PGTABLE_LVL2_SMPG_SIZE,

	/* page table entry level 2 small page frame */
	PGTABLE_LVL2_SMPG_FRM = ~ PGTABLE_LVL2_SMPG_OFF,

	/* page table entry level 2 small page shift */
	PGTABLE_LVL2_SMPG_SHFT = 00000014
};

enum pgtable_lvlx_tp_t {
	/* page table entry level x type mask */
	PGTABLE_LVLX_TP_MASK = 03,

	/* page table entry level x type size */
	PGTABLE_LVLX_TP_SIZE = 01,

	/* page table entry level x type page table */
	PGTABLE_LVLX_TP_PGTBL = 03
};

enum pgtable_lvlx_pgt_t {
	/* page table entry level x page table non secure */
	PGTABLE_LVLX_PGT_NSEC = 000001ULL << 000077,

	/* page table entry level x page table access permissions 0 and 0 */
	PGTABLE_LVLX_PGT_AP00 = 000000ULL << 000075,

	/* page table entry level x page table access permissions 0 and 1 */
	PGTABLE_LVLX_PGT_AP01 = 000001ULL << 000075,

	/* page table entry level x page table access permissions 1 and 0 */
	PGTABLE_LVLX_PGT_AP10 = 000002ULL << 000075,

	/* page table entry level x page table access permissions 1 and 1 */
	PGTABLE_LVLX_PGT_AP11 = 000003ULL << 000075,

	/* page table entry level x page table execute never */
	PGTABLE_LVLX_PGT_XN = 000001ULL << 000074,

	/* page table entry level x page table privileged execute never */
	PGTABLE_LVLX_PGT_PXN = 000001ULL << 000073,

	/* page table entry level x table align */
	PGTABLE_LVLX_TBL_ALIGN = 010000
};

enum pgtable_attr_t {
	/* page table entry attribute mask high */
	PGTABLE_ATTR_MASK_HIGH = 01777600000000000000000,

	/* page table entry attribute mask low */
	PGTABLE_ATTR_MASK_LOW = 00000000000000000007777,

	/* page table entry attribute mask */
	PGTABLE_ATTR_MASK = PGTABLE_ATTR_MASK_LOW | PGTABLE_ATTR_MASK_HIGH,

	/* page table entry attribute software managed */
	PGTABLE_ATTR_SW_MGD = 001UL << 070,

	/* page table entry attribute software wired */
	PGTABLE_ATTR_SW_WIRED = 001UL << 067,

	/* page table entry attribute u execute never */
	PGTABLE_ATTR_UEXECNVR = 001UL << 066,

	/* page table entry attribute privileged execute never */
	PGTABLE_ATTR_PEXECNVR = 001UL << 065,

	/* page table entry attribute not global */
	PGTABLE_ATTR_NOTGLOBAL = 001 << 013,

	/* page table entry attribute access flag */
	PGTABLE_ATTR_ACCESSFLG = 001 << 012,

	/* page table entry attribute access permissions read + write bit */
	PGTABLE_ATTR_AP_RW_BIT = 001 << 007,

	/* page table entry attribute access permissions mask */
	PGTABLE_ATTR_AP_MASK = 003 << 006,

	/* page table entry attribute not secure */
	PGTABLE_ATTR_NONSECURE = 001 << 005,

	/* page table entry attribute idx mask */
	PGTABLE_ATTR_IDX_MASK = 007 << 002,

	/* page table entry attribute device */
	PGTABLE_ATTR_DEVICE = 00,

	/* page table entry attribute c i */
	PGTABLE_ATTR_CI = 01,

	/* page table entry attribute w b */
	PGTABLE_ATTR_WB = 02,

	/* page table entry attribute write through */
	PGTABLE_ATTR_WTHRU = 03
};

enum pgtable_sh_t {
	/* page table entry sh inner */
	PGTABLE_SH_INNER = 03,

	/* page table entry sh outer */
	PGTABLE_SH_OUTER = 02,

	/* page table entry sh none */
	PGTABLE_SH_NONE = 00
};

enum pgtable_lvl0_t {
	/* page table entry level 0 shift */
	PGTABLE_LVL0_SHIFT = 047,

	/* page table entry level 0 invalid */
	PGTABLE_LVL0_INVALID = 000,

	/* page table entry level 0 block */
	PGTABLE_LVL0_BLOCK = 001,

	/* page table entry level 0 table */
	PGTABLE_LVL0_TABLE = 003
};

enum pgtable_lvl1_t {
	/* page table entry level 1 shift */
	PGTABLE_LVL1_SHIFT = 025,

	/* page table entry level 1 size */
	PGTABLE_LVL1_SIZE = 001 << PGTABLE_LVL1_SHIFT,

	/* page table entry level 1 offset */
	PGTABLE_LVL1_OFFSET = (- 001) + PGTABLE_LVL1_SIZE,

	/* page table entry level 1 invalid */
	PGTABLE_LVL1_INVALID = PGTABLE_LVL0_INVALID,

	/* page table entry level 1 block */
	PGTABLE_LVL1_BLOCK = PGTABLE_LVL0_BLOCK,

	/* page table entry level 1 table */
	PGTABLE_LVL1_TABLE = PGTABLE_LVL0_TABLE
};

enum pgtable_lvl2_t {
	/* page table entry level 2 shift */
	PGTABLE_LVL2_SHIFT = 036,

	/* page table entry level 2 size */
	PGTABLE_LVL2_SIZE = 001 << PGTABLE_LVL2_SHIFT,

	/* page table entry level 2 offset */
	PGTABLE_LVL2_OFFSET = (- 001) + PGTABLE_LVL2_SIZE,

	/* page table entry level 2 invalid */
	PGTABLE_LVL2_INVALID = PGTABLE_LVL0_INVALID,

	/* page table entry level 2 block */
	PGTABLE_LVL2_BLOCK = PGTABLE_LVL0_BLOCK,

	/* page table entry level 2 table */
	PGTABLE_LVL2_TABLE = PGTABLE_LVL0_TABLE
};

enum pgtable_lvl3_t {
	/* page table entry level 3 page */
	PGTABLE_LVL3_PAGE = 03
};

enum pgtable_lvln_t {
	/* page table entry level n entries */
	PGTABLE_LVLN_ENTRIES = 001 << 011,

	/* page table entry level n address mask */
	PGTABLE_LVLN_ADDR_MASK = (- 001) + PGTABLE_LVLN_ENTRIES,

	/* page table entry level n table mask */
	PGTABLE_LVLN_TBL_MASK = (- 001) + (001 << 014)
};

enum pgtable_rpgn_t {
	/* page table entry r p g n */
	PGTABLE_RPGN = ~ PAGE_MASK & ((- 001) + (001ULL << 060))
};

#ifndef _LOCORE
struct pagetable_t {
	struct {
		unsigned long long value;
	} data_s;
};

struct physical_virtual_t {
	struct {
	} node_s;
};
#endif

int64_t
pagetable_attribute_sh(int64_t value)
{
	return value << 010;
}

int64_t
pagetable_attribute_accesspermissions(int64_t value)
{
	return value << 006;
}

int64_t
pagetable_attribute_idx(int64_t value)
{
	return value << 002;
}
