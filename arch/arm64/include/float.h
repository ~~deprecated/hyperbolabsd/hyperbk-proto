/*
 * Copyright (c) 1989 Regents of the University of California.
 * All rights reserved.
 *
 * Modifications to support HyperbolaBSD:
 * Copyright (c) 2020 Hyperbola Project
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _MACHINE_FLOAT_H_
#define _MACHINE_FLOAT_H_

#define __FLT_RADIX		2		/* b */

__BEGIN_DECLS
extern int32_t __flt_rounds (void);
__END_DECLS

#define __FLT_ROUNDS		__flt_rounds()

#if 1999 <= __ISO_C_VISIBLE
#define __FLT_EVAL_METHOD	0		/* no promotions */
#endif

#define __FLT_MANT_DIG		24		/* p */
#define __FLT_EPSILON		1.19209290E-07F	/* b**(1-p) */
#define __FLT_DIG		6		/* floor((p-1)*log10(b))+(b == 10) */
#define __FLT_MIN_EXP		(-125)		/* emin */
#define __FLT_MIN		1.17549435E-38F	/* b**(emin-1) */
#define __FLT_MIN_10_EXP	(-37)		/* ceil(log10(b**(emin-1))) */
#define __FLT_MAX_EXP		128		/* emax */
#define __FLT_MAX		3.40282347E+38F	/* (1-b**(-p))*b**emax */
#define __FLT_MAX_10_EXP	38		/* floor(log10((1-b**(-p))*b**emax)) */

#define __DBL_MANT_DIG		53
#define __DBL_EPSILON		2.2204460492503131E-16
#define __DBL_DIG		15
#define __DBL_MIN_EXP		(-1021)
#define __DBL_MIN		2.2250738585072014E-308
#define __DBL_MIN_10_EXP	(-307)
#define __DBL_MAX_EXP		1024
#define __DBL_MAX		1.7976931348623157E+308
#define __DBL_MAX_10_EXP	308

#define DBL_MANT_DIG		__DBL_MANT_DIG
#define DBL_EPSILON		__DBL_EPSILON
#define DBL_DIG			__DBL_DIG
#define DBL_MIN_EXP		__DBL_MIN_EXP
#define DBL_MIN			__DBL_MIN
#define DBL_MIN_10_EXP		__DBL_MIN_10_EXP
#define DBL_MAX_EXP		__DBL_MAX_EXP
#define DBL_MAX			__DBL_MAX
#define DBL_MAX_10_EXP		__DBL_MAX_10_EXP

#define __LDBL_MANT_DIG		113
#define __LDBL_EPSILON		1.925929944387235853055977942584927319E-34L
#define __LDBL_DIG		33
#define __LDBL_MIN_EXP		(-16381)
#define __LDBL_MIN		3.362103143112093506262677817321752603E-4932L
#define __LDBL_MIN_10_EXP	(-4931)
#define __LDBL_MAX_EXP		(+16384)
#define __LDBL_MAX		1.189731495357231765085759326628007016E+4932L
#define __LDBL_MAX_10_EXP	(+4932)

#define LDBL_MANT_DIG		__LDBL_MANT_DIG
#define LDBL_EPSILON		__LDBL_EPSILON
#define LDBL_DIG		__LDBL_DIG
#define LDBL_MIN_EXP		__LDBL_MIN_EXP
#define LDBL_MIN		__LDBL_MIN
#define LDBL_MIN_10_EXP		__LDBL_MIN_10_EXP
#define LDBL_MAX_EXP		__LDBL_MAX_EXP
#define LDBL_MAX		__LDBL_MAX
#define LDBL_MAX_10_EXP		__LDBL_MAX_10_EXP

#if 1999 <= __ISO_C_VISIBLE
#define __DECIMAL_DIG		0x11
#endif

#include <sys/cdefs.h>

#endif /* _MACHINE__FLOAT_H_ */
