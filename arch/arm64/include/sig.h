/*
 * Signal header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#if __XPG_VISIBLE >= 420 || __BSD_VISIBLE
#include <sys/types.h>

struct sig_context_t {
	struct {
		int32_t unused;
	} unused_s;
	struct {
		int32_t value;
	} mask_s;
	struct {
		u_int64_t sp;	/* stack pointer */
	} pointer_reg_s;
	struct {
		u_int64_t lr;	/* link register */
		u_int64_t elr;	/* exception link register */
	} link_reg_s;
	struct {
		u_int64_t spsr;	/* saved process state register */
	} prog_status_reg_s;
	struct {
		u_int64_t x[30];	/* general purpose registers array */
	} general_reg_s;
	struct {
		int64_t value;
	} cookie_s;
};
#endif
