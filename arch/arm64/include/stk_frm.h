/*
 * Stack frame header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _LOCORE
#include <sys/siginfo.h>

#include <machine/sig.h>

/* stack frame trap */
struct stkfrm_trap_t {
	struct {
		long sp;	/* stack pointer */
	} pointer_reg_s;
	struct {
		long lr;	/* link register */
		long elr;	/* exception link register */
	} link_reg_s;
	struct {
		long spsr;	/* saved process state register */
	} prog_status_reg_s;	/* program status register */
	struct {
		long x[30];	/* general purpose registers array */
	} general_reg_s;
};

/* stack frame switch */
struct stkfrm_switch_t {
	struct {
		long x19;	/* general purpose x19 (r13_svc_sp) */
		long x20;	/* general purpose x20 (r14_abt_lr) */
		long x21;	/* general purpose x21 (r13_abt_sp) */
		long x22;	/* general purpose x22 (r14_und_lr) */
		long x23;	/* general purpose x23 (r13_und_sp) */
		long x24;	/* general purpose x24 (r8_fiq) */
		long x25;	/* general purpose x25 (r9_fiq) */
		long x26;	/* general purpose x26 (r10_fiq) */
		long x27;	/* general purpose x27 (r11_fiq_fp) */
		long x28;	/* general purpose x28 (r12_fiq_ip) */
		long x29;	/* general purpose x29 (r13_fiq_sp) */
	} general_reg_s;
	struct {
		long lr;	/* link register */
	} link_reg_s;
};

/* stack frame signal */
struct stkfrm_signal_t {
	struct {
		int32_t value;
	} number_s;
	struct {
		struct sig_context_t structure_s;
	} context_s;
	struct {
		siginfo_t structure_s;
	} information_s;
};

/* stack frame call */
struct stkfrm_call_t {
	struct {
		struct stkfrm_call_t *pointer_s;
	} frame_s;
	struct {
		long lr;	/* link register */
	} link_reg_s;
};
#endif
