/*
 * EISA machine-independent code header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

#include <machine/sysbus.h>

/* extended industry standard architecture machine independent code id */
static const unsigned char EISA_MIC_ID[] = {
	'E', 'I', 'S', 'A', '\0'
};

enum eisa_mic_t {
	/*
	 * extended industry standard architecture machine independent code id
	 * length
	 */
	EISA_MIC_ID_LENGTH = (- 1) + sizeof(EISA_MIC_ID),

	/*
	 * extended industry standard architecture machine independent code id
	 * physical address
	 */
	EISA_MIC_ID_PADDRESS = 03777731,

	/*
	 * extended industry standard architecture machine independent code
	 * edge/level control register 0
	 */
	EISA_MIC_ELCR0 = 00002320,

	/*
	 * extended industry standard architecture machine independent code
	 * edge/level control register 1
	 */
	EISA_MIC_ELCR1 = 00002321
};

extern struct {
	struct {
		struct sysbus_dma_tag_ptr_t tag_s;
	} data_s;
} eisa_mic_bus_dma_s;

struct eisa_mic_chipset_t {
	struct {
		void *pointer;
	} tag_s;
};

struct eisa_mic_intr_t {
	struct {
		int32_t value;
	} handle_s;
};
