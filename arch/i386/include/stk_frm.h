/*
 * Stack frame header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/siginfo.h>

#include <machine/sig.h>

/* stack frame trap */
struct stkfrm_trap_t {
	struct {
		long fs;	/* general purpose f segment */
		long gs;	/* general purpose g segment */
		long es;	/* extra segment */
		long ds;	/* data segment */
	} segment_reg_s;
	struct {
		long edi;	/* destination index */
		long esi;	/* source index */
	} index_reg_s;
	struct {
		long err;
	} err_reg_s;
	struct {
		long ebx;	/* base */
		long edx;	/* data */
		long ecx;	/* counter */
		long eax;	/* accumulator */
	} general_reg_s;
	struct {
		long trapno;
	} trapno_reg_s;
	struct {
		long ebp;	/* base pointer */
		long eip;	/* instruction pointer */
	} pointer_reg_s;
	struct {
		long cs;	/* extra segment */
	} extra_seg_reg_s;
	struct {
		long eflags;
	} eflags_reg_s;
	struct {
		long esp;	/* stack pointer */
		long ss;	/* stack segment */
	} stack_reg_s;
	struct {
		long es;	/* extra segment */
		long ds;	/* data segment */
		long fs;	/* general purpose f segment */
		long gs;	/* general purpose g segment */
	} virtual8086_reg_s;
};

/* stack frame interrupt */
struct stkfrm_intrpt_t {
	struct {
		long ppl;
	} ppl_reg_s;
	struct {
		long fs;	/* general purpose f segment */
		long gs;	/* general purpose g segment */
		long es;	/* extra segment */
		long ds;	/* data segment */
	} segment_reg_s;
	struct {
		long edi;	/* destination index */
		long esi;	/* source index */
	} index_reg_s;
	struct {
		long unused;
	} err_reg_s;
	struct {
		long ebx;	/* base */
		long edx;	/* data */
		long ecx;	/* counter */
		long eax;	/* accumulator */
	} general_reg_s;
	struct {
		long unused;
	} trapno_reg_s;
	struct {
		long ebp;	/* base pointer */
		long eip;	/* instruction pointer */
	} pointer_reg_s;
	struct {
		long cs;	/* extra segment */
	} extra_seg_reg_s;
	struct {
		long eflags;
	} eflags_reg_s;
	struct {
		long esp;	/* stack pointer */
		long ss;	/* stack segment */
	} stack_reg_s;
};

/* stack frame switch */
struct stkfrm_switch_t {
	struct {
		long edi;	/* destination index */
		long esi;	/* source index */
	} index_reg_s;
	struct {
		long ebp;	/* base pointer */
		long eip;	/* instruction pointer */
	} pointer_reg_s;
};

/* stack frame signal */
struct stkfrm_signal_t {
	struct {
		int32_t value;
	} number_s;
	struct {
		siginfo_t *pointer_s;
	} information_ptr_s;
	struct {
		struct sig_context_t *pointer_s;
	} context_ptr_s;	/* context pointer */
//	struct {
//	} handler_ptr_s;
	struct {
		struct sig_context_t structure_s;
	} context_s;
	struct {
		siginfo_t structure_s;
	} information_s;
};

/* stack frame call */
struct stkfrm_call_t {
	struct {
		struct stkfrm_call_t *pointer_s;
	} frame_s;
	struct {
		long address;
	} ret_s;
	struct {
		long value0;
	} argument_s;
};

/* stack frame interrupt return */
struct stkfrm_intrpt_ret_t {
	struct {
		long trapno;
	} trapno_reg_s;
	struct {
		long err;
	} err_reg_s;
	struct {
		long eip;	/* instruction pointer */
	} pointer_reg_s;
	struct {
		long cs;	/* extra segment */
	} extra_seg_reg_s;
	struct {
		long eflags;
	} eflags_reg_s;
	struct {
		long esp;	/* stack pointer */
		long ss;	/* stack segment */
	} stack_reg_s;
	struct {
		long es;	/* extra segment */
		long ds;	/* data segment */
		long fs;	/* general purpose f segment */
		long gs;	/* general purpose g segment */
	} virtual8086_reg_s;
};

/* stack frame trampoline */
struct stkfrm_trampoline_t {
	struct {
		long deadbeef;
	} data_s;
	struct {
		long esp;	/* stack pointer */
	} kernel_stack_reg_s;
	struct {
		long fs;	/* general purpose f segment */
	} segment_reg_s;
	struct {
		long eax;	/* accumulator */
	} general_reg_s;
	struct {
		long ebp;	/* base pointer */
	} pointer_reg_s;
	struct {
		long trapno;
	} trapno_reg_s;
	struct {
		long err;
	} err_reg_s;
	struct {
		long eip;	/* instruction pointer */
	} pointer_reg_1_s;
	struct {
		long cs;	/* extra segment */
	} extra_seg_reg_s;
	struct {
		long eflags;
	} eflags_reg_s;
	struct {
		long esp;	/* stack pointer */
		long ss;	/* stack segment */
	} stack_reg_s;
	struct {
		long es;	/* extra segment */
		long ds;	/* data segment */
		long fs;	/* general purpose f segment */
		long gs;	/* general purpose g segment */
	} virtual8086_reg_s;
};
