/*
 * Fault header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

enum fault_type_t {
	/* fault type, privileged instruction */
	FAULT_TYPE_PRIV_INSTR = 0000,

	/* fault type, break point */
	FAULT_TYPE_BREAK_POINT = 0001,

	/* fault type, arithmetic */
	FAULT_TYPE_ARITHMETIC = 0002,

	/* fault type, reserved */
	FAULT_TYPE_RESERVED = 0003,

	/* fault type, protection */
	FAULT_TYPE_PROTECTION = 0004,

	/* fault type, trace */
	FAULT_TYPE_TRACE = 0005,

	/* fault type, page */
	FAULT_TYPE_PAGE = 0006,

	/* fault type, alignment */
	FAULT_TYPE_ALIGNMENT = 0007,

	/* fault type, integer divide */
	FAULT_TYPE_INT_DIVIDE = 0010,

	/* fault type, non-maskable interrupt */
	FAULT_TYPE_N_MSK_INTR = 0011,

	/* fault type, overflow */
	FAULT_TYPE_OVERFLOW = 0012,

	/* fault type, bounds check */
	FAULT_TYPE_BOUNDS_CHK = 0013,

	/* fault type, device not available */
	FAULT_TYPE_DEV_N_AVAIL = 0014,

	/* fault type, double */
	FAULT_TYPE_DOUBLE = 0015,

	/* fault type, floating point coprocessor */
	FAULT_TYPE_FLTPT_CP = 0016,

	/* fault type, invalid task state segment */
	FAULT_TYPE_INV_TSS = 0017,

	/* fault type, segment not present */
	FAULT_TYPE_SEGM_N_PRES = 0020,

	/* fault type, stack */
	FAULT_TYPE_STACK = 0021,

	/* fault type, machine check */
	FAULT_TYPE_MACHINE_CHK = 0022,

	/* fault type, single instruction multiple data floating point */
	FAULT_TYPE_SIMD_FLTPT = 0023,
};

enum fault_user_mode_t {
	/* fault user mode */
	FAULT_USER_MODE = 0400
};
