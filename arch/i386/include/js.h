/*
 * Joystick header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/ioccom.h>
#include <sys/types.h>

enum js_io_t {
	/* joystick input output set timeout */
	JS_IO_SET_TIMEOUT = _IOC(IOC_IN, 0112, 01, sizeof(int32_t)),

	/* joystick input output get timeout */
	JS_IO_GET_TIMEOUT = _IOC(IOC_OUT, 0112, 02, sizeof(int32_t)),

	/* joystick input output set offset x */
	JS_IO_SET_OFFSET_X = _IOC(IOC_IN, 0112, 03, sizeof(int32_t)),

	/* joystick input output set offset y */
	JS_IO_SET_OFFSET_Y = _IOC(IOC_IN, 0112, 04, sizeof(int32_t)),

	/* joystick input output get offset x */
	JS_IO_GET_OFFSET_X = _IOC(IOC_OUT, 0112, 05, sizeof(int32_t)),

	/* joystick input output get offset y */
	JS_IO_GET_OFFSET_Y = _IOC(IOC_OUT, 0112, 06, sizeof(int32_t))
};

struct js_t {
	struct {
		int32_t x;
		int32_t y;
	} axis_s;
	struct {
		int32_t b1;
		int32_t b2;
	} button_s;
};
