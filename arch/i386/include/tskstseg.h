/*
 * Task State Segment (TSS) header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

struct task_state_segment_t {
	struct {
		int32_t value;
	} link_s;
	struct {
		struct {
			struct {
				int32_t esp;	/* 32bits stack pointer */
			} integer_s;
			struct {
				int32_t ss;	/* stack segment */
			} segment_s;
		} privilege_level0_s;
		struct {
			struct {
				int32_t esp;	/* 32bits stack pointer */
			} integer_s;
			struct {
				int32_t ss;	/* stack segment */
			} segment_s;
		} __privilege_level1_s;
		struct {
			struct {
				int32_t esp;	/* 32bits stack pointer */
			} integer_s;
			struct {
				int32_t ss;	/* stack segment */
			} segment_s;
		} __privilege_level2_s;
		struct {
			int32_t cr3;	/* control 3 */
		} control_s;
		struct {
			int32_t eip;	/* instruction */
		} pointer_s;
		struct {
			int32_t value;
		} eflags_s;
		struct {
			int32_t eax;	/* 32bits accumulator */
			int32_t ecx;	/* 32bits counter */
			int32_t edx;	/* 32bits data */
			int32_t ebx;	/* 32bits base */
			int32_t esp;	/* 32bits stack pointer */
			int32_t ebp;	/* 32bits base pointer */
			int32_t esi;	/* 32bits source index */
			int32_t edi;	/* 32bits destination index */
		} integer_s;
		struct {
			int32_t es;	/* extra segment */
			int32_t cs;	/* code segment */
			int32_t ss;	/* stack segment */
			int32_t ds;	/* data segment */
			int32_t fs;	/* general purpose f segment */
			int32_t gs;	/* general purpose g segment */
		} segment_s;
	} cpu_register_s;
	struct {
		int32_t local;
	} descriptor_table_s;
	struct {
		int32_t options;
	} input_output_s;
};
