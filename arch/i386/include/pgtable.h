/*
 * Page Table Entry (PTE) header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

enum pgtable_pgdir_t {
	/* page table entry page directory shift */
	PGTABLE_PGDIR_SHIFT = 026
};

enum pgtable_nbpd_t {
	/* page table entry nbpd (n b page directory?) */
	PGTABLE_NBPD = 01 << PGTABLE_PGDIR_SHIFT
};

enum pgtable_pg_t {
	/* page table entry page directory shift */
	PGTABLE_PG_READONLY = 0000000000,

	/* page table entry nbpd (n b page directory?) */
	PGTABLE_PG_VALID = 0000000001,

	/* page table entry page read + write */
	PGTABLE_PG_READWRITE = 0000000002,

	/* page table entry page user accessible */
	PGTABLE_PG_USRACCESS = 0000000004,

	/* page table entry page write through */
	PGTABLE_PG_WTHRU = 0000000010,

	/* page table entry page non cacheable */
	PGTABLE_PG_NCACHE = 0000000020,

	/* page table entry page used */
	PGTABLE_PG_USED = 0000000040,

	/* page table entry page modified */
	PGTABLE_PG_MODIFIED = 0000000100,

	/* page table entry page pat bit */
	PGTABLE_PG_PATBIT = 0000000200,

	/* page table entry page -page- size 2mb */
	PGTABLE_PG_SIZE2MB = 0000000200,

	/* page table entry page global */
	PGTABLE_PG_GLOBAL = 0000000400,

	/* page table entry page available 1 */
	PGTABLE_PG_AVL1 = 0000001000,

	/* page table entry page available 2 */
	PGTABLE_PG_AVL2 = 0000002000,

	/* page table entry page available 3 */
	PGTABLE_PG_AVL3 = 0000004000,

	/* page table entry page protection */
	PGTABLE_PG_PROTECTION = 0000004006,

	/* page table entry page pat on large pages */
	PGTABLE_PG_PATONLGPGS = 0000010000,

	/* page table entry page w b (write bit?) */
	PGTABLE_PG_WB = 00,

	/* page table entry page w c (write cache or write cycle?) */
	PGTABLE_PG_WC = PGTABLE_PG_WTHRU,

	/* page table entry page u c (user cache?) minus */
	PGTABLE_PG_UCMINUS = PGTABLE_PG_NCACHE,

	/* page table entry page u c (user cache?) */
	PGTABLE_PG_UC = PGTABLE_PG_NCACHE | PGTABLE_PG_WTHRU,

	/* page table entry page kernel read only */
	PGTABLE_PG_KREADONLY = 000000000,

	/* page table entry page kernel read + write */
	PGTABLE_PG_KREADWRITE = 000000002
};

enum pgtable_pgexc_t {
	/* page table entry page exception protection violation */
	PGTABLE_PGEXC_PROTVIO = 001,

	/* page table entry page exception write cycle */
	PGTABLE_PGEXC_WCYCLE = 002,

	/* page table entry page exception user mode */
	PGTABLE_PGEXC_USRMODE = 004,

	/* page table entry page exception instruction fetch blocked */
	PGTABLE_PGEXC_IFBLKD = 020
};
