/*
 * System call header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

enum _syscl_io_plvl_t {
	/* system call input output privilege level */
	SYSCL_IO_PLVL = 002
};

enum syscl_virtual8086_t {
	/* system call virtual 8086 (vm86) */
	SYSCL_VIRTUAL8086 = 005
};

enum syscl_get_t {
	/* system call get f s base */
	SYSCL_GET_FS_BASE = 006,

	/* system call get g s base */
	SYSCL_GET_GS_BASE = 010
};

enum syscl_set_t {
	/* system call set f s base */
	SYSCL_SET_FS_BASE = 007,

	/* system call set g s base */
	SYSCL_SET_GS_BASE = 011
};

/* system call input output privilege level */
struct syscl_io_plvl_t {
	struct {
		int32_t value;
	} arguments_s;
};

//#ifndef _KERNEL
//__BEGIN_DECLS
//__END_DECLS
//#else
//#endif
