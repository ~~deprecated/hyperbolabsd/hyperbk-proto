/*
 * Signal header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#if __XPG_VISIBLE >= 420 || __BSD_VISIBLE
#include <sys/types.h>

struct sig_context_t {
	struct {
		int32_t gs;	/* general purpose g segment */
		int32_t fs;	/* general purpose f segment */
		int32_t es;	/* extra segment */
		int32_t ds;	/* data segment */
	} segment_reg_s;
	struct {
		int32_t edi;	/* destination index */
		int32_t esi;	/* source index */
	} index_reg_s;
	struct {
		int32_t ebp;	/* base pointer */
		int32_t fp;	/* frame pointer */
	} pointer_reg_u;
	struct {
		int32_t ebx;	/* base */
		int32_t edx;	/* data */
		int32_t ecx;	/* counter */
		int32_t eax;	/* accumulator */
	} general_reg_s;
	struct {
		int32_t eip;	/* instruction pointer */
		int32_t pc;	/* program counter */
	} counter_reg_u;
	struct {
		int32_t cs;	/* extra segment */
	} extra_seg_reg_s;
	union {
		int32_t eflags;
		int32_t ps;	/* program state */
	} program_reg_u;
	union {
		int32_t esp;	/* stack pointer */
		int32_t sp;	/* stack pointer */
	} stack_ptr_reg_u;	/* stack pointer register */
	struct {
		int32_t ss;	/* stack segment */
	} stack_seg_reg_s;	/* stack segment register */
	struct {
		int32_t value;
	} cookie_s;
	struct {
		int32_t value;
	} mask_s;
	struct {
		int32_t trapno;
	} trapno_reg_s;
	struct {
		int32_t err;
	} err_reg_s;
	struct {
		struct savefpu *state_u;
	} floating_point_s;
};
#endif	/* __XPG_VISIBLE >= 420 || __BSD_VISIBLE */
