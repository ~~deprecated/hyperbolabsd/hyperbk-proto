/*
 * Main Processor (MP) variables header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

enum mpvar_role_t {
	MPVAR_ROLE_SP = 00,
	MPVAR_ROLE_BP = 01,
	MPVAR_ROLE_AP = 02
};

enum mpvar_mp_pic_t {
	MPVAR_MP_PIC_MODE = 01
};

struct mpvar_attach_arguments_t {
	struct {
		int8_t *pointer;
	} caa_name_s;
	struct {
		int32_t apic_id;
		int32_t acpi_proc_id;
		int32_t role;
	} data_s;
	struct {
		int32_t value;
	} signature_s;
	struct {
		int32_t flags;
	} feature_s;
};
