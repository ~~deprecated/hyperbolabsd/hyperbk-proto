/*
 * Interrupt definitions header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

enum intrpt_legacy_irq_num_t {
	/* interrupt; legacy interrupt requests, number */
	INTRPT_LEGACY_IRQS_NUM = 020
};

enum intrpt_dtbl_t {
	/* interrupt descriptor table vector off */
	INTRPT_DTBL_VEC_OFF = 040,

	/* interrupt descriptor table low */
	INTRPT_DTBL_LOW = INTRPT_LEGACY_IRQS_NUM + 040,

	/* interrupt descriptor table high */
	INTRPT_DTBL_HIGH = 0357
};

enum intrpt_plvl_t {
	/* interrupt priority level shift */
	INTRPT_PLVL_SHIFT = 004,

	/* interrupt priority level number */
	INTRPT_PLVL_NUMBER = 020,

	/* interrupt priority level none */
	INTRPT_PLVL_NONE = 000,

	/* interrupt priority level software clock */
	INTRPT_PLVL_SOFT_CLOCK = (001 << INTRPT_PLVL_SHIFT) +
	    INTRPT_DTBL_VEC_OFF,

	/* interrupt priority level software network */
	INTRPT_PLVL_SOFT_NET = (002 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF,

	/* interrupt priority level block input/output */
	INTRPT_PLVL_BLOCK_IO = (003 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF,

	/* interrupt priority level network */
	INTRPT_PLVL_NETWORK = (004 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF,

	/* interrupt priority level software teletype */
	INTRPT_PLVL_SOFT_TTY = (005 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF,

	/* interrupt priority level teletype */
	INTRPT_PLVL_TTY = (006 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF,

	/* interrupt priority level virtual memory */
	INTRPT_PLVL_VIRT_MEM = (007 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF,

	/* interrupt priority level audio */
	INTRPT_PLVL_AUDIO = (010 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF,

	/* interrupt priority level clock, -status- -clock- -and- schreduler */
	INTRPT_PLVL_CLK_SCH = (011 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF,

	/* interrupt priority level high */
	INTRPT_PLVL_HIGH = (012 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF,

	/* interrupt priority level, inter-processor interrupt */
	INTRPT_PLVL_IPROCINT = (013 << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF,

	/* interrupt priority level m p floor */
	INTRPT_PLVL_MP_FLOOR = (0006 << INTRPT_PLVL_SHIFT) +
	    INTRPT_DTBL_VEC_OFF,

	/* interrupt priority level m p safe */
	INTRPT_PLVL_MP_SAFE = 0400
};

enum intrpt_styp_none_t {
	/* interrupt sharing type none */
	INTRPT_STYP_NONE = 00,

	/* interrupt sharing type pulse */
	INTRPT_STYP_PULSE = 01,

	/* interrupt sharing type edge */
	INTRPT_STYP_EDGE = 02,

	/* interrupt sharing type level */
	INTRPT_STYP_LEVEL = 03
};

enum intrpt_lireq_t {
	/* interrupt; legacy interrupt requests, inter-processor interrupt */
	INTRPT_LIREQ_IPI = 037,

	/* interrupt; legacy interrupt requests, timer */
	INTRPT_LIREQ_TIMER = 036
};

enum intrpt_sireq_t {
	/* interrupt; soft interrupt requests, clock */
	INTRPT_SIREQ_CLOCK = 035,

	/* interrupt; soft interrupt requests, network */
	INTRPT_SIREQ_NETWORK = 034,

	/* interrupt; soft interrupt requests, teletype */
	INTRPT_SIREQ_TTY = 033
};

enum intrpt_sources_maximum_t {
	/* interrupt sources, maximum */
	INTRPT_SOURCES_MAXIMUM = 040
};

enum intrpt_iprocint_t {
	/* interrupt; inter-processor interrupt, halt */
	INTRPT_IPROCINT_HALT = 0001,

	/* interrupt; inter-processor interrupt, n op (no operation ?) */
	INTRPT_IPROCINT_NOP = 0002,

	/* interrupt; inter-processor interrupt, flush floating-point unit */
	INTRPT_IPROCINT_F_FPU = 0004,

	/*
	 * interrupt; inter-processor interrupt, synchronize floating-point
	 * unit
	 */
	INTRPT_IPROCINT_S_FPU = 0010,

	/* interrupt; inter-processor interrupt, memory type range registers */
	INTRPT_IPROCINT_MTRR = 0020,

	/*
	 * interrupt; inter-processor interrupt, g d t (global descriptor
	 * table?)
	 */
	INTRPT_IPROCINT_GDT = 0040,

	/* interrupt; inter-processor interrupt, d d b */
	INTRPT_IPROCINT_DDB = 0100,

	/* interrupt; inter-processor interrupt, set perf */
	INTRPT_IPROCINT_S_PERF = 0200,

	/* interrupt; inter-processor interrupt, number */
	INTRPT_IPROCINT_NUMBER = 010,
};

enum intrpt_ireent_magic_t {
	/* interrupt; i r e e n t magic */
	INTRPT_IREENT_MAGIC = 03001014551
};

u_int16_t
intrpt_iprocint(int32_t lvl)
{
	return lvl >> INTRPT_PLVL_SHIFT;
};

u_int16_t
intrpt_make_iprocint(u_char pri)
{
	return (pri << INTRPT_PLVL_SHIFT) + INTRPT_DTBL_VEC_OFF;
}
