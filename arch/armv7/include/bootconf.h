/*
 * Boot configurations header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

#if defined(_STANDALONE) || defined(_KERNEL)
enum bootconf_dram_blocks_t {
	/* boot configurations, dynamic random-access memory blocks */
	BOOTCONF_DRAM_BLOCKS = 0002,
};

enum bootconf_max_boot_str_t {
	/* boot configurations, maximum boot string */
	BOOTCONF_MAX_BOOT_STR = 0377
};

struct bootconf_t {
	struct {
		struct bootconf_physmem_t {
			struct {
				u_int32_t addr;
				u_int32_t pgs;
			} data_s;
		} physmem_s[BOOTCONF_DRAM_BLOCKS];
		struct {
			u_int32_t value;
		} blocks_s;
	} dram_s;	/* dynamic random access memory */
	struct {
		int8_t string[BOOTCONF_MAX_BOOT_STR];
	} boot_s;
};

extern struct {
	struct bootconf_t data_s;
} bootconf_structure_s;
#endif	/* _STANDALONE || _KERNEL */

#ifdef _KERNEL
extern struct {
	int8_t *arguments;
	int8_t *file;
} bootconf_s;
#endif	/* _KERNEL */
