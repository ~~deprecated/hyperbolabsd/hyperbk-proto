/*
 * Sitara control module header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

/* sitara control module pad state */
struct sctlmod_pad_state_t {
	struct {
		const int8_t *pointer;
	} state_s;
	struct {
		unsigned short value;
	} register_s;
};

/* sitara control module pad configuration */
struct sctlmod_pad_config_t {
	struct {
		unsigned short off;
	} register_s;
	struct {
		unsigned short pin;
		unsigned short mode;
	} gpio_s;	/* general purporse input output */
	struct {
		const int8_t *name;
	} ball_s;
	struct {
		const int8_t *modes[8];
	} mux_s;	/* multiplexer */
};

/* sitara control module device */
struct sctlmod_device_t {
	struct {
		unsigned short mux_mode_mask;
		unsigned short sate_mask;
	} pad_config_s;
	struct {
		struct sctlmod_pad_state_t *state_s;
		struct sctlmod_pad_config_t *config_s;	/* configuration */
	} pad_s;
};
