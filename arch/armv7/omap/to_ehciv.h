/*
 * ARM TI OMAP EHCI variables header for HyperbolaBSD
 * Copyright (c) 2020 Hyperbola Project
 * Copyright (c) 2020 André Silva <emulatorman@hyperbola.info>
 * Copyright (c) 2020 Márcio Silva <coadde@hyperbola.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>

enum tiomap_ehci_tll_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; revision
	 */
	TIOMAP_EHCI_TLL_REV = 000,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; system
	 * configuration
	 */
	TIOMAP_EHCI_TLL_SYSCFG = 020,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; system status
	 */
	TIOMAP_EHCI_TLL_SYSSTT = 024,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; interrupt
	 * request status
	 */
	TIOMAP_EHCI_TLL_IRQSTT = 030,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; interrupt
	 * request enable
	 */
	TIOMAP_EHCI_TLL_IRQENA = 034,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; shared
	 * configuration
	 */
	TIOMAP_EHCI_TLL_SHRCFG = 060
};

enum tiomap_euhh_hst_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h host; revision
	 */
	TIOMAP_EUHH_HST_REV = 0000,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h host; system configuration
	 */
	TIOMAP_EUHH_HST_SYSCFG = 0020,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h host; system status
	 */
	TIOMAP_EUHH_HST_SYSSTT = 0024,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h host; host configuration
	 */
	TIOMAP_EUHH_HST_HSTCFG = 0100,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h host; debug c s r
	 */
	TIOMAP_EUHH_HST_DBGCSR = 0104
};

enum tiomap_ehci_hst_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; h c cap base
	 */
	TIOMAP_EHCI_HST_HCCB = 0000,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; h c s parameters
	 */
	TIOMAP_EHCI_HST_HCSP = 0004,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; h c c parameters
	 */
	TIOMAP_EHCI_HST_HCCP = 0010,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; universal serial bus command
	 */
	TIOMAP_EHCI_HST_USBCMD = 0020,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; universal serial bus s t s
	 */
	TIOMAP_EHCI_HST_USBSTS = 0024,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; universal serial bus int (integer?)
	 */
	TIOMAP_EHCI_HST_USBINT = 0030,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; f r index
	 */
	TIOMAP_EHCI_HST_FRIND = 0034,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; control d s segment
	 */
	TIOMAP_EHCI_HST_CDSEG = 0040,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; periodic list base
	 */
	TIOMAP_EHCI_HST_PLB = 0044,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; async list address
	 */
	TIOMAP_EHCI_HST_ASLA = 0050,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; configuration flag
	 */
	TIOMAP_EHCI_HST_CFGFLG = 0120,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 00
	 */
	TIOMAP_EHCI_HST_INR0 = 0220,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 01
	 */
	TIOMAP_EHCI_HST_INR1 = 0224,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 02
	 */
	TIOMAP_EHCI_HST_INR2 = 0230,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 03
	 */
	TIOMAP_EHCI_HST_INR3 = 0234,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 04
	 */
	TIOMAP_EHCI_HST_INR4 = 0240,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 05
	 */
	TIOMAP_EHCI_HST_INR5 = 0244,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 06
	 */
	TIOMAP_EHCI_HST_INR6 = 0250,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 07
	 */
	TIOMAP_EHCI_HST_INR7 = 0254,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 08
	 */
	TIOMAP_EHCI_HST_INR8 = 0260
};

enum tiomap_ehcihst_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 04, disable
	 * unsuspend
	 */
	TIOMAP_EHCIHST_INR4DUS = 040,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 05, u t m i +
	 * low pin interface control shift
	 */
	TIOMAP_EHCIHST_INR5UCS = 037,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 05, u t m i +
	 * low pin interface port sel shift
	 */
	TIOMAP_EHCIHST_INR5UPS = 030,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 05, u t m i +
	 * low pin interface op (operation?) sel control shift
	 */
	TIOMAP_EHCIHST_INR5UOS = 026,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 05, u t m i +
	 * low pin interface reg (register?) add (address or add?) shift
	 */
	TIOMAP_EHCIHST_INR5URS = 020,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 05, u t m i +
	 * low pin interface ext (extended?) reg (register?) add (address or
	 * add?) shift
	 */
	TIOMAP_EHCIHST_INR5UXS = 010,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, host; ins n reg (register?) 05, u t m i +
	 * low pin interface w (write?) r (read?) data shift
	 */
	TIOMAP_EHCIHST_INR5UWS = 000
};

enum tiomap_ettl_syscfg_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; system
	 * configuration; c (clock?) activity
	 */
	TIOMAP_ETTL_SYSCFG_CAC = 0400UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; system
	 * configuration; sidle, smart idle
	 */
	TIOMAP_ETTL_SYSCFG_SSI = 0020UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; system
	 * configuration; sidle, no idle
	 */
	TIOMAP_ETTL_SYSCFG_SNI = 0010UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; system
	 * configuration; sidle, forced idle
	 */
	TIOMAP_ETTL_SYSCFG_SFI = 0000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; system
	 * configuration; ena (enable?) wakeup
	 */
	TIOMAP_ETTL_SYSCFG_EWU = 0004UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; system
	 * configuration; soft reset
	 */
	TIOMAP_ETTL_SYSCFG_SR = 0002UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; system
	 * configuration; auto idle
	 */
	TIOMAP_ETTL_SYSCFG_AI = 0001UL
};

enum tiomap_ettl_sysstt_rd_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; system status;
	 * reset done
	 */
	TIOMAP_ETTL_SYSSTT_RD = 0001UL
};

enum tiomap_ettl_sc_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; shared
	 * configuration; universal serial bus 90 d double data rate e n
	 */
	TIOMAP_ETTL_SC_U090DDE = 0100UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; shared
	 * configuration; universal serial bus 180 d single data rate e n
	 */
	TIOMAP_ETTL_SC_U180DSE = 0040UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; shared
	 * configuration; universal serial bus division ratio mask
	 */
	TIOMAP_ETTL_SC_UDRMASK = 0034UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; shared
	 * configuration; universal serial bus division ratio 128
	 */
	TIOMAP_ETTL_SC_UDR128 = 0034UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; shared
	 * configuration; universal serial bus division ratio 64
	 */
	TIOMAP_ETTL_SC_UDR064 = 0030UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; shared
	 * configuration; universal serial bus division ratio 32
	 */
	TIOMAP_ETTL_SC_UDR032 = 0024UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; shared
	 * configuration; universal serial bus division ratio 16
	 */
	TIOMAP_ETTL_SC_UDR016 = 0020UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; shared
	 * configuration; universal serial bus division ratio 8
	 */
	TIOMAP_ETTL_SC_UDR008 = 0014UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; shared
	 * configuration; universal serial bus division ratio 4
	 */
	TIOMAP_ETTL_SC_UDR004 = 0010UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; shared
	 * configuration; universal serial bus division ratio 2
	 */
	TIOMAP_ETTL_SC_UDR002 = 0004UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; shared
	 * configuration; universal serial bus division ratio 1
	 */
	TIOMAP_ETTL_SC_UDR001 = 0000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; shared
	 * configuration; f c l k request
	 */
	TIOMAP_ETTL_SC_FCLK_R = 0002UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; shared
	 * configuration; f c l k -is- on
	 */
	TIOMAP_ETTL_SC_FCLK_O = 0001UL
};

enum tiomap_ettl_cc_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; channel
	 * configuration; driver v (virtual?) bus
	 */
	TIOMAP_ETTL_CC_DRVB = 0200000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; channel
	 * configuration; c h r g  v (virtual?) bus
	 */
	TIOMAP_ETTL_CC_CHRGVB = 0100000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; channel
	 * configuration; u t m i + low pin interface no bit stuff
	 */
	TIOMAP_ETTL_CC_ULPINBS = 0004000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; channel
	 * configuration; u t m i + low pin interface auto idle
	 */
	TIOMAP_ETTL_CC_ULPIAI = 0002000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; channel
	 * configuration; u s b 2.0 transceiver macrocell interface auto idle
	 */
	TIOMAP_ETTL_CC_UTMIAI = 0001000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; channel
	 * configuration; u t m i + low pin interface double data rate mode
	 */
	TIOMAP_ETTL_CC_ULPIDM = 0000400UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; channel
	 * configuration; u t m i + low pin interface out c l k (clock?) mode
	 */
	TIOMAP_ETTL_CC_ULPIOCM = 0000200UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; channel
	 * configuration; transceiver-less interface full speed
	 */
	TIOMAP_ETTL_CC_TLLFS = 0000100UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; channel
	 * configuration; transceiver-less interface connect
	 */
	TIOMAP_ETTL_CC_TLLCONN = 0000040UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; channel
	 * configuration; transceiver-less interface attach
	 */
	TIOMAP_ETTL_CC_TLLATT = 0000020UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; channel
	 * configuration; u s b 2.0 transceiver macrocell interface i s a
	 * (instruction set architecture?) device
	 */
	TIOMAP_ETTL_CC_UTMIID = 0000010UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, transceiver-less interface; channel
	 * configuration; c h a n (channel?) e n
	 */
	TIOMAP_ETTL_CC_CHANEN = 0000001UL
};

enum tiomap_euhh_sc_mm_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; middle mode
	 * mask
	 */
	TIOMAP_EUHH_SC_MM_MASK = 030000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; middle mode,
	 * smart stand by
	 */
	TIOMAP_EUHH_SC_MM_SSBY = 020000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; middle mode,
	 * no smart stand by
	 */
	TIOMAP_EUHH_SC_MM_NSBY = 010000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; middle mode,
	 * force stand by
	 */
	TIOMAP_EUHH_SC_MM_FSBY = 000000UL
};

enum tiomap_euhh_sc_clockac_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; clock
	 * activity
	 */
	TIOMAP_EUHH_SC_CLOCKAC = 000400UL
};

enum tiomap_euhh_sc_sm_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; sidle mode
	 * mask
	 */
	TIOMAP_EUHH_SC_SM_MASK = 000030UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; sidle mode,
	 * smart idle
	 */
	TIOMAP_EUHH_SC_SM_ST = 000020UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; sidle mode,
	 * no idle
	 */
	TIOMAP_EUHH_SC_SM_NI = 000010UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; sidle mode,
	 * force idle
	 */
	TIOMAP_EUHH_SC_SM_FI = 000000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; ena (enable?)
	 * wakeup
	 */
	TIOMAP_EUHH_SC_EWAKEUP = 000004UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; soft reset
	 */
	TIOMAP_EUHH_SC_SRESET = 000002UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; auto idle
	 */
	TIOMAP_EUHH_SC_AIDLE = 000001UL
};

enum tiomap_euhh_hc_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; host configuration; application
	 * start c l k (clock?)
	 */
	TIOMAP_EUHH_HC_A_S_CLK = 020000000000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; host configuration; p3 connect
	 * status
	 */
	TIOMAP_EUHH_HC_P3_CSTT = 000000002000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; host configuration; p2 connect
	 * status
	 */
	TIOMAP_EUHH_HC_P2_CSTT = 000000001000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; host configuration; p1 connect
	 * status
	 */
	TIOMAP_EUHH_HC_P1_CSTT = 000000000400UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; host configuration; e n a (enable?)
	 * i n c r align
	 */
	TIOMAP_EUHH_HC_ENA_I_A = 000000000040UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; host configuration; e n a (enable?)
	 * i n c r 16
	 */
	TIOMAP_EUHH_HC_ENA_I16 = 000000000020UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; host configuration; e n a (enable?)
	 * i n c r 8
	 */
	TIOMAP_EUHH_HC_ENA_I08 = 000000000010UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; host configuration; e n a (enable?)
	 * i n c r 4
	 */
	TIOMAP_EUHH_HC_ENA_I04 = 000000000004UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; host configuration; auto p p d on
	 * over c u r  e n
	 */
	TIOMAP_EUHH_HC_APOOCE = 000000000002UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; host configuration; p1 u t m i +
	 * low pin interface bypass
	 */
	TIOMAP_EUHH_HC_P1_UBYP = 000000000001UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; host configuration; p1 mode mask
	 */
	TIOMAP_EUHH_HC_P1_M_MH = 00600000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; host configuration; p1 mode,
	 * u t m i + low pin interface phy (physical?)
	 */
	TIOMAP_EUHH_HC_P12MULP = 00000000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; host configuration; p1 mode,
	 * u s b 2.0 transceiver macrocell interface phy (physical?)
	 */
	TIOMAP_EUHH_HC_P1_MUTP = 00200000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; host configuration; p2 mode mask
	 */
	TIOMAP_EUHH_HC_P2_M_MH = 03000000UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; host configuration; p2 mode,
	 * u t m i + low pin interface phy (physical?)
	 */
	TIOMAP_EUHH_HC_P2_MUTP = 01000000UL
};

enum tiomap_euhh_sc_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; idle mode
	 * mask
	 */
	TIOMAP_EUHH_SC_IM_MASK = 014UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; idle mode,
	 * no idle
	 */
	TIOMAP_EUHH_SC_IM_NI = 004UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; stand by
	 * mode mask
	 */
	TIOMAP_EUHH_SC_BM_MASK = 060UL,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; system configuration; stand by
	 * mode, no stand by
	 */
	TIOMAP_EUHH_SC_BM_NO = 020UL
};

enum tiomap_eulpi_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u t m i + low pin interface; function
	 * control; reset
	 */
	TIOMAP_EULPI_F_C_RESET = 040,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u t m i + low pin interface; vendor,
	 * i d (identification?) low
	 */
	TIOMAP_EULPI_V_ID_LOW = 000,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u t m i + low pin interface; vendor,
	 * i d (identification?) high
	 */
	TIOMAP_EULPI_V_ID_HIGH = 001,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u t m i + low pin interface; product,
	 * i d (identification?) low
	 */
	TIOMAP_EULPI_P_ID_LOW = 002,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u t m i + low pin interface; product,
	 * i d (identification?) high
	 */
	TIOMAP_EULPI_P_ID_HIGH = 003,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u t m i + low pin interface; function
	 * control
	 */
	TIOMAP_EULPI_FUNC_CTRL = 004,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u t m i + low pin interface; i f c
	 * control
	 */
	TIOMAP_EULPI_IFC_CTRL = 007,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u t m i + low pin interface; on-the-go
	 * control
	 */
	TIOMAP_EULPI_OTG_CTRL = 012,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u t m i + low pin interface; universal
	 * serial bus; int (integer?) e n rise
	 */
	TIOMAP_EULPI_UI_ENRISE = 015,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u t m i + low pin interface; universal
	 * serial bus; int (integer?) e n fall
	 */
	TIOMAP_EULPI_UI_ENFALL = 020,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u t m i + low pin interface; universal
	 * serial bus; int (integer?) s t s
	 */
	TIOMAP_EULPI_UI_STS = 023,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u t m i + low pin interface; universal
	 * serial bus; int (integer?) latch
	 */
	TIOMAP_EULPI_UI_LATCH = 024,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u t m i + low pin interface; debug
	 */
	TIOMAP_EULPI_DEBUG = 025,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u t m i + low pin interface; scratch
	 */
	TIOMAP_EULPI_SCRATCH = 026
};

enum tiomap_ehci_uhh_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; revision 1
	 */
	TIOMAP_EHCI_UHH_REV1 = 000000000020,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, u h h; revision 2
	 */
	TIOMAP_EHCI_UHH_REV2 = 012034000400
};

enum tiomap_ehci_vendid_03_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, vendor i d (identification?); open
	 * multimedia applications platform 3
	 */
	TIOMAP_EHCI_VENDID_O3 = 020575005
};

enum tiomap_ehci_hcd_om_t {
	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, h c d; open multimedia applications
	 * platform mode; unknown
	 */
	TIOMAP_EHCI_HCD_OM_UNK = 00,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, h c d; open multimedia applications
	 * platform mode; phy (physical?)
	 */
	TIOMAP_EHCI_HCD_OM_PHY = 01,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, h c d; open multimedia applications
	 * platform mode; transceiver-less interface
	 */
	TIOMAP_EHCI_HCD_OM_TLL = 02,

	/*
	 * texas instruments open multimedia applications platform; enhanced
	 * host controller interface, h c d; open multimedia applications
	 * platform mode; h s i c
	 */
	TIOMAP_EHCI_HCD_OM_H = 03
};

/*
 * texas instruments open multimedia applications platform; enhanced host
 * controller interface, h c; device string
 */
static const unsigned char TIOMAP_EHCI_HC_DEVSTR[] = {
	'T', 'I', ' ', 'O', 'M', 'A', 'P', ' ', 'U', 'S', 'B', ' ', '2', '.',
	   '0', ' ', 'c', 'o', 'n', 't', 'r', 'o', 'l', 'l', 'e', 'r', '\0'
};

u_int16_t
tiomap_ehci_tll_channel_configuration(u_int8_t integer)
{
	return (integer * 04) + 00100;
}

u_int16_t
tiomap_ehci_tll_sar_count_x(u_int8_t integer)
{
	return (integer * 04) + 02000;
}

int32_t
tiomap_ehci_tll_ulpi_vendorid_low(int16_t integer)
{
	return (integer * 0400) + 000 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_vendorid_high(int16_t integer)
{
	return (integer * 0400) + 001 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_productid_low(int16_t integer)
{
	return (integer * 0400) + 002 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_productid_high(int16_t integer)
{
	return (integer * 0400) + 003 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_function_control(int16_t integer)
{
	return (integer * 0400) + 004 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_function_control_set(int16_t integer)
{
	return (integer * 0400) + 005 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_function_control_clear(int16_t integer)
{
	return (integer * 0400) + 006 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_interface_control(int16_t integer)
{
	return (integer * 0400) + 007 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_interface_control_set(int16_t integer)
{
	return (integer * 0400) + 010 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_interface_control_clear(int16_t integer)
{
	return (integer * 0400) + 011 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_otg_control(int16_t integer)
{
	return (integer * 0400) + 012 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_otg_control_set(int16_t integer)
{
	return (integer * 0400) + 013 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_otg_control_clear(int16_t integer)
{
	return (integer * 0400) + 014 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_usb_int_en_rise(int16_t integer)
{
	return (integer * 0400) + 015 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_usb_int_en_rise_set(int16_t integer)
{
	return (integer * 0400) + 016 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_usb_int_en_rise_clear(int16_t integer)
{
	return (integer * 0400) + 017 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_usb_int_en_fall(int16_t integer)
{
	return (integer * 0400) + 020 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_usb_int_en_fall_set(int16_t integer)
{
	return (integer * 0400) + 021 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_usb_int_en_fall_clear(int16_t integer)
{
	return (integer * 0400) + 022 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_usb_int_status(int16_t integer)
{
	return (integer * 0400) + 023 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_usb_int_latch(int16_t integer)
{
	return (integer * 0400) + 024 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_debug(int16_t integer)
{
	return (integer * 0400) + 025 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_scratch_register(int16_t integer)
{
	return (integer * 0400) + 026 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_scratch_register_set(int16_t integer)
{
	return (integer * 0400) + 027 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_scratch_register_clear(int16_t integer)
{
	return (integer * 0400) + 030 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_extended_set_access(int16_t integer)
{
	return (integer * 0400) + 057 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_utmi_vcontrol_en(int16_t integer)
{
	return (integer * 0400) + 060 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_utmi_vcontrol_en_set(int16_t integer)
{
	return (integer * 0400) + 061 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_utmi_vcontrol_en_clear(int16_t integer)
{
	return (integer * 0400) + 062 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_utmi_vcontrol_status(int16_t integer)
{
	return (integer * 0400) + 063 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_utmi_vcontrol_latch(int16_t integer)
{
	return (integer * 0400) + 064 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_utmi_vstatus(int16_t integer)
{
	return (integer * 0400) + 065 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_utmi_vstatus_set(int16_t integer)
{
	return (integer * 0400) + 066 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_utmi_vstatus_clear(int16_t integer)
{
	return (integer * 0400) + 067 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_usb_int_latch_noclear(int16_t integer)
{
	return (integer * 0400) + 070 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_vendor_int_en(int16_t integer)
{
	return (integer * 0400) + 073 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_vendor_int_en_set(int16_t integer)
{
	return (integer * 0400) + 074 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_vendor_int_en_clear(int16_t integer)
{
	return (integer * 0400) + 075 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_vendor_int_status(int16_t integer)
{
	return (integer * 0400) + 076 + 04000;
}

int32_t
tiomap_ehci_tll_ulpi_vendor_int_latch(int16_t integer)
{
	return (integer * 0400) + 077 + 04000;
}

u_int16_t
tiomap_ehci_host_port_sc(u_int8_t integer)
{
	return (integer * 04) + 0124;
}

u_int16_t
tiomap_ehci_ulpi_set(u_int8_t data)
{
	return data + 01 + 00;
}

u_int16_t
tiomap_ehci_ulpi_clear(u_int8_t data)
{
	return data + 01 + 01;
}
