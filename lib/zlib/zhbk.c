/*
 * zhbk.c -- target dependent utility functions for the compression library
 * Copyright (C) 1995-2005 Jean-loup Gailly.
 *
 * Modifications to support HyperbolaBSD:
 * Copyright (c) 2020 Hyperbola Project
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Jean-loup Gailly jloup@gzip.org
 */

#include <sys/types.h>
#include <sys/malloc.h>

#define ZHBK_MALLOC(items, size) do {				\
	mallocarray((items), (size), M_DEVBUF, M_NOWAIT);	\
} while (0)

#define ZHBK_FREE(ptr) do {		\
	free((ptr), M_DEVBUF, FALSE);	\
} while (0)

void *
zcalloc(void *opaque, unsigned items, unsigned size)
{
	ZHBK_MALLOC(items, size);
}

void
zcfree(void *opaque, void *ptr)
{
	ZHBK_FREE(ptr);
}
